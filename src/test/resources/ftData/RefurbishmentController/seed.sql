INSERT INTO servicecities VALUES (110,'Delhi', 'NCR');
INSERT INTO users VALUES (1,'akshay','akshay.singh@revv.co.in','1234','REFURB_SPOC',NOW(),110);
INSERT INTO workshops VALUES(1,'Competent motors','gurgaon',1,NOW(),NOW(),1,1);
INSERT INTO carmodels VALUES ('113','Tata','Nano','XT','PETROL','MANUAL','HATCHBACK','4','2010','2016','100000','1','1');
INSERT INTO sellers VALUES ('111','dash1','110','a@b.com','2147483647','UCD',NULL,NULL,NULL,NULL,NULL,NULL,'1','1',NULL,NULL,NULL);
INSERT INTO leads VALUES (132,1,113,111,'ab11dc1113','black','2014-04-28 10:30:49.455','2014-04-28 10:30:49.455','2014-04-28 10:30:49','rto','reg type','own ser',110,'commented',1,'2020-12-28 12:26:21',NULL,1000.25,'COMPREHENSIVE',0,NULL,'CREATED',NULL,NULL,NULL);
INSERT INTO leads VALUES (133,1,113,111,'ab11dc1113','black','2014-04-28 10:30:49.455','2014-04-28 10:30:49.455','2014-04-28 10:30:49','rto','reg type','own ser',110,'commented',1,'2020-12-28 12:26:21',NULL,1000.25,'COMPREHENSIVE',0,NULL,'CREATED',NULL,NULL,NULL);
INSERT INTO offers VALUES (37, null , 132, 'Ok', 'Ok', 20000, 19000, 1000, 0, 0, 0, 1, '2021-02-12 10:27:09', null, null, '1', 'CREATED', '0',null,0);
INSERT INTO procuredCars VALUES (1,113,132,37,NOW(),1,1,NOW(),null,1,null,'ON_BOARDED',0,null,null);
INSERT INTO procuredCars VALUES (2,113,133,37,NOW(),1,1,NOW(),null,1,null,'ON_BOARDED',0,null,null);
INSERT INTO refurbishments VALUES(1,1,1,1,NOW(),DATEADD('DAY',1, CURRENT_DATE),'SCHEDULED',1,NOW(),null,1,1,0);
INSERT INTO inspections VALUES (1,'E3',132,1,1,1,NOW(),'COMPLETED',NOW(),null,1,0,0,0,0,1);
INSERT INTO inspections VALUES (2,'E3',133,1,1,1,NOW(),'COMPLETED',NOW(),null,1,0,0,0,0,null);

INSERT INTO headers VALUES (1,'HEADER',1,1);
INSERT INTO parts VALUES (1,null,1,'XYZ-PART','UNIVERSAL',0,1,1);
INSERT INTO subparts VALUES (1,1,'XYZ-SUB-PART','MULTI',1,1,1,1,0);
INSERT INTO inspectionSubParts values (1, 1, 1, 1, 1, NOW(),null,null),
                                      (2, 1, 1, 2, 1, NOW(),null,null);
INSERT INTO subpartissues VALUES (1,null,'ok',null,1,1),
                                 (2,null,'Needs further investigation',null,1,1),
                                 (3,null,'unassured',null,1,1),
                                 (100,1,'dent',null,1,1),
                                 (101,1,'scratch',null,1,1);
INSERT INTO jobs values (1,'No job required',1,1),
                        (200,'repair-XYZ',1,1),
                        (201,'painting-XYZ',1,1);

INSERT INTO jobpricemap values (1,1,null,null,null,0,1,1),
                               (2,200,null,null,null,100,1,1),
                               (3,201,null,null,null,50,1,1);

INSERT INTO issuefollowup VALUES (1,100,'repair',200,1,1),
                                 (2,100,'painting',201,1,1),
                                 (3,101,'repair',200,1,1),
                                 (4,101,'painting',201,1,1);

INSERT INTO inspectionReports VALUES (1,1,1,100,1,1,NOW(),NOW(),1,null,'XYZ','dent');

INSERT INTO refurbishmentJobs VALUES (1,1,1,200,NOW(),null,1,1,1,1,'XYZ-SUB-PART','repair-XYZ'),
                                     (2,1,1,201,NOW(),null,1,1,1,1,'XYZ-SUB-PART','painting');

INSERT INTO documentStageMasters VALUES (1, 'Job Card', 'JOB_CARD', 'REFURB_WORKSHOP', 1, 1),
                                        (2, 'Refurb Card', 'JOB_CARD', 'REFURB_WORKSHOP', 1, 1);
