insert into servicecities values (110,'Delhi', 'NCR');
insert into users values (1,'akshay','akshay.singh@revv.co.in','1234','INSPECTOR',NOW(),110);
INSERT INTO inspections values (1,'E1',132,1,1,null, NOW(),'SCHEDULED','2020-12-21 14:23:12.000000','2020-12-21 14:23:12.000000',1,0,null,1000,0,null);
INSERT INTO inspections values (2,'E2',132,1,1,null,DATEADD('DAY',1, CURRENT_DATE),'SCHEDULED','2020-12-21 14:23:12.000000','2020-12-21 14:23:12.000000',1,0,null,1000,0,null);
INSERT INTO inspections values (3,'E3',132,1,1,null,DATEADD('DAY',-1, CURRENT_DATE),'SCHEDULED','2020-12-21 14:23:12.000000','2020-12-21 14:23:12.000000',1,0,null,1000,0,null);
INSERT INTO inspections values (4,'E4',132,1,1,null,DATEADD('DAY',-1, CURRENT_DATE),'COMPLETED','2020-12-21 14:23:12.000000','2020-12-21 14:23:12.000000',1,0,null,1000,0,null);
INSERT INTO carmodels values ('113','Tata','Nano','XT','PETROL','MANUAL','HATCHBACK','4','2010','2016','100000','1','1');
INSERT INTO sellers values ('111','dash1','110','a@b.com','2147483647','UCD',NULL,NULL,NULL,NULL,NULL,NULL,'1','1',NULL,NULL,NULL);
INSERT INTO `leads` VALUES (132,1,113,111,'ab11dc1113','black','2014-04-28 10:30:49.455','2014-04-28 10:30:49.455','2014-04-28 10:30:49','rto','reg type','own ser',110,'commented',1,'2020-12-28 12:26:21',NULL,1000.25,'COMPREHENSIVE',0,NULL,'CREATED',NULL,1,'2020-02-02 12:26:21');
INSERT INTO `leads` VALUES (133,1,113,111,'ab11dc1113','red','2014-04-28 10:30:49.455','2014-04-28 10:30:49.455','2014-04-28 10:30:49','rto','reg type','own ser',110,'commented',1,'2020-12-28 12:26:21',NULL,1000.25,'COMPREHENSIVE',0,NULL,'REJECTED',NULL,1,'2020-02-02 12:26:21');
INSERT INTO `leads` VALUES (134,1,113,111,'ab11dc1113','blue','2014-04-28 10:30:49.455','2014-04-28 10:30:49.455','2014-04-28 10:30:49','rto','reg type','own ser',110,'commented',1,'2020-12-28 12:26:21',NULL,1000.25,'COMPREHENSIVE',0,NULL,'APPROVED',NULL,1,'2020-02-02 12:26:21');

INSERT INTO headers VALUES (1,'HEADER',1,1);
INSERT INTO parts VALUES (1,null,1,'XYZ-PART','UNIVERSAL',0,1,1);
INSERT INTO subparts VALUES (1,1,'XYZ-SUB-PART','MULTI',1,1,1,1,0);
INSERT INTO inspectionSubParts values (1, 1, 1, 1, 1, NOW(),null,null);
INSERT INTO subpartissues VALUES (1,null,'ok',null,1,1),
                                 (2,null,'No Inspection required',null,1,1),
                                 (3,null,'unassured',null,1,1),
                                 (100,1,'dent',null,1,1),
                                 (101,1,'scratch',null,1,1);
INSERT INTO jobs values (1,'No job required',1,1),
                        (200,'repair-XYZ',1,1),
                        (201,'painting-XYZ',1,1);

INSERT INTO jobpricemap values (1,1,null,null,null,0,1,1),
                               (2,200,null,null,null,100,1,1),
                               (3,201,null,null,null,50,1,1);

INSERT INTO issuefollowup VALUES (1,100,'repair',200,1,1),
                                 (2,100,'painting',201,1,1),
                                 (3,101,'repair',200,1,1),
                                 (4,101,'painting',201,1,1);

INSERT INTO documentStageMasters VALUES (1, 'Registration copy', 'CAR_PICKUP_DOCUMENT', 'E2_APPROVED', 1, 1),
                                       (2, 'Insurance', 'CAR_PICKUP_DOCUMENT', 'E2_APPROVED', 1, 1),
                                       (3, 'Sales deed', 'CAR_PICKUP_DOCUMENT', 'E2_APPROVED', 1, 1),
                                       (4, 'Form 29', 'CAR_PICKUP_DOCUMENT', 'E2_APPROVED', 1, 1),
                                       (5, 'Form 30', 'CAR_PICKUP_DOCUMENT', 'E2_APPROVED', 1, 1),
                                       (6, 'Car keys', 'CAR_PICKUP_DOCUMENT', 'E2_APPROVED', 1, 1),
                                       (7, 'Service History', 'SERVICE_HISTORY', 'OFFER_CREATION', 0, 1),
                                       (8, 'Lead Payment', 'LEAD_PAYMENT', 'TOKEN_PAID', 0, 1);

INSERT INTO offers VALUES (35, null , 132, 'Ok', 'Ok', 20000, 19000, 1000, 0, 0, 2, 1, '2021-02-12 10:27:09', null, null, '1', 'CREATED', '0',null,1000);
INSERT INTO offers VALUES (36, null , 132, 'Ok', 'Ok', 20000, 19000, 1000, 0, 0, 2, 1, '2021-02-12 10:27:09', null, null, '1', 'CREATED', '0',null,1000);
INSERT INTO offers VALUES (37, null , 132, 'Ok', 'Ok', 20000, 19000, 1000, 0, 0, 2, 1, '2021-02-12 10:27:09', null, null, '1', 'CREATED', '0',null,1000);

INSERT INTO commonDocuments VALUES (1, 'OFFER', 37, 7, 'www.no1.com', 1, '2021-01-29 11:19:36', NULL, NULL, 1),
                                   (2, 'OFFER', 37, 7, 'www.no2.com', 1, '2021-01-29 11:19:36', NULL, NULL, 1);

INSERT INTO commonDocuments VALUES (3, 'OFFER', 36, 7, 'www.no3.com', 1, '2021-01-29 11:19:36', NULL, NULL, 1),
                                   (4, 'OFFER', 36, 7, 'www.no4.com', 1, '2021-01-29 11:19:36', NULL, NULL, 0);
