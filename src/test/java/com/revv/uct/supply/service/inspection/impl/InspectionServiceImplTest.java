package com.revv.uct.supply.service.inspection.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.controller.inspection.model.ScheduleInspectionRequest;
import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspection.dao.IInspectionDAO;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.inspectionSubPart.IInspectionSubPartService;
import com.revv.uct.supply.service.lead.ILeadService;
import com.revv.uct.supply.service.lead.impl.LeadServiceImpl;
import com.revv.uct.supply.service.subParts.ISubPartsService;
import com.revv.uct.supply.service.user.IUserService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static com.revv.uct.supply.db.enums.InspectionStatus.COMPLETED;
import static com.revv.uct.supply.db.enums.InspectionStatus.SCHEDULED;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class InspectionServiceImplTest {

    @Autowired
    private InspectionServiceImpl inspectionService;

    @MockBean
    private IInspectionDAO inspectionDAO;

    @MockBean
    private ILeadService leadService;

    @MockBean
    private IUserService userService;

    @MockBean
    private ISubPartsService subPartsService;

    @MockBean
    private IInspectionSubPartService inspectionSubPartService;

    @Before
    public void setUp() {
        Inspection inspection = Inspection.builder()
                .id(1L).inspectionType(InspectionType.E1).inspectionTime(LocalDateTime.now())
                .inspectorID(1L).isActive(true).lastDataSyncTime(0L).
                        build();


    }

    @Test
    void scheduleInspectionTest() {
        when(leadService.existByIdAndIsActive(anyLong(),anyBoolean())).thenReturn(true);
        when(userService.existById(anyLong())).thenReturn(true);
        ScheduleInspectionRequest request = ScheduleInspectionRequest.builder()
                .inspectionType(InspectionType.E1)
                .leadID(1L)
                .inspectorID(1L)
                .inspectionTime(LocalDateTime.now())
                .build();

        Inspection inspection = Inspection.builder()
                .id(1L)
                .inspectorID(request.getInspectorID())
                .inspectionType(request.getInspectionType())
                .inspectionTime(request.getInspectionTime())
                .leadID(request.getLeadID())
                .build();

        when(inspectionDAO.scheduleInspection(any())).thenReturn(inspection);
        when(subPartsService.findAllByIsActive(anyBoolean())).thenReturn(null);
        when(inspectionSubPartService.insertDataForScheduledInspection(anyLong(),anyLong(),anyList())).thenReturn(null);

        System.out.println(inspectionService);

        try {
            assertEquals(inspection, inspectionService.scheduleInspection(request));
//            fail("Exception not produced");
        } catch (EntityNotFoundException| SupplyException| JsonProcessingException e){
            fail("Exception not produced");
//            assertTrue(e instanceof EntityNotFoundException);
        }


    }

    @Test
    void scheduleInspectionException1() {
        when(leadService.existByIdAndIsActive(anyLong(),anyBoolean())).thenReturn(false);
        when(userService.existById(anyLong())).thenReturn(true);
        ScheduleInspectionRequest request = ScheduleInspectionRequest.builder()
                .inspectionType(InspectionType.E1)
                .leadID(1L)
                .inspectorID(1L)
                .inspectionTime(LocalDateTime.now())
                .build();

        try {
            inspectionService.scheduleInspection(request);
            fail("Exception not produced");
        } catch (EntityNotFoundException| SupplyException| JsonProcessingException e){
            assertTrue(e instanceof EntityNotFoundException);
        }


    }

    @Test
    void scheduleInspectionException2() {
        when(leadService.existByIdAndIsActive(anyLong(),anyBoolean())).thenReturn(true);
        when(userService.existById(anyLong())).thenReturn(false);
        ScheduleInspectionRequest request = ScheduleInspectionRequest.builder()
                .inspectionType(InspectionType.E1)
                .leadID(1L)
                .inspectorID(1L)
                .inspectionTime(LocalDateTime.now())
                .build();

        try {
            inspectionService.scheduleInspection(request);
            fail("Exception not produced");
        } catch (EntityNotFoundException| SupplyException| JsonProcessingException e){
            assertTrue(e instanceof EntityNotFoundException);
        }
    }
}