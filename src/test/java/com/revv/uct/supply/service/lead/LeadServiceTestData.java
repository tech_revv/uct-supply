package com.revv.uct.supply.service.lead;
/*
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.db.enums.InsuranceTypeEnum;
import com.revv.uct.supply.db.lead.entity.Lead;

import java.time.LocalDateTime;

public class LeadServiceTestData
{
    public static CreateLeadRequest getValidCreateLeadRequest()
    {
        return CreateLeadRequest.builder()
                .adminID(100L)
                .carModelID(113L)
                .sellerID(111L)
                .registrationNumber("AA11 BB2222")
                .carColour("black")
                .registrationTimestamp(LocalDateTime.now())
                .manufacturingTimestamp(LocalDateTime.now())
                .insuranceValidity(LocalDateTime.now())
                .insuranceType(InsuranceTypeEnum.COMPREHENSIVE)
                .rto("rto")
                .registrationType("r type")
                .ownershipSerial("o serial")
                .serviceCityID(110L)
                .comments("comment")
                .expOfferPrice(100000.0)
                .kmsDriven(1000.0)
                .build();
    }

    public static Lead getExpectedLeadEntity(CreateLeadRequest leadRequest)
    {
        return Lead.builder()
                .adminID(leadRequest.getAdminID())
                .carModelID(leadRequest.getCarModelID())
                .sellerID(leadRequest.getSellerID())
                .registrationNumber(leadRequest.getRegistrationNumber())
                .carColour(leadRequest.getCarColour())
                .registrationTimestamp(leadRequest.getRegistrationTimestamp())
                .manufacturingTimestamp(leadRequest.getManufacturingTimestamp())
                .insuranceValidity(leadRequest.getInsuranceValidity())
                .insuranceType(leadRequest.getInsuranceType())
                .rto(leadRequest.getRto())
                .registrationType(leadRequest.getRegistrationType())
                .ownershipSerial(leadRequest.getOwnershipSerial())
                .serviceCityID(leadRequest.getServiceCityID())
                .comments(leadRequest.getComments())
                .expOfferPrice(leadRequest.getExpOfferPrice())
                .kmsDriven(leadRequest.getKmsDriven())
                .build();
    }
}

 */