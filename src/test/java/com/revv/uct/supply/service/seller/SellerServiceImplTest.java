package com.revv.uct.supply.service.seller;

/*
import com.revv.uct.supply.controller.seller.model.CreateSellerRequest;
import com.revv.uct.supply.db.seller.dao.ISellerDao;
import com.revv.uct.supply.db.serviceCity.dao.IServiceCityDAO;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.seller.impl.SellerServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.Mockito.times;

class SellerServiceImplTest {

    private final ISellerDao sellerDao = Mockito.mock(ISellerDao.class);
    private final IServiceCityDAO serviceCityDAO = Mockito.mock(IServiceCityDAO.class);
    @Autowired
    private final SellerServiceImpl sellerService = new SellerServiceImpl(sellerDao,serviceCityDAO);
    @Test
    @DisplayName("Create seller for null input. This should throw NullPointerException")
    @Tag("Create API")
    void createSellerNull()
    {
        Assertions.assertThrows(NullPointerException.class,()->{
           sellerService.createSeller(null) ;
        });

    }


    @Test
    @DisplayName("Create seller for valid input")
    @Tag("Create API")
    void createSellerHappyCase() throws UCTSupplyException {
        CreateSellerRequest request = SellerServiceTestData.getValidCreateSellerRequest();
        sellerService.createSeller(request);
        Mockito.
                verify(sellerDao,times(1)).
                createSeller(SellerServiceTestData.getExpectedSellerFromRequest(request));

    }
}

 */