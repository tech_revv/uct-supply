package com.revv.uct.supply.service.lead;
/*

import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.db.lead.dao.ILeadDao;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.lead.impl.LeadServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class LeadServiceImplTest {

    private final ILeadDao leadDao = Mockito.mock(ILeadDao.class);
    @Autowired
    private final LeadServiceImpl leadService = new LeadServiceImpl(leadDao);

    @Test
    @DisplayName("Create Lead for null input")
    @Tag("Create API")
    void createLeadNull()
    {
        Assertions.assertThrows(NullPointerException.class,()->{
//            leadService.createLead(null);
        });
    }

    @Test
    @DisplayName("Create Lead for valid input")
    @Tag("Create API")

    void createLeadValid() throws SupplyException {
        CreateLeadRequest leadRequest = LeadServiceTestData.getValidCreateLeadRequest();
//        leadService.createLead(leadRequest);
        Mockito.verify(leadDao,Mockito.times(1)).createLead(LeadServiceTestData.getExpectedLeadEntity(leadRequest));

    }
}

 */