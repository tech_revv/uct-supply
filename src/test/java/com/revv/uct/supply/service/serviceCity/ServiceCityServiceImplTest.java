package com.revv.uct.supply.service.serviceCity;
/*
import com.revv.uct.supply.controller.serviceCity.model.CreateServiceCityRequest;
import com.revv.uct.supply.db.serviceCity.dao.IServiceCityDAO;
import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.serviceCity.impl.ServiceCityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class ServiceCityServiceImplTest {

    private final IServiceCityDAO serviceCityDAO = Mockito.mock(IServiceCityDAO.class);
    private final ServiceCityServiceImpl serviceCityService = new ServiceCityServiceImpl(serviceCityDAO);
    @Test
    @DisplayName("Create service city  for null input. This should throw NullPointerException")
    @Tag("Create API")
    void createServiceCityNull()
    {
        Assertions.assertThrows(NullPointerException.class,()->
                serviceCityService.createServiceCity(null));
    }

    @Test
    @DisplayName("Create service city with valid input")
    @Tag("Create API")
    void createServiceCity() throws UCTSupplyException {
        CreateServiceCityRequest createServiceCityRequest = ServiceCityServiceTestData.getHappyServiceCityRequest();
        ServiceCity expectedCityServiceCity = ServiceCityServiceTestData.getExpectedCityServiceCity(createServiceCityRequest);
        serviceCityService.createServiceCity(createServiceCityRequest);
        Mockito.verify(serviceCityDAO,times(1)).createServiceCity(expectedCityServiceCity);
    }

    @Test
    @DisplayName("Find Service City By Id null")
    @Tag("GET API")
    void findServiceCityByIdNull()
    {
        when(serviceCityDAO.findServiceCityById(null)).thenReturn(null);
        System.out.println("in null check");
        Assertions.assertNull(serviceCityService.findServiceCityById(null));
    }
    @Test
    @DisplayName("Find Service City By Invalid Id")
    @Tag("GET API")
    void findServiceCityByIdInvalid()
    {
        when(serviceCityDAO.findServiceCityById(-1L)).thenReturn(null);
        Assertions.assertNull(serviceCityService.findServiceCityById(-1L));
    }

    @Test
    @DisplayName("Find Service City By Valid Id")
    @Tag("GET API")
    void findServiceCityByIdValid()
    {
        when(serviceCityDAO.findServiceCityById(1L)).thenReturn(ServiceCityServiceTestData.getExpectedCityServiceCityById(1L));
        Assertions.assertEquals(ServiceCityServiceTestData.getExpectedCityServiceCityById(1L),serviceCityService.findServiceCityById(1L));
    }

}

 */