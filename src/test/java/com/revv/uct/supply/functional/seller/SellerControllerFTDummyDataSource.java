package com.revv.uct.supply.functional.seller;

import com.revv.uct.supply.TestUtils;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.util.JsonUtil;

public class SellerControllerFTDummyDataSource {

    public static String getHappyCreateSellerRequestJson() {
        String jsonPath = "ftData/SellerController/create/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyCreateSellerResponse() {
        String jsonResponsePath = "ftData/SellerController/create/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getExpectedHappyDataForSellerCreationResponse() {
        String jsonResponsePath = "ftData/SellerController/dataForSellerCreation/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getExpectedHappySellersForLeadCreationResponse() {
        String jsonResponsePath = "ftData/SellerController/sellersForLeadCreation/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getExpectedHappyGetSellerByNameAndCityResponse() {
        String jsonResponsePath = "ftData/SellerController/getSellerByNameAndCity/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getHappyUpdateSellerRequestJson() {
        String jsonPath = "ftData/SellerController/update/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse geExpectedtHappyUpdateSellerResponseJson() {
        String jsonResponsePath = "ftData/SellerController/update/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getSellerNotFoundExceptionRequestJson() {
        String jsonPath = "ftData/SellerController/update/exceptions/seller_not_found_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedSellerNotFoundExceptionResponse() {
        String jsonResponsePath = "ftData/SellerController/update/exceptions/seller_not_found_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }
}
