package com.revv.uct.supply.functional.seller;

import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.functional.Auth.AuthDummyStore;
import com.revv.uct.supply.functional.Base;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SupplyApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:ftData/SellerController/cleanup.sql","classpath:ftData/SellerController/seed.sql"})
//@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {"classpath:ftData/SellerController/cleanup.sql"})
@Category(Base.class)
public class SellerControllerFunctionalTest extends Base{
    private static final String baseControllerURL = "api/seller";

    //
    //ID cast to be discussed
    //
    @Test
    public void testCreateSellerHappyCase() {
        //the dummy request used to test the controller
        String requestJson = SellerControllerFTDummyDataSource.getHappyCreateSellerRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        //ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.postForEntity(baseControllerURL+"/create", request, BaseResponse.class);
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/create"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actualList = (List<Object>) actualResponse.getData();
        LinkedHashMap actaulData = (LinkedHashMap) actualList.get(0);
        int actualID = (int) actaulData.get("id");
        String actualSellerType = (String) actaulData.get("sellerType");
        int actaulServiceCityID = (int) actaulData.get("serviceCityID");

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = SellerControllerFTDummyDataSource.getExpectedHappyCreateSellerResponse();
        List<Object> expectedList = (List<Object>) expectedResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) expectedList.get(0);
        int expectedID = (int) expectedData.get("id");
        String expectedSellerType = (String) expectedData.get("sellerType");
        int expectedServiceCityID = (int) expectedData.get("serviceCityID");

        //check if expected response and actual response is correct
        Assert.assertEquals(actualID, expectedID);
        Assert.assertEquals(actaulServiceCityID, expectedServiceCityID);
        Assert.assertEquals(actualSellerType, expectedSellerType);
    }

    @Test
    public void testGetDataForSellerCreation() {
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/dataForSellerCreation"), HttpMethod.GET, new HttpEntity<>(headers), BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //expected response from controller
        BaseResponse expectedResponse = SellerControllerFTDummyDataSource.getExpectedHappyDataForSellerCreationResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void testGetSellersForLeadCreation() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());

        //adding parameters
        Map<String, Object> params = new HashMap<>();
        params.put("serviceCityID", 110L);
        params.put("sellerType", "UCD");
        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/sellersForLeadCreation?serviceCityID={serviceCityID}&sellerType={sellerType}"),
                HttpMethod.GET,
                new HttpEntity<>(headers),
                BaseResponse.class,
                params);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //expected response from controller
        BaseResponse expectedResponse = SellerControllerFTDummyDataSource.getExpectedHappySellersForLeadCreationResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //
    //ID cast to be discussed
    //
    @Test
    public void testGetSellerByNameAndCity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());

        //adding parameters
        Map<String, Object> params = new HashMap<>();
        params.put("serviceCityID", 110L);
        params.put("name", "dash1");
        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/getSellerByNameAndCity?serviceCityID={serviceCityID}&name={name}"),
                HttpMethod.GET,
                new HttpEntity<>(headers),
                BaseResponse.class,
                params);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actaulList = (List<Object>) actualResponse.getData();
        LinkedHashMap actaulData = (LinkedHashMap) actaulList.get(0);
        int actaulSellerID = (int) actaulData.get("id");
        String actaulSellerType = (String) actaulData.get("sellerType");


        //expected response from controller
        BaseResponse expectedResponse = SellerControllerFTDummyDataSource.getExpectedHappyGetSellerByNameAndCityResponse();
        List<Object> expectedList = (List<Object>) expectedResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) expectedList.get(0);
        int expectedSellerID = (int) expectedData.get("id");
        String expectedSellerType = (String) expectedData.get("sellerType");

        //check if expected response and actual response is correct
        Assert.assertEquals(actaulSellerID, expectedSellerID);
        Assert.assertEquals(actaulSellerType, expectedSellerType);
    }

    //
    //ID cast to be discussed
    //
    @Test
    public void testUpdateSellerHappyCase() {
        //the dummy request used to test the controller
        String requestJson = SellerControllerFTDummyDataSource.getHappyUpdateSellerRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        //ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.postForEntity(baseControllerURL+"/create", request, BaseResponse.class);
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actaulList = (List<Object>) actualResponse.getData();
        LinkedHashMap actaulData = (LinkedHashMap) actaulList.get(0);
        int actualSellerID = (int) actaulData.get("id");
        String actualEmail = (String) actaulData.get("email");
        String actaulSpocName = (String) actaulData.get("spocName");
        String actualBankAcDetails = (String) actaulData.get("bankAcDetails");
        String actualGstNumber = (String) actaulData.get("gstNumber");
        String actualPanNumber = (String) actaulData.get("panNumber");

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = SellerControllerFTDummyDataSource.geExpectedtHappyUpdateSellerResponseJson();
        List<Object> expectedList = (List<Object>) actualResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) actaulList.get(0);
        int expectedSellerID = (int) expectedData.get("id");
        String expectedEmail = (String) expectedData.get("email");
        String expectedSpocName = (String) expectedData.get("spocName");
        String expectedBankAcDetails = (String) expectedData.get("bankAcDetails");
        String expectedGstNumber = (String) expectedData.get("gstNumber");
        String expectedPanNumber = (String) expectedData.get("panNumber");

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedSellerID, actualSellerID);
        Assert.assertEquals(expectedEmail, actualEmail);
        Assert.assertEquals(expectedSpocName, actaulSpocName);
        Assert.assertEquals(expectedBankAcDetails, actualBankAcDetails);
        Assert.assertEquals(expectedGstNumber, actualGstNumber);
        Assert.assertEquals(expectedPanNumber, actualPanNumber);
    }

    //Exception test - seller not found
    @Test
    public void testSellerNotFoundException() {
        //the dummy request used to test the controller
        String requestJson = SellerControllerFTDummyDataSource.getSellerNotFoundExceptionRequestJson();

        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //expected response from controller
        BaseResponse expectedResponse = SellerControllerFTDummyDataSource.getExpectedSellerNotFoundExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }
}
