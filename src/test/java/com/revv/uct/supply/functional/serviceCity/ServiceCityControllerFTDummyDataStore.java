package com.revv.uct.supply.functional.serviceCity;


import com.revv.uct.supply.TestUtils;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.util.JsonUtil;

public class ServiceCityControllerFTDummyDataStore
{
    public static String getHappyCreateServiceCityRequestJSON()
    {
        String jsonPath = "ftData/ServiceCityController/createServiceCity/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static  BaseResponse getExpectedHappyResponse()
    {
        String jsonResponsePath = "ftData/ServiceCityController/createServiceCity/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }
}
