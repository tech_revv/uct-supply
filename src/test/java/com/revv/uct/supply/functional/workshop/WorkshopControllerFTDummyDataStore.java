package com.revv.uct.supply.functional.workshop;

import com.revv.uct.supply.TestUtils;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.util.JsonUtil;

public class WorkshopControllerFTDummyDataStore {

    public static String getHappyCreateWorkshopRequestJSON()
    {
        String jsonPath = "ftData/WorkshopController/createWorkshop/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyCreateWorkshopResponse()
    {
        String jsonResponsePath = "ftData/WorkshopController/createWorkshop/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getExpectedHappyGetWorkshopsDataResponse()
    {
        String jsonResponsePath = "ftData/WorkshopController/getWorkshopsData/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

}
