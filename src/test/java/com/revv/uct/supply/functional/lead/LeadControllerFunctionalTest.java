package com.revv.uct.supply.functional.lead;

import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.functional.Base;
import org.junit.Assert;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SupplyApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:ftData/LeadController/cleanup.sql","classpath:ftData/LeadController/seed.sql"})
//@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {"classpath:ftData/InspectionController/cleanup.sql"})
@Category(Base.class)
public class LeadControllerFunctionalTest extends Base {
    private static final String baseControllerURL = "api/lead";

    //create lead test
    @Test
    public void testCreateLeadHappyCase() {
        //the dummy request used to test the controller
        String requestJson = LeadControllerFTDummyDataSource.getHappyCreateLeadRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/create"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = LeadControllerFTDummyDataSource.getExpectedHappyCreateLeadResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //get data for lead creation test
    @Test
    public void testGetDataForLeadCreation() {
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/dataForLeadCreation"), HttpMethod.GET, new HttpEntity<>(headers), BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //expected response from controller
        BaseResponse expectedResponse = LeadControllerFTDummyDataSource.getExpectedHappyDataForLeadCreationResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //update lead test
    @Test
    public void testUpdateLeadHappyCase() {
        //the dummy request used to test the controller
        String requestJson = LeadControllerFTDummyDataSource.getHappyUpdateLeadRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = LeadControllerFTDummyDataSource.getExpectedHappyUpdateLeadResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - lead not found
    @Test
    public void testLeadNotFoundException() {
        //the dummy request used to test the controller
        String requestJson = LeadControllerFTDummyDataSource.getLeadNotFoundExceptionRequestJson();

        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //expected response from controller
        BaseResponse expectedResponse = LeadControllerFTDummyDataSource.getExpectedLeadNotFoundExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - rejectionSubStatus required
    @Test
    public void testRejectionSubStatusRequiredException() {
        //the dummy request used to test the controller
        String requestJson = LeadControllerFTDummyDataSource.getRejectionSubStatusRequiredExceptionRequestJson();

        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //expected response from controller
        BaseResponse expectedResponse = LeadControllerFTDummyDataSource.getExpectedRejectionSubStatusRequiredExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - rejection reason required
    @Test
    public void testRejectionReasonRequiredException() {
        //the dummy request used to test the controller
        String requestJson = LeadControllerFTDummyDataSource.getRejectionReasonRequiredExceptionRequestJson();

        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //expected response from controller
        BaseResponse expectedResponse = LeadControllerFTDummyDataSource.getExpectedRejectionReasonRequiredExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - approved lead update
    @Test
    public void testApprovedLeadUpdateException() {
        //the dummy request used to test the controller
        String requestJson = LeadControllerFTDummyDataSource.getApprovedLeadUpdateExceptionRequestJson();

        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //expected response from controller
        BaseResponse expectedResponse = LeadControllerFTDummyDataSource.getExpectedApprovedLeadUpdateExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - rejected lead update
    @Test
    public void testRejectedLeadUpdateException() {
        //the dummy request used to test the controller
        String requestJson = LeadControllerFTDummyDataSource.getRejectedLeadUpdateExceptionRequestJson();

        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //expected response from controller
        BaseResponse expectedResponse = LeadControllerFTDummyDataSource.getExpectedRejectedLeadUpdateExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //create online lead test
    @Test
    public void testCreateOnlineLeadHappyCase() {
        //the dummy request used to test the controller
        String requestJson = LeadControllerFTDummyDataSource.getHappyCreateOnlineLeadRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/createOnlineLead"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = LeadControllerFTDummyDataSource.getExpectedHappyCreateOnlineLeadResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }
}
