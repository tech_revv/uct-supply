package com.revv.uct.supply.functional.serviceCity;

import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.functional.Base;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertTrue;


@RunWith(SpringRunner.class)
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SupplyApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:ftData/InspectionController/cleanup.sql","classpath:ftData/InspectionController/seed.sql"})
//@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {"classpath:ftData/InspectionController/cleanup.sql"})
@Category(Base.class)
public class ServiceCityControllerFunctionalTest  extends Base
{
    private static final String baseControllerURL = "api/service_city";

//    @Test
//    @DisplayName("Happy Case Create Service City")
//    @Tag("API ")
//    public void createServiceCityHappyCase()
//    {
//        String requestJson = ServiceCityControllerFTDummyDataStore.getHappyCreateServiceCityRequestJSON();
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);
//        //calling controller and getting response from API
//        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.postForEntity(getUrl(baseControllerURL+"/"),request, BaseResponse.class);
//        BaseResponse actualResponse = actualResponseEntity.getBody();
//        //check if response is successful
//        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());
//        //expected response from controller
//        BaseResponse expectedResponse = ServiceCityControllerFTDummyDataStore.getExpectedHappyResponse();
//        //check if expected response and actual response is correct
//        Assert.assertEquals(expectedResponse,actualResponse);
//    }

}
