package com.revv.uct.supply.functional.refurbishment;

import com.revv.uct.supply.TestUtils;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.util.JsonUtil;

public class RefurbishmentControllerFTDummyDataStore {

    public static BaseResponse getExpectedRefurbJobListJSON()
    {
        String jsonPath = "ftData/RefurbishmentController/refurbJobList/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonPath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getSubmitRefurbDocumentsResponseJson()
    {
        String jsonPath = "ftData/RefurbishmentController/saveDocuments/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonPath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getSubmitRefurbDocumentsRequestJson() {
        String jsonPath = "ftData/RefurbishmentController/saveDocuments/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static String getSubmitRefurbDocumentsInvalidDocumentIDRequestJson() {
        String jsonPath = "ftData/RefurbishmentController/saveDocuments/invalid_document_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static String getSubmitRefurbDocumentsDocumentMandatoryRequestJson() {
        String jsonPath = "ftData/RefurbishmentController/saveDocuments/mandatory_document_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static String getScheduleRefurbishmentHappyCaseRequestJson() {
        String jsonPath = "ftData/RefurbishmentController/scheduleRefurbishment/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getScheduleRefurbishmentHappyCaseResponseJson()
    {
        String jsonPath = "ftData/RefurbishmentController/scheduleRefurbishment/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonPath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

}
