package com.revv.uct.supply.functional.inspection;

import com.revv.uct.supply.TestUtils;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.util.JsonUtil;

public class InspectionControllerFTDummyDataStore {

    public static String getHappyCreateServiceCityRequestJSON()
    {
        String jsonPath = "ftData/InspectionController/getInspectionList/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedInspectionListHappyResponse()
    {
        String jsonResponsePath = "ftData/InspectionController/getInspectionList/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getHappyScheduleInspectionRequestJSON()
    {
        String jsonPath = "ftData/InspectionController/scheduleInspection/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedScheduleInspectionResponseJSON()
    {
        String jsonPath = "ftData/InspectionController/scheduleInspection/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonPath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getExpectedHappyInspectionChecklistResponse() {
        String jsonResponsePath = "ftData/InspectionController/getInspectionChecklist/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getExpectedReportExistInspectionChecklistResponse() {
        String jsonResponsePath = "ftData/InspectionController/getInspectionChecklist/report_exist_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getInspectionPickupChecklistResponse() {
        String jsonResponsePath = "ftData/InspectionController/getInspectionPickupChecklist/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getSaveInspectionPickupChecklistRequestJSON()
    {
        String jsonPath = "ftData/InspectionController/saveInspectionPickupChecklist/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }
    public static BaseResponse getSaveInspectionPickupChecklistResponse()
    {
        String jsonResponsePath = "ftData/InspectionController/saveInspectionPickupChecklist/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }
    public static BaseResponse getExpectedE2InspectionChecklistHappyCase()
    {
        String jsonResponsePath = "ftData/InspectionController/getInspectionChecklist/e2_happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getExpectedInspectionDifferenceHappyCase() {
        String jsonResponsePath = "ftData/InspectionController/getInspectionChecklist/inspection_difference_happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getExpectedInspectionDifferenceExceptionCase() {
        String jsonResponsePath = "ftData/InspectionController/getInspectionChecklist/inspection_difference_exception_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }
}
