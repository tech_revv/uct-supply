package com.revv.uct.supply.functional.offer;

import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.functional.Base;
import org.junit.Assert;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SupplyApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:ftData/OfferController/cleanup.sql","classpath:ftData/OfferController/seed.sql"})
//@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {"classpath:ftData/InspectionController/cleanup.sql"})
@Category(Base.class)
public class OfferControllerFunctionalTest extends Base {
    private static final String baseControllerURL = "api/offer";

    //create lead test
    @Test
    public void testCreateOfferHappyCase() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getHappyCreateOfferRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/create"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actualList = (List<Object>) actualResponse.getData();
        LinkedHashMap actualData = (LinkedHashMap) actualList.get(0);
        int actualID = (int) actualData.get("id");
        int actualLeadID = (int) actualData.get("leadID");
        int actualInspectionID = (int) actualData.get("inspectionID");
        boolean actualIsActive = (boolean) actualData.get("isActive");

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedHappyCreateOfferResponse();
        List<Object> expectedList = (List<Object>) expectedResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) expectedList.get(0);
        int expectedID = (int) expectedData.get("id");
        int expectedLeadID = (int) expectedData.get("leadID");
        int expectedInspectionID = (int) expectedData.get("inspectionID");
        boolean expectedIsActive = (boolean) expectedData.get("isActive");


        //check if expected response and actual response is correct
//        Assert.assertEquals(expectedResponse, actualResponse);
        Assert.assertEquals(expectedID, actualID);
        Assert.assertEquals(expectedLeadID, actualLeadID);
        Assert.assertEquals(expectedInspectionID, actualInspectionID);
        Assert.assertEquals(expectedIsActive, actualIsActive);
    }

    @Test
    public void checkCalculateOfferPriceHappyCase() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getHappyCalculateOfferPriceRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/calculateOfferPrice"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedHappyCalculateOfferPriceResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void checkUpdateOfferHappyCase() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getHappyUpdateOfferRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actualList = (List<Object>) actualResponse.getData();
        LinkedHashMap actualData = (LinkedHashMap) actualList.get(0);
        int actualID = (int) actualData.get("id");
        int actualLeadID = (int) actualData.get("leadID");
        String actualOfferStatus = (String) actualData.get("offerStatus");

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedHappyUpdateOfferReponse();
        List<Object> expectedList = (List<Object>) expectedResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) expectedList.get(0);
        int expectedID = (int) expectedData.get("id");
        int expectedLeadID = (int) expectedData.get("leadID");
        String expectedOfferStatus = (String) expectedData.get("offerStatus");

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedID, actualID);
        Assert.assertEquals(expectedLeadID, actualLeadID);
        Assert.assertEquals(expectedOfferStatus, actualOfferStatus);
    }

    @Test
    public void checkReviseOfferHappyCase() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getHappyReviseOfferRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/revise"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actualList = (List<Object>) actualResponse.getData();
        LinkedHashMap actualData = (LinkedHashMap) actualList.get(0);
        int actualID = (int) actualData.get("id");
        int actualLeadID = (int) actualData.get("leadID");
        String actualOfferReviseStage = (String) actualData.get("offerReviseStage");
        Boolean actualIsRevised = (Boolean) actualData.get("isRevised");

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedHappyReviseOfferResponse();
        List<Object> expectedList = (List<Object>) expectedResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) expectedList.get(0);
        int expectedID = (int) expectedData.get("id");
        int expectedLeadID = (int) expectedData.get("leadID");
        String expectedOfferReviseStage = (String) expectedData.get("offerReviseStage");
        Boolean expectexIsRevised = (Boolean) actualData.get("isRevised");

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedID, actualID);
        Assert.assertEquals(expectedLeadID, actualLeadID);
        Assert.assertEquals(expectedOfferReviseStage, actualOfferReviseStage);
        Assert.assertEquals(expectexIsRevised, actualIsRevised);
    }

    /*
    ======
    *****following two are handled at validation using pojo CreateOfferRequest*****
    ======
    //Exception test - challan status required
    @Test
    public void checkChallanStatusRequiredException() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getChallanStatusRequiredExceptionRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/create"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedChallanStatusRequiredExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - serviceHistory status required
    @Test
    public void checkServiceHistoryStatusRequiredException() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getServiceHistoryStatusRequiredExceptionRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/create"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedServiceHistoryStatusRequiredExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }
     */

    //Exception test - offer not found update offer
    @Test
    public void checkOfferNotFoundException() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getOfferDataNotFoundExceptionRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedOfferDataNotFoundExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - rejectionReason required
    @Test
    public void checkRejectionReasonRequiredException() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getRejectionReasonRequiredExceptionRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedRejectionReasonRequiredExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - offer not found revise offer
    @Test
    public void checkOfferNotFoundException_revise() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getOfferDataNotFoundExceptionRequestJson_revise();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/revise"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedOfferDataNotFoundExceptionResponse_revise();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - service docs not found
    @Test
    public void checkServiceDocsNotFoundException() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getServiceDocsNotFoundExceptionRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/revise"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedServiceDocsNotFoundExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //Exception test - service docs count mismatch
    @Test
    public void checkServiceDocsCountMismatchException() {
        //the dummy request used to test the controller
        String requestJson = OfferControllerFTDummyDataSource.getServiceDocsCountMismatchExceptionRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/revise"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = OfferControllerFTDummyDataSource.getExpectedServiceDocsCountMismatchExceptionResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }
}
