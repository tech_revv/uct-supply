package com.revv.uct.supply.functional.lead;

import com.revv.uct.supply.TestUtils;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.util.JsonUtil;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ser.Serializers;

public class LeadControllerFTDummyDataSource {

    public static String getHappyCreateLeadRequestJson() {
        String jsonPath = "ftData/LeadController/create/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyCreateLeadResponse() {
        String jsonResponsePath = "ftData/LeadController/create/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static BaseResponse getExpectedHappyDataForLeadCreationResponse () {
        String jsonResponsePath = "ftData/LeadController/dataForLeadCreation/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getHappyUpdateLeadRequestJson() {
        String jsonPath = "ftData/LeadController/update/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyUpdateLeadResponse() {
        String jsonResponsePath = "ftData/LeadController/update/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getLeadNotFoundExceptionRequestJson() {
        String jsonPath = "ftData/LeadController/update/exceptions/lead_not_found_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedLeadNotFoundExceptionResponse() {
        String jsonResponsePath = "ftData/LeadController/update/exceptions/lead_not_found_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getRejectionSubStatusRequiredExceptionRequestJson() {
        String jsonPath = "ftData/LeadController/update/exceptions/rejectionSubStatus_required_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedRejectionSubStatusRequiredExceptionResponse() {
        String jsonResponsePath = "ftData/LeadController/update/exceptions/rejectionSubStatus_required_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getRejectionReasonRequiredExceptionRequestJson() {
        String jsonPath = "ftData/LeadController/update/exceptions/rejectionReason_required_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedRejectionReasonRequiredExceptionResponse() {
        String jsonResponsePath = "ftData/LeadController/update/exceptions/rejectionReason_required_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getApprovedLeadUpdateExceptionRequestJson() {
        String jsonPath = "ftData/LeadController/update/exceptions/approvedLeadUpdateException_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedApprovedLeadUpdateExceptionResponse() {
        String jsonResponsePath = "ftData/LeadController/update/exceptions/approvedLeadUpdateException_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getRejectedLeadUpdateExceptionRequestJson() {
        String jsonPath = "ftData/LeadController/update/exceptions/rejectedLeadUpdateException_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedRejectedLeadUpdateExceptionResponse() {
        String jsonResponsePath = "ftData/LeadController/update/exceptions/rejectedLeadUpdateException_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getHappyCreateOnlineLeadRequestJson() {
        String jsonPath = "ftData/LeadController/createOnlineLead/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyCreateOnlineLeadResponse() {
        String jsonResponsePath = "ftData/LeadController/createOnlineLead/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }
}
