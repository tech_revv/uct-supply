package com.revv.uct.supply.functional.procuredCar;

import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.functional.Base;
import org.junit.Assert;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SupplyApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:ftData/ProcuredCarController/cleanup.sql","classpath:ftData/ProcuredCarController/seed.sql"})
//@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {"classpath:ftData/InspectionController/cleanup.sql"})
@Category(Base.class)
public class ProcuredCarControllerFunctionalTest extends Base{
    private static final String baseControllerURL = "api/procuredCar";

    //save procured car test
    @Test
    public void testSaveProcuredCarDetailsHappyCase() {
        //the dummy request used to test the controller
        String requestJson = ProcuredCarControllerFTDummyDataSource.getHappySaveProcuredCarDetailsRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/saveProcuredCarDetails"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actualList = (List<Object>) actualResponse.getData();
        LinkedHashMap actualData = (LinkedHashMap) actualList.get(0);
        int actualID = (int) actualData.get("id");
        int actualLeadID = (int) actualData.get("leadID");
        String actualStatus = (String) actualData.get("status");
        boolean actualIsActive = (boolean) actualData.get("active");

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = ProcuredCarControllerFTDummyDataSource.getExpectedHappySaveProcuredCarDetailsResponse();
        List<Object> expectedList = (List<Object>) expectedResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) expectedList.get(0);
        int expectedID = (int) expectedData.get("id");
        int expectedLeadID = (int) expectedData.get("leadID");
        String expectedStatus = (String) expectedData.get("status");
        boolean expectedIsActive = (boolean) expectedData.get("active");

        //check if expected response and actual response is correct
//        Assert.assertEquals(expectedResponse, actualResponse);
        Assert.assertEquals(expectedID, actualID);
        Assert.assertEquals(expectedLeadID, actualLeadID);
        Assert.assertEquals(expectedStatus, actualStatus);
        Assert.assertEquals(expectedIsActive, actualIsActive);
    }

    //update procured car test
    @Test
    public void testUpdateProcuredCarDetailsHappyCase() {
        //the dummy request used to test the controller
        String requestJson = ProcuredCarControllerFTDummyDataSource.getHappyUpdateProcuredCarDetailsRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/update"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actualList = (List<Object>) actualResponse.getData();
        LinkedHashMap actualData = (LinkedHashMap) actualList.get(0);
        int actualID = (int) actualData.get("id");
        int actualLeadID = (int) actualData.get("leadID");
        String actualStatus = (String) actualData.get("status");
        boolean actualIsIssueReported = (boolean) actualData.get("issueReported");

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = ProcuredCarControllerFTDummyDataSource.getExpectedHappyUpdateProcuredCarDetailsResponse();
        List<Object> expectedList = (List<Object>) expectedResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) expectedList.get(0);
        int expectedID = (int) expectedData.get("id");
        int expectedLeadID = (int) expectedData.get("leadID");
        String expectedStatus = (String) expectedData.get("status");
        boolean expectedIsIssueReported = (boolean) expectedData.get("issueReported");

        //check if expected response and actual response is correct
//        Assert.assertEquals(expectedResponse, actualResponse);
        Assert.assertEquals(expectedID, actualID);
        Assert.assertEquals(expectedLeadID, actualLeadID);
        Assert.assertEquals(expectedStatus, actualStatus);
        Assert.assertEquals(expectedIsIssueReported, actualIsIssueReported);
    }

    //report issue test
    @Test
    public void testReportIssueHappyCase() {
        //the dummy request used to test the controller
        String requestJson = ProcuredCarControllerFTDummyDataSource.getHappyReportIssueRequestJson();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/reportIssue"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actualList = (List<Object>) actualResponse.getData();
        LinkedHashMap actualData = (LinkedHashMap) actualList.get(0);
        int actualID = (int) actualData.get("id");
        int actualLeadID = (int) actualData.get("leadID");
        String actualStatus = (String) actualData.get("status");
        boolean actualIsIssueReported = (boolean) actualData.get("issueReported");
        String actualIssueStage = (String) actualData.get("issueStage");

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = ProcuredCarControllerFTDummyDataSource.getExpectedHappyReportIssueResponse();
        List<Object> expectedList = (List<Object>) expectedResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) expectedList.get(0);
        int expectedID = (int) expectedData.get("id");
        int expectedLeadID = (int) expectedData.get("leadID");
        String expectedStatus = (String) expectedData.get("status");
        boolean expectedIsIssueReported = (boolean) expectedData.get("issueReported");
        String expectedIssueStage = (String) expectedData.get("issueStage");

        //check if expected response and actual response is correct
//        Assert.assertEquals(expectedResponse, actualResponse);
        Assert.assertEquals(expectedID, actualID);
        Assert.assertEquals(expectedLeadID, actualLeadID);
        Assert.assertEquals(expectedStatus, actualStatus);
        Assert.assertEquals(expectedIsIssueReported, actualIsIssueReported);
        Assert.assertEquals(expectedIssueStage, actualIssueStage);
    }
}
