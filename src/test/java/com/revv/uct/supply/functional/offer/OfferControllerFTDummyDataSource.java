package com.revv.uct.supply.functional.offer;

import com.revv.uct.supply.TestUtils;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.util.JsonUtil;

public class OfferControllerFTDummyDataSource {
    public static String getHappyCreateOfferRequestJson() {
        String jsonPath = "ftData/OfferController/create/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyCreateOfferResponse() {
        String jsonResponsePath = "ftData/OfferController/create/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getHappyCalculateOfferPriceRequestJson() {
        String jsonPath = "ftData/OfferController/calculateOfferPrice/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyCalculateOfferPriceResponse() {
        String jsonResponsePath = "ftData/OfferController/calculateOfferPrice/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getHappyUpdateOfferRequestJson() {
        String jsonPath = "ftData/OfferController/update/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyUpdateOfferReponse() {
        String jsonResponsePath = "ftData/OfferController/update/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getHappyReviseOfferRequestJson() {
        String jsonPath = "ftData/OfferController/revise/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyReviseOfferResponse() {
        String jsonResponsePath = "ftData/OfferController/revise/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getChallanStatusRequiredExceptionRequestJson() {
        String jsonPath = "ftData/OfferController/create/exceptions/challanStatus_required_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedChallanStatusRequiredExceptionResponse() {
        String jsonResponsePath = "ftData/OfferController/create/exceptions/challanStatus_required_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getServiceHistoryStatusRequiredExceptionRequestJson() {
        String jsonPath = "ftData/OfferController/create/exceptions/serviceHistoryStatus_required_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedServiceHistoryStatusRequiredExceptionResponse() {
        String jsonResponsePath = "ftData/OfferController/create/exceptions/serviceHistoryStatus_required_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getOfferDataNotFoundExceptionRequestJson() {
        String jsonPath = "ftData/OfferController/update/exceptions/offer_not_found_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedOfferDataNotFoundExceptionResponse() {
        String jsonResponsePath = "ftData/OfferController/update/exceptions/offer_not_found_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getRejectionReasonRequiredExceptionRequestJson() {
        String jsonPath = "ftData/OfferController/update/exceptions/rejectionReason_required_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedRejectionReasonRequiredExceptionResponse() {
        String jsonResponsePath = "ftData/OfferController/update/exceptions/rejectionReason_required_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getOfferDataNotFoundExceptionRequestJson_revise() {
        String jsonPath = "ftData/OfferController/revise/exceptions/offer_not_found_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedOfferDataNotFoundExceptionResponse_revise() {
        String jsonResponsePath = "ftData/OfferController/revise/exceptions/offer_not_found_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getServiceDocsNotFoundExceptionRequestJson() {
        String jsonPath = "ftData/OfferController/revise/exceptions/serviceDocs_not_found_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedServiceDocsNotFoundExceptionResponse() {
        String jsonResponsePath = "ftData/OfferController/revise/exceptions/serviceDocs_not_found_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getServiceDocsCountMismatchExceptionRequestJson() {
        String jsonPath = "ftData/OfferController/revise/exceptions/serviceDocs_count_mismatch_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedServiceDocsCountMismatchExceptionResponse() {
        String jsonResponsePath = "ftData/OfferController/revise/exceptions/serviceDocs_count_mismatch_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }
}
