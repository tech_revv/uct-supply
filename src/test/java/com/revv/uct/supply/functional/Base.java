package com.revv.uct.supply.functional;


import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.functional.Auth.AuthDummyStore;
import org.junit.Ignore;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SupplyApplication.class)
@TestPropertySource(locations = "classpath:application.properties")
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Category(Base.class)
public class Base {
    @LocalServerPort
    public int port;

    public RestTemplate restTemplate = new RestTemplate();
    public String getUrl(String path){return "http://localhost:"+port+path;}


    @Value("${user.service.baseURL}")
    private String USER_SERVICE_URL;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    private String token;

    @BeforeEach
    public void init() throws Exception {
        System.out.println("init==>>");
        String requestJson = AuthDummyStore.getHappyRequestJSON();
        //preparing request payload
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);
        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.postForEntity(USER_SERVICE_URL+"login", request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());
        this.setToken(actualResponseEntity.getHeaders().getFirst("Token"));
    }

}
