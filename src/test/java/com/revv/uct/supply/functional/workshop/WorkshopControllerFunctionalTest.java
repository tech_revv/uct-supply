package com.revv.uct.supply.functional.workshop;

import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.functional.Base;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SupplyApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:ftData/WorkshopController/cleanup.sql","classpath:ftData/WorkshopController/seed.sql"})
//@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {"classpath:ftData/InspectionController/cleanup.sql"})
@Category(Base.class)
public class WorkshopControllerFunctionalTest extends Base{

    private static final String baseControllerURL = "api/workshop";

    //create workshop test
    @Test
    public void testCreateWorkshopHappyCase() {
        //the dummy request used to test the controller
        String requestJson = WorkshopControllerFTDummyDataStore.getHappyCreateWorkshopRequestJSON();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        JSONObject obj = new JSONObject((Map) actualResponse.getData());
        String workshopName = (String) obj.get("workshopName");
        String address = (String) obj.get("address");
        Boolean isActive = (Boolean) obj.get("isActive");

        //expected response from controller
        BaseResponse expectedResponse = WorkshopControllerFTDummyDataStore.getExpectedHappyCreateWorkshopResponse();
        JSONObject objExpected = new JSONObject((Map) expectedResponse.getData());
        String workshopNameExpected = (String) objExpected.get("workshopName");
        String addressExpected = (String) objExpected.get("address");
        Boolean isActiveExpected = (Boolean) objExpected.get("isActive");

        //check if expected response and actual response is correct
        Assert.assertEquals(workshopName, workshopNameExpected);
        Assert.assertEquals(address, addressExpected);
        Assert.assertEquals(isActive, isActiveExpected);
    }

    //get workshops data test
    @Test
    public void testGetWorkshopsDataHappyCase() {

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/dataForWorkshop"), HttpMethod.GET, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = WorkshopControllerFTDummyDataStore.getExpectedHappyGetWorkshopsDataResponse();

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedResponse, actualResponse);
    }

}
