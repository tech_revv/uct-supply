package com.revv.uct.supply.functional.inspection;

import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.constants.APIConstants;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.functional.Base;

import org.junit.Assert;
import com.revv.uct.supply.service.inspection.model.InspectionList;
import com.revv.uct.supply.service.inspection.model.InspectionListHeader;
import com.revv.uct.supply.util.JsonUtil;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;

import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SupplyApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:ftData/InspectionController/cleanup.sql","classpath:ftData/InspectionController/seed.sql"})
//@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {"classpath:ftData/InspectionController/cleanup.sql"})
@Category(Base.class)
public class InspectionControllerFunctionalTest extends Base
{
    private static final String baseControllerURL = "/api/v1/inspection";

    @Test
    @DisplayName("Happy Case Get Inspection List")
    @Tag("API ")
    public void getInspectionListHappyCase() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/list"), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        System.out.println("actualResponse");
        System.out.println(actualResponse);
        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());
        //actualResponse.getData()
        List<InspectionListHeader> inspectionListHeader = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        List<Object> inspections = (List<Object>) actualResponse.getData();
        for (Object inspection : inspections) {
            inspectionListHeader.add(JsonUtil.jsonDecode(mapper.writeValueAsString(inspection), InspectionListHeader.class));
        }
        assertEquals(inspectionListHeader.size(), 2);
        InspectionListHeader scheduled = inspectionListHeader.stream()
                .filter(inspection -> inspection.getName().equals(APIConstants.INSPECTION_HEADER_SCHEDULE)).findAny().orElse(null);

        InspectionListHeader past = inspectionListHeader.stream()
                .filter(inspection -> inspection.getName().equals(APIConstants.INSPECTION_HEADER_PAST)).findAny().orElse(null);

        assert scheduled != null;
        assert past != null;

        InspectionList today = scheduled.getInspectionsList().stream()
                .filter(lis -> lis.getName().equals(APIConstants.INSPECTION_LIST_TODAY))
                .findAny()
                .orElse(null);
        InspectionList later = scheduled.getInspectionsList().stream()
                .filter(lis -> lis.getName().equals(APIConstants.INSPECTION_LIST_LATER))
                .findAny()
                .orElse(null);

        assert today != null;
        assertEquals(today.getData().get(0).getId(), 1);
        assert later != null;
        assertEquals(later.getData().get(0).getId(), 2);

        InspectionList completed = past.getInspectionsList().stream()
                .filter(lis -> lis.getName().equals(APIConstants.INSPECTION_LIST_COMPLETED))
                .findAny()
                .orElse(null);
        InspectionList incomplete = past.getInspectionsList().stream()
                .filter(lis -> lis.getName().equals(APIConstants.INSPECTION_LIST_UNCOMPLETED))
                .findAny()
                .orElse(null);

        assert completed != null;
        assertEquals(completed.getData().get(0).getId(), 4);
        assert incomplete != null;
        assertEquals(incomplete.getData().get(0).getId(), 3);
    }

    @Test
    @DisplayName("Happy Case Get Inspection Checklist")
    @Tag("API ")
    public void getInspectionChecklistHappyCase() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/checklist/1"), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));
        //check if response is successful
        BaseResponse expectedResponse = InspectionControllerFTDummyDataStore.getExpectedHappyInspectionChecklistResponse();

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    @DisplayName("Happy Case Get E2 Inspection Checklist")
    @Sql(scripts = {"classpath:ftData/InspectionController/e2.sql"})
    @Tag("API ")
    public void getE2InspectionChecklistHappyCase() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/checklist/2"), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));
        //check if response is successful
        BaseResponse expectedResponse = InspectionControllerFTDummyDataStore.getExpectedE2InspectionChecklistHappyCase();

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    @DisplayName("Happy Case Get Inspection Checklist Differences")
    @Sql(scripts = {"classpath:ftData/InspectionController/e2.sql","classpath:ftData/InspectionController/e2_report.sql"})
    @Tag("API ")
    public void getInspectionChecklistDifferenceHappyCase() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        //adding the query params to the URL
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(getUrl(baseControllerURL+"/difference"))
                .queryParam("leadID", 132)
                .queryParam("inspectionType1", "E1")
                .queryParam("inspectionType2", "E2");
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                uriBuilder.toUriString(), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));
        //check if response is successful
        BaseResponse expectedResponse = InspectionControllerFTDummyDataStore.getExpectedInspectionDifferenceHappyCase();

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    @DisplayName("Happy Case Get Inspection Checklist Differences")
    @Sql(scripts = {"classpath:ftData/InspectionController/e2.sql","classpath:ftData/InspectionController/e2_report.sql"})
    @Tag("API ")
    public void getInspectionChecklistDifferenceExceptionCase() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        //adding the query params to the URL
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(getUrl(baseControllerURL+"/difference"))
                .queryParam("leadID", 139)
                .queryParam("inspectionType1", "E1")
                .queryParam("inspectionType2", "E2");
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                uriBuilder.toUriString(), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));
        //check if response is successful
        BaseResponse expectedResponse = InspectionControllerFTDummyDataStore.getExpectedInspectionDifferenceExceptionCase();

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    @Sql(scripts = {"classpath:ftData/InspectionController/report.sql"})
    @DisplayName("Reports exist Case Get Inspection Checklist")
    @Tag("API ")
    public void getInspectionChecklistReportExist() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/checklist/1"), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));
        //check if response is successful
        BaseResponse expectedResponse = InspectionControllerFTDummyDataStore.getExpectedReportExistInspectionChecklistResponse();

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    //
    //==>>to be discussed for integer id
    //
    //schedule inspection test
    @Test
    public void testScheduleInspectionHappyCase() {
        //the dummy request used to test the controller
        String requestJson = InspectionControllerFTDummyDataStore.getHappyScheduleInspectionRequestJSON();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);

        //calling controller and getting response from API
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(getUrl(baseControllerURL+"/schedule"), HttpMethod.POST, request, BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        List<Object> actualList = (List<Object>) actualResponse.getData();
        LinkedHashMap actualData = (LinkedHashMap) actualList.get(0);
        int actualLeadID = (int) actualData.get("leadID");
        int actualID = (int) actualData.get("id");

        //check if response is successful
        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());

        //expected response from controller
        BaseResponse expectedResponse = InspectionControllerFTDummyDataStore.getExpectedScheduleInspectionResponseJSON();
        List<Object> expectedList = (List<Object>) expectedResponse.getData();
        LinkedHashMap expectedData = (LinkedHashMap) expectedList.get(0);
        int expectedLeadID = (int) expectedData.get("leadID");
        int expectedID = (int) expectedData.get("id");

        //check if expected response and actual response is correct
        Assert.assertEquals(expectedLeadID, actualLeadID);
        Assert.assertEquals(expectedID, actualID);
    }

    @Test
    @Sql(scripts = {"classpath:ftData/InspectionController/commonDocument.sql"})
    @DisplayName("Get Inspection Pickup Checklist")
    @Tag("API ")
    public void getInspectionPickupChecklist() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/pickupChecklist/1"), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));
        //check if response is successful
        BaseResponse expectedResponse = InspectionControllerFTDummyDataStore.getInspectionPickupChecklistResponse();

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    @Sql(scripts = {"classpath:ftData/InspectionController/commonDocument.sql"})
    @DisplayName("save Inspection Pickup Checklist")
    @Tag("API ")
    public void saveInspectionPickupChecklist() throws Exception {
        String requestJson = InspectionControllerFTDummyDataStore.getSaveInspectionPickupChecklistRequestJSON();

        //preparing request payload
        org.springframework.http.HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        HttpEntity<String> request = new HttpEntity<>(requestJson,headers);


        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/pickupChecklist/1/save"), HttpMethod.POST, request,
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        assert actualResponse != null;
        assertEquals(actualResponse.getStatusCode(), 200);

        ResponseEntity<BaseResponse> updatedResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/pickupChecklist/1"), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse updatedActualResponse = updatedResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("updatedActualResponse");
        System.out.println(mapper.writeValueAsString(updatedActualResponse));
        //check if response is successful
        BaseResponse expectedResponse = InspectionControllerFTDummyDataStore.getSaveInspectionPickupChecklistResponse();

        Assert.assertEquals(expectedResponse, updatedActualResponse);
    }

}
