package com.revv.uct.supply.functional.Auth;

import com.revv.uct.supply.TestUtils;

public class AuthDummyStore {
    public static String getHappyRequestJSON()
    {
        String jsonPath = "ftData/Auth/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }
}
