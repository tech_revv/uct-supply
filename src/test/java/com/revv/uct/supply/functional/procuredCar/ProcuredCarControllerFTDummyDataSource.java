package com.revv.uct.supply.functional.procuredCar;

import com.revv.uct.supply.TestUtils;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.util.JsonUtil;

public class ProcuredCarControllerFTDummyDataSource {
    public static String getHappySaveProcuredCarDetailsRequestJson() {
        String jsonPath = "ftData/ProcuredCarController/saveProcuredCarDetails/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappySaveProcuredCarDetailsResponse() {
        String jsonResponsePath = "ftData/ProcuredCarController/saveProcuredCarDetails/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getHappyUpdateProcuredCarDetailsRequestJson() {
        String jsonPath = "ftData/ProcuredCarController/update/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyUpdateProcuredCarDetailsResponse() {
        String jsonResponsePath = "ftData/ProcuredCarController/update/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }

    public static String getHappyReportIssueRequestJson() {
        String jsonPath = "ftData/ProcuredCarController/reportIssue/happy_request.json";
        return TestUtils.readTextFile(jsonPath);
    }

    public static BaseResponse getExpectedHappyReportIssueResponse() {
        String jsonResponsePath = "ftData/ProcuredCarController/reportIssue/happy_response.json";
        String jsonResponse = TestUtils.readTextFile(jsonResponsePath);
        return JsonUtil.jsonDecode(jsonResponse, BaseResponse.class);
    }
}
