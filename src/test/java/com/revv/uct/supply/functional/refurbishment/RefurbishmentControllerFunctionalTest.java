package com.revv.uct.supply.functional.refurbishment;

import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.functional.Base;
import com.revv.uct.supply.functional.inspection.InspectionControllerFTDummyDataStore;
import com.revv.uct.supply.functional.lead.LeadControllerFTDummyDataSource;
import io.swagger.models.auth.In;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SupplyApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:ftData/RefurbishmentController/cleanup.sql","classpath:ftData/RefurbishmentController/seed.sql"})
//@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {"classpath:ftData/InspectionController/cleanup.sql"})
@Category(Base.class)
public class RefurbishmentControllerFunctionalTest extends Base {
    private static final String baseControllerURL = "/api/v1/refurb";


    @Test
    @DisplayName("Happy Case Get refurb job List")
    @Tag("API ")
    public void getRefurbJobList() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/jobList/1"), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));

        BaseResponse expectedResponse = RefurbishmentControllerFTDummyDataStore.getExpectedRefurbJobListJSON();

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    @DisplayName("Happy Case Get refurb job List")
    @Tag("API ")
    public void getRefurbJobListExceptionRefurbID() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/jobList/2"), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));

        assert actualResponse != null;
        int statusCode = actualResponse.getStatusCode();
        Assert.assertEquals(statusCode, 400); //invalid refurb ID
    }

    @Test
    @DisplayName("Happy Case Submit refurb documents")
    @Tag("API ")
    public void submitRefurbDocuments() throws Exception {
        String requestJson = RefurbishmentControllerFTDummyDataStore.getSubmitRefurbDocumentsRequestJson();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/1/documents/save"), HttpMethod.POST, new HttpEntity<Object>(requestJson, headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));

        assert actualResponse != null;
        int statusCode = actualResponse.getStatusCode();
        Assert.assertEquals(statusCode, 200);

        ResponseEntity<BaseResponse> newResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/jobList/1"), HttpMethod.GET, new HttpEntity<Object>(headers),
                BaseResponse.class);
        BaseResponse actualNewResponse = newResponseEntity.getBody();
        System.out.println("newResponse");
        System.out.println(mapper.writeValueAsString(actualNewResponse));

        BaseResponse expectedResponse = RefurbishmentControllerFTDummyDataStore.getSubmitRefurbDocumentsResponseJson();

        Assert.assertEquals(expectedResponse, actualNewResponse);
    }

    @Test
    @DisplayName("Exception Case1 Submit refurb documents")
    @Tag("API ")
    public void submitRefurbDocumentsExceptionInvalidRefurbID() throws Exception {
        String requestJson = RefurbishmentControllerFTDummyDataStore.getSubmitRefurbDocumentsRequestJson();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/2/documents/save"), HttpMethod.POST, new HttpEntity<Object>(requestJson, headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));

        assert actualResponse != null;
        int statusCode = actualResponse.getStatusCode();
        Assert.assertEquals(statusCode, 400); //bad request because invalid refurb ID
    }

    @Test
    @DisplayName("Exception Case2 Submit refurb documents")
    @Tag("API ")
    public void submitRefurbDocumentsExceptionInvalidDocumentID() throws Exception {
        String requestJson = RefurbishmentControllerFTDummyDataStore.getSubmitRefurbDocumentsInvalidDocumentIDRequestJson();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/1/documents/save"), HttpMethod.POST, new HttpEntity<Object>(requestJson, headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));

        assert actualResponse != null;
        int statusCode = actualResponse.getStatusCode();
        Assert.assertEquals(statusCode, 400); //bad request because invalid document ID
    }

    @Test
    @DisplayName("Exception Case3 Submit refurb documents")
    @Tag("API ")
    public void submitRefurbDocumentsExceptionDocumentMandatory() throws Exception {
        String requestJson = RefurbishmentControllerFTDummyDataStore.getSubmitRefurbDocumentsDocumentMandatoryRequestJson();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/1/documents/save"), HttpMethod.POST, new HttpEntity<Object>(requestJson, headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));

        assert actualResponse != null;
        int statusCode = actualResponse.getStatusCode();
        Assert.assertEquals(statusCode, 400); //bad request because invalid document ID
    }

    @Test
    @DisplayName("Schedule Refurbishment-Create Happy Test Case")
    @Tag("API ")
    public void testScheduleRefurbishmentHappyCase() throws Exception {
        String requestJson = RefurbishmentControllerFTDummyDataStore.getScheduleRefurbishmentHappyCaseRequestJson();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Token", this.getToken());
        ResponseEntity<BaseResponse> actualResponseEntity = restTemplate.exchange(
                getUrl(baseControllerURL+"/schedule"), HttpMethod.POST, new HttpEntity<Object>(requestJson, headers),
                BaseResponse.class);
        BaseResponse actualResponse = actualResponseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("actualResponse");
        System.out.println(mapper.writeValueAsString(actualResponse));

        assertTrue(actualResponseEntity.getStatusCode().is2xxSuccessful());
        assert actualResponse != null;

        ArrayList t = (ArrayList)actualResponse.getData();

        JSONObject obj = new JSONObject((Map) t.get(0));
        Integer workshopID = (Integer) obj.get("workshopID");
        Integer refurbSpocID = (Integer) obj.get("refurbSpocID");
        Integer procuredCarID = (Integer) obj.get("procuredCarID");

        BaseResponse expectedResponse = RefurbishmentControllerFTDummyDataStore.getScheduleRefurbishmentHappyCaseResponseJson();

        ArrayList t1 = (ArrayList)expectedResponse.getData();
        JSONObject objExpected = new JSONObject((Map) t1.get(0));
        Integer workshopIDExpected = (Integer) objExpected.get("workshopID");
        Integer refurbSpocIDExpected= (Integer) objExpected.get("refurbSpocID");
        Integer procuredCarIDExpected = (Integer) objExpected.get("procuredCarID");


        Assert.assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
        Assert.assertEquals(workshopID, workshopIDExpected);
        Assert.assertEquals(refurbSpocID, refurbSpocIDExpected);
        Assert.assertEquals(procuredCarID, procuredCarIDExpected);

    }


}
