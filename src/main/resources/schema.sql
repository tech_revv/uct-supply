create table bodyparamsmappings
(
    id        int auto_increment
        primary key,
    Header    varchar(255) null,
    Parameter varchar(255) null,
    mongoID   varchar(255) null
);

create table bodyparamsmappingsmongo
(
    _id       text null,
    parameter text null,
    header    text null
);

create table carblocks
(
    id          int auto_increment
        primary key,
    CarID       int                         not null,
    BlockType   enum ('MAINTANENCE', 'BKG') null,
    Description varchar(255)                null,
    BlockStatus enum ('ACTIVE', 'INACTIVE') null,
    Block_st    datetime                    null,
    Block_et    datetime                    null,
    CreatedAt   datetime                    null,
    UpdatedAt   datetime                    null
);

create table cardetails
(
    id                 int                                         not null
        primary key,
    RegistrationNumber varchar(255)                                not null,
    bbgPrice1          decimal(10, 3)                              not null,
    bbgPrice2          decimal(10, 3)                              not null,
    bbgPrice3          decimal(10, 3)                              not null,
    bbgPrice4          decimal(10, 3)                              not null,
    bbgPrice5          decimal(10, 3)                              not null,
    BodyType           varchar(45)                                 not null,
    CarID              varchar(45)                                 not null,
    CarPrice           decimal(10, 3)                              not null,
    Color              varchar(45)                                 null,
    EmiStartingPrice   decimal(10, 3)                              not null,
    Fuel               enum ('PETROL', 'DIESEL')                   null,
    InsuranceValidity  varchar(45)                                 null,
    Make               varchar(45)                                 not null,
    ManufacturingMonth int(2)                                      not null,
    ManufacturingYear  int(4)                                      not null,
    MarketPrice        decimal(10, 3)                              null,
    Model              varchar(45)                                 not null,
    NewCarPrice        decimal(10, 3)                              not null,
    OdoReading         decimal(10, 3)                              null,
    OriginalOdoReading decimal(10, 3)                              null,
    OwnershipSerial    int                                         null,
    QualityScore       decimal(10, 3)                              null,
    RegistrationMonth  int                                         not null,
    RegistrationYear   int                                         not null,
    Rto                varchar(45)                                 not null,
    Status             enum ('ACTIVE', 'INACTIVE', 'SOLD', 'HOLD') not null,
    Tag                varchar(45)                                 null,
    Transmission       enum ('AUTOMATIC', 'MANUAL')                null,
    Variant            varchar(45)                                 null,
    PdfLink            varchar(255)                                null,
    CreatedAt          datetime                                    null,
    UpdatedAt          datetime                                    null,
    mongoID            varchar(255)                                null,
    constraint registrationNumber
        unique (RegistrationNumber)
);

create table cardetailsmongo
(
    _id                text null,
    registrationNumber text null,
    bbgPrice1          int  null,
    bbgPrice2          int  null,
    bbgPrice3          int  null,
    bbgPrice4          int  null,
    bbgPrice5          int  null,
    bodyType           text null,
    carID              int  null,
    carPrice           int  null,
    color              text null,
    createdDate        text null,
    emiStartingPrice   int  null,
    fuel               text null,
    insuranceValidity  text null,
    make               text null,
    manufacturingMonth int  null,
    manufacturingYear  int  null,
    marketPrice        int  null,
    model              text null,
    newCarPrice        int  null,
    odoReading         int  null,
    originalOdoReading int  null,
    ownershipSerial    int  null,
    qualityScore       int  null,
    registrationMonth  int  null,
    registrationYear   int  null,
    rto                text null,
    status             int  null,
    tag                text null,
    transmission       text null,
    updatedDate        text null,
    variant            text null,
    pdfLink            text null,
    idUnique           int auto_increment
        primary key
);

create table carimages
(
    id            int auto_increment
        primary key,
    CarID         int           not null,
    ImageURL      varchar(255)  not null,
    ImageName     varchar(255)  not null,
    ImageCategory varchar(255)  null,
    Serial        int default 0 not null,
    CreatedAt     datetime      null,
    UpdatedAt     datetime      null,
    constraint fk_IMAGE_CAR1
        foreign key (CarID) references cardetails (id)
);

create index fk_IMAGE_CAR1_idx
    on carimages (CarID);

create table carimagesmongo
(
    _id           text null,
    imageURL      text null,
    carID         text null,
    imageCategory text null,
    imageName     text null,
    serial        int  null
);

create table carmodels
(
    id            int auto_increment
        primary key,
    make          varchar(100) null,
    model         varchar(100) null,
    variant       varchar(100) null,
    fuel          varchar(100) null,
    transmission  varchar(100) null,
    bodyType      varchar(100) null,
    seats         int          null,
    startYear     int          null,
    endYear       int          null,
    price         double       null,
    versionNumber int          null,
    isActive      tinyint(1)   null
)
    charset = utf8;

create table cities
(
    id    int auto_increment
        primary key,
    name  varchar(100) not null,
    state varchar(100) null
);

create table customers
(
    id               int auto_increment
        primary key,
    FirstName        varchar(100)                                    null,
    MiddleName       varchar(100)                                    null,
    LastName         varchar(100)                                    null,
    Email            varchar(100)                                    null,
    SecondaryEmail   varchar(100)                                    null,
    Password         varchar(100)                                    null,
    PhoneNumber      varchar(45)                                     not null,
    AlternateNumber  varchar(45)                                     null,
    AddressLine1     varchar(100)                                    null,
    AddressLine2     varchar(100)                                    null,
    HouseNumber      varchar(100)                                    null,
    Location         varchar(100)                                    null,
    Latitude         varchar(100)                                    null,
    Longitude        varchar(100)                                    null,
    City             varchar(100)                                    null,
    State            varchar(100)                                    null,
    Pincode          varchar(100)                                    null,
    FbID             varchar(100)                                    null,
    GooglePlusID     varchar(100)                                    null,
    AppleID          varchar(100)                                    null,
    Status           enum ('ACTIVE', 'INACTIVE', 'PENDING_APPROVAL') null,
    CustomerType     enum ('OWNER', 'CUSTOMER')                      null,
    RegistrationDate datetime default CURRENT_TIMESTAMP              null,
    IsEmailVerified  tinyint(1)                                      null,
    IsPhoneVerified  tinyint(1)                                      null,
    CreatedAt        datetime                                        null,
    updatedAt        datetime                                        null,
    validationLevel  int                                             null,
    createdBy        int                                             null,
    type             int      default 1                              null,
    isVerified       tinyint  default 0                              null
);

create table guest
(
    id          int auto_increment
        primary key,
    phoneNumber varchar(14) null,
    createdAt   datetime    null,
    updatedAt   varchar(45) null
);

create table headers
(
    id            int auto_increment
        primary key,
    name          varchar(100) null,
    versionNumber int          null,
    isActive      tinyint(1)   null
)
    charset = utf8;

create table homescreenbanners
(
    id             int          not null
        primary key,
    itemType       varchar(45)  not null,
    itemText       varchar(255) null,
    headerText1    varchar(45)  null,
    headerText2    varchar(45)  null,
    itemImage      varchar(100) null,
    itemLink       varchar(45)  null,
    active         tinyint      not null,
    showOnHome     tinyint      null,
    positionInList int          null,
    isVideo        tinyint      null
);

create table hubmapsmongo
(
    car_id      text   null,
    model_id    text   null,
    map_st      double null,
    map_et      double null,
    hub_id      text   null,
    createdBy   text   null,
    modifiedBy  text   null,
    createdDate text   null,
    bookingID   text   null,
    segment_id  int    null,
    logs        text   null
);

create table hubs
(
    id                int auto_increment
        primary key,
    Name              varchar(100) not null,
    CityID            int          not null,
    Latitude          double       not null,
    Longitude         double       not null,
    Location          geometry     null,
    Email             varchar(45)  null,
    TestDrivesAllowed int          null,
    constraint fk_CITY_HUB1
        foreign key (CityID) references cities (id)
);

create table fsadetails
(
    id          int                         not null
        primary key,
    FirstName   varchar(45)                 null,
    LastName    varchar(45)                 null,
    Email       varchar(45)                 null,
    PhoneNumber varchar(45)                 not null,
    Status      enum ('ACTIVE', 'INACTIVE') not null,
    AssignedHub int                         not null,
    constraint fk_FSA_HUB1
        foreign key (AssignedHub) references hubs (id)
);

create index fk_FSA_HUB1_idx
    on fsadetails (AssignedHub);

create table hubmaps
(
    id       int auto_increment
        primary key,
    hubID    int        not null,
    carID    int        not null,
    map_st   datetime   not null,
    map_et   datetime   null,
    isActive tinyint(1) not null,
    constraint fk_MAP_CAR1
        foreign key (carID) references cardetails (id),
    constraint fk_MAP_HUB1
        foreign key (hubID) references hubs (id)
);

create index fk_MAP_CAR1_idx
    on hubmaps (carID);

create table inspectionImages
(
    id           int auto_increment
        primary key,
    inspectionID int          not null,
    subPartID    int          not null,
    imageURL     varchar(255) not null,
    isActive     tinyint(1)   null,
    createdBy    int          null,
    updatedBy    int          null,
    createdAt    datetime     null,
    updatedAt    datetime     null
)
    charset = utf8;

create table inspectionJobs
(
    id           int auto_increment
        primary key,
    inspectionID int        not null,
    jobID        int        not null,
    cost         double     not null,
    isActive     tinyint(1) null,
    createdBy    int        null,
    updatedBy    int        null,
    createdAt    datetime   null,
    updatedAt    datetime   null,
    subPartID    int        null
)
    charset = utf8;

create table inspectionReports
(
    id             int auto_increment
        primary key,
    inspectionID   int          not null,
    subPartID      int          not null,
    subPartIssueID int          not null,
    selected       tinyint(1)   null,
    createdBy      int          null,
    createdAt      datetime(6)  null,
    updatedAt      datetime(6)  null,
    isActive       tinyint(1)   null,
    updatedBy      int          null,
    comments       varchar(255) null,
    issueName      varchar(50)  null
)
    charset = utf8;

create table inspectionSubParts
(
    id           int auto_increment
        primary key,
    subPartID    int        not null,
    isActive     tinyint(1) null,
    inspectionID int        not null,
    createdBy    int        null,
    createdAt    datetime   null,
    updatedBy    int        null,
    updatedAt    datetime   null
)
    charset = utf8;

create table inspections
(
    id               int auto_increment
        primary key,
    inspectionType   varchar(100) null,
    leadID           int          not null,
    inspectorID      int          null,
    createdBy        int          null,
    updatedBy        int          null,
    inspectionTime   datetime(6)  null,
    status           varchar(100) null,
    createdAt        datetime(6)  null,
    updatedAt        datetime(6)  null,
    isActive         tinyint      null,
    lastDataSyncTime mediumtext   null,
    verdictRemarks   varchar(255) null,
    refurbJobCost   double default 0,
    pickupDocumentsCount    int default 0,
    refurbID    int default null
)
    charset = utf8;

create table jobs
(
    id            int auto_increment
        primary key,
    jobName       varchar(100) not null,
    versionNumber int(10)      null,
    isActive      tinyint(1)   null
)
    charset = utf8;

create table jobpricemap
(
    id            int auto_increment
        primary key,
    jobID         int          not null,
    bodyType      varchar(100) null,
    model         varchar(100) null,
    workshop      varchar(100) null,
    price         double       not null,
    versionNumber int(10)      null,
    isActive      tinyint(1)   null,
    constraint UniqueKey
        unique (jobID, bodyType, model, workshop, price, isActive)
)
    charset = utf8;

create table parts
(
    id            int auto_increment
        primary key,
    headerName    varchar(100) null,
    headerID      int          null,
    partName      varchar(100) null,
    accessoryType varchar(100) null,
    isAccessory   tinyint      null,
    versionNumber int(10)      null,
    isActive      tinyint(1)   null
)
    charset = utf8;

create index headerID
    on parts (headerID);

create table scoresheets
(
    id                  int auto_increment
        primary key,
    CarID               int          not null,
    Parameter           varchar(255) null,
    BodyParamsMappingID int          not null,
    Comments            varchar(255) null,
    IsCategory          tinyint(1)   null,
    Score               int(3)       null,
    constraint fk_SCORE_BODYPARAM1
        foreign key (BodyParamsMappingID) references bodyparamsmappings (id)
);

create index fk_SCORE_BODYPARAM1_idx
    on scoresheets (BodyParamsMappingID);

create table scoresheetsmongo
(
    _id                 text null,
    carID               int  null,
    parameter           text null,
    bodyParamsMappingID text null,
    comments            text null,
    isCategory          text null,
    score               int  null
);

create table servicecities
(
    id   int auto_increment
        primary key,
    name varchar(100) null,
    state varchar(100) null
)
    charset = utf8;

create table sellers
(
    id                 int auto_increment
        primary key,
    name               varchar(100) null,
    serviceCityID      int          null,
    email              varchar(45)  null,
    contactNumber      varchar(25)  null,
    sellerType         varchar(45)  null,
    spocName           varchar(45)  null,
    bankAccountDetails varchar(45)  null,
    gstNumber          varchar(45)  null,
    panNumber          varchar(45)  null,
    createdAt          datetime     null,
    updatedAt          datetime     null,
    isActive           tinyint      null,
    adminID            int          null,
    address            varchar(255) null,
    latitude           double       null,
    longitude          double       null
)
    charset = utf8;

create table subparts
(
    id            int auto_increment
        primary key,
    partID        int                  null,
    subPartName   varchar(100)         null,
    inputType     varchar(100)         null,
    fileUpload    tinyint              null,
    isMandatory   tinyint              null,
    versionNumber int(10)              null,
    isActive      tinyint(1)           null,
    unAssured     tinyint(1) default 0 null
)
    charset = utf8;

create table subpartissues
(
    id            int auto_increment
        primary key,
    subPartID     int          null,
    issueName     varchar(100) null,
    jobName       varchar(100) null,
    versionNumber int(10)      null,
    isActive      tinyint(1)   null
)
    charset = utf8;

create table issuefollowup
(
    id             int auto_increment
        primary key,
    subPartIssueID int          null,
    jobName        varchar(100) null,
    jobID          int(10)      null,
    versionNumber  int(10)      null,
    isActive       tinyint(1)   null
)
    charset = utf8;

create index jobID
    on issuefollowup (jobID);

create index subPartIssueID
    on issuefollowup (subPartIssueID);

create index subPartID
    on subpartissues (subPartID);

create index partID
    on subparts (partID);

create table taskdetails
(
    id              int auto_increment
        primary key,
    TDBookingID     int                                                                   not null,
    TaskStage       enum ('HUB_IN', 'HUB_OUT', 'DESTINATION_REACHED', 'DESTINATION_LEFT') null,
    Fuel            decimal(11, 2)                                                        null,
    Odo             decimal(11, 2)                                                        null,
    FuelPicLocation varchar(100)                                                          null,
    OdoPicLocation  varchar(100)                                                          null,
    ExteriorVideo   varchar(100)                                                          null,
    Damaged         tinyint(1)                                                            null,
    Latitude        double                                                                null,
    Longitude       double                                                                null,
    CreatedAt       datetime default CURRENT_TIMESTAMP                                    null,
    UpdatedAt       datetime                                                              null
);

create table testing
(
    geom geometry null
);

create table users
(
    id        int auto_increment
        primary key,
    userName  varchar(100) null,
    userEmail varchar(100) null,
    password  varchar(100) null,
    adminType varchar(20)  null,
    timeStamp datetime(6)  null,
    userCity  int          null
)
    charset = utf8;

create table fsablocks
(
    id           int                     not null,
    FsaID        int                     not null,
    BlockType    varchar(45)             not null,
    Block_St     datetime                not null,
    Block_Et     datetime                not null,
    BlockedBy    int                     null,
    IsActive     tinyint                 not null,
    TDBlockedFor enum ('HOME', 'WALKIN') null,
    CreatedAt    datetime                null,
    UpdatedAt    datetime                null,
    constraint id_UNIQUE
        unique (id),
    constraint fk_BLOCK_USER1
        foreign key (BlockedBy) references users (id),
    constraint fk_FSA_BLOCK1
        foreign key (FsaID) references fsadetails (id)
);

create index fk_BLOCK_USER1_idx
    on fsablocks (BlockedBy);

create index fk_FSA_BLOCK1_idx
    on fsablocks (FsaID);

create table history
(
    id           int auto_increment
        primary key,
    editEntityID int         null,
    editEntity   varchar(45) not null,
    adminID      int         not null,
    oldData      longblob    null,
    newData      longblob    null,
    updatedAt    datetime    null,
    constraint history_ibfk_2
        foreign key (adminID) references users (id)
)
    charset = utf8;

create index adminID
    on history (adminID);

create table leads
(
    id                     int auto_increment
        primary key,
    adminID                int          null,
    carModelID             int          null,
    sellerID               int          null,
    registrationNumber     varchar(100) null,
    carColour              varchar(100) null,
    registrationTimestamp  varchar(100) null,
    manufacturingTimestamp varchar(100) null,
    insuranceValidity      datetime     null,
    rto                    varchar(100) null,
    registrationType       varchar(100) null,
    ownershipSerial        varchar(100) null,
    serviceCityID          int          null,
    comments               varchar(100) null,
    isActive               tinyint(1)   null,
    createdAt              datetime     null,
    updatedAt              datetime     null,
    expectedOfferPrice     double       null,
    insuranceType          varchar(45)  null,
    kmsDriven              double       null,
    rejectionReason        varchar(45)  null,
    leadStatus             varchar(45)  null,
    leadRejectSubStatus    varchar(45)  null,
    pickupBy    int(11)  default null,
    handoverDate datetime default null
)
    charset = utf8;

create index adminIDLeads
    on leads (adminID);

create index carModelID
    on leads (carModelID);

create index sellerID
    on leads (sellerID);

create index serviceCityID
    on leads (serviceCityID);

create table testdrives
(
    id                   int auto_increment
        primary key,
    TDScheduledStartTime datetime                                                         not null,
    TDScheduledEndTime   datetime                                                         not null,
    TDStartTime          datetime                                                         null,
    TDEndTime            datetime                                                         null,
    TDBookingTime        datetime                                                         null,
    HubInTime            datetime                                                         null,
    HubOutTime           datetime                                                         null,
    CustomerID           int                                                              not null,
    AssignedFSAID        int                                                              not null,
    BookedBy             int                                                              not null,
    PickUpLocation       varchar(255)                                                     null,
    Latitude             double                                                           not null,
    Longitude            double                                                           not null,
    HubID                int                                                              not null,
    CarID                int                                                              not null,
    Status               enum ('UPCOMING', 'CANCELLED', 'ONGOING', 'MISSED', 'COMPLETED') not null,
    BookingPrice         decimal(11, 2) default 0.00                                      not null,
    TDBookingType        enum ('HOME', 'WALKIN')                                          not null,
    RescheduleCount      int(4)         default 0                                         not null,
    constraint fk_TD_CAR1
        foreign key (CarID) references cardetails (id),
    constraint fk_TD_CUSTOMER1
        foreign key (CustomerID) references customers (id),
    constraint fk_TD_FSA1
        foreign key (AssignedFSAID) references fsadetails (id),
    constraint fk_TD_TSA1
        foreign key (BookedBy) references users (id)
);

create index fk_TD_CAR1_idx
    on testdrives (CarID);

create index fk_TD_CUSTOMER1_idx
    on testdrives (CustomerID);

create index fk_TD_FSA1_idx
    on testdrives (AssignedFSAID);

create index fk_TD_TSA1_idx
    on testdrives (BookedBy);

create table versions
(
    id            int auto_increment
        primary key,
    versionType   varchar(100) null,
    versionNumber int          null,
    enabled       tinyint(1)   null
)
    charset = utf8;

CREATE TABLE offers (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  inspectionID int(11) DEFAULT NULL,
  leadID int(11) DEFAULT NULL,
  challanStatus varchar(100) DEFAULT NULL,
  serviceHistoryStatus varchar(100) DEFAULT NULL,
  offeredPrice double DEFAULT NULL,
  basePrice double DEFAULT NULL,
  refubCost double DEFAULT NULL,
  challanCost double DEFAULT NULL,
  adjustCost double DEFAULT NULL,
  noOfDocuments int(4) DEFAULT NULL,
  createdBy int(11) DEFAULT NULL,
  createdAt datetime DEFAULT NULL,
  updatedBy int(11) DEFAULT NULL,
  updatedAt datetime DEFAULT NULL,
  isActive tinyint(1) DEFAULT NULL,
  offerStatus varchar(100) DEFAULT NULL,
  isRevised tinyint(1) DEFAULT NULL,
  offerReviseStage varchar(100) DEFAULT NULL,
  refubCostE2 int(11) DEFAULT NULL,
  CONSTRAINT fk_TD_INSPECTION1 FOREIGN KEY (inspectionID) REFERENCES inspections (id),
  CONSTRAINT fk_TD_LEAD1 FOREIGN KEY (leadID) REFERENCES leads (id),
  CONSTRAINT fk_TD_USER2 FOREIGN KEY (createdBy) REFERENCES users (id),
  CONSTRAINT fk_TD_USER3 FOREIGN KEY (updatedBy) REFERENCES users (id)
) CHARSET=utf8;

create index fk_TD_INSPECTION1_idx
    on offers (inspectionID);
create index fk_TD_LEAD1_idx
    on offers (leadID);
create index fk_TD_USER2_idx
    on offers (createdBy);
create index fk_TD_USER3_idx
    on offers (updatedBy);

CREATE TABLE offerimages (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  offerID int(11) DEFAULT NULL,
  imageURL varchar(100) DEFAULT NULL,
  createdBy int(11) DEFAULT NULL,
  createdAt datetime DEFAULT NULL,
  updatedBy int(11) DEFAULT NULL,
  updatedAt datetime DEFAULT NULL,
  isActive tinyint(1) DEFAULT NULL,
  CONSTRAINT fk_TD_OFFER1 FOREIGN KEY (offerID) REFERENCES offers (id),
  CONSTRAINT fk_TD_USER4 FOREIGN KEY (createdBy) REFERENCES users (id)
) DEFAULT CHARSET=utf8;

create index fk_TD_OFFER1_idx
    on offerimages (offerID);
create index fk_TD_USER4_idx
    on offerimages (createdBy);

create table if not exists documentStageMasters
(
    id int auto_increment
        primary key,
    documentName varchar(100) not null,
    documentType varchar(100) not null,
    documentStage varchar(100) not null,
    isMandatory tinyint(1) not null,
    isActive tinyint(1) not null
)
    charset=utf8;

create table commonDocuments
(
    id                    int auto_increment
        primary key,
    commonDocumentEntity  varchar(100) null,
    entityID              int          null,
    documentStageMasterID int          null,
    imageURL              varchar(255) null,
    createdBy             int          null,
    createdAt             datetime     null,
    updatedBy             int          null,
    updatedAt             datetime     null,
    isActive              tinyint(1)   null
)
    charset = utf8;

create index createdBy
    on commonDocuments (createdBy);

create index documentStageMasterID
    on commonDocuments (documentStageMasterID);



create table procuredCars
(
    id            int auto_increment
        primary key,
    carModelID    int         not null,
    leadID        int         not null,
    offerID       int         null,
    onBoardedDate datetime(6) null,
    pickUpBy      int         null,
    isActive      tinyint(1)  not null,
    createdAt     datetime(6) null,
    updatedAt     datetime(6) null,
    createdBy     int         not null,
    updatedBy     int         null,
    status        varchar(50) not null,
    isIssueReported tinyint(1) null,
    issue         varchar(50)  null,
    issueStage    varchar(50) null
)
    charset = utf8;

create index procuredCars_carModelID
    on procuredCars (carModelID);

create index procuredCars_isActive
    on procuredCars (isActive);

create index procuredCars_leadID
    on procuredCars (leadID);


create table refurbishmentJobs
(
    id          int auto_increment
        primary key,
    refurbID    int          not null,
    subpartID   int          null,
    jobID       int          not null,
    createdAt   datetime(6)  null,
    updatedAt   datetime(6)  null,
    createdBy   int          not null,
    updatedBy   int          null,
    isActive    tinyint(1)   not null,
    selected    tinyint(1)   not null,
    subPartName varchar(100) not null,
    jobName     varchar(100) not null
)
    charset = utf8;

create index refurbishmentJobs_refurbID
    on refurbishmentJobs (refurbID);

create table refurbishments
(
    id               int auto_increment
        primary key,
    workshopID       int         not null,
    refurbSpocID     int         null,
    procuredCarID    int         not null,
    startDate        datetime(6) not null,
    endDate          datetime(6) not null,
    status           varchar(50) not null,
    isActive         tinyint(1)  not null,
    createdAt        datetime(6) null,
    updatedAt        datetime(6) null,
    createdBy        int         not null,
    updatedBy        int         null,
    lastDataSyncTime mediumtext  null
)
    charset = utf8;

create index refurbishments_procuredCarID
    on refurbishments (procuredCarID);

create index refurbishments_status
    on refurbishments (status);

create index refurbishments_workshopID
    on refurbishments (workshopID);


create table workshops
(
    id           int auto_increment
        primary key,
    workshopName varchar(100) not null,
    address      varchar(100) null,
    isActive     tinyint(1)   not null,
    createdAt    datetime(6)  null,
    updatedAt    datetime(6)  null,
    createdBy    int          not null,
    updatedBy    int          null
)
    charset = utf8;

create index workshops_isActive
    on workshops (isActive);

create index workshops_workshopName
    on workshops (workshopName);