package com.revv.uct.supply.service.jobs.impl;

import com.google.gson.JsonObject;
import com.revv.uct.supply.constants.APIConstants;
import com.revv.uct.supply.db.jobPriceMap.entity.JobPriceMap;
import com.revv.uct.supply.db.jobs.dao.IJobsDao;
import com.revv.uct.supply.db.jobs.entity.Jobs;
import com.revv.uct.supply.db.version.entity.Version;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.jobPriceMap.IJobPriceMapService;
import com.revv.uct.supply.service.jobPriceMap.model.CheckJobPriceMap;
import com.revv.uct.supply.service.jobs.IJobsService;
import com.revv.uct.supply.service.version.IVersionService;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Slf4j
public class JobsServiceImpl implements IJobsService {
    private final IJobsDao jobsDao;
    private final IVersionService versionService;
    private final IJobPriceMapService jobPriceMapService;

    @Autowired
    public JobsServiceImpl(IJobsDao jobsDao, IVersionService versionService, IJobPriceMapService jobPriceMapService) {
        this.jobsDao = jobsDao;
        this.versionService = versionService;
        this.jobPriceMapService = jobPriceMapService;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Jobs> uploadJobs(List<Object> list) throws EntityNotFoundException, SupplyException {
        //List<CheckJobPriceMap> jobPriceMaps = (List<CheckJobPriceMap> ) jobPriceMapService.getAllByIsActive(true);
        List<JobPriceMap> jobPriceMaps = jobPriceMapService.findAllByIsActive(true);
        log.info("jobPriceMaps==>", jobPriceMaps.toString());

        //if price is less than 0 or job name is null throw exception
        Iterator checkJobs = list.iterator();
        while (checkJobs.hasNext()) {
            JSONObject obj = new JSONObject((Map) checkJobs.next());
            Double price = Double.valueOf( obj.get("price")+"");
            String jobName = (String) obj.get("jobName");

            if(price < 0 || price == null) {
                throw new EntityNotFoundException("Price is mandatory");
            }
            if(jobName.length() <= 0 || jobName == null) {
                throw new EntityNotFoundException("Job name is mandatory");
            }


//            Jobs job = jobsDao.findByJobNameAndIsActive(jobName, true);
//            if(job != null) {
//                for(JobPriceMap existing : jobPriceMaps) {
//                    CheckJobPriceMap check = jobPriceMapService.getCheckJobPriceMapFromrequest(job.getId(), obj);
//
//                    boolean checkDuplicate = checkDuplicateInJobPriceMap(existing, check);
//
//                    if(checkDuplicate) {
//                        throw new SupplyException("JobPriceMap data already exist for jobID: "+String.valueOf(job.getId())+" and jobPriceMap: "+String.valueOf(existing.getBodyType()+", "+existing.getModel()+", "+existing.getWorkshop()+", "+existing.getPrice()));
//                    }
//
//                    /*
//                    boolean a = o.getBodyType().equals( check.getBodyType());
//                    boolean b = false;
//                    if(o.getModel() != null) {
//                        b = o.getModel().equals (check.getModel());
//                    }
//                    boolean c = false;
//                    if(o.getWorkshop() != null) {
//                        c = o.getWorkshop().equals(check.getWorkshop());
//                    }
//                    boolean d = o.getPrice().equals (check.getPrice());
//                    if(a && b && c && d) {
//                        throw new SupplyException("JobPriceMap data already exist for: "+String.valueOf(o.getBodyType()+", "+o.getModel()+", "+o.getWorkshop()+", "+o.getPrice()));
//                    }
//                     */
//                }
//
//            }

        }

        //==jobs table
        //get current version
        int version = versionService.getCurrentVersion("Jobs");
        int newVersion = version + 1;
        //disable current version
        versionService.disableCurrentVersion("Jobs", version);
        //add new version
        Version addedVersion = versionService.addNewVersion("Jobs", newVersion, true);

        //==job price map table
        int jobPriceMapVersion = versionService.getCurrentVersion("JobPriceMap");
        int newJobPriceMapVersion = jobPriceMapVersion + 1;
        //disable current version
        versionService.disableCurrentVersion("JobPriceMap", jobPriceMapVersion);
        //add new version
        Version addedJobPriceMapVersion = versionService.addNewVersion("JobPriceMap", newJobPriceMapVersion, true);

        //upload new jobs
        List<Jobs> result = new ArrayList<>();
        Iterator<Object> it = list.iterator();
        HashMap<String, Long> check = new HashMap<>();
        HashMap<String, JobPriceMap> checkJPM = new HashMap<>();
        while (it.hasNext()) {
            JSONObject jsonObject = new JSONObject((Map) it.next());
            Jobs existedJob = jobsDao.findByJobNameAndIsActive((String)jsonObject.get("jobName"), true);

            if(existedJob != null || check.containsKey(jsonObject.get("jobName"))) {
                Long id = existedJob != null ? existedJob.getId() : check.get(jsonObject.get("jobName"));

                for(JobPriceMap existing : jobPriceMaps) {
                    boolean checkDuplicate = checkDuplicateInJobPriceMap(existing, jobPriceMapService.getCheckJobPriceMapFromrequest(id, jsonObject));
                    if(checkDuplicate) {
                        throw new SupplyException("JobPriceMap data already exist for jobID: "+String.valueOf(id+" and jobPriceMap: "+String.valueOf(existing.getBodyType()+", "+existing.getModel()+", "+existing.getWorkshop()+", "+existing.getPrice())));
                    }
                }

                if(checkJPM.containsKey(jsonObject.get("jobName"))) {
                    JobPriceMap entered = (JobPriceMap) checkJPM.get(jsonObject.get("jobName"));
                    log.info("entered==>>", entered.toString());
                    boolean checkDuplicate = checkDuplicateInJobPriceMap(entered, jobPriceMapService.getCheckJobPriceMapFromrequest(id, jsonObject));
                    if(checkDuplicate) {
                        throw new SupplyException("JobPriceMap data already exist for jobID: "+String.valueOf(id+" and jobPriceMap: "+String.valueOf(entered.getBodyType()+", "+entered.getModel()+", "+entered.getWorkshop()+", "+entered.getPrice())));
                    }
                }

                JobPriceMap jobPriceMap = jobPriceMapService.getJobPriceMapFromRequest(jsonObject, id, newJobPriceMapVersion);
                JobPriceMap savedJobPriceMap = jobPriceMapService.save(jobPriceMap);
            }
            else {
                Jobs job = getJobFromRequest(jsonObject, newVersion);
                Jobs savedJob = jobsDao.uploadJob(job);
                System.out.println("asvedJob==>>"+savedJob.toString());

                check.put((String) jsonObject.get("jobName"), savedJob.getId());

                JobPriceMap jobPriceMap = jobPriceMapService.getJobPriceMapFromRequest(jsonObject, savedJob.getId(), newJobPriceMapVersion);
                JobPriceMap savedJobPriceMap = jobPriceMapService.save(jobPriceMap);
                System.out.println("savedJobPriceMap==>>"+savedJobPriceMap.toString());

                checkJPM.put((String) jsonObject.get("jobName"), savedJobPriceMap);

                result.add(savedJob);
            }
        }
        return result;
    }

    private boolean checkDuplicateInJobPriceMap(JobPriceMap existing, CheckJobPriceMap check) {
        System.out.println("existing==>>"+String.valueOf(existing));
        System.out.println("check==>>"+String.valueOf(check));
        boolean a = false;
        if ((existing.getBodyType() != null && check.getBodyType() != null)) {
            a = existing.getBodyType().equals( check.getBodyType());
        } else if(existing.getBodyType() == null && check.getBodyType() == null) {
            a = true;
        }
        System.out.println("a==>>"+String.valueOf(a));

        boolean b = false;
        if(existing.getModel() != null && check.getModel() != null) {
            b = existing.getModel().equals (check.getModel());
        } else if(existing.getModel() == null && check.getModel() == null) {
            b= true;
        }
        System.out.println("b==>>"+String.valueOf(b));

        boolean c = false;
        if(existing.getWorkshop() != null && check.getWorkshop() != null) {
            c = existing.getWorkshop().equals(check.getWorkshop());
        } else if (existing.getWorkshop() == null && check.getWorkshop() == null) {
            c = true;
        }
        System.out.println("c==>>"+String.valueOf(c));

        boolean d = Double.valueOf(existing.getPrice()).equals(Double.valueOf(check.getPrice()));
        System.out.println("d==>>"+String.valueOf(d));

        boolean e = existing.getJobID().equals(check.getJobID());
        System.out.println("eJobID==>>"+String.valueOf(existing.getJobID()));
        System.out.println("cJobID==>>"+String.valueOf(check.getJobID()));
        System.out.println("e==>>"+String.valueOf(e));

        if(a && b && c && d && e) {
            return true;
        }
        return false;
    }

    @Override
    public boolean existsByJobNameAndIsActive(String jobName, boolean isActive) {
        if(jobsDao.existsByJobNameAndIsActive(jobName, isActive)) {
            return true;
        }
        return false;
    }

    @Override
    public Jobs findByJobNameAndIsActive(String jobName, boolean b) {
        Jobs job = jobsDao.findByJobNameAndIsActive(jobName, b);
        return job;
    }

    @Override
    public List<Jobs> getDefaultJobs() {
        return jobsDao.findByJobID(APIConstants.DEFAULT_JOB_ID);
    }

    @Override
    public List<Jobs> getAllJobsByIDs(List<Long> jobIDs) {
        return jobsDao.findAllByIdAndIsActive(jobIDs);
    }

    private Jobs getJobFromRequest(JSONObject jsonObject, int newVersion) {
        return Jobs.builder()
                .jobName((String) jsonObject.get("jobName"))
                .versionNumber(newVersion)
                .isActive(true)
                .build();
    }
}
