package com.revv.uct.supply.service.header;

import com.revv.uct.supply.db.header.entity.Header;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;

import java.util.List;

public interface IHeaderService {
    List<Header> uploadNewHeaders(List<Object> list) throws SupplyException, EntityNotFoundException;

    boolean existsByHeaderNameAndIsActive(String headerName, boolean isActive);

    boolean existsByHeaderNameAndVersionNumber(String headerName, int headerVersion);

    Header findByHeaderNameAndIsActive(String headerName, boolean isActive);

    //List<Header> uploadHeaders(ValidList<Header> list);
}
