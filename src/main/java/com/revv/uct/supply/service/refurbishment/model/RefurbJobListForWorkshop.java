package com.revv.uct.supply.service.refurbishment.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RefurbJobListForWorkshop {
    private String subPartName;
    private Long subPartID;
    private List<SubPartIssue> issues;
    private List<Job> jobs;
}

