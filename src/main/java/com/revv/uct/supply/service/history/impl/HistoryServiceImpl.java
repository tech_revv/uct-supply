package com.revv.uct.supply.service.history.impl;

import com.revv.uct.supply.db.enums.EditEntity;
import com.revv.uct.supply.db.history.dao.IHistoryDao;
import com.revv.uct.supply.db.history.entity.History;
import com.revv.uct.supply.service.history.IHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class HistoryServiceImpl implements IHistoryService {
    private final IHistoryDao historyDao;

    @Autowired
    public HistoryServiceImpl(IHistoryDao leadHistoryDao) {
        this.historyDao = leadHistoryDao;
    }

    @Override
    public History saveHistory(Long adminID, Long editEntityID, EditEntity entity, String oldDataJson, String newDataJson) {
        log.info("oldDataJson ={}", oldDataJson);
        log.info("newDataJson ={}", newDataJson);
        return historyDao.save(getLeadHistoryDataFromRequest(adminID, editEntityID, entity, oldDataJson, newDataJson));
    }

    @Override
    public History getLastHistoricalData(Long entityID, EditEntity entity) {
        List<History> historyList = historyDao.getHistoricalData(entityID, entity);
        if(historyList.size() > 0) {
            return historyList.get(historyList.size()-1);
        }
        return null;
    }

    private History getLeadHistoryDataFromRequest(Long adminID, Long editEntityID, EditEntity entity, String oldDataJson, String newDataJson) {
        return History.builder()
                .adminID(adminID)
                .editEntityID(editEntityID)
                .editEntity(EditEntity.valueOf(String.valueOf(entity)))
                .oldData(oldDataJson)
                .newData(newDataJson)
                .updatedAt(LocalDateTime.now())
                .build();
    }
}
