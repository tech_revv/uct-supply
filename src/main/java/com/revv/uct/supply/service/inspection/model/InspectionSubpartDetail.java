package com.revv.uct.supply.service.inspection.model;

import com.revv.uct.supply.db.enums.AccessoryType;
import com.revv.uct.supply.db.enums.SubPartInputType;
import lombok.Data;

@Data
public class InspectionSubpartDetail {
    private Long inspectionID;
    private Long subPartID;
    private String subPartName;
    private SubPartInputType subPartInputType;
    private Boolean fileUploadRequired;
    private Boolean isMandatory;
    private Boolean unAssured;
    private Long partID;
    private String partName;
    private Boolean isAccessory;
    private AccessoryType accessoryType;
    private Long headerID;
    private String headerName;
    private Long issueID;
    private String issueName;
    private Long jobID;
    private String jobName;

    public InspectionSubpartDetail(Long inspectionID, Long subPartID, String subPartName,
                                   SubPartInputType subPartInputType, Boolean fileUploadRequired, Boolean isMandatory, Boolean unAssured,
                                   Long partID, String partName, Boolean isAccessory, AccessoryType accessoryType,
                                   Long headerID, String headerName, Long issueID, String issueName, Long jobID,
                                   String jobName) {
        this.inspectionID = inspectionID;
        this.subPartID = subPartID;
        this.subPartName = subPartName;
        this.subPartInputType = subPartInputType;
        this.fileUploadRequired = fileUploadRequired;
        this.isMandatory = isMandatory;
        this.unAssured = unAssured;
        this.partID = partID;
        this.partName = partName;
        this.isAccessory = isAccessory;
        this.accessoryType = accessoryType;
        this.headerID = headerID;
        this.headerName = headerName;
        this.issueID = issueID;
        this.issueName = issueName;
        this.jobID = jobID;
        this.jobName = jobName;
    }

}
