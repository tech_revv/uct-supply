package com.revv.uct.supply.service.inspection.model.inspectionDetail;

import com.revv.uct.supply.db.enums.InspectionCardType;
import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.Transmission;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
public class RefurbList extends InspectionDetail{

    private String sellerAddress;

    //Refurbishment list constructor
    public RefurbList(
            Long refurbID, InspectionStatus status, LocalDateTime refurbDate, Long insertTS,
            String workshopName, String workshopAddress,
            String carMake, String carModel, String carVariant, String carFuel, Transmission carTransmission, int startYear, int endYear) {
        this.id = refurbID;
        this.inspectionIDDisplay = "ID-"+refurbID;
        this.inspectionType = "R";
        this.inspectionTime = refurbDate.plusMinutes(330);
        this.inspectionTimeDisplay = refurbDate.plusMinutes(330).format(formatter);
        this.status = status;
        this.insertTS = insertTS != null ? insertTS : 0L;
        this.sellerAddress = workshopName +"-"+workshopAddress;
        this.carMake = carMake;
        this.carModel = carModel;
        this.carVariant = carVariant;
        this.carFuel = carFuel;
        this.carTransmission = carTransmission;
        this.carYear = ""+startYear+"-"+endYear;
        this.cardType = InspectionCardType.REFURBISHMENT;
    }
}
