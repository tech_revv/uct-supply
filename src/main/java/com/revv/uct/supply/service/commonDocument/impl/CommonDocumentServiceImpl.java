package com.revv.uct.supply.service.commonDocument.impl;

import com.revv.uct.supply.db.commonDocument.dao.ICommonDocumentDao;
import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.documentStageMaster.entity.DocumentStageMaster;
import com.revv.uct.supply.db.enums.CommonDocumentEntity;
import com.revv.uct.supply.db.enums.DocumentStage;
import com.revv.uct.supply.service.commonDocument.ICommonDocumentService;
import com.revv.uct.supply.service.documentStageMaster.IDocumentStageMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommonDocumentServiceImpl implements ICommonDocumentService {
    private final ICommonDocumentDao commonDocumentDao;
    private final IDocumentStageMasterService documentStageMasterService;

    @Autowired
    public CommonDocumentServiceImpl(ICommonDocumentDao commonDocumentDao, IDocumentStageMasterService documentStageMasterService) {
        this.commonDocumentDao = commonDocumentDao;
        this.documentStageMasterService = documentStageMasterService;
    }

    @Override
    public void saveDocuments(List<CommonDocument> commonDocuments) {
        commonDocumentDao.saveAll(commonDocuments);
    }

    @Override
    public void markDocumentsInactive(CommonDocumentEntity entityName, Long entityID, Long adminID, List<Long> documentIDs) {
        commonDocumentDao.markDocumentsInactive(entityName, entityID, adminID, documentIDs);
    }

    @Override
    public List<CommonDocument> findAllByEntityIDAndIsActive(Long existingOfferID, boolean b) {
        return commonDocumentDao.findAllByEntityIDAndIsActive(existingOfferID, b);
    }

    @Override
    public List<String> getDocuments(Long entityID) {
        List<CommonDocument> commonDocuments = findAllByEntityIDAndIsActive(entityID, true);
        List<String> documents = new ArrayList<>();
        if(commonDocuments != null) {
            for(CommonDocument doc : commonDocuments) {
                documents.add(doc.getImageURL());
            }
        }
        return documents;
    }

    @Override
    public List<CommonDocument> getDocumentsForEntityIDAndName(Long entityID, CommonDocumentEntity entityName) {
        return commonDocumentDao.getDocumentsForEntityIDAndNameAndIsActive(entityID, entityName, true);
    }

    @Override
    public List<String> getDocuments(Long entityID, Long dsmID, CommonDocumentEntity entity) {
        List<CommonDocument> commonDocuments = findAllByEntityIDAndDocumentStageMasterIDAndCommonDocumentEntityAndIsActive(entityID, dsmID, entity, true);
        List<String> documents = new ArrayList<>();
        if(commonDocuments != null) {
            for(CommonDocument doc : commonDocuments) {
                documents.add(doc.getImageURL());
            }
        }
        return documents;
    }

    private List<CommonDocument> findAllByEntityIDAndDocumentStageMasterIDAndCommonDocumentEntityAndIsActive(Long entityID, Long dsmID, CommonDocumentEntity entity, boolean b) {
        return commonDocumentDao.findAllByEntityIDAndDocumentStageMasterIDAndCommonDocumentEntityAndIsActive(entityID, dsmID, entity, b);
    }

    @Override
    public List<CommonDocument> getDocumentsForEntityIDAndNameAndStage(Long entityID, CommonDocumentEntity entityName, List<Long> documentStageMasterIDs) {
        return commonDocumentDao.getDocumentsForEntityIDAndNameAndStage(entityID, entityName, documentStageMasterIDs, true);
    }

    @Override
    public List<String> getDocumentsByDSMStage(Long entityID, DocumentStage documentStage, CommonDocumentEntity entity) {

        List<DocumentStageMaster> documentStageMasters = documentStageMasterService.getStageDocuments(documentStage);
        List<Long> documentStageMasterIDs = documentStageMasters.stream().map(DocumentStageMaster::getId).collect(Collectors.toList());
        List<CommonDocument> commonDocuments = commonDocumentDao.getDocumentsForEntityIDAndNameAndStage(entityID, entity, documentStageMasterIDs, true);

        List<String> documents = new ArrayList<>();
        if(commonDocuments != null) {
            for(CommonDocument doc : commonDocuments) {
                documents.add(doc.getImageURL());
            }
        }
        return documents;
    }

    @Override
    public List<CommonDocument> getCommonDocumentData(Long entityID, long dsmID, CommonDocumentEntity entity) {
        List<CommonDocument> commonDocuments = findAllByEntityIDAndDocumentStageMasterIDAndCommonDocumentEntityAndIsActive(entityID, dsmID, entity, true);
        return commonDocuments;
    }

    @Override
    public void markDocumentsInactiveAgainstDocumentStageMaster(CommonDocumentEntity entityName, Long entityID, Long adminID, List<Long> documentStageMasterIDs) {
        commonDocumentDao.markDocumentsInactiveAgainstDocumentStageMaster(entityName, entityID, adminID, documentStageMasterIDs);
    }

}
