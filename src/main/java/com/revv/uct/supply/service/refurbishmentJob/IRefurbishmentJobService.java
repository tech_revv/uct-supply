package com.revv.uct.supply.service.refurbishmentJob;


import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;

import java.util.List;

public interface IRefurbishmentJobService {
    List<RefurbishmentJob> getRefurbJobList(Long refurbID);

    void updateRefurbJobs(List<RefurbishmentJob> refurbishmentJobs);

    List<RefurbishmentJob> getRefurbJobList(List<Long> refurbIDs);

    List<RefurbishmentJob> getCompletedRefurbJobListByCarID(Long carID);

}
