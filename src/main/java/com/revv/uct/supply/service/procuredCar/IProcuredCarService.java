package com.revv.uct.supply.service.procuredCar;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.controller.procuredCar.model.ReportIssueRequest;
import com.revv.uct.supply.controller.procuredCar.model.SaveProcuredCarRequest;
import com.revv.uct.supply.controller.procuredCar.model.UpdateProcuredCarRequest;
import com.revv.uct.supply.db.enums.ProcuredCarStatus;
import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;
import com.revv.uct.supply.exceptions.SupplyException;

public interface IProcuredCarService {
    ProcuredCar saveProcuredCarDetails(SaveProcuredCarRequest request);

    ProcuredCar updateProcuredCarDetails(UpdateProcuredCarRequest request) throws JsonProcessingException;

    ProcuredCar reportIssue(ReportIssueRequest request) throws JsonProcessingException, SupplyException;

    ProcuredCar getProcuredCarDetails(Long leadID);

    void updateProcuredCarStatusFromLeadID(Long leadID, Long updatedBy, ProcuredCarStatus status) throws SupplyException, JsonProcessingException;
}
