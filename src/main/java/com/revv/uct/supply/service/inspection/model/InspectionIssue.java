package com.revv.uct.supply.service.inspection.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InspectionIssue {
    private String issueName;
    private Long issueID;
    @Builder.Default
    private Boolean selected = false;
    private int issueType;
    @Builder.Default
    private List<InspectionSubIssue> subIssues = new ArrayList<>();
}
