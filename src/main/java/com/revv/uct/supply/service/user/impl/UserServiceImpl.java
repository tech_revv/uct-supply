package com.revv.uct.supply.service.user.impl;

import com.revv.uct.supply.db.enums.AdminType;
import com.revv.uct.supply.db.user.dao.IUserDao;
import com.revv.uct.supply.db.user.entity.User;
import com.revv.uct.supply.service.user.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements IUserService {
    private final IUserDao userDao;

    @Autowired
    public UserServiceImpl(IUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<User> getAdmins(AdminType adminType) {
        return userDao.getAdmins(adminType);
    }

    @Override
    public boolean existById(Long inspectorID) {
        return userDao.existById(inspectorID);
    }

    @Override
    public List<User> getAdminsByCity(AdminType adminType, Long cityID) {
        return userDao.getAdminsByCity(adminType, cityID);
    }

    @Override
    public List<User> findAllByID(List<Long> userIDs) {
        return userDao.findAllByIDAndIsActive(userIDs, true);
    }
}
