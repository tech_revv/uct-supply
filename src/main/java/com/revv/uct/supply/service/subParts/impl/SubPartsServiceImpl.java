package com.revv.uct.supply.service.subParts.impl;

import com.revv.uct.supply.db.enums.SubPartInputType;
import com.revv.uct.supply.db.parts.entity.Parts;
import com.revv.uct.supply.db.subParts.dao.ISubPartsDao;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.db.version.entity.Version;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.parts.IPartsService;
import com.revv.uct.supply.service.subParts.ISubPartsService;
import com.revv.uct.supply.service.version.IVersionService;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SubPartsServiceImpl implements ISubPartsService {
    private final ISubPartsDao subPartsDao;
    private final IVersionService versionService;
    private final IPartsService partsService;

    @Autowired
    public SubPartsServiceImpl(ISubPartsDao subPartsDao, IVersionService versionService, IPartsService partsService) {
        this.subPartsDao = subPartsDao;
        this.versionService = versionService;
        this.partsService = partsService;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<SubParts> uploadSubParts(List<Object> list) throws SupplyException, EntityNotFoundException {
        Iterator<Object> checkPart = list.iterator();
        while (checkPart.hasNext()){
            JSONObject obj = new JSONObject((Map) checkPart.next());
            if(!partsService.existsByPartNameAndIsActive((String) obj.get("partName"), true)){
                throw new SupplyException("Part data doesn't exist for partName: "+(String) obj.get("partName"));
            }
            String subPartName = (String) obj.get("subPartName");
            if(subPartName == null || subPartName.length() <= 0) {
                throw new EntityNotFoundException("Sub part name is mandatory");
            }
            if(existsBySubPartNameAndIsActive(subPartName, true)) {
                throw new SupplyException("Data already exist for subPartName: "+(String) obj.get("subPartName"));
            }
        }
        //get current version
        int version = versionService.getCurrentVersion("SubParts");
        log.info("version==>>"+String.valueOf(version));

        int newVersion = version + 1;

        //disable current version
        versionService.disableCurrentVersion("SubParts", version);

        //add new version
        Version addedVersion = versionService.addNewVersion("SubParts", newVersion, true);
        log.info("addedVersion==>>"+addedVersion.toString());

        //upload new sub parts
        List<SubParts> result = new ArrayList<>();
        Iterator<Object> it =list.iterator();
        while (it.hasNext()) {
            JSONObject jsonObject = new JSONObject((Map) it.next());
            Parts part = partsService.findByPartNameAndIsActive((String)jsonObject.get("partName"), true);
            log.info("part==>>"+part.toString());

            SubParts subPart = getSubPartFromRequest(jsonObject, newVersion, part.getId());
            log.info("subPart==>>"+subPart.toString());

            SubParts savedSubPart = subPartsDao.uploadSubPart(subPart);
            log.info("savedSubPart==>>"+savedSubPart.toString());
            result.add(savedSubPart);

        }
        return result;
    }

    @Override
    public boolean existsBySubPartNameAndIsActive(String subPartName, boolean isActive) {
        if(subPartsDao.existsBySubPartNameAndIsActive(subPartName, isActive)) {
            return true;
        }
        return false;
    }

    @Override
    public SubParts findBySubPartNameAndIsActive(String subPartName, boolean b) {
        SubParts subPart = subPartsDao.findBySubPartNameAndIsActive(subPartName, b);
        return subPart;
    }

    @Override
    public List<SubParts> findAllByIsActive(boolean b) {
        return subPartsDao.findAllByIsActive(b);
    }

    @Override
    public List<SubParts> findAllByID(List<Long> subPartIds) {
        return subPartsDao.findAllByID(subPartIds);
    }

    private SubParts getSubPartFromRequest(JSONObject jsonObject, int newVersion, Long partID) {
        return SubParts.builder()
                .partID(partID)
                .subPartName((String) jsonObject.get("subPartName"))
                .subPartInputType(SubPartInputType.valueOf((String) jsonObject.get("subPartInputType")))
                .fileUpload((boolean) jsonObject.get("fileUpload"))
                .isMandatory((boolean) jsonObject.get("isMandatory"))
                .versionNumber(newVersion)
                .isActive(true)
                .unAssured((Boolean) jsonObject.get("unAssured"))
                .build();
    }
}
