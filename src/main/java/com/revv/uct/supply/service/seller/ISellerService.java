package com.revv.uct.supply.service.seller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.controller.seller.model.SellerCreationData;
import com.revv.uct.supply.controller.seller.model.UpdateSellerRequest;
import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.seller.entity.Seller;
import com.revv.uct.supply.controller.seller.model.CreateSellerRequest;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface ISellerService
{
    /**
     * Creates a Seller
     * @param request
     * @return
     */
    Seller createSeller(@NotNull @Valid final CreateSellerRequest request) throws NullPointerException, UCTSupplyException;

    List<Object> getSellersForLead(Long serviceCityID, SellerTypeEnum sellerType);

    List<SellerCreationData> getSellerCreationData();

    List<Seller> getSellersByNameAndServiceCity(Long serviceCityID, String name);

    Seller updateSeller(UpdateSellerRequest request) throws SupplyException, JsonProcessingException;

    Seller createSellerForB2CLeads(CreateLeadRequest request);

    List<Seller> findAllByContactNumberAndType(String sellerContactNo);
}
