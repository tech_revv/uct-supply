package com.revv.uct.supply.service.user;

import com.revv.uct.supply.db.enums.AdminType;
import com.revv.uct.supply.db.user.entity.User;

import java.util.List;

public interface IUserService {
    List<User> getAdmins(AdminType adminType);

    boolean existById(Long inspectorID);

    List<User> getAdminsByCity(AdminType adminType, Long cityID);

    List<User> findAllByID(List<Long> userIDs);
}
