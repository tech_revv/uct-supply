package com.revv.uct.supply.service.refurbishment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Job {
    private String jobName;
    private Long jobID;
    private Boolean isSelected;
}

