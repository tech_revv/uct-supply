package com.revv.uct.supply.service.issueFollowUp;

import com.revv.uct.supply.db.issueFollowUp.entity.IssueFollowUp;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

@Service
public interface IIssueFollowUpService {
    IssueFollowUp getIssueFollowUpFromrequest(Long savedSubPartIssueID, JSONObject jsonObject, Long jobID, int issueFollowUpVersion);

    IssueFollowUp uploadIssueFollowUp(IssueFollowUp issueFollowUp);
}
