package com.revv.uct.supply.service.inspection;

import com.revv.uct.supply.controller.inspection.model.*;
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.lead.entity.Lead;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.inspection.model.*;

import java.util.List;

public interface IInspectionService {
    List<InspectionListHeader> getInspectionList(Long inspectorID);

    List<InspectionChecklist> getInspectionChecklist(Long inspectionID) throws UCTSupplyException;

    Inspection scheduleInspection(ScheduleInspectionRequest request) throws EntityNotFoundException, SupplyException, JsonProcessingException;

    void saveInspectionChecklist(Long inspectorID, Long inspectionID, InspectionChecklistRequest request) throws UCTSupplyException;

    void saveInspectionImages(Long inspectorID, Long inspectionID, InspectionChecklistFileRequest inspectionChecklistFileRequest);

    InspectionVerdictDetail getInspectionVerdictData(Long inspectionID) throws UCTSupplyException;

    void saveInspectionVerdict(Long inspectorID, Long inspectionID, InspectionVerdictRequest requestBody) throws UCTSupplyException, JsonProcessingException, SupplyException;

    InspectionVerdictDetailForGenerateOffer getInspectionVerdictDataForGenerateOffer(Long inspectionID) throws UCTSupplyException;

    List<InspectionPickupChecklist> getInspectionPickupChecklist(Long inspectionID) throws UCTSupplyException;

    void saveInspectionPickupChecklist(Long inspectorID, Long inspectionID, List<InspectionPickupChecklist> inspectionPickupChecklists) throws UCTSupplyException;

    List<InspectionChecklistFlat> getInspectionDifferences(InspectionDifferenceRequest requestParams) throws UCTSupplyException;

    Inspection updateInspection(UpdateInspectionRequest request) throws SupplyException, JsonProcessingException, EntityNotFoundException;

    Inspection saveE1InspectionForOnlineLead(CreateLeadRequest request, Lead onlineLead);

    List<InspectionChecklistFlat> getInspectionChecklistFlattened(Long inspectionID) throws UCTSupplyException;

    Inspection getInspectionsByTypeAndCarID(InspectionType inspectionType,Long carID);

}
