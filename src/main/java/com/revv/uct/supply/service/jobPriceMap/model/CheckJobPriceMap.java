package com.revv.uct.supply.service.jobPriceMap.model;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CheckJobPriceMap {
    private Long jobID;
    private String bodyType;
    private String model;
    private String workshop;
    private Double price;
}
