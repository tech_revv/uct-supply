package com.revv.uct.supply.service.inspectionSubPart.impl;

import com.revv.uct.supply.db.inspectionSubPart.dao.IInspectionSubPartDAO;
import com.revv.uct.supply.db.inspectionSubPart.entity.InspectionSubPart;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.service.inspection.model.InspectionSubpartDetail;
import com.revv.uct.supply.service.inspectionSubPart.IInspectionSubPartService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class InspectionSubPartServiceImpl implements IInspectionSubPartService {
    @Autowired
    IInspectionSubPartDAO inspectionSubPartDAO;


    @Override
    public List<InspectionSubpartDetail> getInspectionSubpartDetails(Long inspectionID) {
        return inspectionSubPartDAO.getInspectionSubpartDetails(inspectionID);
    }

    @Override
    public List<InspectionSubPart> insertDataForScheduledInspection(Long createdBy, Long inspectionID, List<SubParts> subPartsList) {
        //getting sub part ids
        List<Long> subPartIDs = new ArrayList<>();
        subPartsList.forEach(subPart -> {
            subPartIDs.add(subPart.getId());
        });

        //separate api to be written when required for update
        /*
        //fetch inspection sub parts based on sub part ids
        List<InspectionSubPart> toBeUpdated = inspectionSubPartDAO.findAllBySubPartIdsAndIsActiveAndInspectionID(subPartIDs, inspectionID);

        //changes to be updated
        toBeUpdated.forEach(update -> {
            update.setActive(false);
            update.setUpdatedBy(createdBy);
            update.setUpdatedAt(LocalDateTime.now());
        });
        //update
        List<InspectionSubPart> updatedData = inspectionSubPartDAO.updateInspectionSubParts(toBeUpdated);
         */

        //get new data to be inserted
        List<InspectionSubPart> newData = getInspectionSubPartDataFromRequest(createdBy, inspectionID, subPartIDs);

        //insert data
        List<InspectionSubPart> insertedData = inspectionSubPartDAO.insertInspectionSubParts(newData);

        return insertedData;
    }

    private List<InspectionSubPart> getInspectionSubPartDataFromRequest(Long createdBy, Long inspectionID, List<Long> subPartIDs) {
        List<InspectionSubPart> list = new ArrayList<>();
        subPartIDs.forEach(subPartID -> {
            list.add(InspectionSubPart.builder()
                    .createdBy(createdBy)
                    .createdAt(LocalDateTime.now())
                    .inspectionID(inspectionID)
                    .subPartID(subPartID)
                    .isActive(true)
                    .build());
        });
        return list;
    }
}
