package com.revv.uct.supply.service.header.impl;

import com.revv.uct.supply.db.header.dao.IHeaderDao;
import com.revv.uct.supply.db.header.entity.Header;
import com.revv.uct.supply.db.version.entity.Version;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.header.IHeaderService;
import com.revv.uct.supply.service.version.IVersionService;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class HeaderServiceImpl implements IHeaderService {

    private final IHeaderDao headerDao;
    private final IVersionService versionService;

    @Autowired
    public HeaderServiceImpl(IHeaderDao headerDao, IVersionService versionService) {
        this.headerDao = headerDao;
        this.versionService = versionService;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Header> uploadNewHeaders(List<Object> list) throws SupplyException, EntityNotFoundException {
        Iterator checkHeader = list.iterator();
        while(checkHeader.hasNext()) {
            JSONObject obj = new JSONObject((Map) checkHeader.next());
            String headerName = (String) obj.get("name");
            if(headerName == null || headerName.length() <= 0) {
                throw new EntityNotFoundException("Header name is mandatory");
            }
            if(existsByHeaderNameAndIsActive(headerName, true)) {
                throw new SupplyException("Header data already exists for headerName: "+(String) obj.get("name"));
            }
        }

        //get current version
        int version = versionService.getCurrentVersion("Header");

        int newVersion = version + 1;

        //disable current version
        versionService.disableCurrentVersion("Header", version);

        //add new version
        Version addedVersion = versionService.addNewVersion("Header", newVersion, true);

        //upload new headers
        List<Header> result = new ArrayList<>();
        Iterator<Object> it = list.iterator();
        while (it.hasNext()) {
            JSONObject jsonObject = new JSONObject((Map) it.next());
            log.info("JSON obj==>>"+jsonObject.toString());
            Header header = getHeaderFromRequest(jsonObject, newVersion);
            Header savedHeader = headerDao.uploadHeader(header);
            result.add(savedHeader);
        }
        return result;
    }

    @Override
    public boolean existsByHeaderNameAndIsActive(String headerName, boolean isActive) {
        if(headerDao.existsByHeaderNameAndIsActive(headerName, isActive)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean existsByHeaderNameAndVersionNumber(String headerName, int headerVersion) {
        if(headerDao.existsByHeaderNameAndVersionNumber(headerName, headerVersion)) {
            return true;
        }
        return false;
    }

    @Override
    public Header findByHeaderNameAndIsActive(String headerName, boolean isActive) {
        Header header = headerDao.findByHeaderNameAndIsActive(headerName, isActive);
        return header;
    }

    private Header getHeaderFromRequest(JSONObject jsonObject, int newVersion) {
        return Header.builder()
                .name((String) jsonObject.get("name"))
                .versionNumber(newVersion)
                .isActive(true)
                .build();
    }
}
