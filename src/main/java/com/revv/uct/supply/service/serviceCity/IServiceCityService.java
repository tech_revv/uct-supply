package com.revv.uct.supply.service.serviceCity;

import com.revv.uct.supply.controller.serviceCity.model.CreateServiceCityRequest;
import com.revv.uct.supply.db.seller.entity.Seller;
import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.UCTSupplyException;

import java.util.List;

public interface IServiceCityService
{

    ServiceCity createServiceCity(CreateServiceCityRequest request) throws UCTSupplyException;

    List<ServiceCity> findAllServiceCities();

    ServiceCity findServiceCityById(Long id) ;
}
