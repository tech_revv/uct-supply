package com.revv.uct.supply.service.inspection.model;

import com.revv.uct.supply.db.enums.SubPartInputType;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InspectionSubpartList {
    private Long subPartID;
    private String subPartName;
    private SubPartInputType allowedSelection;
    private Boolean fileRequired;
    private Boolean isMandatory;
    private Boolean isUnAssured;
    private String lastNFIStatus;
    @Builder.Default
    private String comments = "";
    @Builder.Default
    private List<String> images = new ArrayList<>();
    @Builder.Default
    private List<InspectionIssue> issues = new ArrayList<>();
    @Builder.Default
    private List<InspectionSubIssue> defaultSubIssues = new ArrayList<>();
}
