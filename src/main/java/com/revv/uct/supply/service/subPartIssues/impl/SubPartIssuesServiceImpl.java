package com.revv.uct.supply.service.subPartIssues.impl;

import com.revv.uct.supply.db.issueFollowUp.entity.IssueFollowUp;
import com.revv.uct.supply.db.jobs.entity.Jobs;
import com.revv.uct.supply.db.subPartIssues.dao.ISubPartIssuesDao;
import com.revv.uct.supply.db.subPartIssues.entity.SubPartIssues;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.db.version.entity.Version;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.issueFollowUp.IIssueFollowUpService;
import com.revv.uct.supply.service.jobs.IJobsService;
import com.revv.uct.supply.service.subPartIssues.ISubPartIssuesService;
import com.revv.uct.supply.service.subParts.ISubPartsService;
import com.revv.uct.supply.service.version.IVersionService;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Slf4j
public class SubPartIssuesServiceImpl implements ISubPartIssuesService {
    private final ISubPartIssuesDao subPartIssuesDao;
    private final IVersionService versionService;
    private final ISubPartsService subPartsService;
    private final IJobsService jobsService;
    private final IIssueFollowUpService issueFollowUpService;

    @Autowired
    public SubPartIssuesServiceImpl(ISubPartIssuesDao subPartIssuesDao, IVersionService versionService, ISubPartsService subPartsService, IJobsService jobsService, IIssueFollowUpService issueFollowUpService) {
        this.subPartIssuesDao = subPartIssuesDao;
        this.versionService = versionService;
        this.subPartsService = subPartsService;
        this.jobsService = jobsService;
        this.issueFollowUpService = issueFollowUpService;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<SubPartIssues> uploadSubPartIssues(List<Object> list) throws SupplyException, EntityNotFoundException {
        //if sub part or job name doesn't exist throw exception
        Iterator checkItr = list.iterator();
        while(checkItr.hasNext()) {
            JSONObject obj = new JSONObject((Map) checkItr.next());
            if(!subPartsService.existsBySubPartNameAndIsActive((String)obj.get("subPartName"), true)) {
                throw new SupplyException("Sub part data doesn't exist.");
            }
            if(!jobsService.existsByJobNameAndIsActive((String)obj.get("jobName"), true)) {
                throw new SupplyException("Job data doesn't exist.");
            }
            String issueName = (String) obj.get("issueName");
            if(issueName == null || issueName.length() <= 0) {
                throw new EntityNotFoundException("Issue name is mandatory");
            }
        }

        //==Sub Part Issues Table
        //get current version
        int version = versionService.getCurrentVersion("SubPartIssues");
        int newVersion = version + 1;
        //disable current version
        versionService.disableCurrentVersion("SubPartIssues", version);
        //add new version
        Version addedVersion = versionService.addNewVersion("SubPartIssues", newVersion, true);

        //==Issue Follow Up Table
        //get issue follow up version
        int issueFollowUpVersion = versionService.getCurrentVersion("IssueFollowUp");
        log.info("issuefollowUpversion==>>"+String.valueOf(issueFollowUpVersion));
        int newIssueFollowUpversion = issueFollowUpVersion + 1;
        versionService.disableCurrentVersion("IssueFollowUp", issueFollowUpVersion);
        Version addedIssueFollowUpVersion = versionService.addNewVersion("IssueFollowUp", newIssueFollowUpversion, true);

        //upload new sub part issues
        List<SubPartIssues> result = new ArrayList<>();
        Iterator<Object> it = list.iterator();
        HashMap<String, Long> check = new HashMap<>();
        while (it.hasNext()) {
            JSONObject jsonObject = new JSONObject((Map) it.next());

            SubParts subPart = subPartsService.findBySubPartNameAndIsActive((String)jsonObject.get("subPartName"), true);
            SubPartIssues subPartIssue = null;

            SubPartIssues savedSubPartIssue = null;
            IssueFollowUp savedIssueFollowUp = null;

            SubPartIssues existingSubPartIssue = subPartIssuesDao.findBySubPartIDAndIssueNameAndIsActive(subPart.getId(), (String)jsonObject.get("issueName"), true);

            if(existingSubPartIssue != null || check.containsKey(subPart.getId() + "_" + jsonObject.get("issueName"))) {
                Jobs job = jobsService.findByJobNameAndIsActive((String)jsonObject.get("jobName"), true);
                Long id = existingSubPartIssue != null ? existingSubPartIssue.getId() : check.get(subPart.getId() + "_" + jsonObject.get("issueName"));
                IssueFollowUp issueFollowUp = issueFollowUpService.getIssueFollowUpFromrequest(id,jsonObject,job.getId(),newIssueFollowUpversion);
                savedIssueFollowUp = issueFollowUpService.uploadIssueFollowUp(issueFollowUp);
            }
            else {
                subPartIssue = getSubPartIssueFromRequest(jsonObject,subPart.getId(), newVersion);
                savedSubPartIssue = subPartIssuesDao.uploadSubPartIssue(subPartIssue);
                check.put(subPart.getId() + "_" + jsonObject.get("issueName") , savedSubPartIssue.getId());

                Jobs job = jobsService.findByJobNameAndIsActive((String)jsonObject.get("jobName"), true);
                IssueFollowUp issueFollowUp = issueFollowUpService.getIssueFollowUpFromrequest(savedSubPartIssue.getId(),jsonObject,job.getId(),newIssueFollowUpversion);
                savedIssueFollowUp = issueFollowUpService.uploadIssueFollowUp(issueFollowUp);
            }
            result.add(savedSubPartIssue);
        }
        return result;
    }

    @Override
    public List<SubPartIssues> getDefaultIssues() {
        return subPartIssuesDao.getDefaultIssues();
    }

    private SubPartIssues getSubPartIssueFromRequest(JSONObject jsonObject, Long subPartID, int newVersion) {
        return SubPartIssues.builder()
                .subPartID(subPartID)
                .issueName((String) jsonObject.get("issueName"))
                .jobName((String) jsonObject.get("jobName"))
                .versionNumber(newVersion)
                .isActive(true)
                .build();
    }
}
