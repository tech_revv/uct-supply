package com.revv.uct.supply.service.documentStageMaster.impl;

import com.revv.uct.supply.db.documentStageMaster.dao.IDocumentStageMasterDAO;
import com.revv.uct.supply.db.documentStageMaster.entity.DocumentStageMaster;
import com.revv.uct.supply.db.enums.DocumentStage;
import com.revv.uct.supply.db.enums.DocumentType;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.documentStageMaster.IDocumentStageMasterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class DocumentStageMasterServiceImpl implements IDocumentStageMasterService {
    private final IDocumentStageMasterDAO documentStageMasterDAO;

    @Autowired
    public DocumentStageMasterServiceImpl(IDocumentStageMasterDAO documentStageMasterDAO) {
        this.documentStageMasterDAO = documentStageMasterDAO;
    }

    @Override
    public Long findIDByTypeAndStageAndisActive(DocumentType serviceHistory, DocumentStage offerCreation) throws SupplyException {
        log.info("docType=={}", serviceHistory);
        log.info("docStage=={}", offerCreation);

        List<DocumentStageMaster> list = documentStageMasterDAO.findByDocumentTypeAndDocumentStageAndIsActive(serviceHistory, offerCreation);
        log.info("list=={}", list);

        if(list.size() < 1) {
            throw new SupplyException("Document Stage Master data not found.");
        }
        return list.get(0).getId();
    }

    @Override
    public List<DocumentStageMaster> getStageDocuments(DocumentStage documentStage) {
        return documentStageMasterDAO.findByStageName(documentStage);
    }

    @Override
    public List<DocumentStageMaster> findByID(List<Long> documentIDs) {
        return documentStageMasterDAO.findByID(documentIDs);
    }
}
