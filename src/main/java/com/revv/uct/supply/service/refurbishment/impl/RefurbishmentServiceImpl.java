package com.revv.uct.supply.service.refurbishment.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revv.uct.common.date.DateUtilUCT;
import com.revv.uct.supply.constants.APIConstants;
import com.revv.uct.supply.controller.inspection.model.UpdateInspectionRequest;
import com.revv.uct.supply.controller.refurbishment.model.ScheduleRefurbishmentRequest;
import com.revv.uct.supply.controller.refurbishment.model.SubmitDocumentRequest;
import com.revv.uct.supply.controller.refurbishment.model.UpdateRefurbRequest;
import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.documentStageMaster.entity.DocumentStageMaster;
import com.revv.uct.supply.db.enums.*;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.inspectionJob.entity.InspectionJob;
import com.revv.uct.supply.db.inspectionReport.entity.InspectionReport;
import com.revv.uct.supply.db.jobs.entity.Jobs;
import com.revv.uct.supply.db.refurbishment.dao.IRefurbishmentDAO;
import com.revv.uct.supply.db.refurbishment.entity.Refurbishment;
import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.commonDocument.ICommonDocumentService;
import com.revv.uct.supply.service.documentStageMaster.IDocumentStageMasterService;
import com.revv.uct.supply.service.history.IHistoryService;
import com.revv.uct.supply.service.inspection.IInspectionService;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.RefurbList;
import com.revv.uct.supply.service.inspectionJob.IInspectionJobService;
import com.revv.uct.supply.service.inspectionReport.IInspectionReportService;
import com.revv.uct.supply.service.jobs.IJobsService;
import com.revv.uct.supply.service.procuredCar.IProcuredCarService;
import com.revv.uct.supply.service.refurbishment.IRefurbishmentService;
import com.revv.uct.supply.service.refurbishment.model.*;
import com.revv.uct.supply.service.refurbishmentJob.IRefurbishmentJobService;
import com.revv.uct.supply.service.subParts.ISubPartsService;
import com.revv.uct.supply.service.user.IUserService;
import com.revv.uct.supply.service.workshop.IWorkshopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.revv.uct.supply.constants.ResponseMessage.*;

@Service
@Slf4j
public class RefurbishmentServiceImpl implements IRefurbishmentService {

    @Autowired
    IRefurbishmentDAO refurbishmentDAO;

    @Autowired
    IInspectionReportService inspectionReportService;

    @Autowired
    IRefurbishmentJobService refurbishmentJobService;

    @Autowired
    ICommonDocumentService commonDocumentService;

    @Autowired
    IDocumentStageMasterService documentStageMasterService;

    @Autowired
    IHistoryService historyService;

    @Autowired
    IInspectionJobService inspectionJobService;

    @Autowired
    ISubPartsService subPartsService;

    @Autowired
    IJobsService jobsService;

    @Autowired
    IWorkshopService workshopService;

    @Autowired
    IUserService userService;

    @Autowired @Lazy
    IInspectionService inspectionService;

    @Autowired
    IProcuredCarService procuredCarService;

    @Override
    public List<RefurbList> findRefurbishments(Long refurbSpocID, List<InspectionStatus> statusList) {
        return refurbishmentDAO.findRefurbishments(refurbSpocID, statusList);
    }

    @Override
    public RefurbScreen getRefurbJobList(Long refurbID) throws UCTSupplyException {
        //get refurb details
        RefurbDetail refurbDetail = refurbishmentDAO.getRefurbDetails(refurbID);
        if(refurbDetail == null) {
            throw new UCTSupplyException("Refurb details not found", HttpStatus.BAD_REQUEST);
        }

        //get inspection reports
        List<InspectionReport> inspectionReports = inspectionReportService.getInspectionReport(refurbDetail.getInspectionID());

        //get refurb jobs
        List<RefurbishmentJob> refurbJobs = refurbishmentJobService.getRefurbJobList(refurbID);
        refurbJobs = refurbJobs.stream().filter(RefurbishmentJob::getSelected).collect(Collectors.toList());
        int jobCount = refurbJobs.size();
        Map<Long, List<RefurbishmentJob>> refurbJobsMap = groupRefurbs(refurbJobs);

        //logic to fetch documents required at particular stage
        List<DocumentStageMaster> documentStageMasters = documentStageMasterService.getStageDocuments(DocumentStage.REFURB_WORKSHOP);
        List<Long> documentStageMasterIDs = documentStageMasters.stream().map(DocumentStageMaster::getId).collect(Collectors.toList());
        List<CommonDocument> refurbDocuments = commonDocumentService.getDocumentsForEntityIDAndNameAndStage(refurbID, CommonDocumentEntity.REFURBISHMENT,documentStageMasterIDs);


        return constructRefurbScreen(refurbDetail, inspectionReports, refurbJobsMap, documentStageMasters, refurbDocuments, jobCount);
    }

    @Override
    public void saveDocuments(Long userID, Long refurbID, SubmitDocumentRequest requestBody) throws UCTSupplyException {
        Refurbishment oldData = refurbishmentDAO.findByIdAndIsActive(refurbID, true);
        if(oldData == null) {
            throw new UCTSupplyException("Refurb details not found", HttpStatus.BAD_REQUEST);
        }

        validateSaveDocumentRequest(requestBody); //validate request

        List<Long> documentIDs = requestBody.getDocumentList().stream().map(RefurbDocument::getDocumentID).collect(Collectors.toList());
        markPreviousDocumentsInactive(userID, refurbID, documentIDs); //soft delete previously uploaded files

        saveDocumentsInDatabase(refurbID, userID, requestBody.getDocumentList()); //save documents

        UpdateRefurbRequest request = UpdateRefurbRequest.builder()
                .lastDataSyncTime(requestBody.getLastDataSyncTime())
                .build();

        update(oldData, userID, request); //update last sync time on refurbishment

    }

    @Override
    public Refurbishment updateRefurbishemnt(Long refurbID, Long userID, UpdateRefurbRequest request) throws UCTSupplyException {
        Refurbishment oldData = refurbishmentDAO.findByIdAndIsActive(refurbID, true);
        if(oldData == null) {
            throw new UCTSupplyException("Refurb details not found", HttpStatus.BAD_REQUEST);
        }

        update(oldData, userID, request); //perform updates after validation
        return oldData;
    }

    @Override
    public List<Refurbishment> findRefurbishmentsByCarIDAndStatus(List<Long> carIDs, List<InspectionStatus> status) {
        return refurbishmentDAO.findRefurbishmentsByCarIDAndStatus(carIDs, status, true);
    }

    public List<RefurbJobListForWorkshop> getRefurbJobListForSendToWorkshop(Long carID) {

        List<InspectionJob> inspectionJobs = inspectionJobService.getE3InspectionJobsByCarID(carID).
                stream().filter(job->!APIConstants.DEFAULT_JOB_ID.contains(job.getJobID())).collect(Collectors.toList());

        List<Long> inspectionsIDs = inspectionJobs.stream().map(InspectionJob::getInspectionID).distinct().collect(Collectors.toList());

        List<Long> subPartIDs = inspectionJobs.stream().map(InspectionJob::getSubPartID).distinct().collect(Collectors.toList());

        List<SubParts> subParts = subPartsService.findAllByID(subPartIDs);

        List<Long> inspectionJobsIDs = inspectionJobs.stream().map(InspectionJob::getJobID).collect(Collectors.toList());

        List<Jobs> inspectionJobsDetails = jobsService.getAllJobsByIDs(inspectionJobsIDs);

        List<InspectionReport> inspectionReports = inspectionReportService.getInspectionReportByInspectionIDs(inspectionsIDs);
        inspectionReports = inspectionReports.stream().filter(InspectionReport::getSelected).collect(Collectors.toList());

        List<RefurbishmentJob> refurbishmentJobs = refurbishmentJobService.getCompletedRefurbJobListByCarID(carID);

        Map<Long, List<InspectionJob>> groupedJobsBySubParts = constructGroupedJobsBySubParts(inspectionJobs, subPartIDs);

        return constructRefurbJobsListForWorkshops(groupedJobsBySubParts,subParts,inspectionReports,inspectionJobsDetails,refurbishmentJobs);

    }

    @Override
    public ScheduleRefurbishmentResponse scheduleRefurbishment(ScheduleRefurbishmentRequest request) throws EntityNotFoundException, JsonProcessingException, SupplyException, UCTSupplyException {

        if(!workshopService.existById(request.getWorkshopID())) {
            throw new EntityNotFoundException(workshop_not_exit+"for workshopID: "+request.getWorkshopID());
        }
        if(!userService.existById(request.getRefurbSpocID())) {
            throw new EntityNotFoundException(refurb_spoc_not_exit+"for refurbSpocID: "+request.getRefurbSpocID());
        }

        List<Refurbishment> refurbishments = refurbishmentDAO.findRefurbishmentsByCarIDAndStatus(Collections.singletonList(request.getCarID()),Collections.singletonList(InspectionStatus.SCHEDULED),true);

        Refurbishment refurbishment;
        String refurbMsg;

        if(refurbishments != null && refurbishments.toArray().length > 0) {

            refurbishment = refurbishments.get(0);

            updateRefurbishemnt(refurbishment.getId(),request.getUserID(),getUpdateRefurbRequest(request));

            List<RefurbishmentJob> refurbishmentJobs = refurbishmentJobService.getRefurbJobList(refurbishment.getId());
            refurbishmentJobs.forEach(job->job.setActive(false));

            refurbishmentJobService.updateRefurbJobs(refurbishmentJobs);

            refurbMsg = "Refurbishment with refurbID : "+ refurbishment.getId() +" updated.";
        }
        else{

            refurbishment = refurbishmentDAO.updateRefurbishment(getRefurbishmentFromRequest(request));

            Inspection inspection = inspectionService.getInspectionsByTypeAndCarID(InspectionType.E3,request.getCarID());

            inspectionService.updateInspection(getUpdateInspectionRequest(inspection,refurbishment.getId(),request.getUserID()));

            procuredCarService.updateProcuredCarStatusFromLeadID(inspection.getLeadID(),request.getUserID(),ProcuredCarStatus.READY_FOR_REFURB);

            refurbMsg = "Refurbishment with refurbID : "+ refurbishment.getId() +" created.";

        }
        refurbishmentJobService.updateRefurbJobs(getRefurbJobsFromRequest(request,refurbishment.getId()));

        return new ScheduleRefurbishmentResponse(refurbishment,refurbMsg);
    }

    @Override
    public Refurbishment getRefurbDetails(Long refurbID) {
        return refurbishmentDAO.findByIdAndIsActive(refurbID, true);
    }

    private UpdateRefurbRequest getUpdateRefurbRequest(ScheduleRefurbishmentRequest request) {

        return UpdateRefurbRequest.builder()
                .workshopID(request.getWorkshopID())
                .refurbSpocID(request.getRefurbSpocID())
                .refurbStartTime(DateUtilUCT.convertToUTC(request.getRefurbishmentStartTime()))
                .refurbEndTime(DateUtilUCT.convertToUTC(request.getRefurbishmentEndTime()))
                .build();
    }

    private UpdateInspectionRequest getUpdateInspectionRequest(Inspection inspection,Long refurbID,Long userID) {

        return UpdateInspectionRequest.builder()
                .id(inspection.getId())
                .leadID(inspection.getLeadID())
                .refurbID(refurbID)
                .updatedBy(userID)
                .build();
    }

    private Refurbishment getRefurbishmentFromRequest(ScheduleRefurbishmentRequest request) {

        return Refurbishment.builder()
                .refurbSpocID(request.getRefurbSpocID())
                .workshopID(request.getWorkshopID())
                .startDate(DateUtilUCT.convertToUTC(request.getRefurbishmentStartTime()))
                .endDate(DateUtilUCT.convertToUTC(request.getRefurbishmentEndTime()))
                .isActive(true)
                .procuredCarID(request.getCarID())
                .createdBy(request.getUserID())
                .status(InspectionStatus.SCHEDULED)
                .build();
    }

    private List<RefurbishmentJob> getRefurbJobsFromRequest(ScheduleRefurbishmentRequest request,Long refurbID) {

        List<RefurbishmentJob> refurbishmentJobs = new ArrayList<>();

        for(RefurbJobListForWorkshop refurbJobListForWorkshop : request.getRefurbJobs()){
            List<Job> jobs = refurbJobListForWorkshop.getJobs();
            for(Job job : jobs){
                RefurbishmentJob refurbishmentJob = RefurbishmentJob.builder()
                        .refurbID(refurbID)
                        .jobID(job.getJobID())
                        .jobName(job.getJobName())
                        .subpartID(refurbJobListForWorkshop.getSubPartID())
                        .subpartName(refurbJobListForWorkshop.getSubPartName())
                        .createdBy(request.getUserID())
                        .selected(job.getIsSelected())
                        .isActive(true)
                        .build();
                refurbishmentJobs.add(refurbishmentJob);
            }
        }
        return refurbishmentJobs;
    }

    private List<RefurbJobListForWorkshop> constructRefurbJobsListForWorkshops(Map<Long, List<InspectionJob>> groupedJobsBySubParts,List<SubParts> subParts,List<InspectionReport> inspectionReports,List<Jobs> inspectionJobsDetails,List<RefurbishmentJob> refurbishmentJobs){
        List<RefurbJobListForWorkshop> refurbJobListForWorkshopList = new ArrayList<>();

        for(Map.Entry<Long,List<InspectionJob>> map:groupedJobsBySubParts.entrySet()){

            Long subPartID = map.getKey();
            Optional<SubParts> subPartDetails = subParts.stream().filter(x->x.getId().equals(subPartID)).findFirst();
            String subPartName = null;
            if(subPartDetails.isPresent()){
                subPartName = subPartDetails.get().getSubPartName();
            }

            List<InspectionReport> inspectionReportsForSubPart = inspectionReports.stream().filter(x->x.getSubPartID().equals(subPartID)).collect(Collectors.toList());
            List<SubPartIssue> subPartIssues = new ArrayList<>();

            inspectionReportsForSubPart.forEach(inspectionReport ->
                    subPartIssues.add(new SubPartIssue(inspectionReport.getIssueName(),inspectionReport.getSubPartIssueID())));

            List<InspectionJob> jobsForSubPart = map.getValue();
            List<Job> jobs = new ArrayList<>();

            jobsForSubPart.forEach(inspectionJob ->{
                Long jobID = inspectionJob.getJobID();
                String jobName = inspectionJobsDetails.stream().filter(x->x.getId().equals(jobID)).findFirst().get().getJobName();
                if(refurbishmentJobs.stream().anyMatch(x->x.getJobID().equals(jobID))){
                    jobs.add(new Job(jobName,jobID,true));
                }
                else jobs.add(new Job(jobName,jobID,false));
            });

            refurbJobListForWorkshopList.add(new RefurbJobListForWorkshop(subPartName,subPartID,subPartIssues,jobs));
        }
        return refurbJobListForWorkshopList;
    }

    private Map<Long, List<InspectionJob>> constructGroupedJobsBySubParts(List<InspectionJob> inspectionJobs, List<Long> subPartIDs) {
        Map<Long,List<InspectionJob>> groupedJobsBySubParts = new HashMap<>();

        for(Long subPartID: subPartIDs){
            List<InspectionJob> inspectionJobsForSubPart = inspectionJobs.stream().filter(x->x.getSubPartID().equals(subPartID)).collect(Collectors.toList());
            groupedJobsBySubParts.put(subPartID,inspectionJobsForSubPart);
        }
        return groupedJobsBySubParts;
    }

    private Refurbishment update(Refurbishment oldData, Long userID, UpdateRefurbRequest request) throws UCTSupplyException{
        ObjectMapper mapper = new ObjectMapper();
        try {
            String oldDataJson = mapper.writeValueAsString(oldData);
            log.info("oldDataJson ={}", oldDataJson);

            //update fields
            if(request.getRefurbStatus() != null) {
                oldData.setStatus(request.getRefurbStatus());
            }
            if(request.getLastDataSyncTime() != null) {
                oldData.setLastDataSyncTime(request.getLastDataSyncTime());
            }
            if(request.getWorkshopID() != null) {
                oldData.setWorkshopID(request.getWorkshopID());
            }
            if(request.getRefurbSpocID() != null) {
                oldData.setRefurbSpocID(request.getRefurbSpocID());
            }
            if(request.getRefurbStartTime() != null) {
                oldData.setStartDate(request.getRefurbStartTime());
            }
            if(request.getRefurbEndTime() != null) {
                oldData.setEndDate(request.getRefurbEndTime());
            }

            oldData.setUpdatedBy(userID);
            oldData.setUpdatedAt(LocalDateTime.now());
            log.info("newData ={}", oldData);

            String newDataJson = mapper.writeValueAsString(oldData);
            log.info("newDataJson ={}", newDataJson);

            Refurbishment refurbishment = refurbishmentDAO.updateRefurbishment(oldData);

            //save inspection history
            historyService.saveHistory(userID, oldData.getId(), EditEntity.REFURBISHMENT, oldDataJson, newDataJson);

            return refurbishment;

        } catch (JsonProcessingException ex) {
            throw new UCTSupplyException(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void saveDocumentsInDatabase(Long refurbID, Long userID, List<RefurbDocument> documentList) {
        List<CommonDocument> commonDocuments = new ArrayList<>();
        for (RefurbDocument refurbDocument : documentList) {
            if(refurbDocument.getDocumentList().size() > 0) {
                for (String documentPath : refurbDocument.getDocumentList()) {
                    commonDocuments.add(
                            CommonDocument.builder()
                                    .createdBy(userID)
                                    .updatedBy(userID)
                                    .entityID(refurbID)
                                    .commonDocumentEntity(CommonDocumentEntity.REFURBISHMENT)
                                    .documentStageMasterID(refurbDocument.getDocumentID())
                                    .imageURL(documentPath)
                                    .build()
                    );
                }
            }
        }

        if(commonDocuments.size() > 0) {
            commonDocumentService.saveDocuments(commonDocuments);
        }
    }

    private void markPreviousDocumentsInactive(Long userID, Long refurbID, List<Long> documentIDs) {
        commonDocumentService.markDocumentsInactiveAgainstDocumentStageMaster(CommonDocumentEntity.REFURBISHMENT, refurbID, userID, documentIDs);
    }

    private void validateSaveDocumentRequest(SubmitDocumentRequest requestBody) throws UCTSupplyException {
        List<RefurbDocument> documentList = requestBody.getDocumentList();
        List<Long> documentIDs = documentList.stream().map(RefurbDocument::getDocumentID).collect(Collectors.toList());

        List<DocumentStageMaster> documentDetails = documentStageMasterService.findByID(documentIDs);

        for (RefurbDocument refurbDocument : documentList) {
            DocumentStageMaster documentDetail = documentDetails.stream().filter(d -> d.getId().equals(refurbDocument.getDocumentID())).findAny().orElse(null);
            if(documentDetail == null) {
                throw new UCTSupplyException(refurbDocument.getDocumentID() + " document ID is invalid", HttpStatus.BAD_REQUEST);
            }
            if(documentDetail.isMandatory() && refurbDocument.getDocumentList().size() == 0) {
                throw new UCTSupplyException(refurbDocument.getDocumentName() + " is mandatory", HttpStatus.BAD_REQUEST);
            }
        }
    }

    private Map<Long, List<RefurbishmentJob>> groupRefurbs(List<RefurbishmentJob> refurbJobs) {
        return refurbJobs.stream().collect(Collectors.groupingBy(RefurbishmentJob::getSubpartID));
    }

    private RefurbScreen constructRefurbScreen(RefurbDetail refurbDetail, List<InspectionReport> inspectionReports,
                                               Map<Long, List<RefurbishmentJob>> refurbJobs,
                                               List<DocumentStageMaster> documentStageMasters,
                                               List<CommonDocument> refurbImages, int jobCount) {
        RefurbScreen.RefurbScreenBuilder refurbScreenBuilder = RefurbScreen.builder();
        refurbScreenBuilder.carModel(refurbDetail.getCarModel());
        refurbScreenBuilder.regNo(refurbDetail.getRegistrationNumber());
        refurbScreenBuilder.jobsCount(jobCount);
        refurbScreenBuilder.inspectionID(refurbDetail.getInspectionID());
        refurbScreenBuilder.inspectionType(refurbDetail.getInspectionType());
        refurbScreenBuilder.insertTS(refurbDetail.getInsertTS());

        //construct refurb job list
        List<RefurbJobList> refurbJobLists = constructRefurbJobList(refurbJobs, inspectionReports);
        refurbScreenBuilder.jobList(refurbJobLists);

        List<RefurbDocument> refurbDocuments = constructRefurbDocument(documentStageMasters, refurbImages);
        refurbScreenBuilder.documentList(refurbDocuments);

        return refurbScreenBuilder.build();
    }

    private List<RefurbDocument> constructRefurbDocument(List<DocumentStageMaster> documentStageMasters, List<CommonDocument> refurbImages) {
        List<RefurbDocument> refurbDocuments = new ArrayList<>();

        //group documents based on documentStageMasterID
        Map<Long, List<CommonDocument>> refurbImagesMap = refurbImages.stream()
                .collect(Collectors.groupingBy(CommonDocument::getDocumentStageMasterID));

        for (DocumentStageMaster documentStageMaster : documentStageMasters) {
            List<CommonDocument> documents = refurbImagesMap.get(documentStageMaster.getId());
            List<String> commonDocuments = new ArrayList<>();
            if(documents != null) {
                commonDocuments = documents.stream().map(CommonDocument::getImageURL).collect(Collectors.toList());
            }

            refurbDocuments.add(
                    RefurbDocument.builder()
                            .documentName(documentStageMaster.getDocumentName())
                            .documentID(documentStageMaster.getId())
                            .isMandatory(documentStageMaster.isMandatory())
                            .documentList(commonDocuments)
                            .build()
            );
        }

        return refurbDocuments;
    }

    private List<RefurbJobList> constructRefurbJobList(Map<Long, List<RefurbishmentJob>> refurbJobs,
                                                       List<InspectionReport> inspectionReports) {
        List<RefurbJobList> refurbJobLists = new ArrayList<>();
        int listIndex = 1;

        for (Map.Entry<Long, List<RefurbishmentJob>> subPart : refurbJobs.entrySet()) {
            Long subpartID = subPart.getKey();
            List<RefurbishmentJob> jobs = subPart.getValue();
            String subPartHeaderName = getSubPartHeaderName(listIndex, jobs.get(0).getSubpartName());
            String issueNames = inspectionReports.stream()
                    .filter(ir -> subpartID.equals(ir.getSubPartID()) && ir.getSelected())
                    .map(InspectionReport::getIssueName)
                    .collect(Collectors.joining(", "));

            String jobNames = jobs.stream().map(RefurbishmentJob::getJobName).collect(Collectors.joining(", "));
            RefurbJobList refurbJobList = RefurbJobList.builder()
                    .subPartName(subPartHeaderName)
                    .issues(issueNames)
                    .jobs(jobNames)
                    .build();
            refurbJobLists.add(refurbJobList);

            listIndex++;
        }

        return refurbJobLists;
    }

    private String getSubPartHeaderName(int listIndex, String subPartName ) {
        return listIndex + ". " + subPartName;
    }
}
