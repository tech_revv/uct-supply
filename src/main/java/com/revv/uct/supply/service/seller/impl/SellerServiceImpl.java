package com.revv.uct.supply.service.seller.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revv.uct.supply.constants.ResponseMessages;
import com.revv.uct.supply.constants.SellerType;
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.controller.lead.model.SellersForLeadCreation;
import com.revv.uct.supply.controller.seller.model.SellerCreationData;
import com.revv.uct.supply.controller.seller.model.UpdateSellerRequest;
import com.revv.uct.supply.db.enums.EditEntity;
import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.history.entity.History;
import com.revv.uct.supply.db.seller.dao.ISellerDao;
import com.revv.uct.supply.db.seller.entity.Seller;
import com.revv.uct.supply.controller.seller.model.CreateSellerRequest;

import com.revv.uct.supply.db.serviceCity.dao.IServiceCityDAO;
import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;


import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.history.IHistoryService;
import com.revv.uct.supply.service.seller.ISellerService;
import com.revv.uct.supply.service.serviceCity.IServiceCityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class SellerServiceImpl implements ISellerService
{
    private final ISellerDao sellerDao;
    private final IServiceCityDAO serviceCityDAO;
    private final IServiceCityService serviceCityService;
    private final IHistoryService historyService;

    @Autowired
    public SellerServiceImpl(ISellerDao sellerDao, IServiceCityDAO serviceCityDAO, IServiceCityService serviceCityService, IHistoryService historyService) {
        this.sellerDao = sellerDao;
        this.serviceCityDAO = serviceCityDAO;
        this.serviceCityService = serviceCityService;
        this.historyService = historyService;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Seller createSeller(@NotNull @Valid CreateSellerRequest request) throws UCTSupplyException {
        log.debug("created customer service",request);
        return sellerDao.createSeller(getSellerFromRequest(request));
    }

    @Override
    public List<Object> getSellersForLead(Long serviceCityID, SellerTypeEnum sellerType) {
        List<Object[]> sellers = sellerDao.getSellersForLead(serviceCityID, sellerType);
        List<Object> data = new ArrayList<>();
        sellers.forEach((seller)->{
            data.add(new SellersForLeadCreation((String) seller[0], (String) seller[1], (Long) seller[2]));
        });
        return data;
    }

    @Override
    public List<SellerCreationData> getSellerCreationData() {
        List<ServiceCity> city = serviceCityService.findAllServiceCities();
        log.info("city", city);
        List<SellerType> sellerType = SellerType.getSellerTypes();
        log.info("sellerType", String.valueOf(sellerType));

        List<SellerCreationData> list = new ArrayList<>();
        list.add(new SellerCreationData(city, sellerType));

        return list;
    }

    @Override
    public List<Seller> getSellersByNameAndServiceCity(Long serviceCityID, String name) {

        return sellerDao.findAllByNameAndServiceCity(serviceCityID, name);
    }

    @Override
    public Seller updateSeller(UpdateSellerRequest request) throws SupplyException, JsonProcessingException {
        Seller oldData = sellerDao.findByIdAndIsActive(request.getId(), true);
        log.info("oldData ={}", oldData);

        //if data doesn't exist for given sellerID
        if(oldData == null){
            throw new SupplyException("Seller data doesn't exist for sellerID: "+request.getId());
        }

        ObjectMapper mapper = new ObjectMapper();
        String oldDataJson = mapper.writeValueAsString(oldData);
        log.info("oldDataJson ={}", oldDataJson);

        Seller newData = oldData;

        // updates
        if(request.getBankAcDetails() != null && request.getBankAcDetails().length() > 0) {
            newData.setBankAcDetails(request.getBankAcDetails());
        }
        if(request.getEmail() != null && request.getEmail().length() > 0) {
            newData.setEmail(request.getEmail());
        }
        if(request.getSpocName() != null && request.getSpocName().length() > 0) {
            newData.setSpocName(request.getSpocName());
        }
        if(request.getGstNumber() != null && request.getGstNumber().length() > 0) {
            newData.setGstNumber(request.getGstNumber());
        }
        if(request.getPanNumber() != null && request.getPanNumber().length() > 0) {
            newData.setPanNumber(request.getPanNumber());
        }
        newData.setUpdatedAt(LocalDateTime.now());
        log.info("newData ={}", newData);

        String newDataJson = mapper.writeValueAsString(newData);
        log.info("newDataJson ={}", newDataJson);

        Seller updatedSeller = sellerDao.updateSeller(newData);

        //Save seller history
        History savedSellerHistory = historyService.saveHistory(request.getAdminID(), request.getId(), EditEntity.valueOf("SELLER"), oldDataJson, newDataJson);

        return updatedSeller;
    }

    @Override
    public Seller createSellerForB2CLeads(CreateLeadRequest request) {
        return sellerDao.createSeller(Seller.builder()
                .adminID(request.getAdminID())
                .serviceCityID(request.getServiceCityID())
                .name("B2CSeller")
                .sellerType(SellerTypeEnum.B2C)
                .contactNumber(request.getSellerContactNo())
                .isActive(true)
                .createdAt(LocalDateTime.now())
                .lng(0.0)
                .lat(0.0)
                .address("none")
                .build());
    }

    @Override
    public List<Seller> findAllByContactNumberAndType(String sellerContactNo) {
        return sellerDao.findAllByContactNumberAndSellerTypeAndIsActive(sellerContactNo, SellerTypeEnum.B2C,true);
    }

    private Seller getSellerFromRequest(CreateSellerRequest request) throws UCTSupplyException {

        ServiceCity serviceCity = serviceCityDAO.findServiceCityById(request.getServiceCityID());
        if (serviceCity==null)
            throw new UCTSupplyException(ResponseMessages.invalidCityID, HttpStatus.NOT_FOUND);
        return Seller.builder()
                .adminID(request.getAdminID())
                .name(request.getName())
                .sellerType(request.getSellerType())
                .contactNumber(request.getContactNumber())
                .email(request.getEmail())
                .spocName(request.getSpocName())
                .serviceCityID(serviceCity.getId())
                .lat(request.getLat())
                .lng(request.getLng())
                .address(request.getAddress())
                .bankAcDetails(request.getBankAcDetails())
                .gstNumber(request.getGstNumber())
                .panNumber(request.getPanNumber())
                .isActive(true)
                .build();
    }
}
