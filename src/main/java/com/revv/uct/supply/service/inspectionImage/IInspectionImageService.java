package com.revv.uct.supply.service.inspectionImage;

import com.revv.uct.supply.controller.inspection.model.InspectionImageRequest;
import com.revv.uct.supply.db.inspectionImage.entity.InspectionImage;

import java.util.List;

public interface IInspectionImageService {
    List<InspectionImage> getInspectionImages(Long inspectionID);

    void markImagesInactive(Long inspectorID, Long inspectionID);

    void saveInspectionImage(Long inspectorID, Long inspectionID, Long subPartID, List<String> images);
}
