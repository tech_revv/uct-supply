package com.revv.uct.supply.service.leadPaymentDetail.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.constants.PaymentMode;
import com.revv.uct.supply.controller.lead.model.UpdateLeadRequest;
import com.revv.uct.supply.controller.offer.model.UpdateOfferRequest;
import com.revv.uct.supply.controller.payment.model.GetLeadPaymentData;
import com.revv.uct.supply.controller.payment.model.ProceedWithoutTokenRequest;
import com.revv.uct.supply.controller.payment.model.SaveLeadPaymentRequest;
import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.enums.*;
import com.revv.uct.supply.db.leadPaymentDetail.dao.ILeadPaymentDetailDao;
import com.revv.uct.supply.db.leadPaymentDetail.entity.LeadPaymentDetail;
import com.revv.uct.supply.db.user.entity.User;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.commonDocument.ICommonDocumentService;
import com.revv.uct.supply.service.documentStageMaster.IDocumentStageMasterService;
import com.revv.uct.supply.service.lead.ILeadService;
import com.revv.uct.supply.service.leadPaymentDetail.ILeadPaymentDetailService;
import com.revv.uct.supply.service.offer.IOfferService;
import com.revv.uct.supply.service.user.IUserService;
import com.sun.org.apache.bcel.internal.generic.ARETURN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class LeadPaymentDetailServiceImpl implements ILeadPaymentDetailService {
    private final ILeadPaymentDetailDao leadPaymentDetailDao;
    private final IUserService userService;
    private final ILeadService leadService;
    private final IOfferService offerService;
    private final ICommonDocumentService commonDocumentService;
    private final IDocumentStageMasterService documentStageMasterService;

    @Autowired
    public LeadPaymentDetailServiceImpl(ILeadPaymentDetailDao leadPaymentDetailDao, IUserService userService, ILeadService leadService, IOfferService offerService, ICommonDocumentService commonDocumentService, IDocumentStageMasterService documentStageMasterService) {
        this.leadPaymentDetailDao = leadPaymentDetailDao;
        this.userService = userService;
        this.leadService = leadService;
        this.offerService = offerService;
        this.commonDocumentService = commonDocumentService;
        this.documentStageMasterService = documentStageMasterService;
    }

    @Override
    public GetLeadPaymentData getLeadPaymentData() {
        List<PaymentMode> paymentModes = PaymentMode.getPaymentTypes();
        List<User> users = userService.getAdmins(AdminType.FINANCE);

        return new GetLeadPaymentData(paymentModes, users);
    }

    @Override
    public LeadPaymentDetail saveLeadPaymentDetails(SaveLeadPaymentRequest request) throws SupplyException, JsonProcessingException {
        //save lead payment details
        LeadPaymentDetail data = getLeadPaymentDetailsFromRequest(request);
        LeadPaymentDetail savedData = leadPaymentDetailDao.saveLeadPaymentDetails(data);

        OfferStatus offerStatus;
        LeadStatus leadStatus;

        switch (request.getPaymentType()) {
            case TOKEN:
                offerStatus = OfferStatus.TOKEN_PAID;
                leadStatus = LeadStatus.TOKEN_PAID;
                break;
            case FULL_PAYMENT:
                offerStatus = OfferStatus.FULL_PAYMENT_DONE;
                leadStatus = LeadStatus.CAR_PROCURED;
                break;
            default:
                offerStatus = null;
                leadStatus = null;
        }
        //save lead payment documents
        if(request.getPaymentDocs() != null && request.getPaymentDocs().length > 0) {
            saveLeadPaymentDocuments(request, savedData.getId());
        }

        if(offerStatus != null) { //update offer status
            updateOfferStatus(request.getOfferID(), request.getLeadID(), offerStatus, request.getCreatedBy());
        }

        if(leadStatus != null) { //Update lead status
            updateLeadStatus(request.getLeadID(), request.getCreatedBy(), leadStatus);
        }
        return savedData;
    }

    private void updateLeadStatus(long leadID, long updatedBy, LeadStatus status) throws SupplyException, JsonProcessingException {
        UpdateLeadRequest updateLeadRequest = UpdateLeadRequest.builder()
                .id(leadID)
                .adminID(updatedBy)
                .leadStatus(status)
                .build();
        leadService.updateLead(updateLeadRequest);
    }

    @Override
    public LeadPaymentDetail proceedWithoutToken(ProceedWithoutTokenRequest request) throws SupplyException, JsonProcessingException {
        //save data
        LeadPaymentDetail data = getProceedWithoutTokenDetailsFromRequest(request);
        LeadPaymentDetail savedData = leadPaymentDetailDao.saveLeadPaymentDetails(data);

        //update offer status as TOKEN_PAID
        markOfferStatusAsTokenPaidForProceedWithoutToken(request);

        updateLeadStatus(request.getLeadID(), request.getCreatedBy(), LeadStatus.TOKEN_PAID);

        return data;
    }

    private void markOfferStatusAsTokenPaidForProceedWithoutToken(ProceedWithoutTokenRequest request) throws SupplyException, JsonProcessingException {
        UpdateOfferRequest data = UpdateOfferRequest.builder()
                .id(request.getOfferID())
                .leadID(request.getLeadID())
                .offerStatus(OfferStatus.TOKEN_PAID)
                .updatedBy(request.getCreatedBy())
                .build();
        offerService.updateOffer(data);
    }

    private LeadPaymentDetail getProceedWithoutTokenDetailsFromRequest(ProceedWithoutTokenRequest request) {
        LeadPaymentDetail detail =  LeadPaymentDetail.builder()
                .leadID(request.getLeadID())
                .amountPaid(0.0)
                .paymentType(PaymentType.TOKEN)
                .paymentMode(null)
                .referenceNumber(null)
                .paidBy(null)
                .isActive(true)
                .createdBy(request.getCreatedBy())
                .createdAt(LocalDateTime.now())
                .build();

        return detail;
    }

    private void saveLeadPaymentDocuments(SaveLeadPaymentRequest request, Long id) throws SupplyException {
        Long documentStageMasterID = documentStageMasterService.findIDByTypeAndStageAndisActive(DocumentType.LEAD_PAYMENT, DocumentStage.TOKEN_PAID);
        List<CommonDocument> commonDocuments = new ArrayList<>();

        for(String doc : request.getPaymentDocs()) {
            commonDocuments.add(
                    CommonDocument.builder()
                            .commonDocumentEntity(CommonDocumentEntity.LEAD_PAYMENT)
                            .entityID(id)
                            .documentStageMasterID(documentStageMasterID)
                            .createdBy(request.getCreatedBy())
                            .imageURL(doc)
                            .build()
            );
        }
        if(commonDocuments.size() > 0) {
            commonDocumentService.saveDocuments(commonDocuments);
        }
    }

    private void updateOfferStatus(long offerID, long leadID, OfferStatus status, long updatedBy) throws SupplyException, JsonProcessingException {
        UpdateOfferRequest data = UpdateOfferRequest.builder()
                .id(offerID)
                .leadID(leadID)
                .offerStatus(status)
                .updatedBy(updatedBy)
                .build();
        offerService.updateOffer(data);
    }

    private LeadPaymentDetail getLeadPaymentDetailsFromRequest(SaveLeadPaymentRequest request) {
        LeadPaymentDetail detail =  LeadPaymentDetail.builder()
                .leadID(request.getLeadID())
                .paymentType(request.getPaymentType())
                .amountPaid(request.getAmountPaid())
                .paymentMode(PaymentModeEnum.valueOf(String.valueOf(request.getPaymentMode())))
                .referenceNumber(request.getReferenceNumber())
                .paidBy(request.getPaidBy())
                .isActive(true)
                .createdBy(request.getCreatedBy())
                .createdAt(LocalDateTime.now())
                .build();

        if (request.getPaymentDocs() != null && request.getPaymentDocs().length > 0) {
            detail.setNoOfDocuments(request.getPaymentDocs().length);
        }
        else {
            detail.setNoOfDocuments(0);
        }

        return detail;
    }
}
