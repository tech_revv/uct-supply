package com.revv.uct.supply.service.lead.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revv.uct.supply.constants.InsuranceType;
import com.revv.uct.supply.constants.RegistrationType;
import com.revv.uct.supply.constants.SellerType;
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.controller.lead.model.LeadCreationData;
import com.revv.uct.supply.controller.lead.model.UpdateLeadRequest;
import com.revv.uct.supply.db.carModel.entity.CarModel;
import com.revv.uct.supply.db.enums.EditEntity;
import com.revv.uct.supply.db.enums.LeadRejectSubStatus;
import com.revv.uct.supply.db.enums.LeadStatus;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.lead.dao.ILeadDao;
import com.revv.uct.supply.db.lead.entity.Lead;
import com.revv.uct.supply.db.history.entity.History;
import com.revv.uct.supply.db.offer.entity.Offer;
import com.revv.uct.supply.db.seller.entity.Seller;
import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.carModel.ICarModelService;
import com.revv.uct.supply.service.inspection.IInspectionService;
import com.revv.uct.supply.service.lead.ILeadService;
import com.revv.uct.supply.service.history.IHistoryService;
import com.revv.uct.supply.service.offer.IOfferService;
import com.revv.uct.supply.service.seller.ISellerService;
import com.revv.uct.supply.service.serviceCity.IServiceCityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class LeadServiceImpl implements ILeadService
{
    private ILeadDao leadDao;
    private IServiceCityService serviceCityService;
    private ICarModelService carModelService;
    private IHistoryService historyService;
    private ISellerService sellerService;
    private IInspectionService inspectionService;
    private IOfferService offerService;

    public LeadServiceImpl(ILeadDao leadDao, IServiceCityService serviceCityService, ICarModelService carModelService, IHistoryService historyService, ISellerService sellerService, @Lazy IInspectionService inspectionService, @Lazy IOfferService offerService) {
        this.leadDao = leadDao;
        this.serviceCityService = serviceCityService;
        this.carModelService = carModelService;
        this.historyService = historyService;
        this.sellerService = sellerService;
        this.inspectionService = inspectionService;
        this.offerService = offerService;
    }

    @Override
    public Lead createLead(@Valid @NotNull CreateLeadRequest request)throws NullPointerException, SupplyException {
        //not required as of now
        /*
        if(leadDao.existsByRegistrationNumber(request.getRegistrationNumber())) {
            throw  new SupplyException("Lead already exists for registrationNumber: "+request.getRegistrationNumber());
        }
         */
        if(request.getSellerID() == null) {
            if(request.getSellerContactNo() == null) {
                throw new SupplyException("Seller contact no required");
            }
            if(request.getSellerContactNo().length() < 10) {
                throw new SupplyException("Invalid seller contact no");
            }

            List<Seller> sellerList = sellerService.findAllByContactNumberAndType(request.getSellerContactNo());
            System.out.println("sellerList==>>"+sellerList.size());
            if(sellerList != null && sellerList.size() > 0) {
                request.setSellerID(sellerList.get(0).getId());
                if(request.getServiceCityID() != sellerList.get(0).getServiceCityID()) {
                    throw new SupplyException("ServiceCity mismatch b/w seller & lead");
                }
            } else {
                Seller newSeller = sellerService.createSellerForB2CLeads(request);
                request.setSellerID(newSeller.getId());
            }
        }

        if(request.getSellerID() == null) {
            throw new SupplyException("SellerID not found");
        }

        if(request.getRegistrationNumber() != null) {
            String regNo = request.getRegistrationNumber();
            regNo = regNo.replaceAll("[^a-zA-Z0-9]", "");
            regNo = regNo.toUpperCase();
            System.out.println(regNo);
            request.setRegistrationNumber(regNo);
        }

        return leadDao.createLead(getLeadDataFromRequest(request));
    }

    private Lead getLeadDataFromRequest(CreateLeadRequest request) {
        return Lead.builder()
                .adminID(request.getAdminID())
                .carModelID(request.getCarModelID())
                .sellerID(request.getSellerID())
                .registrationNumber(request.getRegistrationNumber())
                .carColour(request.getCarColour())
                .registrationTimestamp(request.getRegistrationTimestamp())
                .manufacturingTimestamp(request.getManufacturingTimestamp())
                .insuranceValidity(request.getInsuranceValidity())
                .insuranceType(request.getInsuranceType())
                .rto(request.getRto())
                .registrationType(request.getRegistrationType())
                .ownershipSerial(request.getOwnershipSerial())
                .serviceCityID(request.getServiceCityID())
                .comments(request.getComments())
                .expOfferPrice(request.getExpOfferPrice())
                .kmsDriven(request.getKmsDriven())
                .leadStatus(LeadStatus.CREATED)
                .isActive(true)
                .build();
    }

    @Override
    public List<LeadCreationData> getLeadCreationData() {
        List<ServiceCity> city = serviceCityService.findAllServiceCities();
        log.info("city", city);
        List<CarModel> carModel = carModelService.findAllCarModels();
        log.info("carModel", String.valueOf(carModel));
        //String[] registrationType = registrationTypes;
        List<RegistrationType> registrationType = RegistrationType.getRegistrationTypes();
        log.info("registrationType"+String.valueOf(registrationType));
        List<SellerType> sellerType = SellerType.getSellerTypes();
        log.info("sellerType", String.valueOf(sellerType));
        List<InsuranceType> insuranceType = InsuranceType.getInsuranceType();
        log.info("insuranceType", String.valueOf(insuranceType));

        List<LeadCreationData> list = new ArrayList<>();
        list.add(new LeadCreationData(city, carModel, registrationType, sellerType, insuranceType));

        return list;
    }

    @Override
    public Lead updateLead(UpdateLeadRequest request) throws SupplyException, JsonProcessingException {
        log.info("Update lead impl");
        Lead oldData = leadDao.findByIdAndIsActive(request.getId(), true);
        log.info("oldData ={}", oldData);

        //if data doesn't exist for given leadID
        if(oldData == null){
            throw new SupplyException("Lead data doesn't exist for leadID: "+request.getId());
        }

        ObjectMapper mapper = new ObjectMapper();
        String oldDataJson = mapper.writeValueAsString(oldData);
        log.info("oldDataJson ={}", oldDataJson);

        Lead newData = oldData;


        if(oldData.getLeadStatus().equals(LeadStatus.REJECTED)) {
            if(request.getRegistrationNumber() != null  ||
                    request.getCarColour() != null ||
                    request.getRegistrationTimestamp() != null || request.getManufacturingTimestamp() != null ||
                    request.getComments() != null ||
                    request.getExpOfferPrice() != null ||
                    request.getRegistrationNumber() != null) {
                throw new SupplyException("Only leadStatus can be updated for REJECTED lead");
            }
            newData.setRejectionReason(null);
            newData.setLeadRejectSubStatus(null);
        }

        //if lead is rejected then rejectSubStatus and rejection reason is mandatory
        if(request.getLeadStatus() != null) {
            if(request.getLeadStatus().equals(LeadStatus.REJECTED)) {
                if(request.getLeadRejectSubStatus() == null) {
                    throw new SupplyException("RejectionSubStatus required for rejected lead.");
                }
                if(request.getRejectionReason() == null || request.getRejectionReason().length() <= 0) {
                    throw new SupplyException("Rejection reason required for rejected lead.");
                }
                newData.setLeadRejectSubStatus(LeadRejectSubStatus.valueOf(String.valueOf(request.getLeadRejectSubStatus())));
                newData.setRejectionReason(request.getRejectionReason());
            }
            newData.setLeadStatus(LeadStatus.valueOf(String.valueOf(request.getLeadStatus())));
        }

        if(request.getRegistrationNumber() != null && request.getRegistrationNumber().length() > 0) {
            String regNo = request.getRegistrationNumber();
            regNo = regNo.replaceAll("[^a-zA-Z0-9]", "");
            regNo = regNo.toUpperCase();
            System.out.println(regNo);
            //newData.setRegistrationNumber(request.getRegistrationNumber());
            newData.setRegistrationNumber(regNo);
        }
        if(request.getCarModelID() != null) {
            newData.setCarModelID(request.getCarModelID());
        }
        if(request.getSellerID() != null) {
            newData.setSellerID(request.getSellerID());
        }
        if(request.getInsuranceValidity() != null) {
            newData.setInsuranceValidity(request.getInsuranceValidity());
        }
        if(request.getInsuranceType() != null) {
            newData.setInsuranceType(request.getInsuranceType().toString());
        }
        if(request.getRto() != null) {
            newData.setRto(request.getRto());
        }
        if(request.getRegistrationType() != null) {
            newData.setRegistrationType(request.getRegistrationType());
        }
        if(request.getOwnershipSerial() != null) {
            newData.setOwnershipSerial(request.getOwnershipSerial());
        }
        if(request.getServiceCityID() != null) {
            newData.setServiceCityID(request.getServiceCityID());
        }
        if(request.getKmsDriven() != null) {
            newData.setKmsDriven(request.getKmsDriven());
        }
        if(request.getCarColour() != null && request.getCarColour().length() > 0) {
            newData.setCarColour(request.getCarColour());
        }
        if(request.getRegistrationTimestamp() != null) {
            newData.setRegistrationTimestamp(request.getRegistrationTimestamp());
        }
        if(request.getManufacturingTimestamp() != null) {
            newData.setManufacturingTimestamp(request.getManufacturingTimestamp());
        }
        if(request.getComments() != null && request.getComments().length() > 0) {
            newData.setComments(request.getComments());
        }
        if(request.getExpOfferPrice() != null && request.getExpOfferPrice() > 0) {
            newData.setExpOfferPrice(request.getExpOfferPrice());
        }
        if(request.getHandoverDate() != null) {
            newData.setHandoverDate(request.getHandoverDate());
        }
        if(request.getPickupBy() != null) {
            newData.setPickupBy(request.getPickupBy());
        }
        newData.setUpdatedAt(LocalDateTime.now());
        log.info("newData ={}", newData);

        String newDataJson = mapper.writeValueAsString(newData);
        log.info("newDataJson ={}", newDataJson);

        Lead updatedLead = leadDao.updateLead(newData);

        //save lead history
        History savedLeadHistory = historyService.saveHistory(request.getAdminID(), request.getId(), EditEntity.LEAD, oldDataJson, newDataJson);

        return updatedLead;
    }

    @Override
    public boolean existByIdAndIsActive(Long leadID, boolean b) {
        return leadDao.existByIdAndIsActive(leadID, b);
    }

    @Override
    public Long getAllLeadsCount() {
        return leadDao.getAllLeadsCount();
    }

    @Override
    public Lead getLeadDetails(Long leadID) throws UCTSupplyException {
        Lead lead = leadDao.findByIdAndIsActive(leadID, true);
        log.info("lead details = {}", lead);

        //if data doesn't exist for given leadID
        if(lead == null){
            throw new UCTSupplyException("Lead data doesn't exist for leadID: "+leadID, HttpStatus.BAD_REQUEST);
        }
        return lead;
    }

    @Override
    public Lead createOnlineLead(CreateLeadRequest request) throws SupplyException {
        //generate lead
        if(request.getRegistrationNumber() != null) {
            String regNo = request.getRegistrationNumber();
            regNo = regNo.replaceAll("[^a-zA-Z0-9]", "");
            regNo = regNo.toUpperCase();
            System.out.println(regNo);
            request.setRegistrationNumber(regNo);
        }

        if(request.getOwnershipSerial() == null || request.getOwnershipSerial().length() <= 0) {
            throw new SupplyException("Ownership serial number is mandatory");
        }

        if(request.getOfferedPrice() == null) {
            throw new SupplyException("Offer price is mandatory");
        }

        if(request.getRefubCost() == null) {
            throw new SupplyException("Refurb cost is mandatory");
        }

        if(request.getBasePrice() == null) {
            throw new SupplyException("Base price is mandatory");
        }

        Lead onlineLead = leadDao.createLead(getOnlineLeadDataFromRequest(request));

        //generate E1 inspection
        Inspection onlineLeadE1 = inspectionService.saveE1InspectionForOnlineLead(request, onlineLead);

        //generate offer
        Offer onlineLeadOffer = offerService.saveOfferForOnlineLead(request, onlineLead, onlineLeadE1);

        return onlineLead;
    }

    private Lead getOnlineLeadDataFromRequest(CreateLeadRequest request) {
        return Lead.builder()
                .adminID(request.getAdminID())
                .sellerID(request.getSellerID())
                .carModelID(request.getCarModelID())
                .registrationTimestamp(request.getRegistrationTimestamp())
                .insuranceType(request.getInsuranceType())
                .insuranceValidity(request.getInsuranceValidity())
                .carColour(request.getCarColour())
                .registrationType(request.getRegistrationType())
                .rto(request.getRto())
                .ownershipSerial(request.getOwnershipSerial())
                .isActive(true)
                .createdAt(LocalDateTime.now())
                .leadStatus(LeadStatus.OFFER_GENERATED)
                .build();
    }

    private Object getLeadUpdateDataFromRequest(UpdateLeadRequest request) {
        return Lead.builder()
                .id(request.getId())
                .adminID(request.getAdminID())
                .registrationNumber(request.getRegistrationNumber())
                .carColour(request.getCarColour())
                .registrationTimestamp(request.getRegistrationTimestamp())
                .manufacturingTimestamp(request.getManufacturingTimestamp())
                .comments(request.getComments())
                .expOfferPrice(request.getExpOfferPrice())
                .leadStatus(LeadStatus.valueOf(String.valueOf(request.getLeadStatus())))
                .rejectionReason(request.getRejectionReason())
                .updatedAt(LocalDateTime.now())
                .build();
    }

}
