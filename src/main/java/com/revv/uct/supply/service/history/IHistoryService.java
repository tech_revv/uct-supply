package com.revv.uct.supply.service.history;

import com.revv.uct.supply.db.enums.EditEntity;
import com.revv.uct.supply.db.history.entity.History;

public interface IHistoryService {
    History saveHistory(Long adminID, Long editEntityID, EditEntity entity, String oldDataJson, String newDataJson);

    History getLastHistoricalData(Long entityID, EditEntity entity);
}
