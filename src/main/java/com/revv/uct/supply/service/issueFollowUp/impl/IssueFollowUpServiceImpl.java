package com.revv.uct.supply.service.issueFollowUp.impl;

import com.revv.uct.supply.db.issueFollowUp.dao.IIssueFollowUpDao;
import com.revv.uct.supply.db.issueFollowUp.entity.IssueFollowUp;
import com.revv.uct.supply.service.issueFollowUp.IIssueFollowUpService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IssueFollowUpServiceImpl implements IIssueFollowUpService {
    private final IIssueFollowUpDao issueFollowUpDao;

    @Autowired
    public IssueFollowUpServiceImpl(IIssueFollowUpDao issueFollowUpDao) {
        this.issueFollowUpDao = issueFollowUpDao;
    }

    @Override
    public IssueFollowUp getIssueFollowUpFromrequest(Long savedSubPartIssueID, JSONObject jsonObject, Long jobID, int newIssueFollowUpversion) {
        return IssueFollowUp.builder()
                .subPartIssueID(savedSubPartIssueID)
                .jobName((String) jsonObject.get("jobName"))
                .jobID(jobID)
                .versionNumber(newIssueFollowUpversion)
                .isActive(true)
                .build();
    }

    @Override
    public IssueFollowUp uploadIssueFollowUp(IssueFollowUp issueFollowUp) {
        IssueFollowUp addedIssueFollowUp = issueFollowUpDao.uploadIssueFollowUp(issueFollowUp);
        return addedIssueFollowUp;
    }
}
