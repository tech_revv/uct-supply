package com.revv.uct.supply.service.subPartIssues;

import com.revv.uct.supply.db.subPartIssues.entity.SubPartIssues;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;

import java.util.List;

public interface ISubPartIssuesService {
    List<SubPartIssues> uploadSubPartIssues(List<Object> list) throws SupplyException, EntityNotFoundException;

    List<SubPartIssues> getDefaultIssues();
}
