package com.revv.uct.supply.service.offerImage.impl;

import com.revv.uct.supply.db.offerImage.dao.IOfferImageDao;
import com.revv.uct.supply.db.offerImage.entity.OfferImage;
import com.revv.uct.supply.service.offerImage.IOfferImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class OfferImageServiceImpl implements IOfferImageService {
    private IOfferImageDao offerImageDao;

    @Autowired
    public OfferImageServiceImpl(IOfferImageDao offerImageDao) {
        this.offerImageDao = offerImageDao;
    }

    @Override
    public void markOfferImagesInActive(Long existingOfferID, Long createdBy) {
        offerImageDao.markOfferImagesInActive(existingOfferID, createdBy);
    }

    @Override
    public void saveOfferImages(Long offerID, Long createdBy, String[] serviceDocs) {
        for(String imageURL : serviceDocs) {
            offerImageDao.saveOfferImage(getOfferImageDataFromRequest(offerID, createdBy, imageURL));
        }
    }

    private OfferImage getOfferImageDataFromRequest(Long offerID, Long createdBy, String imageURL) {
        return OfferImage.builder()
                .offerID(offerID)
                .createdBy(createdBy)
                .imageURL(imageURL)
                .createdAt(LocalDateTime.now())
                .isActive(true)
                .build();
    }
}
