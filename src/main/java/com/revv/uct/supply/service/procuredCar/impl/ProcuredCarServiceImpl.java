package com.revv.uct.supply.service.procuredCar.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revv.uct.common.notification.Publisher;
import com.revv.uct.common.notification.model.EmailDetails;
import com.revv.uct.supply.controller.procuredCar.model.ReportIssueRequest;
import com.revv.uct.supply.controller.procuredCar.model.SaveProcuredCarRequest;
import com.revv.uct.supply.controller.procuredCar.model.UpdateProcuredCarRequest;
import com.revv.uct.supply.db.enums.EditEntity;
import com.revv.uct.supply.db.enums.ProcuredCarStatus;
import com.revv.uct.supply.db.history.entity.History;
import com.revv.uct.supply.db.procuredCar.dao.IProcuredCarDAO;
import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.history.IHistoryService;
import com.revv.uct.supply.service.procuredCar.IProcuredCarService;
import com.revv.uct.supply.util.templates.ITemplates;
import com.revv.uct.supply.util.templates.model.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ProcuredCarServiceImpl implements IProcuredCarService {
    private final IProcuredCarDAO procuredCarDAO;
    private final IHistoryService historyService;
    private final ITemplates templates;

    @Autowired
    public ProcuredCarServiceImpl(IProcuredCarDAO procuredCarDAO, IHistoryService historyService, ITemplates templates) {
        this.procuredCarDAO = procuredCarDAO;
        this.historyService = historyService;
        this.templates = templates;
    }

    @Override
    public ProcuredCar saveProcuredCarDetails(SaveProcuredCarRequest request) {
        ProcuredCar procuredCar = getProcuredCarDetailsFromRequest(request);
        return procuredCarDAO.saveProcuredCarDetails(procuredCar);
    }

    @Override
    public ProcuredCar updateProcuredCarDetails(UpdateProcuredCarRequest request) throws JsonProcessingException {
        log.info("Update Procured Car impl");

        ProcuredCar oldData = procuredCarDAO.findByIdAndIsActive(request.getId(), true);
        log.info("oldData ={}", oldData);

        ObjectMapper mapper = new ObjectMapper();
        String oldDataJson = mapper.writeValueAsString(oldData);
        log.info("oldDataJson ={}", oldDataJson);

        ProcuredCar newData = oldData;

        if(request.getStatus() != null) {
            //change status
            newData.setStatus(request.getStatus());

            //reset issue
            newData.setIssueReported(false);
            newData.setIssue(null);
            newData.setIssueStage(null);
        }

        newData.setUpdatedBy(request.getUpdatedBy());
        newData.setUpdatedAt(LocalDateTime.now());
        log.info("newData ={}", newData);

        String newDataJson = mapper.writeValueAsString(newData);
        log.info("newDataJson ={}", newDataJson);

        ProcuredCar updatedPC = procuredCarDAO.updateProcuredCar(newData);

        //save history
        History savedLeadHistory = historyService.saveHistory(request.getUpdatedBy(), request.getId(), EditEntity.PROCURED_CAR, oldDataJson, newDataJson);

        return updatedPC;
    }

    @Override
    public ProcuredCar reportIssue(ReportIssueRequest request) throws JsonProcessingException, SupplyException {
        log.info("Report issue Procured Car impl");

        ProcuredCar oldData = procuredCarDAO.findByIdAndIsActive(request.getId(), true);
        log.info("oldData ={}", oldData);

        ObjectMapper mapper = new ObjectMapper();
        String oldDataJson = mapper.writeValueAsString(oldData);
        log.info("oldDataJson ={}", oldDataJson);

        ProcuredCar newData = oldData;

        if(request.getIssue() != null) {
            if(request.getIssueStage() == null) {
                throw new SupplyException("Issue stage is required");
            }
            newData.setIssueReported(true);
            newData.setIssue(request.getIssue());
            newData.setIssueStage(request.getIssueStage());
        }

        newData.setUpdatedAt(LocalDateTime.now());
        log.info("newData ={}", newData);

        String newDataJson = mapper.writeValueAsString(newData);
        log.info("newDataJson ={}", newDataJson);

        ProcuredCar updatedPC = procuredCarDAO.updateProcuredCar(newData);

        //save history
        History savedLeadHistory = historyService.saveHistory(request.getUpdatedBy(), request.getId(), EditEntity.PROCURED_CAR, oldDataJson, newDataJson);

        sendReportIssueNotification(updatedPC);
        
        return updatedPC;
    }

    @Override
    public ProcuredCar getProcuredCarDetails(Long leadID) {
        return procuredCarDAO.findByLeadIDAndIsActive(leadID, true);
    }

    @Override
    public void updateProcuredCarStatusFromLeadID(Long leadID, Long updatedBy, ProcuredCarStatus status) throws SupplyException, JsonProcessingException {
        ProcuredCar procuredCar = procuredCarDAO.findByLeadIDAndIsActive(leadID, true);

        if(procuredCar == null) {
            throw new SupplyException("Car entry not found");
        }

        UpdateProcuredCarRequest updateProcuredCarRequest = UpdateProcuredCarRequest.builder()
                .id(procuredCar.getId())
                .status(status)
                .updatedBy(updatedBy)
                .build();

        updateProcuredCarDetails(updateProcuredCarRequest);
    }

    private void sendReportIssueNotification(ProcuredCar updatedPC) {
//        String subject = Templates.report_issue_email_subject;
//        String mailContent = Templates.report_issue_email_text(updatedPC);

        Template template = templates.reportIssuetemplate(updatedPC);

        String subject = template.getEmailSubject();
        String mailContent = template.getEmailContent();

        List<String> emails = new ArrayList<>();
        emails.add("ashish.singh@revv.co.in");
//        emails.add("supply.uct@revv.co.in");
        emails.add("sudhanshu.mishra@revv.co.in");

        List<String> bcc = new ArrayList<>();
        bcc.add("ashish.singh@revv.co.in");

        EmailDetails emailDetails = EmailDetails.builder()
                .subject(subject)
                .mailContent(mailContent)
                .emails(emails)
                .bcc(bcc)
                .build();

        try {
            Publisher.publish(emailDetails, null, "ISSUE_REPORTED");
        } catch (Exception e) {
            log.info("Exception while sending notification");
        }
    }

    private ProcuredCar getProcuredCarDetailsFromRequest(SaveProcuredCarRequest request) {
        return ProcuredCar.builder()
                .leadID(request.getLeadID())
                .carModelID(request.getCarModelID())
                .offerID(request.getOfferID())
                .onBoardedDate(request.getOnBoardedDate())
                .status(ProcuredCarStatus.ON_BOARDED)
                .createdAt(LocalDateTime.now())
                .createdBy(request.getCreatedBy())
                .isActive(true)
                .build();
    }
}
