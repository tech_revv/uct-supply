package com.revv.uct.supply.service.jobPriceMap;

import com.revv.uct.supply.db.jobPriceMap.entity.JobPriceMap;
import com.revv.uct.supply.service.jobPriceMap.model.CheckJobPriceMap;
import org.json.simple.JSONObject;

import java.util.List;

public interface IJobPriceMapService {
    JobPriceMap getJobPriceMapFromRequest(JSONObject jsonObject, Long jobID, int newJobPriceMapVersion);

    JobPriceMap save(JobPriceMap jobPriceMap);

    List<JobPriceMap> findAll();

    List<CheckJobPriceMap> getAllByIsActive(boolean b);

    CheckJobPriceMap getCheckJobPriceMapFromrequest(Long jobID, JSONObject obj);

    List<JobPriceMap> getJobPrice(List<Long> jobIDs);

    List<JobPriceMap> getAllJobPrice();

    List<JobPriceMap> findAllByIsActive(boolean b);
}
