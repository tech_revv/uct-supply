package com.revv.uct.supply.service.inspectionReport.impl;

import com.revv.uct.supply.db.inspectionReport.dao.IInspectionReportDAO;
import com.revv.uct.supply.db.inspectionReport.entity.InspectionReport;
import com.revv.uct.supply.service.inspection.model.InspectionIssue;
import com.revv.uct.supply.service.inspectionReport.IInspectionReportService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class InspectionReportServiceImpl implements IInspectionReportService {
    @Autowired
    IInspectionReportDAO inspectionReportDAO;

    @Override
    public List<InspectionReport> getInspectionReport(Long inspectionID) {
        return inspectionReportDAO.getInspectionReport(inspectionID);
    }

    @Override
    public void markReportsInactive(Long inspectorID, Long inspectionID) {
        inspectionReportDAO.markReportsInactive(inspectorID, inspectionID);
    }

    @Override
    public void saveInspectionReport(Long inspectorID, Long inspectionID, Long subPartID, List<InspectionIssue> issues, String comments) {
        List<InspectionReport> inspectionReports = new ArrayList<>();
        for(InspectionIssue issue: issues) {
            inspectionReports.add(
              InspectionReport.builder()
                      .inspectionID(inspectionID)
                      .subPartID(subPartID)
                      .subPartIssueID(issue.getIssueID())
                      .issueName(issue.getIssueName())
                      .comments(comments)
                      .selected(issue.getSelected())
                      .createdBy(inspectorID)
                      .build()
            );
        }
        if(inspectionReports.size() > 0) {
            inspectionReportDAO.saveInspectionReport(inspectionReports);
        }
    }

    @Override
    public List<InspectionReport> getInspectionReportByInspectionIDs(List<Long> inspectionIDs) {
        return inspectionReportDAO.getInspectionReportByInspectionIDs(inspectionIDs,true);
    }
}
