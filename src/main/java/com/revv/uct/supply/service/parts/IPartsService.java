package com.revv.uct.supply.service.parts;

import com.revv.uct.supply.db.parts.entity.Parts;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;

import java.util.List;

public interface IPartsService {
    List<Parts> uploadParts(List<Object> list) throws SupplyException, EntityNotFoundException;

    boolean existsByPartNameAndIsActive(String partName, boolean isActive);

    Parts findByPartNameAndVersionNumber(String partName, int partVersion);

    Parts findByPartNameAndIsActive(String partName, boolean isActive);
}
