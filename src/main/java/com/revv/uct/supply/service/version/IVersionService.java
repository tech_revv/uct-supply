package com.revv.uct.supply.service.version;

import com.revv.uct.supply.db.version.entity.Version;

public interface IVersionService {
    int getCurrentVersion(String versionType);

    void disableCurrentVersion(String versionType, int version);

    Version addNewVersion(String versionType, int newVersion, boolean enabled);
}
