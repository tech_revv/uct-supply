package com.revv.uct.supply.service.carModel.impl;

import com.revv.uct.supply.db.carModel.dao.ICarModelDao;
import com.revv.uct.supply.db.carModel.entity.CarModel;
import com.revv.uct.supply.db.enums.Transmission;
import com.revv.uct.supply.service.carModel.ICarModelService;
import com.revv.uct.supply.service.version.IVersionService;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CarModelServiceImpl implements ICarModelService {

    private final ICarModelDao carModelDao;
    private final IVersionService versionService;

    @Autowired
    public CarModelServiceImpl(ICarModelDao carModelDao, IVersionService versionService) {
        this.carModelDao = carModelDao;
        this.versionService = versionService;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<CarModel> uploadCarModels(List<Object> list) {
        //get current version
        int version = versionService.getCurrentVersion("CarModel");

        int newVersion = version + 1;

        //disable current version
        versionService.disableCurrentVersion("CarModel", version);

        //add new version
        versionService.addNewVersion("CarModel", newVersion, true);

        //upload new car models
        List<CarModel> result = new ArrayList<>();
        Iterator<Object> it = list.iterator();
        while (it.hasNext()) {
            JSONObject jsonObject = new JSONObject((Map) it.next());
            CarModel carModel = getCarModelFromRequest(jsonObject, newVersion);
            CarModel savedCarModel = carModelDao.uploadCarModel(carModel);
            result.add(savedCarModel);
        }
        return result;
    }

    @Override
    public List<CarModel> findAllCarModels() {
        return carModelDao.findAllCarModels();
    }

    private CarModel getCarModelFromRequest(JSONObject jsonObject, int newVersion) {
        CarModel carmodel = CarModel.builder()
                .make((String) jsonObject.get("make"))
                .model((String) jsonObject.get("model"))
                .variant((String) jsonObject.get("variant"))
                .fuel((String) jsonObject.get("fuel"))
                .transmission(Transmission.valueOf((String) jsonObject.get("transmission")))
                .bodyType((String) jsonObject.get("bodyType"))
                .seats((int) jsonObject.get("seats"))
                .startYear((int) jsonObject.get("startYear"))
                .price(Double.valueOf( jsonObject.get("price")+""))
                .versionNumber(newVersion)
                .isActive(true)
                .build();
        if(jsonObject.get("endYear") != null) {
            carmodel.setEndYear((Integer) jsonObject.get("endYear"));
        }

        return carmodel;
    }
}
