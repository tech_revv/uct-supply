package com.revv.uct.supply.service.subParts;

import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;

import java.util.List;

public interface ISubPartsService {
    List<SubParts> uploadSubParts(List<Object> list) throws SupplyException, EntityNotFoundException;

    boolean existsBySubPartNameAndIsActive(String subPartName, boolean isActive);

    SubParts findBySubPartNameAndIsActive(String subPartName, boolean b);

    List<SubParts> findAllByIsActive(boolean b);

    List<SubParts> findAllByID(List<Long> subPartIds);
}
