package com.revv.uct.supply.service.inspection.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InspectionChecklist {
    private String headerName;
    private Long headerID;
    @Builder.Default
    private List<InspectionSubpartList> subPartList = new ArrayList<>();
}
