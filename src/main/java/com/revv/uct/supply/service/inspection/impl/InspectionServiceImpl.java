package com.revv.uct.supply.service.inspection.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revv.uct.common.notification.Publisher;
import com.revv.uct.common.notification.model.TextDetails;
import com.revv.uct.supply.constants.APIConstants;
import com.revv.uct.supply.controller.inspection.model.*;
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.controller.lead.model.UpdateLeadRequest;
import com.revv.uct.supply.controller.offer.model.UpdateOfferRequest;
import com.revv.uct.supply.controller.procuredCar.model.UpdateProcuredCarRequest;
import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.documentStageMaster.entity.DocumentStageMaster;
import com.revv.uct.supply.db.enums.*;
import com.revv.uct.supply.db.history.entity.History;
import com.revv.uct.supply.db.inspection.dao.IInspectionDAO;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.inspectionImage.entity.InspectionImage;
import com.revv.uct.supply.db.inspectionJob.entity.InspectionJob;
import com.revv.uct.supply.db.inspectionReport.entity.InspectionReport;
import com.revv.uct.supply.db.inspectionSubPart.entity.InspectionSubPart;
import com.revv.uct.supply.db.jobPriceMap.entity.JobPriceMap;
import com.revv.uct.supply.db.jobs.entity.Jobs;
import com.revv.uct.supply.db.lead.entity.Lead;
import com.revv.uct.supply.db.offer.entity.Offer;
import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;
import com.revv.uct.supply.db.refurbishment.entity.Refurbishment;
import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;
import com.revv.uct.supply.db.subPartIssues.entity.SubPartIssues;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.db.user.entity.User;
import com.revv.uct.supply.db.workshop.entity.Workshop;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.commonDocument.ICommonDocumentService;
import com.revv.uct.supply.service.documentStageMaster.IDocumentStageMasterService;
import com.revv.uct.supply.service.history.IHistoryService;
import com.revv.uct.supply.service.inspection.IInspectionService;
import com.revv.uct.supply.service.inspection.model.*;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.*;
import com.revv.uct.supply.service.inspectionImage.IInspectionImageService;
import com.revv.uct.supply.service.inspectionJob.IInspectionJobService;
import com.revv.uct.supply.service.inspectionReport.IInspectionReportService;
import com.revv.uct.supply.service.inspectionSubPart.IInspectionSubPartService;
import com.revv.uct.supply.service.jobPriceMap.IJobPriceMapService;
import com.revv.uct.supply.service.jobs.IJobsService;
import com.revv.uct.supply.service.lead.ILeadService;
import com.revv.uct.supply.service.offer.IOfferService;
import com.revv.uct.supply.service.procuredCar.IProcuredCarService;
import com.revv.uct.supply.service.refurbishment.IRefurbishmentService;
import com.revv.uct.supply.service.refurbishmentJob.IRefurbishmentJobService;
import com.revv.uct.supply.service.subPartIssues.ISubPartIssuesService;
import com.revv.uct.supply.service.subParts.ISubPartsService;
import com.revv.uct.supply.service.user.IUserService;
import com.revv.uct.supply.service.workshop.IWorkshopService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.revv.uct.supply.constants.APIConstants.NFI_ISSUE_ID;
import static com.revv.uct.supply.constants.ResponseMessage.inspector_does_no_exist;
import static com.revv.uct.supply.constants.ResponseMessage.lead_does_not_exist;
import static com.revv.uct.supply.db.enums.InspectionStatus.*;
import static com.revv.uct.supply.db.enums.InspectionType.*;

@Service
@Slf4j
@NoArgsConstructor
public class InspectionServiceImpl implements IInspectionService {

    private IInspectionDAO inspectionDAO;
    private IInspectionSubPartService inspectionSubPartService;
    private IInspectionReportService inspectionReportService;
    private IInspectionJobService inspectionJobService;
    private IJobsService jobsService;
    private IJobPriceMapService jobPriceMapService;
    private IInspectionImageService inspectionImageService;
    private ISubPartIssuesService subPartIssuesService;
    private ISubPartsService subPartsService;
    private ILeadService leadService;
    private IUserService userService;
    private IHistoryService historyService;
    private IDocumentStageMasterService documentStageMasterService;
    private ICommonDocumentService commonDocumentService;
    private IOfferService offerService;
    private IRefurbishmentService refurbishmentService;
    private IProcuredCarService procuredCarService;
    private IRefurbishmentJobService refurbishmentJobService;
    private IWorkshopService workshopService;

    @Autowired
    public InspectionServiceImpl(IInspectionDAO inspectionDAO, IInspectionSubPartService inspectionSubPartService,
                                 IInspectionReportService inspectionReportService, IInspectionJobService inspectionJobService,
                                 IJobsService jobsService, IJobPriceMapService jobPriceMapService,
                                 IInspectionImageService inspectionImageService, ISubPartIssuesService subPartIssuesService,
                                 ISubPartsService subPartsService, ILeadService leadService, IUserService userService,
                                 IHistoryService historyService, IDocumentStageMasterService documentStageMasterService,
                                 ICommonDocumentService commonDocumentService, IOfferService offerService,
                                 IRefurbishmentService refurbishmentService, IProcuredCarService procuredCarService,
                                 IRefurbishmentJobService refurbishmentJobService, IWorkshopService workshopService) {
        this.inspectionDAO = inspectionDAO;
        this.inspectionSubPartService = inspectionSubPartService;
        this.inspectionReportService = inspectionReportService;
        this.inspectionJobService = inspectionJobService;
        this.jobsService = jobsService;
        this.jobPriceMapService = jobPriceMapService;
        this.inspectionImageService = inspectionImageService;
        this.subPartIssuesService = subPartIssuesService;
        this.subPartsService = subPartsService;
        this.leadService = leadService;
        this.userService = userService;
        this.historyService = historyService;
        this.documentStageMasterService = documentStageMasterService;
        this.commonDocumentService = commonDocumentService;
        this.offerService = offerService;
        this.refurbishmentService = refurbishmentService;
        this.procuredCarService = procuredCarService;
        this.refurbishmentJobService = refurbishmentJobService;
        this.workshopService = workshopService;
    }

    @Override
    public List<InspectionListHeader> getInspectionList(Long inspectorID) {
        List<InspectionDetail> inspectionDetails = new ArrayList<>();

        List<InspectionStatus> statusList= Arrays.asList(SCHEDULED, COMPLETED);
        List<InspectionType> inspectionTypes = Arrays.asList(E1,E2,E3);
        List<InspectionDetailNormal> inspectionDetailsNormal = inspectionDAO.findInspectionsByInspectionTypeAndStatus(inspectorID, statusList, inspectionTypes); //get Inspections
        log.info("inspectionDetailsNormal "+inspectionDetailsNormal);
        inspectionDetails.addAll(inspectionDetailsNormal);

        List<PostRefurbList> inspectionDetailsPostRefurbishment = getInspectionListPostRefurbishment(inspectorID, statusList);
        log.info("inspectionDetailsPostRefurbishment "+inspectionDetailsPostRefurbishment);
        inspectionDetails.addAll(inspectionDetailsPostRefurbishment);

        List<InspectionStatus> refurbStatusList= Arrays.asList(SCHEDULED);
        List<RefurbList> refurbDetails = refurbishmentService.findRefurbishments(inspectorID, refurbStatusList); //get refurbs
        log.info("refurbDetails "+refurbDetails);
        inspectionDetails.addAll(refurbDetails);

        inspectionDetails.sort(Comparator.comparing(InspectionDetail::getInspectionTime));

        InspectionListHeader scheduledInspections = getScheduledInspections(inspectionDetails);

        InspectionListHeader pastInspections = getPastInspections(inspectionDetails);

        return Arrays.asList(scheduledInspections, pastInspections);
    }

    private List<PostRefurbList> getInspectionListPostRefurbishment(Long inspectorID, List<InspectionStatus> statusList) {
        List<InspectionType> inspectionTypesPostRefurbishments = Arrays.asList(E4A, E4B);
        //get all inspections
        List<PostRefurbList> inspections = inspectionDAO.findInspectionsByInspectionTypeAndStatusPostRefurbishment(inspectorID, statusList, inspectionTypesPostRefurbishments); //get Inspections post refurbishments
        if(inspections.size() == 0) {
            return inspections;
        }

        List<Long> carIDs = inspections.stream().map(PostRefurbList::getCarID).collect(Collectors.toList());

        List<InspectionStatus> refurbishmentStatus = Arrays.asList(COMPLETED);
        //get all refurbs completed for carIDs
        List<Refurbishment> refurbishments = refurbishmentService.findRefurbishmentsByCarIDAndStatus(carIDs, refurbishmentStatus);
        List<Long> workshopIDs = new ArrayList<>();
        List<Long> refurbIDs = new ArrayList<>();
        List<Long> refurbSpocIDs = new ArrayList<>();
        for (Refurbishment refurbishment : refurbishments) {
            workshopIDs.add(refurbishment.getWorkshopID());
            refurbIDs.add(refurbishment.getId());
            refurbSpocIDs.add(refurbishment.getRefurbSpocID());
        }

        //get all workshop data
        List<Workshop> workshopDetails = workshopService.findAllByWorkshopID(workshopIDs);
        HashMap<Long, Workshop> mappedWorkshopData = new HashMap<>();
        workshopDetails.forEach(workshop -> {
            mappedWorkshopData.put(workshop.getId(), workshop);
        });

        //get all user=>refurbSpoc data
        List<User> refurbSpocs = userService.findAllByID(refurbSpocIDs);
        HashMap<Long, User> mappedRefurbData = new HashMap<>();
        refurbSpocs.forEach(refurbSpoc -> {
            mappedRefurbData.put(refurbSpoc.getId(), refurbSpoc);
        });

        //get all selected refurbishmentJobs for all completed refurbishments
        List<RefurbishmentJob> refurbishmentJobs = refurbishmentJobService.getRefurbJobList(refurbIDs);
        refurbishmentJobs = refurbishmentJobs.stream().filter(RefurbishmentJob::getSelected).collect(Collectors.toList());

        //map workshop data, refurb spoc data and jobs count for all inspections
        constructInspectionDataForPostRefurbishment(inspections, refurbishments, refurbishmentJobs, mappedWorkshopData, mappedRefurbData);

        return inspections;
    }

    private void constructInspectionDataForPostRefurbishment(List<PostRefurbList> inspections, List<Refurbishment> refurbishments, List<RefurbishmentJob> refurbishmentJobs, HashMap<Long, Workshop> mappedWorkshopData, HashMap<Long, User> mappedRefurbData) {
        for (PostRefurbList inspection : inspections) {
            List<Refurbishment> inspectionRefurbishments = refurbishments.stream().filter(r -> r.getProcuredCarID().equals(inspection.getCarID())).collect(Collectors.toList());
            List<Long> inspectionRefurbIDs = inspectionRefurbishments.stream().map(Refurbishment::getId).collect(Collectors.toList());
            List<RefurbishmentJob> inspectionRefurbJobs = refurbishmentJobs.stream().filter(rj -> inspectionRefurbIDs.contains(rj.getRefurbID())).collect(Collectors.toList());
            inspection.setNoOfJobs(inspectionRefurbJobs.size());

            //logic for updating workshop and refurbSpoc details
            updateWorkshopAndRefurbSpoc(inspection, inspectionRefurbJobs, inspectionRefurbishments, mappedWorkshopData, mappedRefurbData);
        }
    }

    private void updateWorkshopAndRefurbSpoc(PostRefurbList inspection, List<RefurbishmentJob> inspectionRefurbJobs,
                                             List<Refurbishment> inspectionRefurbishments,
                                             HashMap<Long, Workshop> mappedWorkshopData, HashMap<Long, User> mappedRefurbData) {
        Map<Long, List<RefurbishmentJob>> mappedRefurbJobs = inspectionRefurbJobs.stream().collect(Collectors.groupingBy(RefurbishmentJob::getRefurbID));
        Long refurbIDWithMaxJobs = null;
        int maxJobs = Integer.MIN_VALUE;
        for (Map.Entry<Long, List<RefurbishmentJob>> entry : mappedRefurbJobs.entrySet()) {
            if(entry.getValue().size() > maxJobs) {
                refurbIDWithMaxJobs = entry.getKey();
                maxJobs = entry.getValue().size();
            }
        }
        Long finalRefurbIDWithMaxJobs = refurbIDWithMaxJobs;
        Refurbishment refurbishment = inspectionRefurbishments.stream().filter(r -> r.getId().equals(finalRefurbIDWithMaxJobs)).findAny().orElse(null);
        if(refurbishment != null) {
            Long workshopID = refurbishment.getWorkshopID();
            Long refurbSpocID = refurbishment.getRefurbSpocID();

            Workshop workshop = mappedWorkshopData.get(workshopID);
            if(workshop != null) {
                inspection.setWorkshopName(workshop.getWorkshopName());
                inspection.setWorkshopAddress(workshop.getAddress());
            }
            User refurbSpoc = mappedRefurbData.get(refurbSpocID);
            if(refurbSpoc != null) {
                inspection.setRefurbSpoc(refurbSpoc.getUserName());
            }
        }

        //get all unique workshops
        HashSet<Long> uniqueWorkshops = inspectionRefurbishments.stream().map(Refurbishment::getWorkshopID).collect(Collectors.toCollection(HashSet::new));
        inspection.setWorkshops(mapWorkshops(uniqueWorkshops, mappedWorkshopData));

        //get all unique refurbSpocs
        HashSet<Long> uniqueRefurbSpocs = inspectionRefurbishments.stream().map(Refurbishment::getRefurbSpocID).collect(Collectors.toCollection(HashSet::new));
        inspection.setRefurbSpocs(mapRefurbSpocs(uniqueRefurbSpocs, mappedRefurbData));
    }

    private List<RefurbSpocData> mapRefurbSpocs(HashSet<Long> uniqueRefurbSpocs, HashMap<Long, User> mappedRefurbData) {
        List<RefurbSpocData> refurbSpocData = new ArrayList<>();

        for (Long uniqueRefurbSpoc : uniqueRefurbSpocs) {
            User refurbSpoc = mappedRefurbData.get(uniqueRefurbSpoc);
            refurbSpocData.add(
                    RefurbSpocData.builder()
                            .refurbSpocID(refurbSpoc.getId())
                            .refurbSpocName(refurbSpoc.getUserName())
                            .build()
            );
        }

        return refurbSpocData;
    }

    private List<WorkshopData> mapWorkshops(HashSet<Long> uniqueWorkshops, HashMap<Long, Workshop> workshopDetails) {
        List<WorkshopData> workshopData = new ArrayList<>();

        for (Long uniqueWorkshop : uniqueWorkshops) {
            Workshop workshop = workshopDetails.get(uniqueWorkshop);
            workshopData.add(
                    WorkshopData.builder()
                            .workshopID(workshop.getId())
                            .workshopName(workshop.getWorkshopName())
                            .build()
            );
        }

        return workshopData;
    }

    @Override
    public List<InspectionChecklist> getInspectionChecklist(Long inspectionID) throws UCTSupplyException {
        Optional<Inspection> inspection = inspectionDAO.getInspectionDetails(inspectionID);
        if (inspection.isPresent()) {
            Inspection inspectionDetails = inspection.get();
            //get inspection subparts along with all headers, parts, issues, followup details for inspection ID
            List<InspectionSubpartDetail> inspectionSubpartDetails = inspectionSubPartService.getInspectionSubpartDetails(inspectionID);
            log.info("inspectionSubpartDetails " + inspectionSubpartDetails.toString());

            //get inspection reports for inspection ID
            List<InspectionReport> inspectionReports = inspectionReportService.getInspectionReport(inspectionID);
            log.info("inspectionReports " + inspectionReports.toString());
            Map<Long, List<InspectionReport>> mappedInspectionReport = groupInspectionReport(inspectionReports);

            //get inspection jobs for inspection ID
            List<InspectionJob> inspectionJobs = inspectionJobService.getInspectionJobs(inspectionID);
            log.info("inspectionJobs " + inspectionJobs.toString());
            Map<Long, List<InspectionJob>> mappedInspectionJob = groupInspectionJob(inspectionJobs);

            //get all job cost
            List<JobPriceMap> jobPriceMaps = jobPriceMapService.getAllJobPrice();
            log.info("jobPriceMaps " + jobPriceMaps.toString());

            //get all InspectionImages
            List<InspectionImage> inspectionImages = inspectionImageService.getInspectionImages(inspectionID);
            log.info("inspectionImages " + inspectionImages.toString());

            //get all default issues and map them to InspectionIssue type
            List<SubPartIssues> defaultIssues = new ArrayList<>();
            if(inspectionDetails.getInspectionType() != E4A) {
                defaultIssues = subPartIssuesService.getDefaultIssues();
            }
            List<InspectionIssue> mappedDefaultIssues = convertToInspectionIssue(defaultIssues);

            //group data based on header, subpart, issue
            Map<Long, Map<Long, Map<Long, List<InspectionSubpartDetail>>>> inspectionSubpartDetailsMap = groupByHeaderSubpartAndIssue(inspectionSubpartDetails);
            log.info("inspectionSubpartDetailsMap " + inspectionSubpartDetailsMap.toString());

            List<InspectionReport> previousInspectionReport = getLastInspectionReport(inspectionDetails);
            log.info("previousInspectionReport "+previousInspectionReport);
            Map<Long, List<InspectionReport>> mappedPreviousInspectionReport = groupInspectionReport(previousInspectionReport);

            //construct checklist and return
            return constructInspectionChecklist(inspectionSubpartDetailsMap, mappedInspectionReport, mappedInspectionJob, jobPriceMaps, inspectionImages, mappedDefaultIssues, mappedPreviousInspectionReport, inspectionDetails);

        } else {
            throw new UCTSupplyException("Inspection ID " + inspectionID + " not found", HttpStatus.BAD_REQUEST);
        }
    }

    private List<InspectionReport> getLastInspectionReport(Inspection inspectionDetails) {
        List<InspectionReport> inspectionReports = new ArrayList<>();
        InspectionType lastInspectionType = getLastInspectionType(inspectionDetails.getInspectionType());
        if(lastInspectionType == inspectionDetails.getInspectionType()) {
            return inspectionReports;
        }
        Inspection lastInspection = inspectionDAO.getInspectionDetailsByLeadIDAndInspectionType(inspectionDetails.getLeadID(), lastInspectionType);
        if(lastInspection != null) {
            inspectionReports = inspectionReportService.getInspectionReport(lastInspection.getId());
        }
        return inspectionReports;
    }

    private InspectionType getLastInspectionType(InspectionType inspectionType) {
        InspectionType lastInspectionType = E1;
        switch (inspectionType) {
            case E2: lastInspectionType = E1; break;
            case E3: lastInspectionType = E2; break;
            case E4A: lastInspectionType = E4A; break;
        }
        return lastInspectionType;
    }

    @Override
    public void saveInspectionChecklist(Long inspectorID, Long inspectionID, InspectionChecklistRequest request) throws UCTSupplyException {
        log.info("inspectorID "+ inspectorID);
        log.info("inspectionID "+ inspectionID);
        log.info("checklistData "+ request);

        Optional<Inspection> inspection = inspectionDAO.getInspectionDetails(inspectionID);
        if(!inspection.isPresent()) {
            throw new UCTSupplyException("Inspection details not found", HttpStatus.BAD_REQUEST);
        }
        Inspection inspectionDetails = inspection.get();

        //validate checklist data
        //validateChecklistData(checklistData);

        //delete previous reports,jobs,images
        deletePreviousInspectionRecords(inspectorID, inspectionID);

        List<InspectionChecklist> checklistData = request.getInspectionChecklists();

        List<RefurbishmentJob> completedRefurbJobs = new ArrayList<>();
        List<RefurbishmentJob> refurbishmentJobs = new ArrayList<>();
        if(inspectionDetails.getRefurbID() != null) {
            Refurbishment refurbishment = refurbishmentService.getRefurbDetails(inspectionDetails.getRefurbID());
            if(refurbishment == null) {
                throw new UCTSupplyException("Refurbishment details not found", HttpStatus.BAD_REQUEST);
            }
            refurbishmentJobs = refurbishmentJobService.getRefurbJobList(inspectionDetails.getRefurbID());
            completedRefurbJobs = refurbishmentJobService.getCompletedRefurbJobListByCarID(refurbishment.getProcuredCarID());
        }

        Double refurbJobCost = 0.0;
        Map<String, Boolean> subIssueMap = new HashMap<>();

        for (InspectionChecklist data : checklistData) {
            Long headerID = data.getHeaderID();
            List<InspectionSubpartList> subPartList = data.getSubPartList();

            for (InspectionSubpartList subPart : subPartList) {
                Long subPartID = subPart.getSubPartID();
                SubPartInputType issuesAllowed = subPart.getAllowedSelection();
                List<String> images = subPart.getImages();
                List<InspectionIssue> issues = subPart.getIssues();
                List<InspectionSubIssue> defaultSubIssues = subPart.getDefaultSubIssues();

                refurbJobCost += computeRefurbJobCost(subIssueMap, defaultSubIssues, issues, subPartID);

                inspectionReportService.saveInspectionReport(inspectorID, inspectionID, subPartID, issues, subPart.getComments()); //make entries in inspection report

                inspectionJobService.saveInspectionJob(inspectorID, inspectionDetails, defaultSubIssues, subPartID, issues, subPart.getSubPartName(), completedRefurbJobs, refurbishmentJobs); //make entries in inspection jobs

                inspectionImageService.saveInspectionImage(inspectorID, inspectionID, subPartID, images); //save inspection images

            }
        }

        //update inspection details post inspection
        UpdateInspectionRequest updateInspectionRequest = UpdateInspectionRequest.builder()
                .id(inspectionID)
                .updatedBy(inspectorID)
                .lastDataSyncTime(request.getLastDataSyncTime())
                .build();
        try {
            if (inspectionDetails.getRefurbID() == null) {
                updateInspectionRequest.setRefurbJobCost(refurbJobCost);
                updateInspectionChecklistHistory(inspectorID, inspectionID, request.getInspectionChecklists()); //save into history table
            }
            updateInspection(updateInspectionRequest);
            updateOffer(inspectionDetails, inspectorID, refurbJobCost);
        } catch (SupplyException |JsonProcessingException |EntityNotFoundException e) {
            log.error(e.getMessage());
            log.error("Update inspection failed");
            throw new UCTSupplyException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void updateInspectionChecklistHistory(Long inspectorID, Long inspectionID, List<InspectionChecklist> inspectionChecklists) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String newData = mapper.writeValueAsString(inspectionChecklists);
        log.info("oldDataJson ={}", newData);
        String oldData = newData;

        History history = historyService.getLastHistoricalData(inspectionID, EditEntity.INSPECTION_CHECKLIST);
        if(history != null) {
            oldData = history.getNewData();
        }

        historyService.saveHistory(inspectorID, inspectionID, EditEntity.INSPECTION_CHECKLIST, oldData, newData);
    }

    private void updateOffer(Inspection inspectionDetails, Long inspectorID, Double refurbJobCost) throws SupplyException, JsonProcessingException {
        if(inspectionDetails.getInspectionType().equals(E3))
            return;

        Offer offer = offerService.getOfferDetailsForLead(inspectionDetails.getLeadID());
        if (offer != null) {
            UpdateOfferRequest data = UpdateOfferRequest.builder()
                    .id(offer.getId())
                    .leadID(offer.getLeadID())
                    .updatedBy(inspectorID)
                    .build();

            if (inspectionDetails.getInspectionType().equals(E1)) {
                data.setRefubCost(refurbJobCost);
            } else if (inspectionDetails.getInspectionType().equals(E2)) {
                data.setE2RefurbCost(refurbJobCost);
            }
            offerService.updateOffer(data);
        }
    }


    private Double computeRefurbJobCost(Map<String, Boolean> subIssueMap, List<InspectionSubIssue> defaultSubIssues, List<InspectionIssue> issues, Long subPartID) {
        Double refurbJobCost = 0.0;
        for (InspectionSubIssue subIssue : defaultSubIssues) {
            if(!subIssueMap.containsKey(subPartID + "-" + subIssue.getJobID()) && subIssue.getSelected()) {
                refurbJobCost += subIssue.getCost(); //compute refurb cost post inspection
                subIssueMap.put(subPartID + "-" + subIssue.getJobID(), true);
            }
        }

        for (InspectionIssue issue : issues) {
            List<InspectionSubIssue> subIssues = issue.getSubIssues();
            for (InspectionSubIssue subIssue : subIssues) {
                if(!subIssueMap.containsKey(subPartID + "-" + subIssue.getJobID()) && subIssue.getSelected()) {
                    refurbJobCost += subIssue.getCost(); //compute refurb cost post inspection
                    subIssueMap.put(subPartID + "-" + subIssue.getJobID(), true);
                }
            }
        }

        return refurbJobCost;
    }

    @Override
    public Inspection scheduleInspection(ScheduleInspectionRequest request) throws EntityNotFoundException, SupplyException, JsonProcessingException {
        //leadID , inspector
        if(!leadService.existByIdAndIsActive(request.getLeadID(), true)) {
            throw new EntityNotFoundException(lead_does_not_exist+"for leadID: "+request.getLeadID());
        }
        if(!userService.existById(request.getInspectorID())) {
            throw new EntityNotFoundException(inspector_does_no_exist+"for inspectorID: "+request.getInspectorID());
        }

//        request.setInspectionTime(request.getInspectionTime().minusMinutes(330));

        Inspection scheduledInspection = inspectionDAO.scheduleInspection(getScheduleInspectionDataFromRequest(request));

        LeadStatus leadStatus = null;
        ProcuredCarStatus status = null;
        switch (request.getInspectionType()){
            case E1: leadStatus = LeadStatus.E1_SCHEDULED; break;
            case E2: leadStatus = LeadStatus.E2_SCHEDULED; break;
            case E3: status = ProcuredCarStatus.E3_SCHEDULED; break;
        }

        if(leadStatus != null) {
            UpdateLeadRequest updateLeadRequest = UpdateLeadRequest.builder()
                    .id(scheduledInspection.getLeadID())
                    .adminID(request.getInspectorID())
                    .leadStatus(leadStatus)
                    .build();
            leadService.updateLead(updateLeadRequest);
        }

        if(status != null) {
            updateProcuredCarStatusFromLeadID(scheduledInspection.getLeadID(), request.getCreatedBy(), status);
        }

        List<SubParts> subPartsList = subPartsService.findAllByIsActive(true);

        List<InspectionSubPart> ISPList = inspectionSubPartService.insertDataForScheduledInspection(request.getCreatedBy(), scheduledInspection.getId(), subPartsList);

//        sendScheduledInspectionNotification(scheduledInspection);
        return scheduledInspection;
    }

    private void updateProcuredCarStatusFromLeadID(Long leadID, Long createdBy, ProcuredCarStatus status) throws SupplyException, JsonProcessingException {
        procuredCarService.updateProcuredCarStatusFromLeadID(leadID, createdBy, status);
    }

    private void sendScheduledInspectionNotification(Inspection scheduledInspection) {
        TextDetails textDetails = TextDetails.builder()
                .smsText("New Inspection scheduled " + scheduledInspection.getId())
               .phoneNumber("7838388154")
               .build();
        try {
            Publisher.publish(null, textDetails, "INSPECTION_SCHEDULED");
        } catch (Exception e) {
            log.info("Exception while sending notification");
        }
    }

    private Inspection getScheduleInspectionDataFromRequest(ScheduleInspectionRequest request) {
        return Inspection.builder()
                .inspectionType(InspectionType.valueOf(String.valueOf(request.getInspectionType())))
                .leadID(request.getLeadID())
                .inspectorID(request.getInspectorID())
                .createdBy(request.getCreatedBy())
                .inspectionTime(request.getInspectionTime())
                .status(InspectionStatus.valueOf("SCHEDULED"))
                .createdAt(LocalDateTime.now())
                .isActive(true)
                .build();
    }

    @Override
    public void saveInspectionImages(Long inspectorID, Long inspectionID, InspectionChecklistFileRequest inspectionChecklistFileRequest) {
        inspectionImageService.markImagesInactive(inspectorID, inspectionID);//mark inspection images inactive

        for (InspectionImageRequest checklistImage : inspectionChecklistFileRequest.getData()) {
            inspectionImageService.saveInspectionImage(inspectorID, inspectionID, checklistImage.getSubPartID(), checklistImage.getImages());//make entries in inspection images
        }

        inspectionDAO.updateLastDataSyncTime(inspectionChecklistFileRequest.getLastDataSyncTime(), inspectionID); //update time
    }

    @Override
    public InspectionVerdictDetail getInspectionVerdictData(Long inspectionID) throws UCTSupplyException {
        Optional<Inspection> inspection = inspectionDAO.getInspectionDetails(inspectionID);
        if(inspection.isPresent()) {
            Inspection inspectionDetails = inspection.get();
            List<InspectionReport> inspectionReports = inspectionReportService.getInspectionReport(inspectionID);
            Map<Long, List<InspectionReport>> mappedInspectionReport = groupInspectionReport(inspectionReports);
            List<Long> subPartIds = new ArrayList<>(mappedInspectionReport.keySet());
            List<SubParts> subParts = subPartsService.findAllByID(subPartIds);
            List<InspectionJob> inspectionJobs = inspectionJobService.getInspectionJobs(inspectionID);

            int unAssuredPartCount = 0;
            int partsNotInspectedCount = 0;
            int issuesCount = 0;
            int jobsCount = 0;
            int refurbishedCost = 0;
            String title = APIConstants.VERDICT_SCREEN_TITLE_SUCCESS;
            boolean warning = false;

            for (Map.Entry<Long, List<InspectionReport>> report : mappedInspectionReport.entrySet()) {
                List<InspectionReport> reports = report.getValue();
                Long subPartID = report.getKey();
                int subPartIssuesCount = 0;
                boolean isUnAssured = false;
                SubParts subPart = subParts.stream().filter(sub -> sub.getId().equals(subPartID)).findFirst().orElse(null);
                if(subPart != null){
                    isUnAssured = subPart.isUnAssured();
                }
                for (InspectionReport inspectionReport : reports) {
                    if(inspectionReport.getSelected() && !APIConstants.DEFAULT_ISSUES_ID.contains(inspectionReport.getSubPartIssueID())) {
                        subPartIssuesCount++;
                    }
                }
                if(subPartIssuesCount == 0) {
                    partsNotInspectedCount++;
                }
                if(isUnAssured && subPartIssuesCount > 0) {
                    unAssuredPartCount++;
                }
                issuesCount += subPartIssuesCount;
            }

            for (InspectionJob inspectionJob : inspectionJobs) {
                if(!APIConstants.DEFAULT_JOB_ID.contains(inspectionJob.getJobID())) {
                    jobsCount++;
                }
                refurbishedCost += inspectionJob.getCost();
            }

            if(unAssuredPartCount > 0) {
                title = APIConstants.VERDICT_SCREEN_TITLE_WARNING;
                warning = true;
            }

            return InspectionVerdictDetail.builder()
                    .leadID(inspectionDetails.getLeadID())
                    .title(title)
                    .warning(warning)
                    .inspectionType(inspectionDetails.getInspectionType())
                    .issuesCount(issuesCount)
                    .jobsCount(jobsCount)
                    .unAssuredPartCount(unAssuredPartCount)
                    .refurbishedCost(refurbishedCost)
                    .partsNotInspectedCount(partsNotInspectedCount)
                    .build();

        } else {
            throw new UCTSupplyException("Inspection ID "+inspectionID + " not found", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public void saveInspectionVerdict(Long inspectorID, Long inspectionID, InspectionVerdictRequest requestBody) throws UCTSupplyException, JsonProcessingException, SupplyException {
        Optional<Inspection> inspection = inspectionDAO.getInspectionDetails(inspectionID);
        if(inspection.isPresent()){
            Inspection oldData = inspection.get();

            InspectionStatus verdict = requestBody.getVerdict();
            String remarks = requestBody.getRemarks();

            ObjectMapper mapper = new ObjectMapper();
            String oldDataJson = mapper.writeValueAsString(oldData);
            log.info("oldDataJson ={}", oldDataJson);

            oldData.setStatus(verdict);
            if(remarks != null) {
                oldData.setVerdictRemarks(remarks);
            }
            String newDataJson = mapper.writeValueAsString(oldData);
            log.info("newDataJson ={}", newDataJson);

            inspectionDAO.updateInspection(oldData);

            //update lead status
            List<InspectionType> updateLeadInspectionTypes = Arrays.asList(E1,E2);
            if(updateLeadInspectionTypes.contains(oldData.getInspectionType())) {
                updateLeadStatus(oldData, verdict, inspectorID, remarks);
            } else {
                updateProcuredCar(oldData, verdict, inspectorID);
            }
            //save inspection history
            historyService.saveHistory(inspectorID, inspectionID, EditEntity.INSPECTION, oldDataJson, newDataJson);

        }else {
            throw new UCTSupplyException("Inspection ID "+inspectionID + " not found", HttpStatus.BAD_REQUEST);
        }

    }

    private void updateProcuredCar(Inspection inspectionDetails, InspectionStatus verdict, Long inspectorID) throws UCTSupplyException {
        ProcuredCarStatus status = null;
        if (inspectionDetails.getInspectionType().equals(E3)) {
            status = ProcuredCarStatus.E3_CONDUCTED;
        }
        if (inspectionDetails.getInspectionType().equals(E4A)) {
            status = ProcuredCarStatus.E4A_CONDUCTED;
        }
        ProcuredCar car = procuredCarService.getProcuredCarDetails(inspectionDetails.getLeadID());

        if (status != null && car != null) {
            UpdateProcuredCarRequest updateProcuredCarRequest = UpdateProcuredCarRequest.builder()
                    .id(car.getId())
                    .status(status)
                    .updatedBy(inspectorID)
                    .build();

            try {
                procuredCarService.updateProcuredCarDetails(updateProcuredCarRequest);
            } catch (JsonProcessingException e) {
                log.info(e.getMessage());
                throw new UCTSupplyException("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }

    }

    @Override
    public InspectionVerdictDetailForGenerateOffer getInspectionVerdictDataForGenerateOffer(Long inspectionID) throws UCTSupplyException {
        /*
        Optional<Inspection> inspection = inspectionDAO.getInspectionDetails(inspectionID);
        if(inspection.isPresent()) {
            Inspection inspectionDetails = inspection.get();
            List<InspectionReport> inspectionReports = inspectionReportService.getInspectionReport(inspectionID);
            List<InspectionJob> inspectionJobs = inspectionJobService.getInspectionJobs(inspectionID);

            int unAssuredPartCount = 0;
            int issuesCount = 0;
            int jobsCount = 0;
            int refurbishedCost = 0;
            int okPartCount = 0;
            int nfiPartCount = 0;
            String title = "Genarate Offer Data";
            boolean warning = false;

            for (InspectionReport inspectionReport : inspectionReports) {
                if(inspectionReport.getSelected() && inspectionReport.getIssueName().equals(APIConstants.UNASSURED_ISSUE)) {
                    unAssuredPartCount++;
                }
                if(inspectionReport.getSelected() && inspectionReport.getIssueName().equals(APIConstants.OK_ISSUE)) {
                    okPartCount++;
                }
                if(inspectionReport.getSelected() && inspectionReport.getIssueName().equals(APIConstants.NFI_ISSUE)) {
                    nfiPartCount++;
                }
                if(inspectionReport.getSelected() && !APIConstants.DEFAULT_ISSUES_ID.contains(inspectionReport.getSubPartIssueID())) {
                    issuesCount++;
                }
            }

            for (InspectionJob inspectionJob : inspectionJobs) {
                if(!APIConstants.DEFAULT_JOB_ID.contains(inspectionJob.getJobID())) {
                    jobsCount++;
                }
                refurbishedCost += inspectionJob.getCost();
            }

            if(unAssuredPartCount > 0) {
                title = "Warning true";
                warning = true;
            }

            return InspectionVerdictDetailForGenerateOffer.builder()
                    .leadID(inspectionDetails.getLeadID())
                    .title(title)
                    .warning(warning)
                    .inspectionType(inspectionDetails.getInspectionType())
                    .issuesCount(issuesCount)
                    .jobsCount(jobsCount)
                    .unAssuredPartCount(unAssuredPartCount)
                    .okPartCount(okPartCount)
                    .nfiPartCount(nfiPartCount)
                    .refurbishedCost(refurbishedCost)
                    .build();

        } else {
            throw new UCTSupplyException("Inspection ID "+inspectionID + " not found", HttpStatus.BAD_REQUEST);
        }
         */

        Optional<Inspection> inspection = inspectionDAO.getInspectionDetails(inspectionID);
        if(inspection.isPresent()) {
            Inspection inspectionDetails = inspection.get();
            List<InspectionReport> inspectionReports = inspectionReportService.getInspectionReport(inspectionID);
            Map<Long, List<InspectionReport>> mappedInspectionReport = groupInspectionReport(inspectionReports);
            List<Long> subPartIds = new ArrayList<>(mappedInspectionReport.keySet());
            List<SubParts> subParts = subPartsService.findAllByID(subPartIds);
            List<InspectionJob> inspectionJobs = inspectionJobService.getInspectionJobs(inspectionID);

            int unAssuredPartCount = 0;
            int partsNotInspectedCount = 0;
            int issuesCount = 0;
            int jobsCount = 0;
            int nfiPartCount = 0;
            int okPartCount = 0;

            int refurbishedCost = 0;
            String title = APIConstants.VERDICT_SCREEN_TITLE_SUCCESS;
            boolean warning = false;

            for (Map.Entry<Long, List<InspectionReport>> report : mappedInspectionReport.entrySet()) {
                List<InspectionReport> reports = report.getValue();
                Long subPartID = report.getKey();
                int subPartIssuesCount = 0;
                boolean isUnAssured = false;
                SubParts subPart = subParts.stream().filter(sub -> sub.getId().equals(subPartID)).findFirst().orElse(null);
                if(subPart != null){
                    isUnAssured = subPart.isUnAssured();
                }
                for (InspectionReport inspectionReport : reports) {
                    if(inspectionReport.getSelected() && !APIConstants.DEFAULT_ISSUES_ID.contains(inspectionReport.getSubPartIssueID())) {
                        subPartIssuesCount++;
                    }
                    if(inspectionReport.getSelected() && inspectionReport.getIssueName().equals(APIConstants.OK_ISSUE)) {
                        okPartCount++;
                    }
                    if(inspectionReport.getSelected() && inspectionReport.getIssueName().equals(APIConstants.NFI_ISSUE)) {
                        nfiPartCount++;
                    }
                }
                if(subPartIssuesCount == 0) {
                    partsNotInspectedCount++;
                }
                if(isUnAssured && subPartIssuesCount > 0) {
                    unAssuredPartCount++;
                }
                issuesCount += subPartIssuesCount;
            }

            for (InspectionJob inspectionJob : inspectionJobs) {
                if(!APIConstants.DEFAULT_JOB_ID.contains(inspectionJob.getJobID())) {
                    jobsCount++;
                }
                refurbishedCost += inspectionJob.getCost();
            }

            if(unAssuredPartCount > 0) {
                title = APIConstants.VERDICT_SCREEN_TITLE_WARNING;
                warning = true;
            }

            return InspectionVerdictDetailForGenerateOffer.builder()
                    .leadID(inspectionDetails.getLeadID())
                    .title(title)
                    .warning(warning)
                    .inspectionType(inspectionDetails.getInspectionType())
                    .issuesCount(issuesCount)
                    .jobsCount(jobsCount)
                    .unAssuredPartCount(unAssuredPartCount)
                    .okPartCount(okPartCount)
                    .nfiPartCount(nfiPartCount)
                    .refurbishedCost(refurbishedCost)
                    .build();

        } else {
            throw new UCTSupplyException("Inspection ID "+inspectionID + " not found", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public List<InspectionPickupChecklist> getInspectionPickupChecklist(Long inspectionID) throws UCTSupplyException {
        Optional<Inspection> inspection = inspectionDAO.getInspectionDetails(inspectionID);
        List<InspectionPickupChecklist> inspectionPickupChecklists = new ArrayList<>();
        if(inspection.isPresent()) {
            Inspection inspectionDetails = inspection.get();
            DocumentStage documentStage = getDocumentStage(inspectionDetails.getInspectionType());
            List<DocumentStageMaster> documentStageMasters = documentStageMasterService.getStageDocuments(documentStage);
            List<CommonDocument> commonDocuments = commonDocumentService.getDocumentsForEntityIDAndName(inspectionID, CommonDocumentEntity.INSPECTION);

            for (DocumentStageMaster documentStageMaster : documentStageMasters) {
                CommonDocument commonDocument = commonDocuments.stream()
                        .filter(cd -> cd.getDocumentStageMasterID().equals(documentStageMaster.getId()))
                        .findAny().orElse(null);
                String documentPath = null;
                if(commonDocument != null) {
                    documentPath = commonDocument.getImageURL();
                }
                inspectionPickupChecklists.add(
                        InspectionPickupChecklist.builder()
                                .documentID(documentStageMaster.getId())
                                .documentName(documentStageMaster.getDocumentName())
                                .documentType(documentStageMaster.getDocumentType())
                                .isMandatory(documentStageMaster.isMandatory())
                                .documentPath(documentPath)
                                .build()
                );
            }

        } else {
            throw new UCTSupplyException("Inspection ID "+inspectionID + " not found", HttpStatus.BAD_REQUEST);
        }
        return inspectionPickupChecklists;
    }

    @Override
    public void saveInspectionPickupChecklist(Long inspectorID, Long inspectionID, List<InspectionPickupChecklist> inspectionPickupChecklists) throws UCTSupplyException {
        Optional<Inspection> inspection = inspectionDAO.getInspectionDetails(inspectionID);

        if(inspection.isPresent()) {
//            Inspection inspectionDetails = inspection.get();
            List<Long> documentIDs = inspectionPickupChecklists.stream().map(InspectionPickupChecklist::getDocumentID).collect(Collectors.toList());

            deletePreviousInspectionPickupDocuments(documentIDs, inspectionID, inspectorID); //soft delete documents for inspectionID

            savePickupDocuments(inspectionID, inspectorID, inspectionPickupChecklists); //save documents

            UpdateInspectionRequest updateInspectionRequest = UpdateInspectionRequest.builder()
                    .id(inspectionID)
                    .updatedBy(inspectorID)
                    .pickupDocumentsCount(inspectionPickupChecklists.size())
                    .build();

            UpdateLeadRequest updateLeadRequest = UpdateLeadRequest.builder()
                    .adminID(inspectorID)
                    .id(inspection.get().getLeadID())
                    .handoverDate(LocalDateTime.now())
                    .pickupBy(inspectorID)
                    .build();

            try {
                updateInspection(updateInspectionRequest);
                updateLead(updateLeadRequest);
            } catch (SupplyException | JsonProcessingException | EntityNotFoundException e) {
                throw new UCTSupplyException("Update inspection/lead failed", HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } else {
            throw new UCTSupplyException("Inspection ID "+inspectionID + " not found", HttpStatus.BAD_REQUEST);
        }
    }

    private void updateLead(UpdateLeadRequest updateLeadRequest) throws SupplyException, JsonProcessingException {
        leadService.updateLead(updateLeadRequest);
    }

    private void savePickupDocuments(Long inspectionID, Long inspectorID, List<InspectionPickupChecklist> inspectionPickupChecklists) {
        List<CommonDocument> commonDocuments = new ArrayList<>();
        for (InspectionPickupChecklist pickupChecklist : inspectionPickupChecklists) {
            commonDocuments.add(
                    CommonDocument.builder()
                            .createdBy(inspectorID)
                            .updatedBy(inspectorID)
                            .entityID(inspectionID)
                            .commonDocumentEntity(CommonDocumentEntity.INSPECTION)
                            .documentStageMasterID(pickupChecklist.getDocumentID())
                            .imageURL(pickupChecklist.getDocumentPath())
                            .build()
            );
        }

        if(commonDocuments.size() > 0) {
            commonDocumentService.saveDocuments(commonDocuments);
        }
    }

    private void deletePreviousInspectionPickupDocuments(List<Long> documentIDs, Long inspectionID, Long inspectorID) {
        commonDocumentService.markDocumentsInactive(CommonDocumentEntity.INSPECTION, inspectionID, inspectorID, documentIDs);
    }

    private DocumentStage getDocumentStage(InspectionType inspectionType) {
        DocumentStage documentStage = DocumentStage.E2_APPROVED;
        switch (inspectionType) {
            case E2: documentStage = DocumentStage.E2_APPROVED; break;
        }

        return documentStage;
    }

    private void updateLeadStatus(Inspection oldData, InspectionStatus verdict, Long inspectorID, String remarks) throws SupplyException, JsonProcessingException {
        LeadStatus leadStatus = null;
        LeadRejectSubStatus leadRejectSubStatus = null;

        if(oldData.getInspectionType().equals(E1)){
            if(verdict.equals(COMPLETED)) {
                leadStatus = LeadStatus.E1_PASS;
            }
            if(verdict.equals(REJECTED)) {
                leadStatus = LeadStatus.REJECTED;
                leadRejectSubStatus = LeadRejectSubStatus.E1_FAIL;
            }
        }
        if(oldData.getInspectionType().equals(E2)){
            leadStatus = LeadStatus.E2_COMPLETED;
        }
        if(leadStatus != null) {
            UpdateLeadRequest updateLeadRequest = UpdateLeadRequest.builder()
                    .id(oldData.getLeadID())
                    .adminID(inspectorID)
                    .leadStatus(leadStatus)
                    .build();
            if(leadRejectSubStatus != null) {
                updateLeadRequest.setLeadRejectSubStatus(leadRejectSubStatus);
                updateLeadRequest.setRejectionReason("Inspection failed " + remarks);
            }
            leadService.updateLead(updateLeadRequest);
        }
    }

    private Map<Long, List<InspectionJob>> groupInspectionJob(List<InspectionJob> inspectionJobs) {
        return inspectionJobs.stream().collect(Collectors.groupingBy(InspectionJob::getSubPartID));

    }

    private Map<Long, List<InspectionReport>> groupInspectionReport(List<InspectionReport> inspectionReports) {
        return inspectionReports.stream().collect(Collectors.groupingBy(InspectionReport::getSubPartID));
    }

    private void validateChecklistData(List<InspectionChecklist> checklistData) throws UCTSupplyException{
        //Mandatory items are filled or not
        //If file required then images should not be empty array
        for(InspectionChecklist data: checklistData) {
            List<InspectionSubpartList> subPartList = data.getSubPartList();

            for(InspectionSubpartList subPart: subPartList){
                List<InspectionIssue> issues = subPart.getIssues();
                Boolean isMandatory = subPart.getIsMandatory();
                Boolean fileRequired = subPart.getFileRequired();
                boolean issueSelected = false;

                for (InspectionIssue issue : issues) {
                    if (issue.getSelected()) {
                        issueSelected = true;
                        break;
                    }
                }
                if(isMandatory && !issueSelected) {
                    throw new UCTSupplyException(subPart.getSubPartName() + " is mandatory", HttpStatus.BAD_REQUEST);
                }
//                if(fileRequired && subPart.getImages().size() == 0) {
//                    throw new UCTSupplyException("File required for "+subPart.getSubPartName(), HttpStatus.BAD_REQUEST);
//                }
            }
        }
    }

    private void deletePreviousInspectionRecords(Long inspectorID, Long inspectionID) {
        inspectionReportService.markReportsInactive(inspectorID, inspectionID);//mark inactive inspection report
        inspectionJobService.markJobsInactive(inspectorID, inspectionID);//mark inactive inspection jobs
        inspectionImageService.markImagesInactive(inspectorID, inspectionID);//mark inactive inspection images
    }

    private List<InspectionIssue> convertToInspectionIssue(List<SubPartIssues> defaultIssues) {
        List<InspectionIssue> issues = new ArrayList<>();
        defaultIssues.forEach(issue -> {
            issues.add(InspectionIssue.builder()
                    .issueName(issue.getIssueName())
                    .issueID(issue.getId())
                    .issueType(APIConstants.CHECKLIST_ISSUE_TYPE.get("DEFAULT"))
                    .build());
        });
        return issues;
    }

    private List<InspectionIssue> mapDefaultIssues(List<SubPartIssues> defaultIssues) {
        List<InspectionIssue> issues = new ArrayList<>();
        defaultIssues.forEach(issue -> {
            issues.add(InspectionIssue.builder()
                    .issueName(issue.getIssueName())
                    .issueID(issue.getId())
                    .issueType(APIConstants.CHECKLIST_ISSUE_TYPE.get("DEFAULT"))
                    .build());
        });
        return issues;
    }

    private List<InspectionChecklist> constructInspectionChecklist(
            Map<Long, Map<Long, Map<Long, List<InspectionSubpartDetail>>>> inspectionSubpartDetailsMap,
            Map<Long, List<InspectionReport>> inspectionReports,
            Map<Long, List<InspectionJob>> inspectionJobs,
            List<JobPriceMap> jobPriceMaps,
            List<InspectionImage> inspectionImages,
            List<InspectionIssue> defaultIssues,
            Map<Long, List<InspectionReport>> mappedPreviousInspectionReport, Inspection inspectionDetails) {
        List<InspectionChecklist> inspectionChecklists = new ArrayList<>();

        //get default subIssues
        List<InspectionSubIssue> defaultSubIssues = getDefaultSubIssues(inspectionDetails.getInspectionType());

        for (Map.Entry<Long, Map<Long, Map<Long, List<InspectionSubpartDetail>>>> header : inspectionSubpartDetailsMap.entrySet()) {
            log.info(header.getKey() + ":" + header.getValue());
            Long headerID = header.getKey();
            Map<Long, Map<Long, List<InspectionSubpartDetail>>> subPartMap = header.getValue();
            InspectionChecklist.InspectionChecklistBuilder inspectionChecklistBuilder = InspectionChecklist.builder();
            inspectionChecklistBuilder.headerID(headerID);

            constructSubpartList(subPartMap, defaultIssues, inspectionJobs,inspectionReports, jobPriceMaps,
                    inspectionChecklistBuilder, defaultSubIssues, inspectionImages, inspectionChecklists,
                    mappedPreviousInspectionReport, inspectionDetails);

        }

        return inspectionChecklists;
    }

    private void constructSubpartList(Map<Long, Map<Long, List<InspectionSubpartDetail>>> subPartMap,
                                      List<InspectionIssue> defaultIssues, Map<Long, List<InspectionJob>> inspectionJobs,
                                      Map<Long, List<InspectionReport>> inspectionReports, List<JobPriceMap> jobPriceMaps,
                                      InspectionChecklist.InspectionChecklistBuilder inspectionChecklistBuilder,
                                      List<InspectionSubIssue> defaultSubIssues, List<InspectionImage> inspectionImages,
                                      List<InspectionChecklist> inspectionChecklists, Map<Long,
            List<InspectionReport>> mappedPreviousInspectionReport, Inspection inspectionDetails) {

        List<InspectionSubpartList> inspectionSubpartLists = new ArrayList<>();

        for (Map.Entry<Long, Map<Long, List<InspectionSubpartDetail>>> subPart : subPartMap.entrySet()){
            log.info(subPart.getKey() + ":" + subPart.getValue());
            Long subPartID = subPart.getKey();
            Map<Long, List<InspectionSubpartDetail>> issueMap = subPart.getValue();

            InspectionSubpartList.InspectionSubpartListBuilder inspectionSubpartListBuilder = InspectionSubpartList.builder();
            inspectionSubpartListBuilder.subPartID(subPartID);

            //Map Last Inspection NFI status
            String lastNFIStatus = null;
            List<InspectionReport> previousReports = mappedPreviousInspectionReport.get(subPartID);
            if(previousReports != null) {
                InspectionReport previousReport = previousReports.stream().filter(pr -> pr.getSubPartIssueID().equals(NFI_ISSUE_ID)).findAny().orElse(null);
                if (previousReport != null && previousReport.getSelected()) {
                    lastNFIStatus = "NFI reported in " + getLastInspectionType(inspectionDetails.getInspectionType());
                }
            }
            inspectionSubpartListBuilder.lastNFIStatus(lastNFIStatus);

            List<InspectionSubIssue> defaultSubIssuesMapped = mapWithInspectionJob(defaultSubIssues, inspectionJobs, subPartID);

            inspectionSubpartListBuilder.defaultSubIssues(defaultSubIssuesMapped);
            String comments = mapComments(subPartID,inspectionReports);
            inspectionSubpartListBuilder.comments(comments);

            constructInspectionIssues(issueMap, jobPriceMaps, inspectionChecklistBuilder, inspectionSubpartListBuilder,
                    inspectionJobs, subPartID, defaultIssues, inspectionImages, inspectionReports, inspectionSubpartLists);

        }

        InspectionChecklist inspectionChecklist = inspectionChecklistBuilder.subPartList(inspectionSubpartLists).build();
        inspectionChecklists.add(inspectionChecklist);
    }

    private void constructInspectionIssues(Map<Long, List<InspectionSubpartDetail>> issueMap,
                                           List<JobPriceMap> jobPriceMaps,
                                           InspectionChecklist.InspectionChecklistBuilder inspectionChecklistBuilder,
                                           InspectionSubpartList.InspectionSubpartListBuilder inspectionSubpartListBuilder,
                                           Map<Long, List<InspectionJob>> inspectionJobs, Long subPartID,
                                           List<InspectionIssue> defaultIssues, List<InspectionImage> inspectionImages,
                                           Map<Long, List<InspectionReport>> inspectionReports,
                                           List<InspectionSubpartList> inspectionSubpartLists){
        List<InspectionIssue> inspectionIssues = new ArrayList<>(defaultIssues);

        for(Map.Entry<Long, List<InspectionSubpartDetail>> issue: issueMap.entrySet()) {
            log.info(issue.getKey() + ":" + issue.getValue());
            Long issueID = issue.getKey();
            List<InspectionSubpartDetail> inspectionSubpartDetails = issue.getValue();

            InspectionIssue.InspectionIssueBuilder inspectionIssuesBuilder = InspectionIssue.builder();
            inspectionIssuesBuilder.issueID(issueID);

            List<InspectionSubIssue> inspectionSubIssues = new ArrayList<>();

            for(InspectionSubpartDetail details: inspectionSubpartDetails) {
                Double cost = getJobPrice(jobPriceMaps, details); //get job price from price maps
                InspectionSubIssue inspectionSubIssue = InspectionSubIssue.builder()
                        .jobName(details.getJobName())
                        .subIssueType(APIConstants.CHECKLIST_SUB_ISSUE_TYPE.get("NORMAL"))
                        .jobID(details.getJobID())
                        .cost(cost)
                        .selected(false)
                        .status(APIConstants.SUB_ISSUE_STATUS.get("DEFAULT_JOB_STATUS"))
                        .build();

                inspectionSubIssues.add(inspectionSubIssue);

                inspectionChecklistBuilder.headerName(details.getHeaderName());
                inspectionSubpartListBuilder.subPartName(details.getSubPartName());
                inspectionSubpartListBuilder.allowedSelection(details.getSubPartInputType());
                inspectionSubpartListBuilder.fileRequired(details.getFileUploadRequired());
                inspectionSubpartListBuilder.isMandatory(details.getIsMandatory());
                inspectionSubpartListBuilder.isUnAssured(details.getUnAssured());
                inspectionIssuesBuilder.issueName(details.getIssueName());
                inspectionIssuesBuilder.selected(false);//change according to checklist report
                inspectionIssuesBuilder.issueType(APIConstants.CHECKLIST_ISSUE_TYPE.get("NORMAL"));
            }

            List<InspectionSubIssue> inspectionSubIssueMapped = mapWithInspectionJob(inspectionSubIssues, inspectionJobs, subPartID);
            InspectionIssue inspectionIssue = inspectionIssuesBuilder.subIssues(inspectionSubIssueMapped).build();
            inspectionIssues.add(inspectionIssue);
        }

        List<String> images = mapInspectionImage(subPartID, inspectionImages);
        List<InspectionIssue> inspectionIssuesMapped = mapWithInspectionReport(inspectionIssues, inspectionReports, subPartID);
        InspectionSubpartList inspectionSubpartList = inspectionSubpartListBuilder.issues(inspectionIssuesMapped).images(images).build();
        inspectionSubpartLists.add(inspectionSubpartList);
    }

    private String mapComments(Long subPartID, Map<Long,List<InspectionReport>> inspectionReports) {
        String comments = "";
        if(inspectionReports.containsKey(subPartID)){
            comments = inspectionReports.get(subPartID).get(0).getComments();
        }
        return comments;
    }

    private List<String> mapInspectionImage(Long subPartID, List<InspectionImage> inspectionImages) {
        return inspectionImages.stream()
                .filter(i -> i.getSubPartID().equals(subPartID))
                .map(InspectionImage::getImageURL)
                .collect(Collectors.toList());
    }

    private List<InspectionIssue> mapWithInspectionReport(List<InspectionIssue> inspectionIssues, Map<Long, List<InspectionReport>> inspectionReports, Long subPartID) {
        List<InspectionIssue> mapped = new ArrayList<>();
        inspectionIssues.forEach(inspectionIssue -> {
            Boolean selected = inspectionIssue.getSelected();
            if (inspectionReports.containsKey(subPartID)) {
                InspectionReport inspectionReport = inspectionReports.get(subPartID).stream()
                        .filter(report -> report.getSubPartIssueID().equals(inspectionIssue.getIssueID()))
                        .findAny().orElse(null);
                if (inspectionReport != null) {
                    selected = inspectionReport.getSelected();
                }
            }
            InspectionIssue issue = InspectionIssue.builder()
                    .issueName(inspectionIssue.getIssueName())
                    .issueID(inspectionIssue.getIssueID())
                    .issueType(inspectionIssue.getIssueType())
                    .subIssues(inspectionIssue.getSubIssues())
                    .selected(selected)
                    .build();
            mapped.add(issue);
        });
        return mapped;
    }

    private Double getJobPrice(List<JobPriceMap> jobPriceMaps, InspectionSubpartDetail details) {
        Double cost = 0.0;
        JobPriceMap jobPriceMap = jobPriceMaps.stream().filter(j -> j.getJobID().equals(details.getJobID())).findAny().orElse(null);
        if(jobPriceMap != null) {
            cost = jobPriceMap.getPrice();
        }
        return cost;
    }

    private List<InspectionSubIssue> mapWithInspectionJob(List<InspectionSubIssue> inspectionSubIssues, Map<Long, List<InspectionJob>> inspectionJobs, Long subPartID) {
        List<InspectionSubIssue> mapped = new ArrayList<>();

        inspectionSubIssues.forEach(inspectionSubIssue -> {
            Boolean selected = inspectionSubIssue.getSelected();
            Double cost = inspectionSubIssue.getCost();
            int status = inspectionSubIssue.getStatus();
            if (inspectionJobs.containsKey(subPartID)) {
                InspectionJob inspectionJob = inspectionJobs.get(subPartID).stream()
                        .filter(j -> j.getJobID().equals(inspectionSubIssue.getJobID())).findAny().orElse(null);
                if (inspectionJob != null) {
                    selected = true;
                    cost = inspectionJob.getCost();
                    status = inspectionJob.getStatus();
                }
            }
            mapped.add(InspectionSubIssue.builder()
                    .jobName(inspectionSubIssue.getJobName())
                    .jobID(inspectionSubIssue.getJobID())
                    .subIssueType(inspectionSubIssue.getSubIssueType())
                    .cost(cost)
                    .maxCost(inspectionSubIssue.getMaxCost())
                    .minCost(inspectionSubIssue.getMinCost())
                    .selected(selected)
                    .status(status)
                    .build()
            );
        });
        return mapped;
    }

    private List<InspectionSubIssue> getDefaultSubIssues(InspectionType inspectionType) {
        List<InspectionSubIssue> inspectionSubIssues = new ArrayList<>();
        if(inspectionType.equals(E4A)) {
            return inspectionSubIssues;
        }

        List<Jobs> jobs = jobsService.getDefaultJobs();
        List<Long> jobIDs = jobs.stream().map(Jobs::getId).collect(Collectors.toList());
        List<JobPriceMap> jobPriceMaps = jobPriceMapService.getJobPrice(jobIDs);

        jobPriceMaps.forEach(jobPrice -> {
            Jobs job = jobs.stream().filter(j -> j.getId().equals(jobPrice.getJobID())).findAny().orElse(null);
            if(job != null) {
                InspectionSubIssue inspectionSubIssue = InspectionSubIssue.builder()
                        .jobName(job.getJobName())
                        .subIssueType(APIConstants.CHECKLIST_SUB_ISSUE_TYPE.get("DEFAULT"))
                        .maxCost(jobPrice.getPrice())
                        .minCost(jobPrice.getPrice())
                        .jobID(job.getId())
                        .cost(jobPrice.getPrice())
                        .status(APIConstants.SUB_ISSUE_STATUS.get("DEFAULT_JOB_STATUS"))
                        .build();
                inspectionSubIssues.add(inspectionSubIssue);
            }
        });

        return inspectionSubIssues;
    }


    private Map<Long, Map<Long, Map<Long, List<InspectionSubpartDetail>>>> groupByHeaderSubpartAndIssue(List<InspectionSubpartDetail> inspectionSubpartDetails) {
        return inspectionSubpartDetails.stream().collect(
                Collectors.groupingBy(InspectionSubpartDetail::getHeaderID,
                        groupBySubpartAndIssue()
                ));
    }

    private Collector<InspectionSubpartDetail, ?, Map<Long, Map<Long, List<InspectionSubpartDetail>>>> groupBySubpartAndIssue() {
        return Collectors.groupingBy(InspectionSubpartDetail::getSubPartID, Collectors.groupingBy(InspectionSubpartDetail::getIssueID));
    }


    private InspectionListHeader getScheduledInspections(List<InspectionDetail> inspectionDetails) {
        List<InspectionDetail> inspectionDetailsScheduled = inspectionDetails.stream()
                .filter(c -> c.getStatus() == SCHEDULED && c.getInsertTS() == 0)
                .collect(Collectors.toList());

        List<InspectionDetail> scheduledForToday = new ArrayList<>();
        List<InspectionDetail> scheduledForLater = new ArrayList<>();

        LocalDateTime currentTime = LocalDate.now().atTime(0, 0);
        LocalDateTime futureTime = LocalDate.now().atTime(23, 59);

        inspectionDetailsScheduled.forEach(inspectionDetail -> {
            if(inspectionDetail.getInspectionTime().isAfter(currentTime) && inspectionDetail.getInspectionTime().isBefore(futureTime)){
                scheduledForToday.add(inspectionDetail);
            } else if(inspectionDetail.getInspectionTime().isAfter(futureTime)) {
                scheduledForLater.add(inspectionDetail);
            }
        });

        log.info("scheduledForToday"+ scheduledForToday);

        InspectionList list1 = InspectionList.builder()
                .name(APIConstants.INSPECTION_LIST_TODAY)
                .data(scheduledForToday)
                .count(scheduledForToday.size())
                .build();

        InspectionList list2 = InspectionList.builder()
                .name(APIConstants.INSPECTION_LIST_LATER)
                .data(scheduledForLater)
                .count(scheduledForLater.size())
                .build();

        log.info("list1 "+ list1);
        log.info("list2 "+ list2);


        return InspectionListHeader.builder()
                .name(APIConstants.INSPECTION_HEADER_SCHEDULE)
                .inspectionsList(Arrays.asList(list1,list2))
                .build();
    }

    private InspectionListHeader getPastInspections(List<InspectionDetail> inspectionDetails) {
        LocalDateTime pastTimeUpperLimit = LocalDate.now().atTime(0, 0);
        LocalDateTime currentTime = LocalDateTime.now().plusMinutes(330);
        log.info("currentTime "+currentTime);
        log.info("System.currentTimeMillis() "+System.currentTimeMillis());
        Long currentMS = System.currentTimeMillis();
        Long pastMS = currentMS - APIConstants.INSPECTION_LOWER_LIMIT * 24 * 60 * 60 * 1000;
        log.info("currentMS "+currentMS);
        log.info("pastMS "+pastMS);
        LocalDateTime pastTimeLowerLimit = currentTime.minusDays(APIConstants.INSPECTION_LOWER_LIMIT);
        List<InspectionDetail> pastInspections = inspectionDetails.stream()
                .filter(c -> {
                    if(c.getInsertTS() > 0) {
                        return c.getInsertTS() < currentMS && c.getInsertTS() > pastMS;
                    } else {
                        return c.getInspectionTime().isBefore(pastTimeUpperLimit) && c.getInspectionTime().isAfter(pastTimeLowerLimit);
                    }
                }).collect(Collectors.toList());

        List<InspectionDetail> completed = new ArrayList<>();
        List<InspectionDetail> uncompleted = new ArrayList<>();

        pastInspections.forEach(inspectionDetail -> {
            if(inspectionDetail.getStatus() == SCHEDULED){
                uncompleted.add(inspectionDetail);
            } else if(inspectionDetail.getStatus() == COMPLETED) {
                completed.add(inspectionDetail);
            }
        });

        InspectionList list1 = InspectionList.builder()
                .name(APIConstants.INSPECTION_LIST_UNCOMPLETED)
                .data(uncompleted)
                .count(uncompleted.size())
                .build();

        InspectionList list2 = InspectionList.builder()
                .name(APIConstants.INSPECTION_LIST_COMPLETED)
                .data(completed)
                .count(completed.size())
                .build();

        return InspectionListHeader.builder()
                .name(APIConstants.INSPECTION_HEADER_PAST)
                .inspectionsList(Arrays.asList(list1,list2))
                .build();
    }

    @Override
    public List<InspectionChecklistFlat> getInspectionDifferences(InspectionDifferenceRequest requestParams) throws UCTSupplyException {
        Lead lead = leadService.getLeadDetails(requestParams.getLeadID());
        Inspection inspection1 = inspectionDAO.getInspectionDetailsByLeadIDAndInspectionType(requestParams.getLeadID(), requestParams.getInspectionType1());
        log.info("inspection1 = {}", inspection1);
        Inspection inspection2 = inspectionDAO.getInspectionDetailsByLeadIDAndInspectionType(requestParams.getLeadID(), requestParams.getInspectionType2());
        log.info("inspection2 = {}", inspection2);
        if(inspection1 == null) {
            throw new UCTSupplyException("Inspection details for inspection type "+requestParams.getInspectionType1()+" not found", HttpStatus.BAD_REQUEST);
        }
        if(inspection2 == null) {
            throw new UCTSupplyException("Inspection details for inspection type "+requestParams.getInspectionType2()+" not found", HttpStatus.BAD_REQUEST);
        }

        return getInspectionDifferences(inspection1.getId(), inspection2.getId());
    }

    public List<InspectionChecklistFlat> getInspectionDifferences(Long inspection1, Long inspection2) throws UCTSupplyException {
        List<InspectionChecklistFlat> differences = new ArrayList<>();
        List<InspectionChecklistFlat> inspectionDetails1 = getInspectionChecklistFlattened(inspection1);
        log.info("inspectionDetails1 = {}", inspectionDetails1);
        List<InspectionChecklistFlat> inspectionDetails2 = getInspectionChecklistFlattened(inspection2);
        log.info("inspectionDetails2 = {}", inspectionDetails2);
        Map<String, Boolean> inspectionDetails1Map = new HashMap<>();

        //create hashmap for inspection1 with all visited false
        for (InspectionChecklistFlat inspectionDetails : inspectionDetails1) {
            String key = inspectionDetails.getSubPartName() + inspectionDetails.getIssueName() + inspectionDetails.getJobName() + inspectionDetails.getJobCost();
            inspectionDetails1Map.put(key, false);
        }

        //check in inspection2 if that entry is not present in map then add to difference otherwise mark it visited in map
        for (InspectionChecklistFlat inspectionDetails : inspectionDetails2) {
            String key = inspectionDetails.getSubPartName() + inspectionDetails.getIssueName() + inspectionDetails.getJobName() + inspectionDetails.getJobCost();
            if (!inspectionDetails1Map.containsKey(key)) {
                differences.add(inspectionDetails);
            } else {
                inspectionDetails1Map.put(key, true); //mark as visited
            }
        }

//        //check in inspection1 for those entries which are not visited yet and add them to the difference
//        for(InspectionChecklistFlat inspectionDetails: inspectionDetails1) {
//            String key = inspectionDetails.getSubPartName() + inspectionDetails.getIssueName() + inspectionDetails.getJobName() + inspectionDetails.getJobCost();
//            if(!inspectionDetails1Map.get(key)) {
//                differences.add(inspectionDetails);
//            }
//        }

        differences.sort(InspectionChecklistFlat::compareTo); //sort using header name in comparator method
        return differences;
    }

    public List<InspectionChecklistFlat> getInspectionChecklistFlattened(Long inspectionID) throws UCTSupplyException {
        List<InspectionChecklistFlat> response = new ArrayList<>();
        List<InspectionChecklist> inspectionChecklists = getInspectionChecklistFromHistory(inspectionID);
        log.info("inspectionChecklists = {}", inspectionChecklists);

        for (InspectionChecklist inspectionChecklist : inspectionChecklists) {
            for (InspectionSubpartList inspectionSubpartList : inspectionChecklist.getSubPartList()) {
                List<InspectionIssue> selectedIssues = inspectionSubpartList.getIssues().stream().filter(InspectionIssue::getSelected).collect(Collectors.toList());
                for (InspectionIssue issue : selectedIssues) {
                    List<InspectionSubIssue> selectedSubIssues = issue.getSubIssues().stream().filter(InspectionSubIssue::getSelected).collect(Collectors.toList());
                    for (InspectionSubIssue subIssue : selectedSubIssues) {
                        response.add(constructInspectionChecklistFlat(inspectionChecklist, inspectionSubpartList, issue, subIssue));
                    }
                    if(APIConstants.DEFAULT_ISSUES_ID.contains(issue.getIssueID())) {
                        response.add(constructInspectionChecklistFlat(inspectionChecklist, inspectionSubpartList, issue, null));
                    }
                }
                for(InspectionSubIssue defaultSubIssue : inspectionSubpartList.getDefaultSubIssues()) {
                    if(defaultSubIssue.getSelected()) {
                        response.add(constructInspectionChecklistFlat(inspectionChecklist, inspectionSubpartList,null, defaultSubIssue));
                    }
                }
            }
        }
        return response;
    }

    @Override
    public Inspection getInspectionsByTypeAndCarID(InspectionType inspectionType, Long carID) {
        return inspectionDAO.findByInspectionTypeAndCarID(inspectionType,carID);
    }

    private List<InspectionChecklist> getInspectionChecklistFromHistory(Long inspectionID) throws UCTSupplyException {
        History history = historyService.getLastHistoricalData(inspectionID, EditEntity.INSPECTION_CHECKLIST);
        if(history != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                return mapper.reader(new TypeReference<List<InspectionChecklist>>() {})
                        .readValue(history.getNewData());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                throw new UCTSupplyException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return getInspectionChecklist(inspectionID);
    }

    private InspectionChecklistFlat constructInspectionChecklistFlat(InspectionChecklist inspectionChecklist, InspectionSubpartList inspectionSubpartList, @Null InspectionIssue issue, @Null InspectionSubIssue subIssue) {
        InspectionChecklistFlat.InspectionChecklistFlatBuilder builder =  InspectionChecklistFlat.builder();

        builder.headerName(inspectionChecklist.getHeaderName())
                .subPartName(inspectionSubpartList.getSubPartName());
        if(subIssue != null ){
            builder.jobName(subIssue.getJobName()).jobCost(subIssue.getCost());
        }
        if(issue != null) {
            builder.issueName(issue.getIssueName()).issueSelected(issue.getSelected());
        }

        return builder.build();

    }

    @Override
    public Inspection updateInspection(UpdateInspectionRequest request) throws SupplyException, JsonProcessingException, EntityNotFoundException {
        Inspection oldData = inspectionDAO.findByIdAndIsActive(request.getId(), true);
        log.info("oldData ={}", oldData);
        log.info("request ={}", request);

//        if(oldData.getLastDataSyncTime() > 0 && (request.getInspectionTime() != null || request.getInspectorID() != null || request.getPickupDocumentsCount() <=0 )) {
//            throw new SupplyException("Inspection already in progress");
//        }

        if(oldData == null){
            throw new EntityNotFoundException("Inspection data doesn't exist for inspectionID: "+request.getId());
        }

        ObjectMapper mapper = new ObjectMapper();
        String oldDataJson = mapper.writeValueAsString(oldData);
        log.info("oldDataJson ={}", oldDataJson);

        if(request.getInspectionTime() != null) {
            oldData.setInspectionTime(request.getInspectionTime());
        }

        if(request.getInspectorID() != null) {
            oldData.setInspectorID(request.getInspectorID());
        }
        if(request.getLastDataSyncTime() != null) {
            oldData.setLastDataSyncTime(request.getLastDataSyncTime());
        }
        if(request.getRefurbJobCost() != null) {
            oldData.setRefurbJobCost(request.getRefurbJobCost());
        }
        if(request.getPickupDocumentsCount() > 0) {
            oldData.setPickupDocumentsCount(request.getPickupDocumentsCount());
        }

        if(request.getRefurbID() != null) {
            oldData.setRefurbID(request.getRefurbID());
        }

        oldData.setUpdatedBy(request.getUpdatedBy());
        oldData.setUpdatedAt(LocalDateTime.now());
        log.info("newData ={}", oldData);

        String newDataJson = mapper.writeValueAsString(oldData);
        log.info("newDataJson ={}", newDataJson);

        Inspection updatedInspection = inspectionDAO.updateInspection(oldData);

        //save inspection history
        History savedLeadHistory = historyService.saveHistory(request.getUpdatedBy(), request.getId(), EditEntity.INSPECTION, oldDataJson, newDataJson);

        return updatedInspection;
    }

    @Override
    public Inspection saveE1InspectionForOnlineLead(CreateLeadRequest request, Lead onlineLead) {
        Inspection onlineLeadE1 = inspectionDAO.scheduleInspection(getE1InspectionDataForOnlineLead(request, onlineLead));
        return onlineLeadE1;
    }

    private Inspection getE1InspectionDataForOnlineLead(CreateLeadRequest request, Lead onlineLead) {
        return Inspection.builder()
                .inspectionType(E1)
                .leadID(onlineLead.getId())
                .inspectorID(1L)
                .createdBy(request.getAdminID())
                .createdAt(LocalDateTime.now())
                .inspectionTime(LocalDateTime.now())
                .status(COMPLETED)
                .refurbJobCost(request.getRefubCost())
                .isActive(true)
                .build();
    }
}
