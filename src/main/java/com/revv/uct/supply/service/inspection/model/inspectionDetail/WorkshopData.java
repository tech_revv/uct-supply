package com.revv.uct.supply.service.inspection.model.inspectionDetail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkshopData {
    private Long workshopID;
    private String workshopName;
}
