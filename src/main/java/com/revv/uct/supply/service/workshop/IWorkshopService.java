package com.revv.uct.supply.service.workshop;

import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.controller.workshop.model.CreateWorkshopRequest;
import com.revv.uct.supply.controller.workshop.model.WorkshopData;
import com.revv.uct.supply.db.workshop.entity.Workshop;

import java.util.List;

public interface IWorkshopService {

    List<Workshop> getAllActiveWorkshops();

    List<WorkshopData> getWorkshopsData();

    Workshop createWorkshop(CreateWorkshopRequest createWorkshopRequest, UserData userData);

    List<Workshop> findAllByWorkshopID(List<Long> workshopIDs);

    boolean existById(Long workshopID);

}
