package com.revv.uct.supply.service.carModel;

import com.revv.uct.supply.db.carModel.entity.CarModel;

import java.util.List;

public interface ICarModelService {
    List<CarModel> uploadCarModels(List<Object> list);

    List<CarModel> findAllCarModels();
}
