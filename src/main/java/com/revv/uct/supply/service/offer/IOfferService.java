package com.revv.uct.supply.service.offer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.controller.offer.model.CalculateOfferPriceRequest;
import com.revv.uct.supply.controller.offer.model.CreateOfferRequest;
import com.revv.uct.supply.controller.offer.model.ReviseOfferRequest;
import com.revv.uct.supply.controller.offer.model.UpdateOfferRequest;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.lead.entity.Lead;
import com.revv.uct.supply.service.offer.model.CalculateOfferPrice;
import com.revv.uct.supply.service.offer.model.GenerateOfferData;
import com.revv.uct.supply.db.offer.entity.Offer;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;

public interface IOfferService {
    Offer createOffer(CreateOfferRequest request) throws SupplyException, JsonProcessingException;

    GenerateOfferData getGenerateOfferData(Long inspectionID) throws UCTSupplyException;

    CalculateOfferPrice calculateOfferPrice(CalculateOfferPriceRequest request);

    Offer updateOffer(UpdateOfferRequest request) throws SupplyException, JsonProcessingException;

    Offer reviseOffer(ReviseOfferRequest request) throws SupplyException, JsonProcessingException;

    Offer getOfferDetailsForLead(Long leadID);

    Offer saveOfferForOnlineLead(CreateLeadRequest request, Lead onlineLead, Inspection onlineLeadE1);
}
