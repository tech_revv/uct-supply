package com.revv.uct.supply.service.refurbishment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubPartIssue {
    private String issueName;
    private Long issueID;
}
