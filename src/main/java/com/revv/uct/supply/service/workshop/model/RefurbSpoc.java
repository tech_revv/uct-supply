package com.revv.uct.supply.service.workshop.model;

import com.revv.uct.supply.db.enums.AdminType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RefurbSpoc {

    private Long id;
    private String name;
    private AdminType adminType;

}
