package com.revv.uct.supply.service.inspection.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.InspectionDetail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InspectionList {
    @JsonProperty("name") private String name;
    @Builder.Default
    @JsonProperty("count") private Integer count = 0;
    @JsonProperty("data") private List<InspectionDetail> data;
}
