package com.revv.uct.supply.service.refurbishment;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.controller.refurbishment.model.ScheduleRefurbishmentRequest;
import com.revv.uct.supply.controller.refurbishment.model.SubmitDocumentRequest;
import com.revv.uct.supply.controller.refurbishment.model.UpdateRefurbRequest;
import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.refurbishment.entity.Refurbishment;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.RefurbList;
import com.revv.uct.supply.service.refurbishment.model.RefurbJobListForWorkshop;
import com.revv.uct.supply.service.refurbishment.model.RefurbScreen;
import com.revv.uct.supply.service.refurbishment.model.ScheduleRefurbishmentResponse;

import java.util.List;

public interface IRefurbishmentService {

    List<RefurbList> findRefurbishments(Long refurbSpocID, List<InspectionStatus> refurbishmentStatuses);

    RefurbScreen getRefurbJobList(Long refurbID) throws UCTSupplyException;

    void saveDocuments(Long userID, Long refurbID, SubmitDocumentRequest requestBody) throws UCTSupplyException;

    Refurbishment updateRefurbishemnt(Long refurbID, Long userID, UpdateRefurbRequest request) throws UCTSupplyException;

    List<Refurbishment> findRefurbishmentsByCarIDAndStatus(List<Long> carIDs, List<InspectionStatus> completed);

    List<RefurbJobListForWorkshop> getRefurbJobListForSendToWorkshop(Long carID);

    ScheduleRefurbishmentResponse scheduleRefurbishment(ScheduleRefurbishmentRequest request) throws EntityNotFoundException, JsonProcessingException, SupplyException, UCTSupplyException;

    Refurbishment getRefurbDetails(Long refurbID);
}
