package com.revv.uct.supply.service.offer.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revv.uct.supply.constants.ChallanStatus;
import com.revv.uct.supply.constants.ServiceHistoryStatus;
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.controller.lead.model.UpdateLeadRequest;
import com.revv.uct.supply.controller.offer.model.CalculateOfferPriceRequest;
import com.revv.uct.supply.controller.offer.model.CreateOfferRequest;
import com.revv.uct.supply.controller.offer.model.ReviseOfferRequest;
import com.revv.uct.supply.controller.offer.model.UpdateOfferRequest;
import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.enums.*;
import com.revv.uct.supply.db.history.entity.History;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.lead.entity.Lead;
import com.revv.uct.supply.service.commonDocument.ICommonDocumentService;
import com.revv.uct.supply.service.documentStageMaster.IDocumentStageMasterService;
import com.revv.uct.supply.service.inspection.model.InspectionChecklistFlat;
import com.revv.uct.supply.service.offer.model.CalculateOfferPrice;
import com.revv.uct.supply.service.offer.model.GenerateOfferData;
import com.revv.uct.supply.db.offer.dao.IOfferDao;
import com.revv.uct.supply.db.offer.entity.Offer;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.history.IHistoryService;
import com.revv.uct.supply.service.inspection.IInspectionService;
import com.revv.uct.supply.service.inspection.model.InspectionChecklist;
import com.revv.uct.supply.service.inspection.model.InspectionVerdictDetailForGenerateOffer;
import com.revv.uct.supply.service.lead.ILeadService;
import com.revv.uct.supply.service.offer.IOfferService;
import com.revv.uct.supply.service.offerImage.IOfferImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class OfferServiceImpl implements IOfferService {
    private IOfferDao offerDao;
    private IInspectionService inspectionService;
    private IOfferImageService offerImageService;
    private IHistoryService historyService;
    private ILeadService leadService;
    private ICommonDocumentService commonDocumentService;
    private IDocumentStageMasterService documentStageMasterService;

    @Autowired
    public OfferServiceImpl(IOfferDao offerDao, @Lazy IInspectionService inspectionService, IOfferImageService offerImageService, IHistoryService historyService, ILeadService leadService, ICommonDocumentService commonDocumentService, IDocumentStageMasterService documentStageMasterService) {
        this.offerDao = offerDao;
        this.inspectionService = inspectionService;
        this.offerImageService = offerImageService;
        this.historyService = historyService;
        this.leadService = leadService;
        this.commonDocumentService = commonDocumentService;
        this.documentStageMasterService = documentStageMasterService;
    }

    @Override
    public Offer createOffer(CreateOfferRequest request) throws SupplyException, JsonProcessingException {
        //check for challanStatus and serviceHistoryStatus
        if(request.getChallanStatus() == null || request.getChallanStatus().length() <= 0) {
            throw new SupplyException("Challan status is mandatory.");
        }
        if(request.getServiceHistoryStatus() == null || request.getServiceHistoryStatus().length() <= 0) {
            throw new SupplyException("Service history status is mandatory.");
        }

        //save offer data
        Offer offer = getOfferFromRequest(request);
        Offer savedOffer = offerDao.createOffer(offer);
        log.info("savedOffer==>>", savedOffer);

        //mark leadStatus as OFFER_GENERATED
        markLeadStatusAsOfferGenerated(request.getLeadID(), request.getCreatedBy());

        //save service documents
        if(request.getServiceDocs() != null && request.getServiceDocs().length > 0) {
            //offerImageService.saveOfferImages(savedOffer.getId(), request.getCreatedBy(), request.getServiceDocs());

            //get documentStageMasterID for serviceDocs at offerCreation stage

            saveOfferDocuments(savedOffer.getId(), request.getCreatedBy(), request.getServiceDocs());
        }

        return savedOffer;
    }

    private void saveOfferDocuments(Long offerID, Long createdBy, String[] serviceDocs) throws SupplyException {
        Long documentStageMasterID = documentStageMasterService.findIDByTypeAndStageAndisActive(DocumentType.SERVICE_HISTORY, DocumentStage.OFFER_CREATION);
        List<CommonDocument> commonDocuments = new ArrayList<>();

        for(String doc : serviceDocs) {
            commonDocuments.add(
                    CommonDocument.builder()
                            .commonDocumentEntity(CommonDocumentEntity.OFFER)
                            .entityID(offerID)
                            .documentStageMasterID(documentStageMasterID)
                            .createdBy(createdBy)
                            .updatedBy(createdBy)
                            .imageURL(doc)
                            .build()
            );
        }
        if(commonDocuments.size() > 0) {
            commonDocumentService.saveDocuments(commonDocuments);
        }
    }

    private void markLeadStatusAsOfferGenerated(Long leadID, Long createdBy) throws SupplyException, JsonProcessingException {
        UpdateLeadRequest request = UpdateLeadRequest.builder()
                .id(leadID)
                .adminID(createdBy)
                .leadStatus(LeadStatus.valueOf("OFFER_GENERATED"))
                .build();
        leadService.updateLead(request);
    }

    private Offer getOfferFromRequest(CreateOfferRequest request) {
        Offer offer = Offer.builder()
                .createdBy(request.getCreatedBy())
                .inspectionID(request.getInspectionID())
                .leadID(request.getLeadID())
                .challanStatus(request.getChallanStatus())
                .serviceHistoryStatus(request.getServiceHistoryStatus())
                .offeredPrice(request.getOfferedPrice())
                .basePrice(request.getBasePrice())
                .refubCost(request.getRefubCost())
                .challanCost(request.getChallanCost())
                .adjustCost(request.getAdjustCost())
                .createdAt(LocalDateTime.now())
                .offerStatus(OfferStatus.valueOf("CREATED"))
                .isActive(true)
                .isRevised(false)
                .build();
        if(request.getServiceDocs() != null && request.getServiceDocs().length > 0) {
            offer.setNoOfDocuments(request.getServiceDocs().length);
        }
        else {
            offer.setNoOfDocuments(0);
        }
        return offer;
    }

    private void markOfferInActive(Long existingOfferID, Long updatedBy) {
        offerDao.markOfferInActive(existingOfferID, updatedBy);
    }

    @Override
    public GenerateOfferData getGenerateOfferData(Long inspectionID) throws UCTSupplyException {

        List<InspectionChecklistFlat> inspectionChecklist = inspectionService.getInspectionChecklistFlattened(inspectionID);
        //InspectionVerdictDetail inspectionVerdictData = inspectionService.getInspectionVerdictData(inspectionID);

        InspectionVerdictDetailForGenerateOffer inspectionVerdictData = inspectionService.getInspectionVerdictDataForGenerateOffer(inspectionID);

        List<ChallanStatus> challanStatus = ChallanStatus.getChallanStatus();
        List<ServiceHistoryStatus> serviceHistoryStatus = ServiceHistoryStatus.getServiceHistoryStatus();

        return new GenerateOfferData(inspectionChecklist, inspectionVerdictData, challanStatus, serviceHistoryStatus);
    }

    @Override
    public CalculateOfferPrice calculateOfferPrice(CalculateOfferPriceRequest request) {
        Double basePrice = request.getBasePrice();
        Double refubCost = request.getRefubCost();
        Double challanCost = 0.0;
        Double adjustCost = 0.0;

        if(request.getChallanCost() != null)
            challanCost = request.getChallanCost();

        if(request.getAdjustCost() != null) {
            adjustCost = request.getAdjustCost();
        }

        Double calculatedPrice = (basePrice - refubCost - challanCost + adjustCost);

        return new CalculateOfferPrice(calculatedPrice);
    }

    @Override
    public Offer updateOffer(UpdateOfferRequest request) throws SupplyException, JsonProcessingException {
        log.info("Update offer impl");
        Offer oldData = offerDao.findByIdAndIsActive(request.getId(), true);
        log.info("oldData ={}", oldData);

        //if data doesn't exist for given offerID
        if(oldData == null){
            throw new SupplyException("Offer data doesn't exist for offerID: "+request.getId());
        }

        ObjectMapper mapper = new ObjectMapper();
        String oldDataJson = mapper.writeValueAsString(oldData);
        log.info("oldDataJson ={}", oldDataJson);

        Offer newData = oldData;

        if(request.getOfferStatus() != null) {
            if(request.getOfferStatus().equals(OfferStatus.REJECTED)) {
                //rejection reason is required for rejected offer(will be updated in leads collection)
                if(request.getRejectionReason() == null) {
                    throw new SupplyException("RejectionSubStatus required for rejected offer.");
                }

                //marking leadStatus as REJECTED for rejected offer
                markLeadStatusAsRejected(request.getLeadID(), request.getUpdatedBy(), request.getRejectionReason());
            }

            if(request.getOfferStatus().equals(OfferStatus.ACCEPTED)) {
                //marking leadStatus as OFFER_ACCEPTED
                markLeadStatusAsOfferAccepted(request.getLeadID(), request.getUpdatedBy());
            }

            if(request.getOfferStatus().equals(OfferStatus.APPROVED)) {
                //marking leadStatus as OFFER_APPROVED
                markLeadStatusAsOfferApproved(request.getLeadID(), request.getUpdatedBy());
            }

            newData.setOfferStatus(OfferStatus.valueOf(String.valueOf(request.getOfferStatus())));
        }

        if(request.getRefubCost() != null) {
            newData.setRefubCost(request.getRefubCost());
        }
        if(request.getE2RefurbCost() != null) {
            newData.setRefubCostE2(request.getE2RefurbCost());
        }

        if(request.getServiceHistoryStatus() != null) {
            newData.setServiceHistoryStatus(request.getServiceHistoryStatus());
            newData.setNoOfDocuments(newData.getNoOfDocuments() + request.getServiceDocs().length);
            if(request.getServiceDocs() != null && request.getServiceDocs().length > 0) {
                saveOfferDocuments(newData.getId(), request.getUpdatedBy(), request.getServiceDocs());
            }
        }

        newData.setUpdatedBy(request.getUpdatedBy());
        newData.setUpdatedAt(LocalDateTime.now());
        log.info("newData ={}", newData);

        String newDataJson = mapper.writeValueAsString(newData);
        log.info("newDataJson ={}", newDataJson);

        Offer updatedOffer = offerDao.updateOffer(newData);

        //save offer history
        History savedLeadHistory = historyService.saveHistory(request.getUpdatedBy(), request.getId(), EditEntity.OFFER, oldDataJson, newDataJson);

        return updatedOffer;
    }

    private void markLeadStatusAsTokenPaid(Long leadID, Long updatedBy) throws SupplyException, JsonProcessingException {
        UpdateLeadRequest request = UpdateLeadRequest.builder()
                .id(leadID)
                .adminID(updatedBy)
                .leadStatus(LeadStatus.TOKEN_PAID)
                .build();
        leadService.updateLead(request);
    }

    private void markLeadStatusAsOfferApproved(Long leadID, Long updatedBy) throws SupplyException, JsonProcessingException {
        UpdateLeadRequest request = UpdateLeadRequest.builder()
                .id(leadID)
                .adminID(updatedBy)
                .leadStatus(LeadStatus.OFFER_APPROVED)
                .build();
        leadService.updateLead(request);
    }

    @Override
    public Offer reviseOffer(ReviseOfferRequest request) throws SupplyException, JsonProcessingException {
        //get existing offer
        Offer existingOffer = offerDao.findByIdAndIsActive(request.getId(), true);

        if(existingOffer == null) {
            throw new SupplyException("Offer data doesn't exist for offerID: "+request.getId());
        }

        //get documents for existing offer
        String[] serviceDocs = null;
        if(existingOffer.getNoOfDocuments() > 0) {
            serviceDocs = getServiceDocsForExistingOffer(existingOffer.getId(), existingOffer.getNoOfDocuments());
            System.out.println(serviceDocs.length);
            if(serviceDocs == null || serviceDocs.length == 0) {
                throw new SupplyException("Service docs not found");
            }
            if(serviceDocs.length != existingOffer.getNoOfDocuments()) {
                throw new SupplyException("Service docs count mismatch");
            }
        }

        //mark existing offer and docs inActive
        markInActiveExistingOfferAndServiceDocs(existingOffer.getId(), request.getUpdatedBy());

        //save offer data
        Offer offer = getReviseOfferFromRequest(request, existingOffer);
        Offer revisedOffer = offerDao.createOffer(offer);
        log.info("revisedOffer==>>", revisedOffer);


        //save service documents
        if(revisedOffer.getNoOfDocuments() > 0 && serviceDocs != null && serviceDocs.length > 0) {
            saveOfferDocuments(revisedOffer.getId(), request.getUpdatedBy(), serviceDocs);
        }


        saveOfferHistory(request.getUpdatedBy(), existingOffer, revisedOffer);

        return revisedOffer;
    }

    private String[] getServiceDocsForExistingOffer(Long existingOfferID, int noOfDocuments) {
        String[] serviceDocs = commonDocumentService.getDocuments(existingOfferID, 7L, CommonDocumentEntity.OFFER).toArray(new String[0]);
        return serviceDocs;
    }


    @Override
    public Offer getOfferDetailsForLead(Long leadID) {
        List<Offer> offers = offerDao.findByLeadIDAndIsActive(leadID, true);
        if(offers.size() > 0) {
            return offers.get(0);
        }
        return null;
    }

    @Override
    public Offer saveOfferForOnlineLead(CreateLeadRequest request, Lead onlineLead, Inspection onlineLeadE1) {
        Offer onlineLeadOffer = offerDao.createOffer(getOfferDetailsForOnlineLead(request, onlineLead, onlineLeadE1));
        return onlineLeadOffer;
    }

    private Offer getOfferDetailsForOnlineLead(CreateLeadRequest request, Lead onlineLead, Inspection onlineLeadE1) {
        return Offer.builder()
                .createdBy(request.getAdminID())
                .createdAt(LocalDateTime.now())
                .leadID(onlineLead.getId())
                .inspectionID(onlineLeadE1.getId())
                .challanStatus("Ok")
                .serviceHistoryStatus("Ok")
                .offeredPrice(request.getOfferedPrice())
                .basePrice(request.getBasePrice())
                .refubCost(request.getRefubCost())
                .offerStatus(OfferStatus.CREATED)
                .isActive(true)
                .build();
    }

    private Offer getReviseOfferFromRequest(ReviseOfferRequest request, Offer existingOffer) {
        /*
        Offer offer = Offer.builder()
                .createdBy(request.getCreatedBy())
                .inspectionID(request.getInspectionID())
                .leadID(request.getLeadID())
                .challanStatus(request.getChallanStatus())
                .serviceHistoryStatus(request.getServiceHistoryStatus())
                .offeredPrice(request.getOfferedPrice())
                .basePrice(request.getBasePrice())
                .refubCost(request.getRefubCost())
                .challanCost(request.getChallanCost())
                .adjustCost(request.getAdjustCost())
                .createdAt(LocalDateTime.now())
                .offerStatus(request.getOfferStatus())
                .isActive(true)
                .isRevised(true)
                .refubCostE2(request.getRefubCostE2())
                .offerReviseStage(request.getOfferReviseStage())
                .build();

        if(request.getServiceDocs() != null && request.getServiceDocs().length > 0) {
            offer.setNoOfDocuments(request.getServiceDocs().length);
        }
        else {
            offer.setNoOfDocuments(0);
        }

         */
        Offer offer = Offer.builder()
                .inspectionID(existingOffer.getInspectionID())
                .leadID(existingOffer.getLeadID())
                .challanStatus(existingOffer.getChallanStatus())
                .serviceHistoryStatus(existingOffer.getServiceHistoryStatus())
                .offeredPrice(existingOffer.getOfferedPrice())
                .basePrice(existingOffer.getBasePrice())
                .refubCost(existingOffer.getRefubCost())
                .refubCostE2(existingOffer.getRefubCostE2())
                .challanCost(existingOffer.getChallanCost())
                .adjustCost(existingOffer.getAdjustCost())
                .offerStatus(existingOffer.getOfferStatus())
                .noOfDocuments(existingOffer.getNoOfDocuments())
                .isActive(true)
                .isRevised(true)
                .offerReviseStage(request.getOfferReviseStage())
                .createdBy(existingOffer.getCreatedBy())
                .updatedBy(request.getUpdatedBy())
                .createdAt(existingOffer.getCreatedAt())
                .updatedAt(LocalDateTime.now())
                .build();


        if(request.getOfferedPrice() != null) {
            offer.setOfferedPrice(request.getOfferedPrice());
        }
        if(request.getBasePrice() != null) {
            offer.setBasePrice(request.getBasePrice());
        }
        if(request.getRefubCost() != null) {
            offer.setRefubCost(request.getRefubCost());
        }
        if(request.getChallanCost() != null) {
            offer.setChallanCost(request.getChallanCost());
        }
        if(request.getAdjustCost() != null) {
            offer.setAdjustCost(request.getAdjustCost());
        }
        if(request.getRefubCostE2() != null) {
            offer.setRefubCostE2(request.getRefubCostE2());
        }

        return offer;
    }

    private void markInActiveExistingOfferAndServiceDocs(Long id, Long updatedBy) {
        markOfferInActive(id, updatedBy);
        markOfferServiceDocumentsInActive(id, updatedBy);
    }

    private void markOfferServiceDocumentsInActive(Long existingOfferID, Long updatedBy) {
        //List<CommonDocument> commonDocuments = commonDocumentService.findAllByEntityIDAndIsActive(existingOfferID, true);
        List<CommonDocument> commonDocuments = commonDocumentService.getCommonDocumentData(existingOfferID, 7L, CommonDocumentEntity.OFFER);
        if(commonDocuments.size() > 0) {
            List<Long> commonDocumentIDs = new ArrayList<>();
            commonDocuments.forEach(doc -> {
                commonDocumentIDs.add(doc.getId());
            });
            commonDocumentService.markDocumentsInactive(CommonDocumentEntity.OFFER, existingOfferID, updatedBy, commonDocumentIDs);
        }
    }

    private void saveOfferHistory(Long createdBy, Offer existingOffer, Offer revisedOffer) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String oldDataJson = mapper.writeValueAsString(existingOffer);
        log.info("oldDataJson ={}", oldDataJson);

        String newDataJson = mapper.writeValueAsString(revisedOffer);
        log.info("newDataJson ={}", newDataJson);

        historyService.saveHistory(createdBy, existingOffer.getId(), EditEntity.OFFER, oldDataJson, newDataJson);
    }

    private void markLeadStatusAsOfferAccepted(Long leadID, Long updatedBy) throws SupplyException, JsonProcessingException {
        UpdateLeadRequest request = UpdateLeadRequest.builder()
                .id(leadID)
                .adminID(updatedBy)
                .leadStatus(LeadStatus.OFFER_ACCEPTED)
                .build();
        leadService.updateLead(request);
    }

    private void markLeadStatusAsRejected(Long leadID, Long updatedBy, String rejectionReason) throws SupplyException, JsonProcessingException {
        UpdateLeadRequest request = UpdateLeadRequest.builder()
                .id(leadID)
                .adminID(updatedBy)
                .leadStatus(LeadStatus.REJECTED)
                .leadRejectSubStatus(LeadRejectSubStatus.OFFER_REJECTED)
                .rejectionReason(rejectionReason)
                .build();
        leadService.updateLead(request);
    }

}
