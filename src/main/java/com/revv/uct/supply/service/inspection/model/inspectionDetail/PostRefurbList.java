package com.revv.uct.supply.service.inspection.model.inspectionDetail;

import com.revv.uct.supply.db.enums.InspectionCardType;
import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.enums.Transmission;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
public class PostRefurbList extends InspectionDetail{

    private Long leadID;
    private String carRegistrationNumber;
    private LocalDateTime registrationTime;
    private String registrationTimeDisplay;
    private String kmDriven;
    private String kmDrivenDisplay;
    private String refurbSpoc;
    private String e3Inspector;
    private Long e3InspectionID;
    private Long carID;
    private String workshopName;
    private String workshopAddress;
    private List<WorkshopData> workshops;
    private List<RefurbSpocData> refurbSpocs;
    private int noOfJobs;

    public PostRefurbList(
            Long inspectionID, InspectionType inspectionType, LocalDateTime inspectionTime, InspectionStatus status, Long insertTS,
            String carMake, String carModel, String carVariant, String carFuel, Transmission carTransmission, int startYear, int endYear,
            Long leadID, String registrationNumber, LocalDateTime registrationTimestamp, Double kmsDriven,
            Long carID, String e3Inspector, Long e3InspectionID) {
        this.id = inspectionID;
        this.inspectionIDDisplay = "ID-"+inspectionID;
        this.inspectionType = inspectionType.toString();
        this.inspectionTime = inspectionTime.plusMinutes(330);
        this.inspectionTimeDisplay = inspectionTime.plusMinutes(330).format(formatter);
        this.status = status;
        this.insertTS = insertTS != null ? insertTS : 0L;
        this.carMake = carMake;
        this.carModel = carModel;
        this.carVariant = carVariant;
        this.carFuel = carFuel;
        this.carTransmission = carTransmission;
        this.carYear = ""+startYear+"-"+endYear;
        this.leadID = leadID;
        this.carRegistrationNumber = registrationNumber;
        this.registrationTimeDisplay = registrationTimestamp.format(formatter2);
        this.registrationTime = registrationTimestamp;
        this.kmDrivenDisplay = new DecimalFormat("#,###.00").format(kmsDriven) + " km";
        this.kmDriven = kmsDriven.toString();
        this.carID = carID;
        this.e3Inspector = e3Inspector;
        this.e3InspectionID = e3InspectionID;
        this.cardType = InspectionCardType.POST_REFURBISHMENT;
        this.noOfJobs = 0;
    }
}
