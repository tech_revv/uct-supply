package com.revv.uct.supply.service.inspection.model.inspectionDetail;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.revv.uct.supply.db.enums.InspectionCardType;
import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.Transmission;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Data
public class InspectionDetail {

    protected Long id;
    protected String inspectionIDDisplay;
    protected String inspectionType;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    protected LocalDateTime inspectionTime;
    protected String inspectionTimeDisplay;
    protected InspectionStatus status;
    protected Long insertTS;
    protected String carModel;
    protected String carMake;
    protected String carVariant;
    protected String carFuel;
    protected Transmission carTransmission;
    protected String carYear;
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE dd MMM yyyy, h:mma");
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    protected InspectionCardType cardType = InspectionCardType.INSPECTION;
    protected Boolean editLead = false;

    public InspectionDetail(){}

}
