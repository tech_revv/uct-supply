package com.revv.uct.supply.service.inspectionImage.impl;

import com.revv.uct.supply.controller.inspection.model.InspectionImageRequest;
import com.revv.uct.supply.db.inspectionImage.dao.IInspectionImageDAO;
import com.revv.uct.supply.db.inspectionImage.entity.InspectionImage;
import com.revv.uct.supply.service.inspectionImage.IInspectionImageService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@NoArgsConstructor
public class InspectionImageServiceImpl implements IInspectionImageService {
    @Autowired
    IInspectionImageDAO inspectionImageDAO;


    @Override
    public List<InspectionImage> getInspectionImages(Long inspectionID) {
        return inspectionImageDAO.getInspectionImages(inspectionID);
    }

    @Override
    public void markImagesInactive(Long inspectorID, Long inspectionID) {
        inspectionImageDAO.markImagesInactive(inspectorID, inspectionID);
    }

    @Override
    public void saveInspectionImage(Long inspectorID, Long inspectionID, Long subPartID, List<String> images) {
        List<InspectionImage> inspectionImages = new ArrayList<>();
        for (String image : images) {
            inspectionImages.add(
                    InspectionImage.builder()
                            .inspectionID(inspectionID)
                            .subPartID(subPartID)
                            .imageURL(image)
                            .createdBy(inspectorID)
                            .build()
            );
        }
        if(inspectionImages.size() > 0) {
            inspectionImageDAO.saveInspectionImage(inspectionImages);
        }
    }
}
