package com.revv.uct.supply.service.version.impl;

import com.revv.uct.supply.db.version.dao.IVersionDao;
import com.revv.uct.supply.db.version.entity.Version;
import com.revv.uct.supply.service.version.IVersionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class VersionServiceImpl implements IVersionService {

    IVersionDao versionDao;

    public VersionServiceImpl(IVersionDao versionDao) {
        this.versionDao = versionDao;
    }

    @Override
    public int getCurrentVersion(String versionType) {
        Version version =versionDao.getCurrentVersion(versionType);
        int currentVersion = version.getVersionNumber();
        return currentVersion;
    }

    @Override
    public void disableCurrentVersion(String versionType, int version) {
        versionDao.disableCurrentVersion(versionType, version);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Version addNewVersion(String versionType, int newVersion, boolean enabled) {
        Version version = new Version(versionType, newVersion, enabled);
        Version version1 = getVersionFromRequest(versionType, newVersion, enabled);
        //version.setVersionType(versionType);
        //version.setVersionNumber(newVersion);
        //version.setEnabled(enabled);
        Version addedVersion = versionDao.addNewVersion(version1);
        return addedVersion;
    }

    private Version getVersionFromRequest(String versionType, int newVersion, boolean enabled) {
        return Version.builder()
                .versionType(versionType)
                .versionNumber(newVersion)
                .enabled(enabled)
                .build();
    }
}
