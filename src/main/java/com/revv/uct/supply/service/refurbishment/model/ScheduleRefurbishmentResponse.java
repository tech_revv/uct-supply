package com.revv.uct.supply.service.refurbishment.model;

import com.revv.uct.supply.db.refurbishment.entity.Refurbishment;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ScheduleRefurbishmentResponse {
    private Refurbishment refurbishment;
    private String message;
}
