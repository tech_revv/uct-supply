package com.revv.uct.supply.service.jobs;

import com.revv.uct.supply.db.jobs.entity.Jobs;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;

import java.util.List;

public interface IJobsService {
    List<Jobs> uploadJobs(List<Object> list) throws EntityNotFoundException, SupplyException;

    boolean existsByJobNameAndIsActive(String jobName, boolean isActive);

    Jobs findByJobNameAndIsActive(String jobName, boolean b);

    List<Jobs> getDefaultJobs();

    List<Jobs> getAllJobsByIDs(List<Long> jobIDs);
}
