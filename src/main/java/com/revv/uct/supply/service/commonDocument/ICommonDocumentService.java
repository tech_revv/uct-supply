package com.revv.uct.supply.service.commonDocument;

import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.enums.CommonDocumentEntity;
import com.revv.uct.supply.db.enums.DocumentStage;

import java.util.List;

public interface ICommonDocumentService {
    void saveDocuments(List<CommonDocument> commonDocuments);

    void markDocumentsInactive(CommonDocumentEntity entityName, Long entityID, Long adminID, List<Long> documentIDs);

    List<CommonDocument> findAllByEntityIDAndIsActive(Long existingOfferID, boolean b);

    List<String> getDocuments(Long entityID);

    List<CommonDocument> getDocumentsForEntityIDAndName(Long entityID, CommonDocumentEntity entityName);

    List<String> getDocuments(Long entityID, Long dsmID, CommonDocumentEntity entity);

    List<CommonDocument> getDocumentsForEntityIDAndNameAndStage(Long entityID, CommonDocumentEntity entityName, List<Long> documentStageMasterIDs);

    List<String> getDocumentsByDSMStage(Long entityID, DocumentStage documentStage, CommonDocumentEntity entity);

    List<CommonDocument> getCommonDocumentData(Long entityID, long dsmID, CommonDocumentEntity entity);

    void markDocumentsInactiveAgainstDocumentStageMaster(CommonDocumentEntity entityName, Long entityID, Long adminID, List<Long> documentStageMasterIDs);
}
