package com.revv.uct.supply.service.inspection.model;

import com.revv.uct.supply.db.enums.InspectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InspectionVerdictDetailForGenerateOffer {
    private Long leadID;
    private String title;
    private Boolean warning;
    private InspectionType inspectionType;
    @Builder.Default
    private int issuesCount = 0;
    @Builder.Default
    private int jobsCount = 0;
    @Builder.Default
    private int unAssuredPartCount = 0;
    @Builder.Default
    private int okPartCount = 0;
    @Builder.Default
    private int nfiPartCount = 0;
    @Builder.Default
    private int refurbishedCost = 0;
}
