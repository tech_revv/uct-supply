package com.revv.uct.supply.service.inspection.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.DocumentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InspectionPickupChecklist {
    @JsonProperty("documentID") private Long documentID;
    @JsonProperty("documentType") private DocumentType documentType;
    @JsonProperty("documentName") private String documentName;
    @Builder.Default
    @JsonProperty("isMandatory") private boolean isMandatory = true;
    @Builder.Default
    @JsonProperty("documentPath") private String documentPath = null;
}
