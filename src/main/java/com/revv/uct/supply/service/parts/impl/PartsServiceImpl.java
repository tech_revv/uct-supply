package com.revv.uct.supply.service.parts.impl;

import com.revv.uct.supply.db.enums.AccessoryType;
import com.revv.uct.supply.db.header.entity.Header;
import com.revv.uct.supply.db.parts.dao.IPartsDao;
import com.revv.uct.supply.db.parts.entity.Parts;
import com.revv.uct.supply.db.version.entity.Version;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.header.IHeaderService;
import com.revv.uct.supply.service.parts.IPartsService;
import com.revv.uct.supply.service.version.IVersionService;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class PartsServiceImpl implements IPartsService {

    private final IPartsDao partsDao;
    private final IVersionService versionService;
    private final IHeaderService headerService;

    @Autowired
    public PartsServiceImpl(IPartsDao partsDao, IVersionService versionService, IHeaderService headerService) {
        this.partsDao = partsDao;
        this.versionService = versionService;
        this.headerService = headerService;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Parts> uploadParts(List<Object> list) throws SupplyException, EntityNotFoundException {
        Iterator<Object> checkHeader = list.iterator();
        while (checkHeader.hasNext()){
            JSONObject obj = new JSONObject((Map) checkHeader.next());
            if(!headerService.existsByHeaderNameAndIsActive((String) obj.get("headerName"), true)){
                throw new SupplyException("Header data doesn't exist for headerName: "+(String) obj.get("headerName"));
            }
            String partName = (String) obj.get("partName");
            if(partName == null || partName.length() <= 0) {
                throw new EntityNotFoundException("Part name is mandatory");
            }
            if(existsByPartNameAndIsActive(partName, true)) {
                throw new SupplyException("Part data already exist for partName: "+(String) obj.get("partName"));
            }
        }

        //get current version
        int version = versionService.getCurrentVersion("Parts");
        log.info("version==>>"+String.valueOf(version));

        int newVersion = version + 1;

        //disable current version
        versionService.disableCurrentVersion("Parts", version);

        //add new version
        Version addedVersion = versionService.addNewVersion("Parts", newVersion, true);
        log.info("addedVersion==>>"+addedVersion.toString());

        //upload new parts
        List<Parts> result = new ArrayList<>();
        Iterator<Object> it = list.iterator();
        while (it.hasNext()) {
            JSONObject jsonObject = new JSONObject((Map) it.next());
            Header header = headerService.findByHeaderNameAndIsActive((String)jsonObject.get("headerName"), true);
            log.info("header==>>"+header.toString());

            Parts part = getPartFromRequest(jsonObject, newVersion, header.getId());
            log.info("part from request==>>"+part.toString());

            Parts savedPart = partsDao.uploadPart(part);
            log.info("savedPart==>>"+savedPart.toString());

            result.add(savedPart);
        }
        return result;
    }

    @Override
    public boolean existsByPartNameAndIsActive(String partName, boolean isActive) {
        if(partsDao.existsByPartNameAndIsActive(partName, isActive)) {
            return true;
        }
        return false;
    }

    @Override
    public Parts findByPartNameAndVersionNumber(String partName, int partVersion) {
        Parts part = partsDao.findByPartNameAndVersionNumber(partName, partVersion);
        return part;
    }

    @Override
    public Parts findByPartNameAndIsActive(String partName, boolean isActive) {
        Parts part = partsDao.findByPartNameAndIsActive(partName, isActive);
        return part;
    }

    private Parts getPartFromRequest(JSONObject jsonObject, int newVersion, Long headerID) {
        return Parts.builder()
                .headerName((String) jsonObject.get("headerName"))
                .headerID(headerID)
                .partName((String) jsonObject.get("partName"))
                .accessoryType(AccessoryType.valueOf((String) jsonObject.get("accessoryType")))
                .isAccessory((boolean) jsonObject.get("isAccessory"))
                .versionNumber(newVersion)
                .isActive(true)
                .build();
    }
}
