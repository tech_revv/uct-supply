package com.revv.uct.supply.service.refurbishmentJob.impl;

import com.revv.uct.supply.db.refurbishmentJob.dao.IRefurbishmentJobDAO;
import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;
import com.revv.uct.supply.service.refurbishmentJob.IRefurbishmentJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RefurbishmentJobServiceImpl implements IRefurbishmentJobService {
    @Autowired
    IRefurbishmentJobDAO refurbishmentJobDAO;


    @Override
    public List<RefurbishmentJob> getRefurbJobList(Long refurbID) {
        return refurbishmentJobDAO.getRefurbJobList(refurbID);
    }

    @Override
    public void updateRefurbJobs(List<RefurbishmentJob> refurbishmentJobs) {
       refurbishmentJobDAO.saveAll(refurbishmentJobs);
    }

    @Override
    public List<RefurbishmentJob> getRefurbJobList(List<Long> refurbIDs) {
        return refurbishmentJobDAO.getRefurbJobList(refurbIDs);
    }
    public List<RefurbishmentJob> getCompletedRefurbJobListByCarID(Long carID) {
        return refurbishmentJobDAO.getCompletedRefurbJobListByCarID(carID);
    }
}
