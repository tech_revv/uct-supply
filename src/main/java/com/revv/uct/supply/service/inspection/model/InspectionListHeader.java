package com.revv.uct.supply.service.inspection.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InspectionListHeader {
    @JsonProperty("name") private String name;
    @JsonProperty("inspectionsList") private List<InspectionList> inspectionsList;
}
