package com.revv.uct.supply.service.offerImage;

public interface IOfferImageService {
    void markOfferImagesInActive(Long existingOfferID, Long createdBy);

    void saveOfferImages(Long offerID, Long createdBy, String[] serviceDocs);
}
