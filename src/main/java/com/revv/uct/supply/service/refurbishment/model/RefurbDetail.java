package com.revv.uct.supply.service.refurbishment.model;

import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class RefurbDetail {
    public RefurbDetail(){};


    private Long refurbID;
    private Long inspectionID;
    private InspectionType inspectionType;
    private Long insertTS;
    private Long procuredCarID;
    private InspectionStatus refurbStatus;
    private String carModel;
    private String registrationNumber;

    public RefurbDetail(Long inspectionID, Long insertTS, InspectionType inspectionType,
                        Long refurbID, InspectionStatus refurbStatus, Long procuredCarID,
                        String make, String model, String variant, String registrationNumber) {
        this.refurbID = refurbID;
        this.inspectionID = inspectionID;
        this.inspectionType = inspectionType;
        this.insertTS = insertTS;
        this.procuredCarID = procuredCarID;
        this.refurbStatus = refurbStatus;
        this.carModel = make + " "+ model + " " + variant ;
        this.registrationNumber = registrationNumber;
    }

}
