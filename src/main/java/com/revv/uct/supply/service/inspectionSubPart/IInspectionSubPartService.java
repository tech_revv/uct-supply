package com.revv.uct.supply.service.inspectionSubPart;

import com.revv.uct.supply.db.inspectionSubPart.entity.InspectionSubPart;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.service.inspection.model.InspectionSubpartDetail;

import java.util.List;

public interface IInspectionSubPartService {
    List<InspectionSubpartDetail> getInspectionSubpartDetails(Long inspectionID);

    List<InspectionSubPart> insertDataForScheduledInspection(Long createdBy, Long inspectionID, List<SubParts> subPartsList);
}
