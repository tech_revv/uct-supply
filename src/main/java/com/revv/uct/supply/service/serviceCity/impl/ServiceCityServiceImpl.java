package com.revv.uct.supply.service.serviceCity.impl;

import com.revv.uct.supply.controller.serviceCity.model.CreateServiceCityRequest;
import com.revv.uct.supply.db.seller.entity.Seller;
import com.revv.uct.supply.db.serviceCity.dao.IServiceCityDAO;
import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.serviceCity.IServiceCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public class ServiceCityServiceImpl implements IServiceCityService
{
    private final IServiceCityDAO serviceCityDAO;

    @Autowired
    public ServiceCityServiceImpl(IServiceCityDAO serviceCityDAO) {
        this.serviceCityDAO = serviceCityDAO;
    }

    @Override
    @Transactional
    public ServiceCity createServiceCity(@Valid @NotNull  CreateServiceCityRequest request) throws UCTSupplyException
    {

        return serviceCityDAO.createServiceCity(ServiceCity.builder().name(request.getCity_name()).build());
    }

    @Override
    @Transactional
    public List<ServiceCity> findAllServiceCities() {
        return serviceCityDAO.findAllServiceCities();
    }

    @Override
    @Transactional
    public ServiceCity findServiceCityById(Long id)  {
        return serviceCityDAO.findServiceCityById(id);
    }


}
