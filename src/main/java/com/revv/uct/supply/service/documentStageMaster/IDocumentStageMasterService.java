package com.revv.uct.supply.service.documentStageMaster;

import com.revv.uct.supply.db.documentStageMaster.entity.DocumentStageMaster;
import com.revv.uct.supply.db.enums.DocumentStage;
import com.revv.uct.supply.db.enums.DocumentType;
import com.revv.uct.supply.exceptions.SupplyException;

import java.util.List;

public interface IDocumentStageMasterService {
    Long findIDByTypeAndStageAndisActive(DocumentType serviceHistory, DocumentStage offerCreation) throws SupplyException;

    List<DocumentStageMaster> getStageDocuments(DocumentStage documentStage);

    List<DocumentStageMaster> findByID(List<Long> documentIDs);
}
