package com.revv.uct.supply.service.jobPriceMap.impl;

import com.revv.uct.supply.db.jobPriceMap.dao.IJobPriceMapDao;
import com.revv.uct.supply.db.jobPriceMap.entity.JobPriceMap;
import com.revv.uct.supply.service.jobPriceMap.IJobPriceMapService;
import com.revv.uct.supply.service.jobPriceMap.model.CheckJobPriceMap;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class JobPriceMapServiceImpl implements IJobPriceMapService {

    private final IJobPriceMapDao jobPriceMapDao;

    @Autowired
    public JobPriceMapServiceImpl(IJobPriceMapDao jobPriceMapDao) {
        this.jobPriceMapDao = jobPriceMapDao;
    }

    @Override
    public JobPriceMap getJobPriceMapFromRequest(JSONObject jsonObject, Long jobID, int newJobPriceMapVersion) {
        return JobPriceMap.builder()
                .jobID(jobID)
                .bodyType((String) jsonObject.get("bodyType"))
                .model((String) jsonObject.get("model"))
                .workshop((String) jsonObject.get("workshop"))
                .price(Double.valueOf(jsonObject.get("price")+""))
                .versionNumber(newJobPriceMapVersion)
                .isActive(true)
                .build();
    }

    @Override
    public JobPriceMap save(JobPriceMap jobPriceMap) {
        JobPriceMap saved = jobPriceMapDao.save(jobPriceMap);
        System.out.println("savedAtService==>>"+saved.toString());

        return saved;
    }

    @Override
    public List<JobPriceMap> findAll() {
        return jobPriceMapDao.findAll();
    }

    @Override
    public List<CheckJobPriceMap> getAllByIsActive(boolean b) {
        List<Object[]> jobPriceMaps = jobPriceMapDao.getAllByIsActive(b);
        List<CheckJobPriceMap> list = new ArrayList<>();
        jobPriceMaps.forEach(jpm -> {
            log.info("jpm==>>",jpm);
            //list.add(new CheckJobPriceMap(String.valueOf( jpm[0]), (String)jpm[1], (String)jpm[2],(Double) jpm[3]));
        });
        return list;
    }

    @Override
    public CheckJobPriceMap getCheckJobPriceMapFromrequest(Long jobID, JSONObject obj) {
        return CheckJobPriceMap.builder()
                .jobID(jobID)
                .bodyType((String) obj.get("bodyType"))
                .model((String) obj.get("model"))
                .workshop((String) obj.get("workshop"))
                .price(Double.valueOf(obj.get("price")+""))
                .build();

    }

    @Override
    public List<JobPriceMap> getJobPrice(List<Long> jobIDs) {
        return jobPriceMapDao.getJobPriceByIDs(jobIDs);
    }

    @Override
    public List<JobPriceMap> getAllJobPrice() {
        return jobPriceMapDao.getAllJobPrice();
    }

    @Override
    public List<JobPriceMap> findAllByIsActive(boolean b) {
        return jobPriceMapDao.findAllByIsActive(b);
    }
}
