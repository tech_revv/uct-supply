package com.revv.uct.supply.service.offer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.revv.uct.supply.constants.ChallanStatus;
import com.revv.uct.supply.constants.ServiceHistoryStatus;
import com.revv.uct.supply.service.inspection.model.InspectionChecklist;
import com.revv.uct.supply.service.inspection.model.InspectionChecklistFlat;
import com.revv.uct.supply.service.inspection.model.InspectionVerdictDetailForGenerateOffer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GenerateOfferData {
    List<InspectionChecklistFlat> inspectionChecklist;
    InspectionVerdictDetailForGenerateOffer inspectionVerdictData;
    List<ChallanStatus> challanStatus;
    List<ServiceHistoryStatus> serviceHistoryStatus;
}
