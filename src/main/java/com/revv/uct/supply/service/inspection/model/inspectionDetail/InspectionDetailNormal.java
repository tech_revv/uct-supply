package com.revv.uct.supply.service.inspection.model.inspectionDetail;

import com.revv.uct.supply.constants.APIConstants;
import com.revv.uct.supply.db.enums.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
public class InspectionDetailNormal extends InspectionDetail{

    private Long sellerID;
    private String sellerType;
    private String contactNumber;
    private String sellerName;
    private String sellerAddress;
    private String carRegistrationNumber;
    private Double sellerLat;
    private Double sellerLng;
    private Long leadID;
    private String rto;
    private LocalDateTime registrationTime;
    private String registrationTimeDisplay;
    private LocalDateTime manufacturingTimestamp;
    private String insuranceValidityDisplay;
    private LocalDateTime insuranceValidity;
    private String assignedBDE;
    private String insuranceType;
    private LeadStatus leadStatus;
    private Long serviceCityID;
    private String registrationType;
    private String ownershipSerial;
    private String carColor;
    private String comments;
    private String kmDriven;
    private String kmDrivenDisplay;

    //Inspection list constructor
    public InspectionDetailNormal(
            Long id, InspectionType inspectionType, LocalDateTime inspectionTime, InspectionStatus status, Long insertTS,
            Long sellerID, SellerTypeEnum sellerType, String contactNumber, String sellerName, String sellerAddress, Double sellerLat, Double sellerLng,
            String carMake, String carModel, String carVariant, String carFuel, Transmission carTransmission, int startYear, int endYear,
            String registrationNumber, Long leadID, String rto, LocalDateTime registrationTimestamp,
            LocalDateTime insuranceValidity, String comments, Double kmsDriven, String insuranceType,
            LeadStatus leadStatus, Long serviceCityID, LocalDateTime manufacturingTimestamp, String registrationType,
            String ownershipSerial, String carColor, String assignedBDE) {
        this.id = id;
        this.inspectionIDDisplay = "ID-"+id;
        this.inspectionType = inspectionType.toString();
        if(inspectionType.equals(InspectionType.E1) && insertTS.equals(0L)) {
            this.editLead = true;
        }
        this.inspectionTime = inspectionTime.plusMinutes(330);
        this.inspectionTimeDisplay = inspectionTime.plusMinutes(330).format(formatter);
        this.status = status;
        this.insertTS = insertTS != null ? insertTS : 0L;
        this.sellerID = sellerID;
        this.sellerType = sellerType.toString();
        this.contactNumber = contactNumber;
        this.sellerName = sellerName;
        this.sellerAddress = sellerAddress;
        this.sellerLat = sellerLat;
        this.sellerLng = sellerLng;
        this.carMake = carMake;
        this.carModel = carModel;
        this.carVariant = carVariant;
        this.carFuel = carFuel;
        this.carTransmission = carTransmission;
        this.leadID = leadID;
        this.rto = rto;
        this.registrationTimeDisplay = registrationTimestamp.format(formatter2);
        this.registrationTime = registrationTimestamp;
        this.insuranceValidity = insuranceValidity;
        this.insuranceValidityDisplay = insuranceValidity.format(formatter2);
        this.assignedBDE = assignedBDE;
        this.comments = comments;
        this.carYear = ""+startYear+"-"+endYear;
        this.carRegistrationNumber = registrationNumber;
        this.kmDrivenDisplay = new DecimalFormat("#,###.00").format(kmsDriven) + " km";
        this.kmDriven = kmsDriven.toString();
        this.insuranceType = insuranceType;
        this.leadStatus = leadStatus;
        this.serviceCityID = serviceCityID;
        this.registrationType = registrationType;
        this.ownershipSerial = ownershipSerial;
        this.carColor = carColor;
        this.manufacturingTimestamp = manufacturingTimestamp;
        if(leadStatus == LeadStatus.E2_APPROVED) {
            this.cardType = InspectionCardType.PICKUP_CHECKLIST;
        }
    }

}
