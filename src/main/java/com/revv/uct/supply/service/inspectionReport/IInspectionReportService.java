package com.revv.uct.supply.service.inspectionReport;

import com.revv.uct.supply.db.inspectionReport.entity.InspectionReport;
import com.revv.uct.supply.service.inspection.model.InspectionIssue;

import java.util.ArrayList;
import java.util.List;

public interface IInspectionReportService {
    List<InspectionReport> getInspectionReport(Long inspectionID);

    void markReportsInactive(Long inspectorID, Long inspectionID);

    void saveInspectionReport(Long inspectorID, Long inspectionID, Long subPartID, List<InspectionIssue> issues, String comments);

    List<InspectionReport> getInspectionReportByInspectionIDs(List<Long> inspectionIDs);

}
