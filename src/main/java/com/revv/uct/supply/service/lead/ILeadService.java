package com.revv.uct.supply.service.lead;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.controller.lead.model.LeadCreationData;
import com.revv.uct.supply.controller.lead.model.UpdateLeadRequest;
import com.revv.uct.supply.db.lead.entity.Lead;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;

import java.util.List;
import java.util.Optional;

public interface ILeadService
{

    Lead createLead(CreateLeadRequest request) throws SupplyException;

    List<LeadCreationData> getLeadCreationData();

    Lead updateLead(UpdateLeadRequest request) throws SupplyException, JsonProcessingException;

    boolean existByIdAndIsActive(Long leadID, boolean b);

    Long getAllLeadsCount();

    Lead getLeadDetails(Long leadID) throws UCTSupplyException;

    Lead createOnlineLead(CreateLeadRequest request) throws SupplyException;
}
