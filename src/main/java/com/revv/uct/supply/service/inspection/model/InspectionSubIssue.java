package com.revv.uct.supply.service.inspection.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InspectionSubIssue {
    private String jobName;
    private Long jobID;
    @Builder.Default
    private int status = -1;
    private int subIssueType;
    @Builder.Default
    private Double cost = 0.0;
    @Builder.Default
    private Double maxCost = 10000.0;
    @Builder.Default
    private Double minCost = 100.0;
    @Builder.Default
    private Boolean selected = false;
}
