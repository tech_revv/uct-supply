package com.revv.uct.supply.service.refurbishment.model;

import com.revv.uct.supply.db.enums.InspectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RefurbScreen {
    private Long inspectionID;
    private InspectionType inspectionType;
    private Long insertTS;
    private String carModel;
    private String regNo;
    private int jobsCount;
    @Builder.Default
    private List<RefurbJobList> jobList = new ArrayList<>();
    @Builder.Default
    private List<RefurbDocument> documentList = new ArrayList<>();
}
