package com.revv.uct.supply.service.workshop.impl;

import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.controller.workshop.model.CreateWorkshopRequest;
import com.revv.uct.supply.controller.workshop.model.WorkshopData;
import com.revv.uct.supply.db.enums.AdminType;
import com.revv.uct.supply.db.user.entity.User;
import com.revv.uct.supply.db.workshop.dao.IWorkshopDAO;
import com.revv.uct.supply.db.workshop.entity.Workshop;
import com.revv.uct.supply.service.user.IUserService;
import com.revv.uct.supply.service.workshop.IWorkshopService;
import com.revv.uct.supply.service.workshop.model.RefurbSpoc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class WorkshopServiceImpl implements IWorkshopService {

    private final IWorkshopDAO workshopDAO;
    private final IUserService userService;

    @Autowired
    public WorkshopServiceImpl(IWorkshopDAO workshopDAO, IUserService userService) {
        this.workshopDAO = workshopDAO;
        this.userService = userService;
    }

    @Override
    public List<Workshop> getAllActiveWorkshops() {
        return workshopDAO.getAllActiveWorkshops();
    }

    @Override
    public List<WorkshopData> getWorkshopsData() {
        List<Workshop> workshops = workshopDAO.getAllActiveWorkshops();
        List<User> refurbSpocs = userService.getAdmins(AdminType.REFURB_SPOC);
        List<RefurbSpoc> refurbSpocList = refurbSpocs.stream().map(spoc->
            new RefurbSpoc(spoc.getId(), spoc.getUserName(), spoc.getAdminType())
        ).collect(Collectors.toList());

        List<WorkshopData> workshopsData = new ArrayList<>();
        workshopsData.add(new WorkshopData(workshops,refurbSpocList));

        return workshopsData;

    }

    @Override
    public Workshop createWorkshop(CreateWorkshopRequest createWorkshopRequest, UserData userData) {
        return workshopDAO.createWorkshop(getWorkshop(createWorkshopRequest,userData));
    }

    @Override
    public List<Workshop> findAllByWorkshopID(List<Long> workshopIDs) {
        return workshopDAO.findAllByWorkshopIDAndIsActive(workshopIDs, true);
    }

    @Override
    public boolean existById(Long workshopID) {
        return workshopDAO.existById(workshopID);
    }

    private Workshop getWorkshop(CreateWorkshopRequest createWorkshopRequest, UserData userData) {

        return Workshop.builder()
                .workshopName(createWorkshopRequest.getWorkshopName())
                .address(createWorkshopRequest.getAddress())
                .createdBy(userData.getId())
                .build();
    }
}
