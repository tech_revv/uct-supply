package com.revv.uct.supply.service.inspectionJob.impl;

import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.inspectionJob.dao.IInspectionJobDAO;
import com.revv.uct.supply.db.inspectionJob.entity.InspectionJob;
import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;
import com.revv.uct.supply.service.inspection.model.InspectionIssue;
import com.revv.uct.supply.service.inspection.model.InspectionSubIssue;
import com.revv.uct.supply.service.inspectionJob.IInspectionJobService;
import com.revv.uct.supply.service.refurbishment.IRefurbishmentService;
import com.revv.uct.supply.service.refurbishmentJob.IRefurbishmentJobService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Service
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class InspectionJobServiceImpl implements IInspectionJobService {

    @Autowired
    IInspectionJobDAO inspectionJobDAO;

    @Autowired
    IRefurbishmentJobService refurbishmentJobService;

    @Autowired
    IRefurbishmentService refurbishmentService;


    @Override
    public List<InspectionJob> getInspectionJobs(Long inspectionID) {
        return inspectionJobDAO.getInspectionJobs(inspectionID);
    }

    @Override
    public void markJobsInactive(Long inspectorID, Long inspectionID) {
        inspectionJobDAO.markJobsInactive(inspectorID, inspectionID);
    }

    @Override
    public void saveInspectionJob(Long inspectorID, Inspection inspectionDetails, List<InspectionSubIssue> subIssues,
                                  Long subPartID, List<InspectionIssue> issues, String subPartName,
                                  List<RefurbishmentJob> completedRefurbJobs, List<RefurbishmentJob> refurbishmentJobs) {
        List<InspectionJob> inspectionJobs = new ArrayList<>();
        Map<String, Boolean> subIssueMap = new HashMap<>();
        mapInspectionJob(inspectorID, inspectionDetails.getId(), subPartID, inspectionJobs, subIssueMap, subIssues);
        for (InspectionIssue issue : issues) {
            List<InspectionSubIssue> inspectionSubIssues = issue.getSubIssues();
            if(issue.getSelected()) {
                mapInspectionJob(inspectorID, inspectionDetails.getId(), subPartID, inspectionJobs, subIssueMap, inspectionSubIssues);
            }
        }
        if(inspectionJobs.size() > 0) {
            inspectionJobDAO.saveInspectionJob(inspectionJobs);
        }

        saveRefurbJobs(inspectorID, inspectionDetails, issues, subPartID, subPartName, completedRefurbJobs, refurbishmentJobs);
    }

    @Override
    public List<InspectionJob> getInspectionJobs(List<Long> inspectionIDs) {
        return inspectionJobDAO.getInspectionJobs(inspectionIDs);
    }

    public List<InspectionJob> getE3InspectionJobsByCarID(Long carID) {
        return inspectionJobDAO.getInspectionJobsByCarIDAndType(carID,InspectionType.E3);
    }

    //Note: Here refurbishmentJobs is the most updated value after each iteration
    //as we are updating this for all subparts and we are fetching it only once
    private void saveRefurbJobs(Long inspectorID, Inspection inspectionDetails, List<InspectionIssue> issues,
                                Long subPartID, String subPartName, List<RefurbishmentJob> completedRefurbJobs,
                                List<RefurbishmentJob> refurbishmentJobs) {
        if(!inspectionDetails.getInspectionType().equals(InspectionType.E3) || inspectionDetails.getRefurbID() == null)
            return;

        Map<Long, Boolean> jobMap = new HashMap<>();
        Long refurbID = inspectionDetails.getRefurbID();

        List<RefurbishmentJob> newJobs = new ArrayList<>();

        for (InspectionIssue issue : issues) {
            List<InspectionSubIssue> inspectionSubIssues = issue.getSubIssues();
            for (InspectionSubIssue inspectionSubIssue : inspectionSubIssues) {
                if(!jobMap.containsKey(inspectionSubIssue.getJobID())) { //we have not dealt with this job before
                    int index = IntStream.range(0, refurbishmentJobs.size()).filter(i -> refurbishmentJobs.get(i).getJobID().equals(inspectionSubIssue.getJobID()))
                            .findFirst().orElse(-1);
                    if (inspectionSubIssue.getSelected()) { //if this job is selected
                        if (completedRefurbJobs.stream().filter(cr -> cr.getJobID().equals(inspectionSubIssue.getJobID())).findAny().orElse(null) != null) { //and is already part of completed refurb then continue
                            continue;
                        }
                        if(index == -1) { //if job is not found in scheduled refurbishment then add it
                            newJobs.add(addNewRefurbJob(inspectorID, inspectionSubIssue, refurbID, subPartID, subPartName));
                            jobMap.put(inspectionSubIssue.getJobID(), true); //update jobMap so that we don't deal it later
                        }

                    } else if(index != -1) {//if job is not selected and is present in refurbJobs update that entry
                        refurbishmentJobs.get(index).setUpdatedBy(inspectorID);
                        refurbishmentJobs.get(index).setSelected(false);
                        refurbishmentJobs.get(index).setActive(false);
                    }
                }
            }
        }

        refurbishmentJobs.addAll(newJobs); //add new jobs required for scheduled refurbishment

        refurbishmentJobService.updateRefurbJobs(refurbishmentJobs); //save all new jobs plus changes in existing jobs
    }

    private RefurbishmentJob addNewRefurbJob(Long inspectorID, InspectionSubIssue inspectionSubIssue, Long refurbID, Long subPartID, String subPartName) {
        return RefurbishmentJob.builder()
                .createdBy(inspectorID)
                .jobID(inspectionSubIssue.getJobID())
                .refurbID(refurbID)
                .subpartID(subPartID)
                .subpartName(subPartName)
                .jobName(inspectionSubIssue.getJobName())
                .selected(inspectionSubIssue.getSelected())
                .build();
    }

    private void mapInspectionJob(Long inspectorID, Long inspectionID, Long subPartID, List<InspectionJob> inspectionJobs, Map<String, Boolean> subIssueMap, List<InspectionSubIssue> inspectionSubIssues) {
        for (InspectionSubIssue inspectionSubIssue : inspectionSubIssues) {
            if(!subIssueMap.containsKey(subPartID +"-"+ inspectionSubIssue.getJobID()) && inspectionSubIssue.getSelected()) {
                inspectionJobs.add(
                        InspectionJob.builder()
                                .inspectionID(inspectionID)
                                .jobID(inspectionSubIssue.getJobID())
                                .subPartID(subPartID)
                                .cost(inspectionSubIssue.getCost())
                                .createdBy(inspectorID)
                                .isActive(true)
                                .status(inspectionSubIssue.getStatus())
                                .build()
                );
                subIssueMap.put(subPartID +"-"+ inspectionSubIssue.getJobID(), true);
            }
        }
    }
}
