package com.revv.uct.supply.service.inspection.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InspectionChecklistFlat implements Comparable<InspectionChecklistFlat>{
    private String headerName;
    private String subPartName;
    private String issueName;
    private Boolean issueSelected;
    private String jobName;
    private Double jobCost;

    @Override
    public int compareTo(InspectionChecklistFlat checklist) {
        return this.headerName.compareTo(checklist.headerName);
//        return this.headerName.concat(this.subPartName).compareTo(checklist.headerName+checklist.subPartName);
    }
}
