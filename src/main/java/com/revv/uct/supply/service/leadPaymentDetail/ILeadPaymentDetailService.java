package com.revv.uct.supply.service.leadPaymentDetail;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.controller.payment.model.GetLeadPaymentData;
import com.revv.uct.supply.controller.payment.model.ProceedWithoutTokenRequest;
import com.revv.uct.supply.controller.payment.model.SaveLeadPaymentRequest;
import com.revv.uct.supply.db.leadPaymentDetail.entity.LeadPaymentDetail;
import com.revv.uct.supply.exceptions.SupplyException;

public interface ILeadPaymentDetailService {
    GetLeadPaymentData getLeadPaymentData();

    LeadPaymentDetail saveLeadPaymentDetails(SaveLeadPaymentRequest request) throws SupplyException, JsonProcessingException;

    LeadPaymentDetail proceedWithoutToken(ProceedWithoutTokenRequest request) throws SupplyException, JsonProcessingException;
}
