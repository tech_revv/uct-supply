package com.revv.uct.supply.service.refurbishment.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RefurbDocument {
    private String documentName;
    private Long documentID;
    private Boolean isMandatory;
    private List<String> documentList;
}
