package com.revv.uct.supply.service.refurbishment.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RefurbJobList {
    private String subPartName;
    private String issues;
    private String jobs;
}
