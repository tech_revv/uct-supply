package com.revv.uct.supply.service.inspectionJob;

import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.inspectionJob.entity.InspectionJob;
import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;
import com.revv.uct.supply.service.inspection.model.InspectionIssue;
import com.revv.uct.supply.service.inspection.model.InspectionSubIssue;

import java.util.List;

public interface IInspectionJobService {
    List<InspectionJob> getInspectionJobs(Long inspectionID);

    void markJobsInactive(Long inspectorID, Long inspectionID);

    void saveInspectionJob(Long inspectorID, Inspection inspectionDetails, List<InspectionSubIssue> subIssues, Long subPartID, List<InspectionIssue> issues, String subPartName, List<RefurbishmentJob> completedRefurbJobs, List<RefurbishmentJob> refurbishmentJobs);

    List<InspectionJob> getInspectionJobs(List<Long> inspectionIDs);

    List<InspectionJob> getE3InspectionJobsByCarID(Long carID);

}
