package com.revv.uct.supply.interceptors.auth;

import com.revv.uct.supply.client.UserServiceClient;
import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.client.model.UserResponse;
import com.revv.uct.supply.constants.APIConstants;
import com.revv.uct.supply.exceptions.SupplyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import retrofit2.Call;
import retrofit2.Response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
@Slf4j
public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    UserServiceClient userServiceClient;

    @Override
    public boolean preHandle(

            HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        log.info("request =>>>" + request.getRequestURI());

        if(request.getHeader("statusCheck") != null && request.getHeader("statusCheck").equals("1")){
            return true;
        }

        if(request.getHeader("Token") == ""){
            throw new Exception("Token required");
        }

        Call<UserResponse> retrofitCall = userServiceClient.verifyAdmin(request.getHeader("Token"));
        Response<UserResponse> res = retrofitCall.execute();

        if(!res.isSuccessful() || res.body().getStatusCode() != HttpStatus.OK.value()){

            //log some error or throw it
            throw new Exception(APIConstants.AUTH_ISSUE);
        }
        log.info("Success " + res.body());

        List<UserData> userData = res.body().getData();

        log.info(userData.toString());

        if(userData.size() == 0) {
            throw new Exception(APIConstants.AUTH_ISSUE);
        }

        request.setAttribute("user", userData.get(0));

        return true;

    }
//    @Override
//    public void postHandle(
//            HttpServletRequest request, HttpServletResponse response, Object handler,
//            ModelAndView modelAndView) throws Exception {}
//
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
//                                Object handler, Exception exception) throws Exception {
//
//    }
}
