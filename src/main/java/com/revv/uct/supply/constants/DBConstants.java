package com.revv.uct.supply.constants;

public class DBConstants {
    public static final String[] REGISTRATION_TYPES = {"Private", "Commercial - Yellow Plate","Commercial - Black Plate"};
    public static final String[] INSURANCE_TYPES = {"Third Party", "Comprehensive", "Zero Dep", "No Insurance"};
    public static final String[] SERVICE_HISTORY_STATUS = {"Ok", "Not Ok"};
    public static final String[] CHALLAN_STATUS = {"Ok", "Not Ok", "Due"};
}
