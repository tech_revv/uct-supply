package com.revv.uct.supply.constants;

import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.seller.entity.Seller;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
public class SellerType {
    String name;
    String value;

    public SellerType(String s) {
        this.name = s;
        this.value = s;
    }

    public static List<SellerType> getSellerTypes() {
        List<SellerType> list = new ArrayList<>();
        SellerTypeEnum[] ste = SellerTypeEnum.values();
        for(SellerTypeEnum s : ste) {
            list.add(new SellerType(String.valueOf(s)));
        }
        return list;
    }
}
