package com.revv.uct.supply.constants;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ServiceHistoryStatus {
    String name;
    String value;

    public ServiceHistoryStatus(String s) {
        this.name = s;
        this.value = s;
    }

    public static List<ServiceHistoryStatus> getServiceHistoryStatus() {
        List<ServiceHistoryStatus> list = new ArrayList<>();
        String[] status = DBConstants.SERVICE_HISTORY_STATUS;
        for(String s : status) {
            list.add(new ServiceHistoryStatus(String.valueOf(s)));
        }
        return list;
    }
}
