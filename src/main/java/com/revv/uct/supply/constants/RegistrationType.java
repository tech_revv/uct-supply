package com.revv.uct.supply.constants;

import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.seller.entity.Seller;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

import static com.revv.uct.supply.constants.DBConstants.REGISTRATION_TYPES;

@Getter
@Setter
@NoArgsConstructor
public class RegistrationType {
    String name;
    String value;

    public RegistrationType(String s){
        this.name = s;
        this.value = s;
    }

    public static List<RegistrationType> getRegistrationTypes() {
        List<RegistrationType> list = new ArrayList<>();
        for(String r : REGISTRATION_TYPES) {
            list.add(new RegistrationType(r));
        }

        return list;
    }
}
