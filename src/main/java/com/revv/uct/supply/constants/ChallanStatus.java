package com.revv.uct.supply.constants;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ChallanStatus {
    String name;
    String value;

    public ChallanStatus(String s) {
        this.name = s;
        this.value = s;
    }

    public static List<ChallanStatus> getChallanStatus() {
        List<ChallanStatus> list = new ArrayList<>();
        String[] status = DBConstants.CHALLAN_STATUS;
        for(String c : status) {
            list.add(new ChallanStatus(String.valueOf(c)));
        }
        return list;
    }
}
