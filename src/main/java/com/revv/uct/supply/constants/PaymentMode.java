package com.revv.uct.supply.constants;

import com.revv.uct.supply.db.enums.PaymentModeEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PaymentMode {
    String name;
    String value;

    public PaymentMode(String s) {
        this.name = s;
        this.value = s;
    }


    public static List<PaymentMode> getPaymentTypes() {
        List<PaymentMode> list = new ArrayList<>();
        PaymentModeEnum[] paymentModes = PaymentModeEnum.values();
        for (PaymentModeEnum p : paymentModes) {
            list.add(new PaymentMode(String.valueOf(p)));
        }
        return list;
    }
}
