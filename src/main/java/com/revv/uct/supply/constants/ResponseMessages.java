package com.revv.uct.supply.constants;

public interface ResponseMessages
{
    String serviceCityCreated="Service City Created";
    String allCityData="All Service Cities";
    String leadCreated="Successfully Created LeadID: ";
    String sellCreated="Successfully Created SellerID: ";
    String invalidCityID ="Invalid City Id" ;
    String leadUpdated = "Successfully Updated LeadID: ";
    String sellerUpdated = "Successfully Updated SellerID: ";
}
