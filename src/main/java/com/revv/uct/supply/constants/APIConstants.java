package com.revv.uct.supply.constants;

import com.revv.uct.supply.service.inspection.model.InspectionIssue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class APIConstants {
    public static final int INSPECTION_LOWER_LIMIT = 2;
    public static final Map<String, Integer> CHECKLIST_ISSUE_TYPE = new HashMap<String, Integer>(){{
        put("DEFAULT", 0);
        put("NORMAL", 1);
    }};
    public static final Map<String, Integer> CHECKLIST_SUB_ISSUE_TYPE = new HashMap<String, Integer>(){{
        put("DEFAULT", 0);
        put("NORMAL", 1);
    }};

    public static final Map<String, Integer> SUB_ISSUE_STATUS = new HashMap<String, Integer>(){{
        put("DONE", 1);
        put("NOT_DONE", 2);
        put("PARTIALLY_DONE", 3);
        put("DEFAULT_JOB_STATUS", -1);
    }};
    public static final String AUTH_ISSUE = "AUTH_ISSUE";
    public static final String VERDICT_SCREEN_TITLE_SUCCESS = "Inspection completed";
    public static final String VERDICT_SCREEN_TITLE_WARNING = "Inspection incomplete";
    public static final String UNASSURED_ISSUE = "unassured";
    public static final String OK_ISSUE = "ok";
    public static final String NFI_ISSUE = "Needs further investigation";
    public static final List<Long> DEFAULT_JOB_ID = Arrays.asList(1L);
    public static final List<Long> DEFAULT_ISSUES_ID = Arrays.asList(1L,2L);
    public static final Long NFI_ISSUE_ID = 2L;
    public static final String INSPECTION_HEADER_SCHEDULE = "SCHEDULED";
    public static final String INSPECTION_HEADER_PAST = "IN PAST 48 HRS";
    public static final String INSPECTION_LIST_TODAY = "Scheduled for today";
    public static final String INSPECTION_LIST_LATER = "Scheduled for later";
    public static final String INSPECTION_LIST_COMPLETED = "Completed";
    public static final String INSPECTION_LIST_UNCOMPLETED = "Incomplete";
    public static final List<String> INSPECTION_EDITABLE_PARAMS = Arrays.asList("carRegistrationNumber","registrationTime");
}
