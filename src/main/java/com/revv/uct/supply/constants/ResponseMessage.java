package com.revv.uct.supply.constants;

public class ResponseMessage {
    public static final String signup_successful = "Signup successful";
    public static final String login_successful = "Login successful";
    public static final String email_already_exists = "Email already exists";
    public static final String unregistered_user = "Unregistered user";
    public static final String verify_admin = "Admin verified";
    public static final String bad_request = "Bad request";
    public static final String inspection_list_success = "List of inspections";
    public static final String inspection_checklist_success = "Checklist for inspection";
    public static final String inspection_pickup_checklist_success = "Pickup Documents for inspection";
    public static final String inspection_checklist_save_success = "Checklist saved";
    public static final String inspection_pickup_checklist_save_success = "Pickup Checklist saved";
    public static final String inspection_checklist_image_save_success = "Checklist images saved";
    public static final String inspection_verdict_success = "Inspection verdict details";
    public static final String inspection_difference_success = "List of difference";
    public static final String inspection_verdict_save_success = "Inspection verdict saved";
    public static String upload_successful = "Upload successful";
    public static final String success = "success";
    public static final String inspection_scheduled = "Inspection scheduled";
    public static final String inspection_updated = "Inspection updated";
    public static final String lead_does_not_exist = "Lead doesn't exist";
    public static final String inspector_does_no_exist = "Inspector doesn't exist";
    public static final String offer_created = "Offer created";
    public static final String offer_updated = "Offer updated";
    public static final String offer_price = "Offer price";
    public static final String offer_revised = "Offer revised";
    public static final String token_paid_details_added = "Token paid details added";
    public static final String refurb_jobs_success = "Refurb Job List";
    public static final String refurb_documents_success = "Documents submitted successfully";
    public static final String procured_car_details_added = "Procured car details added";
    public static final String procured_car_details_updated = "Procured car details updated";
    public static final String issue_reported = "Issue reported";
    public static final String refurb_update_success = "Refurb Updated successfully";
    public static final String refurb_spoc_not_exit = "Refurb Spoc doesn't exist";
    public static final String workshop_not_exit = "Workshop doesn't exist";
}
