package com.revv.uct.supply.constants;

import com.revv.uct.supply.db.enums.InsuranceTypeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import static com.revv.uct.supply.constants.DBConstants.INSURANCE_TYPES;

@Getter
@Setter
@NoArgsConstructor
public class InsuranceType {
    String name;
    String value;

    public InsuranceType(String s) {
        this.name = s;
        this.value = s;
    }

    public static List<InsuranceType> getInsuranceType() {
        List<InsuranceType> list = new ArrayList<>();
        for(String i : INSURANCE_TYPES) {
            list.add(new InsuranceType(i));
        }
        return list;
    }
}
