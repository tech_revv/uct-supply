package com.revv.uct.supply.util.templates.impl;

import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;
import com.revv.uct.supply.util.templates.ITemplates;
import com.revv.uct.supply.util.templates.model.Template;
import org.springframework.stereotype.Component;

@Component
public class ReportIssueTemplatesImpl implements ITemplates {
    @Override
    public Template reportIssuetemplate(ProcuredCar updatedPC) {
        String emailSubject = "Issue Reported";
        String emailContent = "Issue reported for carID: " + updatedPC.getId() + " at stage: " + updatedPC.getIssueStage() + ", \n" +
                "Reported issue: " + updatedPC.getIssue();

        return Template.builder()
                .emailSubject(emailSubject)
                .emailContent(emailContent)
                .build();
    }
}
