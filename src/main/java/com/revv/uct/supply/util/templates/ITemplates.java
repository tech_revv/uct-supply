package com.revv.uct.supply.util.templates;

import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;
import com.revv.uct.supply.util.templates.model.Template;

public interface ITemplates {
    Template reportIssuetemplate(ProcuredCar updatedPC);
}
