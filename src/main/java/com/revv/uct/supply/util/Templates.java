package com.revv.uct.supply.util;

import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;

public class Templates {
    public static final String report_issue_email_subject = "Issue Reported";
    public static String report_issue_email_text(ProcuredCar procuredCar){
        return "Issue reported for carID: " + procuredCar.getId() + " at stage: " + procuredCar.getIssueStage() + ", \n" +
                "Reported issue: " + procuredCar.getIssue();
    }
}
