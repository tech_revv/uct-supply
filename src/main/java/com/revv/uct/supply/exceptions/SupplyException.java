package com.revv.uct.supply.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@Data
@EqualsAndHashCode(callSuper = true)
public class SupplyException extends Throwable{
    private HttpStatus status;
    private String message;
    private String debugMessage;

    public SupplyException(HttpStatus status) {
        this.status = status;
    }

    public SupplyException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public SupplyException(String message, Throwable cause, HttpStatus status) {
        super(message, cause);
        this.status = status;
    }

    public SupplyException(Throwable cause, HttpStatus status) {
        super(cause);
        this.status = status;
    }

    public SupplyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, HttpStatus status) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.status = status;
    }

    public SupplyException(String message) {
        this.message = message;
    }
}
