package com.revv.uct.supply.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@Data
@EqualsAndHashCode(callSuper = true)
public class UCTSupplyException extends Throwable{
    private HttpStatus status;
    private String message;
    private String debugMessage;

    public UCTSupplyException(HttpStatus status) {
        this.status = status;
    }

    public UCTSupplyException(String message, HttpStatus status) {
        super(message);
        this.status = status;
        this.message = message;
    }

    public UCTSupplyException(String message, Throwable cause, HttpStatus status) {
        super(message, cause);
        this.status = status;
    }

    public UCTSupplyException(Throwable cause, HttpStatus status) {
        super(cause);
        this.status = status;
    }

    public UCTSupplyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, HttpStatus status) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.status = status;
    }
}
