package com.revv.uct.supply.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserResponse {
    @JsonProperty("status_code") private Integer statusCode;
    @JsonProperty("status_message") private String statusMessage;
    @JsonProperty("data") private List<UserData> data; // actual data from service API
}
