package com.revv.uct.supply.client;

import com.revv.uct.supply.client.model.UserResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserServiceClient {

    @POST("api/verifyAdmin")
    Call<UserResponse> verifyAdmin(@Header("Token") String token);
}
