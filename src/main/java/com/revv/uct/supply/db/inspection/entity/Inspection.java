package com.revv.uct.supply.db.inspection.entity;


import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "inspections")
public class Inspection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "inspectionType")
    @Enumerated(EnumType.STRING)
    private InspectionType inspectionType;

    @Column(name = "leadID")
    private Long leadID;

    @Column(name = "inspectorID")
    private Long inspectorID;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "updatedBy")
    private Long updatedBy;

    @Column(name = "inspectionTime")
    private LocalDateTime inspectionTime;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private InspectionStatus status;

    @Column(name = "createdAt")
    @CreatedDate
    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();

    @LastModifiedDate
    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;

    @PreUpdate
    protected void preUpdate() {
        this.updatedAt = LocalDateTime.now();
    }

    @Column(name = "isActive", columnDefinition = "boolean default true")
    @Builder.Default
    private boolean isActive = true;

    @Column(name="lastDataSyncTime")
    @Builder.Default
    private Long lastDataSyncTime = 0L;

    @Column(name="verdictRemarks")
    @Builder.Default
    private String verdictRemarks = "";

    @Column(name="refurbJobCost")
    private Double refurbJobCost;

    @Column(name="pickupDocumentsCount")
    @Builder.Default
    private int pickupDocumentsCount = 0;

    @Column(name="refurbID")
    private Long refurbID;
}
