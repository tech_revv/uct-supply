package com.revv.uct.supply.db.parts.dao.impl;

import com.revv.uct.supply.db.parts.dao.IPartsDao;
import com.revv.uct.supply.db.parts.entity.Parts;
import com.revv.uct.supply.db.parts.repository.PartsRRepository;
import com.revv.uct.supply.db.parts.repository.PartsRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PartsDaoImpl implements IPartsDao {

    private final PartsRRepository partsRRepository;
    private final PartsRWRepository partsRWRepository;

    @Autowired
    public PartsDaoImpl(PartsRRepository partsRRepository, PartsRWRepository partsRWRepository) {
        this.partsRRepository = partsRRepository;
        this.partsRWRepository = partsRWRepository;
    }

    @Override
    public Parts uploadPart(Parts part) {
        Parts savedPart = partsRWRepository.save(part);
        return savedPart;
    }

    @Override
    public boolean existsByPartNameAndIsActive(String partName, boolean isActive) {
        if(partsRRepository.existsByPartNameAndIsActive(partName, isActive)) {
            return true;
        }
        return false;
    }

    @Override
    public Parts findByPartNameAndVersionNumber(String partName, int partVersion) {
        Parts part = partsRRepository.findByPartNameAndVersionNumber(partName, partVersion);
        return part;
    }

    @Override
    public Parts findByPartNameAndIsActive(String partName, boolean isActive) {
        Parts part = partsRRepository.findByPartNameAndIsActive(partName, isActive);
        return part;
    }

}
