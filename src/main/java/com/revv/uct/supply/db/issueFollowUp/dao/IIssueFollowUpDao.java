package com.revv.uct.supply.db.issueFollowUp.dao;

import com.revv.uct.supply.db.issueFollowUp.entity.IssueFollowUp;

public interface IIssueFollowUpDao {
    IssueFollowUp uploadIssueFollowUp(IssueFollowUp issueFollowUp);
}
