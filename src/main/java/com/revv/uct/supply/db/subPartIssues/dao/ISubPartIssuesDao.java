package com.revv.uct.supply.db.subPartIssues.dao;

import com.revv.uct.supply.db.subPartIssues.entity.SubPartIssues;

import java.util.List;

public interface ISubPartIssuesDao {
    SubPartIssues uploadSubPartIssue(SubPartIssues subPartIssue);

    boolean existsBySubPartIDAndIssueName(Long id, String issueName);

    SubPartIssues findBySubPartIDAndIssueNameAndIsActive(Long id, String issueName, boolean b);

    List<SubPartIssues> getDefaultIssues();
}
