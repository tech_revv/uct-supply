package com.revv.uct.supply.db.history.repository;

import com.revv.uct.supply.db.history.entity.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryRWRepository extends JpaRepository<History, Long> {
}
