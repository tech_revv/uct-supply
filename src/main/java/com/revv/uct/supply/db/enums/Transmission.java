package com.revv.uct.supply.db.enums;

public enum Transmission {
    MANUAL, AUTOMATIC, AMT
}
