package com.revv.uct.supply.db.user.dao.impl;

import com.revv.uct.supply.db.enums.AdminType;
import com.revv.uct.supply.db.user.dao.IUserDao;
import com.revv.uct.supply.db.user.entity.User;
import com.revv.uct.supply.db.user.repository.UserRRepository;
import com.revv.uct.supply.db.user.repository.UserRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDaoImpl implements IUserDao {
    private final UserRRepository userRRepository;
    private final UserRWRepository userRWRepository;

    @Autowired
    public UserDaoImpl(UserRRepository userRRepository, UserRWRepository userRWRepository) {
        this.userRRepository = userRRepository;
        this.userRWRepository = userRWRepository;
    }

    @Override
    public List<User> getAdmins(AdminType adminType) {
        return userRRepository.findAllByAdminType(adminType);
    }

    @Override
    public boolean existById(Long inspectorID) {
        return userRRepository.existsById(inspectorID);
    }

    @Override
    public List<User> getAdminsByCity(AdminType adminType, Long cityID) {
        return userRRepository.findAllByAdminTypeAndUserCity(adminType, cityID);
    }

    @Override
    public List<User> findAllByIDAndIsActive(List<Long> userIDs, Boolean isActive) {
        return userRRepository.findAllByIdIn(userIDs);
    }
}
