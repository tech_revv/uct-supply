package com.revv.uct.supply.db.procuredCar.entity;

import com.revv.uct.supply.db.enums.IssueStage;
import com.revv.uct.supply.db.enums.ProcuredCarStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "procuredCars")
public class ProcuredCar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "carModelID", nullable = false)
    private Long carModelID;

    @Column(name = "leadID", nullable = false)
    private Long leadID;

    @Column(name = "offerID")
    private Long offerID;

    @Column(name = "onBoardedDate")
    private LocalDateTime onBoardedDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private ProcuredCarStatus status;

    @Column(name = "issue")
    private String issue;

    @Column(name = "isIssueReported")
    private boolean isIssueReported;

    @Enumerated(EnumType.STRING)
    @Column(name = "issueStage")
    private IssueStage issueStage;

    @Column(name = "createdAt", nullable = false)
    @CreatedDate
    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();

    @LastModifiedDate
    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;

    @PreUpdate
    protected void preUpdate() {
        this.updatedAt = LocalDateTime.now();
    }

    @Column(name = "isActive", columnDefinition = "boolean default true")
    @Builder.Default
    private boolean isActive = true;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "updatedBy")
    private Long updatedBy;

    @Column(name = "pickUpBy")
    private Long pickUpBy;
}
