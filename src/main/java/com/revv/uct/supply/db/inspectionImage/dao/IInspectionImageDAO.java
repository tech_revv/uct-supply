package com.revv.uct.supply.db.inspectionImage.dao;

import com.revv.uct.supply.db.inspectionImage.entity.InspectionImage;

import java.util.List;

public interface IInspectionImageDAO {
    List<InspectionImage> getInspectionImages(Long inspectionID);

    void markImagesInactive(Long inspectorID, Long inspectionID);

    void saveInspectionImage(List<InspectionImage> inspectionImages);
}
