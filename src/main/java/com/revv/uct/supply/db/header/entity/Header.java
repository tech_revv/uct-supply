package com.revv.uct.supply.db.header.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "headers")
@AllArgsConstructor
@NoArgsConstructor
public class Header {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "versionNumber")
    private int versionNumber;

    @Column(name = "isActive", columnDefinition = "boolean default true")
    private boolean isActive;

    //@OneToMany(mappedBy = "header", fetch = FetchType.EAGER)
    //private List<Parts> parts;
}
