package com.revv.uct.supply.db.jobs.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.jobs.entity.Jobs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface JobsRRepository extends JpaRepository<Jobs, Long> {
    boolean existsByJobNameAndIsActive(String jobName, boolean isActive);

    List<Jobs> findByJobNameAndIsActive(String jobName, boolean b);

    List<Jobs> findByIdIn(List<Long> jobID);

    List<Jobs> findAllByIdInAndIsActive(List<Long> jobID, Boolean isActive);
}
