package com.revv.uct.supply.db.inspection.dao.impl;

import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspection.dao.IInspectionDAO;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.inspection.repository.InspectionRRepository;
import com.revv.uct.supply.db.inspection.repository.InspectionRWRepository;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.InspectionDetailNormal;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.PostRefurbList;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class InspectionDAOImpl implements IInspectionDAO {

    @Autowired
    InspectionRRepository inspectionRRepository;
    @Autowired
    InspectionRWRepository inspectionRWRepository;

    @Override
    public List<InspectionDetailNormal> findInspectionsByInspectionTypeAndStatus(Long inspectorID, List<InspectionStatus> statusList, List<InspectionType> inspectionTypes) {
        return inspectionRRepository.findByInspectorIDAndStatusAndInspectionType(inspectorID, statusList, inspectionTypes);
    }

    @Override
    public Inspection scheduleInspection(Inspection scheduleData) {
        Inspection scheduled = inspectionRWRepository.save(scheduleData);
        return scheduled;
    }

    @Override
    public Optional<Inspection> getInspectionDetails(Long inspectionID) {
        return inspectionRRepository.findById(inspectionID);
    }

    @Override
    public void updateLastDataSyncTime(Long lastDataSyncTime, Long inspectionID) {
        inspectionRWRepository.updateLastDataSyncTime(lastDataSyncTime, inspectionID);
    }

    @Override
    public Inspection updateInspection(Inspection newData) {
        return inspectionRWRepository.save(newData);
    }

    @Override
    public Inspection getInspectionDetailsByLeadIDAndInspectionType(Long leadID, InspectionType inspectionType) {
        List<Inspection> inspections = inspectionRRepository.findByLeadIDAndInspectionType(leadID, inspectionType);
        if(inspections.size() > 0) {
            return inspections.get(0);
        }
        return null;
    }

    @Override
    public Inspection findByIdAndIsActive(Long id, boolean b) {
        return inspectionRRepository.findByIdAndIsActive(id, b);
    }

    @Override
    public List<PostRefurbList> findInspectionsByInspectionTypeAndStatusPostRefurbishment(Long inspectorID, List<InspectionStatus> statusList, List<InspectionType> inspectionTypes) {
        return inspectionRRepository.findByInspectorIDAndStatusAndInspectionTypePostRefurbishment(inspectorID, statusList, inspectionTypes);
    }

    @Override
    public Inspection findByInspectionTypeAndCarID(InspectionType inspectionType, Long carID) {
        return inspectionRRepository.findByInspectionTypeAndCarID(inspectionType,carID);
    }
}
