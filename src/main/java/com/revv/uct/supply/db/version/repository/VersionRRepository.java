package com.revv.uct.supply.db.version.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.version.entity.Version;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@ReadOnlyRepository
public interface VersionRRepository extends JpaRepository<Version, Long> {
    Version findByVersionTypeAndEnabled(String versionType, boolean b);
//    @Query("select v.versionNumber from versions v where v.versionType=versionType and v.enabled=true)")
//    default int getCurrentVersion(String versionType) {
//        return 0;
//    }
}
