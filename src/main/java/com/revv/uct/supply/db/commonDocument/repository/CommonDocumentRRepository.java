package com.revv.uct.supply.db.commonDocument.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.enums.CommonDocumentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface CommonDocumentRRepository extends JpaRepository<CommonDocument, Long> {
    List<CommonDocument> findAllByEntityIDAndIsActive(Long existingOfferID, boolean b);

    List<CommonDocument> findAllByEntityIDAndCommonDocumentEntityAndIsActive(Long entityID, CommonDocumentEntity entityName, Boolean isActive);

    List<CommonDocument> findAllByEntityIDAndDocumentStageMasterIDAndCommonDocumentEntityAndIsActive(Long entityID, Long dsmID, CommonDocumentEntity entity, boolean b);

    List<CommonDocument> findAllByEntityIDAndCommonDocumentEntityAndDocumentStageMasterIDInAndIsActive(Long entityID, CommonDocumentEntity entityName, List<Long> documentStageMasterIDs, Boolean isActive);
}
