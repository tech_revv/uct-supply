package com.revv.uct.supply.db.refurbishmentJob.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "refurbishmentJobs")
public class RefurbishmentJob {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "refurbID", nullable = false)
    private Long refurbID;

    @Column(name = "subpartID", nullable = false)
    private Long subpartID;

    @Column(name = "subPartName", nullable = false)
    private String subpartName;

    @Column(name = "jobID", nullable = false)
    private Long jobID;

    @Column(name = "jobName", nullable = false)
    private String jobName;

    @Column(name = "selected", nullable = false)
    @Builder.Default
    private Boolean selected = false;

    @Column(name = "createdAt", nullable = false)
    @CreatedDate
    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();

    @LastModifiedDate
    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;

    @PreUpdate
    protected void preUpdate() {
        this.updatedAt = LocalDateTime.now();
    }

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "updatedBy")
    private Long updatedBy;

    @Column(name = "isActive", columnDefinition = "boolean default true")
    @Builder.Default
    private boolean isActive = true;
}
