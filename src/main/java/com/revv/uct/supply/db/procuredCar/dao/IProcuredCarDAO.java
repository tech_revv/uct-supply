package com.revv.uct.supply.db.procuredCar.dao;

import com.revv.uct.supply.db.enums.ProcuredCarStatus;
import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;

public interface IProcuredCarDAO {
    ProcuredCar saveProcuredCarDetails(ProcuredCar procuredCar);

    ProcuredCar findByIdAndIsActive(Long id, boolean b);

    ProcuredCar updateProcuredCar(ProcuredCar newData);

    ProcuredCar findByLeadIDAndIsActive(Long leadID, boolean isActive);
}
