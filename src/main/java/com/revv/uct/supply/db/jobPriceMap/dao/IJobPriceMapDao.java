package com.revv.uct.supply.db.jobPriceMap.dao;

import com.revv.uct.supply.db.jobPriceMap.entity.JobPriceMap;

import java.util.List;

public interface IJobPriceMapDao {
    JobPriceMap save(JobPriceMap jobPriceMap);

    List<JobPriceMap> findAll();

    List<Object[]> getAllByIsActive(boolean b);

    List<JobPriceMap> getJobPriceByIDs(List<Long> jobIDs);

    List<JobPriceMap> getAllJobPrice();

    List<JobPriceMap> findAllByIsActive(boolean b);
}
