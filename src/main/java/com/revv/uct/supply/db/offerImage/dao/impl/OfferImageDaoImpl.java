package com.revv.uct.supply.db.offerImage.dao.impl;

import com.revv.uct.supply.db.offerImage.dao.IOfferImageDao;
import com.revv.uct.supply.db.offerImage.entity.OfferImage;
import com.revv.uct.supply.db.offerImage.repository.OfferImageRRepository;
import com.revv.uct.supply.db.offerImage.repository.OfferImageRWRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OfferImageDaoImpl implements IOfferImageDao {
    private final OfferImageRRepository offerImageRRepository;
    private final OfferImageRWRepository offerImageRWRepository;

    @Autowired
    public OfferImageDaoImpl(OfferImageRRepository offerImageRRepository, OfferImageRWRepository offerImageRWRepository) {
        this.offerImageRRepository = offerImageRRepository;
        this.offerImageRWRepository = offerImageRWRepository;
    }

    @Override
    public void markOfferImagesInActive(Long existingOfferID, Long createdBy) {
        offerImageRWRepository.markOfferImagesInActive(existingOfferID, createdBy);
    }

    @Override
    public void saveOfferImage(OfferImage offerImageData) {
        offerImageRWRepository.save(offerImageData);
    }


}
