package com.revv.uct.supply.db.inspectionSubPart.dao.impl;

import com.revv.uct.supply.db.inspectionSubPart.dao.IInspectionSubPartDAO;
import com.revv.uct.supply.db.inspectionSubPart.entity.InspectionSubPart;
import com.revv.uct.supply.db.inspectionSubPart.repository.InspectionSubPartRRepository;
import com.revv.uct.supply.db.inspectionSubPart.repository.InspectionSubPartRWRepository;
import com.revv.uct.supply.service.inspection.model.InspectionSubpartDetail;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class InspectionSubPartDAOImpl implements IInspectionSubPartDAO {
    @Autowired
    InspectionSubPartRRepository inspectionSubPartRRepository;
    @Autowired
    InspectionSubPartRWRepository inspectionSubPartRWRepository;


    @Override
    public List<InspectionSubpartDetail> getInspectionSubpartDetails(Long inspectionID) {
        return inspectionSubPartRRepository.getInspectionSubpartDetails(inspectionID);
//        return null;
    }

    @Override
    public List<InspectionSubPart> findAllBySubPartIdsAndIsActiveAndInspectionID(List<Long> subPartIDs, Long inspectionID) {
        return inspectionSubPartRRepository.findAllBySubPartIdsAndIsActiveAndInspectionID(subPartIDs, inspectionID);
    }

    @Override
    public List<InspectionSubPart> updateInspectionSubParts(List<InspectionSubPart> toBeUpdated) {
        return inspectionSubPartRWRepository.saveAll(toBeUpdated);
    }

    @Override
    public List<InspectionSubPart> insertInspectionSubParts(List<InspectionSubPart> newData) {
        return inspectionSubPartRWRepository.saveAll(newData);
    }
}
