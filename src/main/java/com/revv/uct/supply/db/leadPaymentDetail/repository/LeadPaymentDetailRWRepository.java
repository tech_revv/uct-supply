package com.revv.uct.supply.db.leadPaymentDetail.repository;

import com.revv.uct.supply.db.leadPaymentDetail.entity.LeadPaymentDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeadPaymentDetailRWRepository extends JpaRepository<LeadPaymentDetail, Long> {
}
