package com.revv.uct.supply.db.offerImage.repository;

import com.revv.uct.supply.db.offerImage.entity.OfferImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface OfferImageRWRepository extends JpaRepository<OfferImage, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE OfferImage o SET o.updatedBy = :createdBy, o.isActive = false where o.offerID = :existingOfferID")
    void markOfferImagesInActive(@Param("existingOfferID") Long existingOfferID, @Param("createdBy") Long createdBy);
}
//    @Query("UPDATE InspectionImage i SET i.updatedBy = :inspectorID, i.isActive = false where i.inspectionID = :inspectionID")