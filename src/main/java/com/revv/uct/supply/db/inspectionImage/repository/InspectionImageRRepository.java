package com.revv.uct.supply.db.inspectionImage.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.inspectionImage.entity.InspectionImage;
import com.revv.uct.supply.db.inspectionJob.entity.InspectionJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface InspectionImageRRepository extends JpaRepository<InspectionImage, Long> {

    List<InspectionImage> findAllByInspectionIDAndIsActive(Long inspectionID, boolean b);
}
