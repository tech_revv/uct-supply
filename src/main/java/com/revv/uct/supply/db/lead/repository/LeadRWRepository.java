package com.revv.uct.supply.db.lead.repository;

import com.revv.uct.supply.db.lead.entity.Lead;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LeadRWRepository extends JpaRepository<Lead, Long>
{

}
