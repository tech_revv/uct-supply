package com.revv.uct.supply.db.header.dao.impl;

import com.revv.uct.supply.db.header.dao.IHeaderDao;
import com.revv.uct.supply.db.header.entity.Header;
import com.revv.uct.supply.db.header.repository.HeaderRRepository;
import com.revv.uct.supply.db.header.repository.HeaderRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HeaderDaoImpl implements IHeaderDao {

    private final HeaderRRepository headerRRepository;
    private final HeaderRWRepository headerRWRepository;

    @Autowired
    public HeaderDaoImpl(HeaderRRepository headerRRepository, HeaderRWRepository headerRWRepository) {
        this.headerRRepository = headerRRepository;
        this.headerRWRepository = headerRWRepository;
    }

    @Override
    public Header uploadHeader(Header header) {
        Header savedHeader = headerRWRepository.save(header);
        return savedHeader;
    }

    @Override
    public boolean existsByHeaderNameAndIsActive(String headerName, boolean isActive) {
        if(headerRRepository.existsByNameAndIsActive(headerName, isActive)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean existsByHeaderNameAndVersionNumber(String headerName, int headerVersion) {
        if(headerRRepository.existsByNameAndVersionNumber(headerName, headerVersion)) {
            return true;
        }
        return false;
    }

    @Override
    public Header findByHeaderNameAndIsActive(String headerName, boolean isActive) {
        Header header = headerRRepository.findByNameAndIsActive(headerName, isActive);
        return header;
    }

    /*
    @Override
    public boolean existsByName(String name) {
        if(headerRRepository.existsByName(name)){
            return true;
        }
        return false;
    }

    @Override
    public Header uploadHeader(Header header) {
        Header addedHeader = headerRWRepository.save(header);
        return addedHeader;
    }
     */
}
