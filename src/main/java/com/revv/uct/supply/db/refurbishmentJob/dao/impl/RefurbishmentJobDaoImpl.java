package com.revv.uct.supply.db.refurbishmentJob.dao.impl;

import com.revv.uct.supply.db.refurbishmentJob.dao.IRefurbishmentJobDAO;
import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;
import com.revv.uct.supply.db.refurbishmentJob.repository.RefurbishmentJobRRepository;
import com.revv.uct.supply.db.refurbishmentJob.repository.RefurbishmentJobRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RefurbishmentJobDaoImpl implements IRefurbishmentJobDAO {

    @Autowired
    RefurbishmentJobRRepository refurbishmentJobRRepository;

    @Autowired
    RefurbishmentJobRWRepository refurbishmentJobRWRepository;


    @Override
    public List<RefurbishmentJob> getRefurbJobList(Long refurbID) {
        return refurbishmentJobRRepository.findAllByRefurbIDAndIsActive(refurbID, true);
    }

    @Override
    public void saveAll(List<RefurbishmentJob> refurbishmentJobs) {
        refurbishmentJobRWRepository.saveAll(refurbishmentJobs);
    }

    @Override
    public List<RefurbishmentJob> getRefurbJobList(List<Long> refurbIDs) {
        return refurbishmentJobRRepository.findAllByRefurbIDInAndIsActive(refurbIDs, true);
    }

    public List<RefurbishmentJob> getCompletedRefurbJobListByCarID(Long carID) {
        return refurbishmentJobRRepository.getCompletedRefurbJobsByCarID(carID,true);
    }
}
