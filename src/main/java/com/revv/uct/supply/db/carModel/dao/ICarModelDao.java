package com.revv.uct.supply.db.carModel.dao;

import com.revv.uct.supply.db.carModel.entity.CarModel;

import java.util.List;

public interface ICarModelDao {
    CarModel uploadCarModel(CarModel carModel);

    List<CarModel> findAllCarModels();
}
