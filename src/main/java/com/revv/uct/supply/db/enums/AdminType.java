package com.revv.uct.supply.db.enums;

public enum AdminType {
    BDE, INSPECTOR, FINANCE, SENIOR_INSPECTOR, REFURB_SPOC
}
