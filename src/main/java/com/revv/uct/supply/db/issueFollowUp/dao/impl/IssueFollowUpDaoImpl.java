package com.revv.uct.supply.db.issueFollowUp.dao.impl;

import com.revv.uct.supply.db.issueFollowUp.dao.IIssueFollowUpDao;
import com.revv.uct.supply.db.issueFollowUp.entity.IssueFollowUp;
import com.revv.uct.supply.db.issueFollowUp.repository.IssueFollowUpRRepository;
import com.revv.uct.supply.db.issueFollowUp.repository.IssueFollowUpRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IssueFollowUpDaoImpl implements IIssueFollowUpDao {
    private final IssueFollowUpRRepository issueFollowUpRRepository;
    private final IssueFollowUpRWRepository issueFollowUpRWRepository;

    @Autowired
    public IssueFollowUpDaoImpl(IssueFollowUpRRepository issueFollowUpRRepository, IssueFollowUpRWRepository issueFollowUpRWRepository) {
        this.issueFollowUpRRepository = issueFollowUpRRepository;
        this.issueFollowUpRWRepository = issueFollowUpRWRepository;
    }

    @Override
    public IssueFollowUp uploadIssueFollowUp(IssueFollowUp issueFollowUp) {
        IssueFollowUp addedIssueFollowUp = issueFollowUpRWRepository.save(issueFollowUp);
        return addedIssueFollowUp;
    }
}
