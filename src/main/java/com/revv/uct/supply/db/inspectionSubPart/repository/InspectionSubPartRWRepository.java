package com.revv.uct.supply.db.inspectionSubPart.repository;

import com.revv.uct.supply.db.inspectionSubPart.entity.InspectionSubPart;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InspectionSubPartRWRepository extends JpaRepository<InspectionSubPart, Long> {
}
