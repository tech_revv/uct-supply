package com.revv.uct.supply.db.user.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.enums.AdminType;
import com.revv.uct.supply.db.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface UserRRepository extends JpaRepository<User, Long> {
    List<User> findAllByAdminType(AdminType inspector);

    List<User> findAllByAdminTypeAndUserCity(AdminType adminType, Long cityID);

    List<User> findAllByIdIn(List<Long> userIDs);
}
