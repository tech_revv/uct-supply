package com.revv.uct.supply.db.workshop.dao.impl;

import com.revv.uct.supply.db.workshop.dao.IWorkshopDAO;
import com.revv.uct.supply.db.workshop.entity.Workshop;
import com.revv.uct.supply.db.workshop.repository.WorkshopRRepository;
import com.revv.uct.supply.db.workshop.repository.WorkshopRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WorkshopDaoImpl implements IWorkshopDAO {

    private final WorkshopRRepository workshopRRepository;
    private final WorkshopRWRepository workshopRWRepository;

    @Autowired
    public WorkshopDaoImpl(WorkshopRRepository workshopRRepository, WorkshopRWRepository workshopRWRepository) {
        this.workshopRRepository = workshopRRepository;
        this.workshopRWRepository = workshopRWRepository;
    }

    @Override
    public List<Workshop> getAllActiveWorkshops() {
        return workshopRRepository.getActiveWorkshops();
    }

    @Override
    public Workshop createWorkshop(Workshop workshop) {
        return workshopRWRepository.save(workshop);
    }

    @Override
    public List<Workshop> findAllByWorkshopIDAndIsActive(List<Long> workshopIDs, Boolean isActive) {
        return workshopRRepository.findAllByIdInAndIsActive(workshopIDs, isActive);
    }

    @Override
    public boolean existById(Long workshopID) {
        return workshopRRepository.existsById(workshopID);
    }
}
