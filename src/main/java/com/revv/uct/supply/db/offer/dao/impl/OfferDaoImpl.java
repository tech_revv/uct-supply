package com.revv.uct.supply.db.offer.dao.impl;

import com.revv.uct.supply.db.offer.dao.IOfferDao;
import com.revv.uct.supply.db.offer.entity.Offer;
import com.revv.uct.supply.db.offer.repository.OfferRRepository;
import com.revv.uct.supply.db.offer.repository.OfferRWRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class OfferDaoImpl implements IOfferDao {
    private final OfferRRepository offerRRepository;
    private final OfferRWRepository offerRWRepository;

    @Autowired
    public OfferDaoImpl(OfferRRepository offerRRepository, OfferRWRepository offerRWRepository) {
        this.offerRRepository = offerRRepository;
        this.offerRWRepository = offerRWRepository;
    }

    @Override
    public List<Offer> findByLeadIDAndIsActive(Long leadID, boolean b) {
        return offerRRepository.findByLeadIDAndIsActive(leadID, b);
    }

    @Override
    public void markOfferInActive(Long existingOfferID, Long updatedBy) {
        offerRWRepository.markOfferInActive(existingOfferID, updatedBy);
    }

    @Override
    public Offer createOffer(Offer offer) {
        return offerRWRepository.save(offer);
    }

    @Override
    public Offer findByIdAndIsActive(Long id, boolean b) {
        return offerRRepository.findByIdAndIsActive(id, b);
    }

    @Override
    public Offer updateOffer(Offer newData) {
        return offerRWRepository.save(newData);
    }
}
