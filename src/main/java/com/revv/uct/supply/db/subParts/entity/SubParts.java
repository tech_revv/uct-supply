package com.revv.uct.supply.db.subParts.entity;

import com.revv.uct.supply.db.enums.SubPartInputType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "subparts")
@AllArgsConstructor
@NoArgsConstructor
public class SubParts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @JoinColumn(name = "partID")
    private Long partID;

    @Column(name = "subPartName")
    private String subPartName;

    @Column(name = "inputType")
    @Enumerated(EnumType.STRING)
    private SubPartInputType subPartInputType;

    @Column(name = "fileUpload")
    private boolean fileUpload;

    @Column(name = "isMandatory")
    private boolean isMandatory;

    @Column(name = "unAssured")
    private boolean unAssured;

    @Column(name = "versionNumber")
    private int versionNumber;

    @Column(name = "isActive")
    private boolean isActive;

    //@OneToMany(mappedBy = "subPart",cascade = CascadeType.ALL)
    //private List<SubPartIssues> subPartIssues;
}
