package com.revv.uct.supply.db.user.repository;

import com.revv.uct.supply.db.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRWRepository extends JpaRepository<User, Long> {
}
