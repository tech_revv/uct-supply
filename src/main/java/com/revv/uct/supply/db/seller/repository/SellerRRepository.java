package com.revv.uct.supply.db.seller.repository;


import com.revv.uct.supply.constants.SellerType;
import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.seller.entity.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface SellerRRepository extends JpaRepository<Seller, Long> {

    @Query("select s.name,b.name,s.id from Seller s join ServiceCity b on (s.serviceCityID=b.id) where s.serviceCityID=:serviceCityID and s.sellerType=:sellerType")
    List<Object[]> getSellersForLead(@Param("serviceCityID") Long serviceCityID, @Param("sellerType") SellerTypeEnum sellerType);

    List<Seller> findAllByServiceCityIDAndName(Long serviceCityID, String name);

    Seller findAllByIdAndIsActive(Long id, boolean b);

    List<Seller> findAllByContactNumberAndSellerTypeAndIsActive(String sellerContactNo, SellerTypeEnum b2c, boolean b);
}
