package com.revv.uct.supply.db.leadPaymentDetail.dao.impl;

import com.revv.uct.supply.db.leadPaymentDetail.dao.ILeadPaymentDetailDao;
import com.revv.uct.supply.db.leadPaymentDetail.entity.LeadPaymentDetail;
import com.revv.uct.supply.db.leadPaymentDetail.repository.LeadPaymentDetailRRepository;
import com.revv.uct.supply.db.leadPaymentDetail.repository.LeadPaymentDetailRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LeadPaymentDetailDaoImpl implements ILeadPaymentDetailDao {
    private final LeadPaymentDetailRRepository leadPaymentDetailRRepository;
    private final LeadPaymentDetailRWRepository leadPaymentDetailRWRepository;

    @Autowired
    public LeadPaymentDetailDaoImpl(LeadPaymentDetailRRepository leadPaymentDetailRRepository, LeadPaymentDetailRWRepository leadPaymentDetailRWRepository) {
        this.leadPaymentDetailRRepository = leadPaymentDetailRRepository;
        this.leadPaymentDetailRWRepository = leadPaymentDetailRWRepository;
    }

    @Override
    public LeadPaymentDetail saveLeadPaymentDetails(LeadPaymentDetail data) {
        return leadPaymentDetailRWRepository.save(data);
    }
}
