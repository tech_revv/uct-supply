package com.revv.uct.supply.db.inspectionImage.dao.impl;

import com.revv.uct.supply.db.inspectionImage.dao.IInspectionImageDAO;
import com.revv.uct.supply.db.inspectionImage.entity.InspectionImage;
import com.revv.uct.supply.db.inspectionImage.repository.InspectionImageRRepository;
import com.revv.uct.supply.db.inspectionImage.repository.InspectionImageRWRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class InspectionImageDAOImpl implements IInspectionImageDAO {
    @Autowired
    InspectionImageRRepository inspectionImageRRepository;
    @Autowired
    InspectionImageRWRepository inspectionImageRWRepository;

    @Override
    public List<InspectionImage> getInspectionImages(Long inspectionID) {
        return inspectionImageRRepository.findAllByInspectionIDAndIsActive(inspectionID, true);
    }

    @Override
    public void markImagesInactive(Long inspectorID, Long inspectionID) {
        inspectionImageRWRepository.markImagesInactive(inspectorID, inspectionID);
    }

    @Override
    public void saveInspectionImage(List<InspectionImage> inspectionImages) {
        inspectionImageRWRepository.saveAll(inspectionImages);
    }
}
