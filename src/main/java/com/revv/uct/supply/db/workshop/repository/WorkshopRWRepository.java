package com.revv.uct.supply.db.workshop.repository;

import com.revv.uct.supply.db.workshop.entity.Workshop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkshopRWRepository extends JpaRepository<Workshop, Long> {

}


