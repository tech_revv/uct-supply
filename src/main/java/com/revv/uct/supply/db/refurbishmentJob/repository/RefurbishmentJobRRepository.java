package com.revv.uct.supply.db.refurbishmentJob.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface RefurbishmentJobRRepository extends JpaRepository<RefurbishmentJob, Long> {

    List<RefurbishmentJob> findAllByRefurbIDAndIsActive(Long refurbID, Boolean isActive);

    List<RefurbishmentJob> findAllByRefurbIDInAndIsActive(List<Long> refurbIDs, Boolean b);

    @Query("SELECT rj FROM RefurbishmentJob rj where rj.refurbID in (SELECT r.id FROM Refurbishment r where r.procuredCarID=:car_id and r.status='COMPLETED') and rj.isActive=:is_active and rj.selected = true")
    List<RefurbishmentJob> getCompletedRefurbJobsByCarID(@Param("car_id")Long carID,@Param("is_active") Boolean isActive);

}
