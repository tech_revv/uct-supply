package com.revv.uct.supply.db.offerImage.dao;

import com.revv.uct.supply.db.offerImage.entity.OfferImage;

public interface IOfferImageDao {
    void markOfferImagesInActive(Long existingOfferID, Long createdBy);

    void saveOfferImage(OfferImage offerImageData);
}
