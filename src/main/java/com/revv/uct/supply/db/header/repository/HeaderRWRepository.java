package com.revv.uct.supply.db.header.repository;

import com.revv.uct.supply.db.header.entity.Header;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HeaderRWRepository extends JpaRepository<Header, Long> {
}
