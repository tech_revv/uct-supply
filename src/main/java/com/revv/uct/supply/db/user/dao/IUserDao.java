package com.revv.uct.supply.db.user.dao;

import com.revv.uct.supply.db.enums.AdminType;
import com.revv.uct.supply.db.user.entity.User;

import java.util.List;

public interface IUserDao {
    List<User> getAdmins(AdminType adminType);

    boolean existById(Long inspectorID);

    List<User> getAdminsByCity(AdminType adminType, Long cityID);

    List<User> findAllByIDAndIsActive(List<Long> userIDs, Boolean isActive);
}
