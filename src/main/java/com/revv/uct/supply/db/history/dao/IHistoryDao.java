package com.revv.uct.supply.db.history.dao;

import com.revv.uct.supply.db.enums.EditEntity;
import com.revv.uct.supply.db.history.entity.History;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface IHistoryDao {
    History save(History data);

    List<History> getHistoricalData(Long entityID, EditEntity entity);
}
