package com.revv.uct.supply.db.jobs.dao.impl;

import com.revv.uct.supply.db.jobs.dao.IJobsDao;
import com.revv.uct.supply.db.jobs.entity.Jobs;
import com.revv.uct.supply.db.jobs.repository.JobsRRepository;
import com.revv.uct.supply.db.jobs.repository.JobsRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JobsDaoImpl implements IJobsDao {
    private final JobsRRepository jobsRRepository;
    private final JobsRWRepository jobsRWRepository;

    @Autowired
    public JobsDaoImpl(JobsRRepository jobsRRepository, JobsRWRepository jobsRWRepository) {
        this.jobsRRepository = jobsRRepository;
        this.jobsRWRepository = jobsRWRepository;
    }

    @Override
    public Jobs uploadJob(Jobs job) {
        Jobs savedJob = jobsRWRepository.save(job);
        return savedJob;
    }

    @Override
    public boolean existsByJobNameAndIsActive(String jobName, boolean isActive) {
        if(jobsRRepository.existsByJobNameAndIsActive(jobName, isActive)) {
            return true;
        }
        return false;
    }

    @Override
    public Jobs findByJobNameAndIsActive(String jobName, boolean b) {
        List<Jobs> jobs = jobsRRepository.findByJobNameAndIsActive(jobName, b);
        if(jobs.size() > 0) {
            Jobs job = jobs.get(0);
            return  job;
        }
        return null;
    }

    @Override
    public List<Jobs> findByJobID(List<Long> jobID) {
        return jobsRRepository.findByIdIn(jobID);
    }

    @Override
    public List<Jobs> findAllByIdAndIsActive(List<Long> jobIDs) {
        return jobsRRepository.findAllByIdInAndIsActive(jobIDs,true);
    }
}
