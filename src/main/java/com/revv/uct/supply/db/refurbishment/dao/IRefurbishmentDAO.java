package com.revv.uct.supply.db.refurbishment.dao;

import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.refurbishment.entity.Refurbishment;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.InspectionDetail;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.RefurbList;
import com.revv.uct.supply.service.refurbishment.model.RefurbDetail;

import java.util.List;

public interface IRefurbishmentDAO {
    List<RefurbList> findRefurbishments(Long refurbSpocID, List<InspectionStatus> statusList);

    RefurbDetail getRefurbDetails(Long refurbID);

    Refurbishment findByIdAndIsActive(Long refurbID, boolean isActive);

    Refurbishment updateRefurbishment(Refurbishment data);

    List<Refurbishment> findRefurbishmentsByCarIDAndStatus(List<Long> carIDs, List<InspectionStatus> status, Boolean isActive);
}
