package com.revv.uct.supply.db.subPartIssues.repository;

import com.revv.uct.supply.db.subPartIssues.entity.SubPartIssues;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubPartIssuesRWRepository extends JpaRepository<SubPartIssues, Long> {
}
