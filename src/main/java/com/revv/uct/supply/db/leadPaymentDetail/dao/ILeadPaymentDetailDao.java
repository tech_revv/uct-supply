package com.revv.uct.supply.db.leadPaymentDetail.dao;

import com.revv.uct.supply.db.leadPaymentDetail.entity.LeadPaymentDetail;

public interface ILeadPaymentDetailDao {
    LeadPaymentDetail saveLeadPaymentDetails(LeadPaymentDetail data);
}
