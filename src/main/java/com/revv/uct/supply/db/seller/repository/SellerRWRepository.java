package com.revv.uct.supply.db.seller.repository;

import com.revv.uct.supply.db.seller.entity.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerRWRepository extends JpaRepository<Seller, Long>
{

}
