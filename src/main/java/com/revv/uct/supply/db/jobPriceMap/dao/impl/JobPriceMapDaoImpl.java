package com.revv.uct.supply.db.jobPriceMap.dao.impl;

import com.revv.uct.supply.db.jobPriceMap.dao.IJobPriceMapDao;
import com.revv.uct.supply.db.jobPriceMap.entity.JobPriceMap;
import com.revv.uct.supply.db.jobPriceMap.repository.JobPriceMapRRepository;
import com.revv.uct.supply.db.jobPriceMap.repository.JobPriceMapRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JobPriceMapDaoImpl implements IJobPriceMapDao {

    private final JobPriceMapRRepository jobPriceMapRRepository;
    private final JobPriceMapRWRepository jobPriceMapRWRepository;

    @Autowired
    public JobPriceMapDaoImpl(JobPriceMapRRepository jobPriceMapRRepository, JobPriceMapRWRepository jobPriceMapRWRepository) {
        this.jobPriceMapRRepository = jobPriceMapRRepository;
        this.jobPriceMapRWRepository = jobPriceMapRWRepository;
    }

    @Override
    public JobPriceMap save(JobPriceMap jobPriceMap) {
        JobPriceMap saved = jobPriceMapRWRepository.save(jobPriceMap);
        System.out.println("savedAtDB==>>"+saved.toString());
        return saved;
    }

    @Override
    public List<JobPriceMap> findAll() {
        return jobPriceMapRRepository.findAll();
    }

    @Override
    public List<Object[]> getAllByIsActive(boolean b) {
        return jobPriceMapRRepository.getAllJobsPriceByIsActive(b);
    }

    @Override
    public List<JobPriceMap> getJobPriceByIDs(List<Long> jobIDs) {
        return jobPriceMapRRepository.getAllByJobIDInAndIsActive(jobIDs, true);
    }

    @Override
    public List<JobPriceMap> getAllJobPrice() {
        return jobPriceMapRRepository.getAllByIsActive(true);
    }

    @Override
    public List<JobPriceMap> findAllByIsActive(boolean b) {
        return jobPriceMapRRepository.findAllByIsActive(b);
    }
}
