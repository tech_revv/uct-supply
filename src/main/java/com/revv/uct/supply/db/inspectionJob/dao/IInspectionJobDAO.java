package com.revv.uct.supply.db.inspectionJob.dao;


import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspectionJob.entity.InspectionJob;

import java.util.List;

public interface IInspectionJobDAO {

    List<InspectionJob> getInspectionJobs(Long inspectionID);

    void markJobsInactive(Long inspectorID, Long inspectionID);

    void saveInspectionJob(List<InspectionJob> inspectionJobs);

    List<InspectionJob> getInspectionJobs(List<Long> inspectionIDs);

    List<InspectionJob> getInspectionJobsByCarIDAndType(Long carID, InspectionType inspectionType);

}
