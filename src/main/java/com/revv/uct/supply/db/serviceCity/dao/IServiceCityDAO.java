package com.revv.uct.supply.db.serviceCity.dao;

import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;

import java.util.List;
import java.util.Optional;


public interface IServiceCityDAO
{
    ServiceCity createServiceCity(ServiceCity serviceCity);

    List<ServiceCity> findAllServiceCities();

    ServiceCity findServiceCityById(Long id);
}
