package com.revv.uct.supply.db.enums;

public enum InspectionCardType {
    INSPECTION,
    PICKUP_CHECKLIST,
    REFURBISHMENT,
    POST_REFURBISHMENT
}
