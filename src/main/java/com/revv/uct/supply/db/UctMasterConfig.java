package com.revv.uct.supply.db;


import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource({"classpath:application.properties"})
@EnableJpaRepositories(
        basePackages = "com.revv.uct.supply.db",
        excludeFilters = @ComponentScan.Filter(ReadOnlyRepository.class),
        entityManagerFactoryRef = "uctSupplyMasterEntityManager",
        transactionManagerRef = "uctSupplyMasterTransactionManager"
)
@EnableTransactionManagement
public class UctMasterConfig {
    @Autowired
    public UctMasterConfig() {
        super();
    }

    @Bean("uctMasterDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.uct-master-datasource")
    public DataSource uctMasterDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean uctSupplyMasterEntityManager(@Qualifier("uctMasterDataSource") final DataSource uctMasterDataSource){
        HibernateJpaVendorAdapter vendorAdapter =new HibernateJpaVendorAdapter();
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(uctMasterDataSource);
        em.setPackagesToScan("com.revv.uct.supply.db");
        em.setJpaVendorAdapter(vendorAdapter);
        Map<String, String> properties = new HashMap<>();
        //properties.put("hibernate.implicit_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy");
        //properties.put("hibernate.physical_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
        em.setJpaPropertyMap(properties);
        return em;
    }


    @Primary
    @Bean
    public PlatformTransactionManager uctSupplyMasterTransactionManager(@Qualifier("uctSupplyMasterEntityManager") final LocalContainerEntityManagerFactoryBean uctSupplyMasterEntityManager){
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(uctSupplyMasterEntityManager.getObject());
        return transactionManager;
    }




}
