package com.revv.uct.supply.db.enums;

public enum PaymentType {
    TOKEN,
    FULL_PAYMENT
}
