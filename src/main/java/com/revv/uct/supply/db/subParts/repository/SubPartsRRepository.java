package com.revv.uct.supply.db.subParts.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface SubPartsRRepository extends JpaRepository<SubParts, Long> {
    boolean existsBySubPartNameAndIsActive(String subPartName, boolean isActive);

    SubParts findBySubPartNameAndIsActive(String subPartName, boolean b);

    List<SubParts> findAllByIsActive(boolean b);
}
