package com.revv.uct.supply.db.documentStageMaster.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.documentStageMaster.entity.DocumentStageMaster;
import com.revv.uct.supply.db.enums.DocumentStage;
import com.revv.uct.supply.db.enums.DocumentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface DocumentStageMasterRRepository extends JpaRepository<DocumentStageMaster, Long> {

    @Query("SELECT dsm FROM DocumentStageMaster dsm WHERE dsm.isActive = TRUE AND dsm.documentType = :serviceHistory AND dsm.documentStage = :offerCreation")
    List<DocumentStageMaster> findAllByDocumentTypeAndDocumentStageAndIsActive(@Param("serviceHistory") DocumentType serviceHistory, @Param("offerCreation") DocumentStage offerCreation);

    List<DocumentStageMaster> findAllByDocumentStageAndIsActive(DocumentStage documentStage, boolean b);
}
