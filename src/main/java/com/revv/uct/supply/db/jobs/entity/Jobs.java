package com.revv.uct.supply.db.jobs.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "jobs")
@AllArgsConstructor
@NoArgsConstructor
public class Jobs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "jobName")
    private String jobName;

    @Column(name = "versionNumber")
    private int versionNumber;

    @Column(name = "isActive")
    private boolean isActive;

    //@OneToOne(mappedBy = "job", fetch = FetchType.EAGER)
    //private IssueFollowUp issueFollowUp;
}
