package com.revv.uct.supply.db.workshop.dao;

import com.revv.uct.supply.db.workshop.entity.Workshop;

import java.util.List;

public interface IWorkshopDAO {

    List<Workshop> getAllActiveWorkshops();

    Workshop createWorkshop(Workshop workshop);

    List<Workshop> findAllByWorkshopIDAndIsActive(List<Long> workshopIDs, Boolean isActive);

    boolean existById(Long workshopID);
}
