package com.revv.uct.supply.db.seller.entity;

import com.revv.uct.supply.db.enums.SellerTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@Table(name = "sellers")
@NoArgsConstructor
@AllArgsConstructor
public class Seller
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "adminID")
    private Long adminID;

    @Column(name = "name")
    private String name;

    @Column(name = "sellerType")
    @Enumerated(EnumType.STRING)
    private SellerTypeEnum sellerType;

    @Column(name = "contactNumber")
    private String contactNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "spocName")
    private String spocName;

    @Column(name="serviceCityID")
    private Long serviceCityID;

    @Column(name = "latitude")
    private Double lat;

    @Column(name = "longitude")
    private Double lng;

    @Column(name = "address")
    private String address;

    @Column(name = "bankAccountDetails")
    private String bankAcDetails;

    @Column(name = "gstNumber")
    private String gstNumber;

    @Column(name = "panNumber")
    private String panNumber;

    @CreationTimestamp
    @Column(name = "createdAt")
    private LocalDateTime createdAt;

    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;

    @Column(name = "isActive")
    private boolean isActive;
}
