package com.revv.uct.supply.db.subParts.repository;

import com.revv.uct.supply.db.subParts.entity.SubParts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubPartsRWRepository extends JpaRepository<SubParts, Long> {
}
