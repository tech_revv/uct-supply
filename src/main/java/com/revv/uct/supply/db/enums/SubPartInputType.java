package com.revv.uct.supply.db.enums;

public enum SubPartInputType {
    SINGLE, MULTI
}
