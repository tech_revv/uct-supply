package com.revv.uct.supply.db.commonDocument.repository;

import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.enums.CommonDocumentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CommonDocumentRWRepository extends JpaRepository<CommonDocument, Long> {
    @Transactional
    @Modifying
    @Query("UPDATE CommonDocument c SET c.updatedBy = :adminID, c.isActive = false where c.entityID = :entityID " +
            "AND c.commonDocumentEntity = :entityName AND c.id in (:documentIDs)")
    void markDocumentsInactive(@Param("entityName") CommonDocumentEntity entityName, @Param("entityID") Long entityID,
                               @Param("adminID") Long adminID, @Param("documentIDs") List<Long> documentIDs);

    @Transactional
    @Modifying
    @Query("UPDATE CommonDocument c SET c.updatedBy = :adminID, c.isActive = false where c.entityID = :entityID " +
            "AND c.commonDocumentEntity = :entityName AND c.documentStageMasterID in (:documentStageMasterIDs)")
    void markDocumentsInactiveAgainstDocumentStageMaster(@Param("entityName") CommonDocumentEntity entityName, @Param("entityID") Long entityID,
                               @Param("adminID") Long adminID, @Param("documentStageMasterIDs") List<Long> documentStageMasterIDs);
}
