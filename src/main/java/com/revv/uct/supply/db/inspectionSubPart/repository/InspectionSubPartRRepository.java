package com.revv.uct.supply.db.inspectionSubPart.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.inspectionSubPart.entity.InspectionSubPart;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.service.inspection.model.InspectionSubpartDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface InspectionSubPartRRepository extends JpaRepository<InspectionSubPart, Long> {

    @Query("SELECT new com.revv.uct.supply.service.inspection.model.InspectionSubpartDetail(" +
            "isp.inspectionID, isp.subPartID," +
            "sp.subPartName, sp.subPartInputType, sp.fileUpload, sp.isMandatory, sp.unAssured," +
            "p.id, p.partName, p.isAccessory, p.accessoryType," +
            "h.id, h.name," +
            "spi.id, spi.issueName," +
            "ifu.jobID, ifu.jobName) " +
            "FROM InspectionSubPart isp " +
            "JOIN SubParts sp ON (sp.id = isp.subPartID) " +
            "JOIN Parts p ON (sp.partID = p.id) " +
            "JOIN Header h ON (h.id = p.headerID) " +
            "JOIN SubPartIssues spi ON (sp.id = spi.subPartID) " +
            "JOIN IssueFollowUp ifu ON (spi.id = ifu.subPartIssueID) " +
            "WHERE isp.inspectionID = :inspectionID AND " +
            "isp.isActive = TRUE AND " +
            "sp.isActive = TRUE AND " +
            "p.isActive = TRUE AND " +
            "h.isActive = TRUE AND " +
            "spi.isActive = TRUE AND " +
            "ifu.isActive = TRUE")
    List<InspectionSubpartDetail> getInspectionSubpartDetails(@Param("inspectionID") Long inspectionID);

    @Query("SELECT isp FROM InspectionSubPart isp WHERE isp.isActive = TRUE AND isp.inspectionID = :inspectionID AND isp.subPartID in (:subPartIDs)")
    List<InspectionSubPart> findAllBySubPartIdsAndIsActiveAndInspectionID(@Param("subPartIDs") List<Long> subPartIDs, @Param("inspectionID") Long inspectionID);
}
