package com.revv.uct.supply.db.enums;

public enum ProcuredCarStatus {
    ON_BOARDED,
    E3_SCHEDULED,
    E3_CONDUCTED,
    READY_FOR_REFURB,
    IN_WORKSHOP,
    E4A_CONDUCTED
}
