package com.revv.uct.supply.db.offer.entity;

import com.revv.uct.supply.db.enums.OfferReviseStage;
import com.revv.uct.supply.db.enums.OfferStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@Table(name = "offers")
@NoArgsConstructor
@AllArgsConstructor
public class Offer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "inspectionID")
    private Long inspectionID;

    @Column(name = "leadID")
    private Long leadID;

    @Column(name = "challanStatus")
    private String challanStatus;

    @Column(name = "serviceHistoryStatus")
    private String serviceHistoryStatus;

    @Column(name = "offeredPrice")
    private Double offeredPrice;

    @Column(name = "basePrice")
    private Double basePrice;

    @Column(name = "refubCost")
    private Double refubCost;

    @Column(name = "refubCostE2")
    private Double refubCostE2;

    @Column(name = "challanCost")
    private Double challanCost;

    @Column(name = "adjustCost")
    private Double adjustCost;

    @Column(name = "offerStatus")
    @Enumerated(EnumType.STRING)
    private OfferStatus offerStatus;

    @Column(name = "noOfDocuments")
    private int noOfDocuments;

    @Column(name = "isActive", nullable = false)
    @Builder.Default
    private Boolean isActive = true;

    @Column(name = "isRevised", nullable = false)
    @Builder.Default
    private Boolean isRevised = false;

    @Enumerated(EnumType.STRING)
    @Column(name = "offerReviseStage")
    private OfferReviseStage offerReviseStage;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "updatedBy")
    private Long updatedBy;

    @Column(name = "createdAt")
    @CreatedDate
    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();

    @LastModifiedDate
    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;

    @PreUpdate
    protected void preUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
}
