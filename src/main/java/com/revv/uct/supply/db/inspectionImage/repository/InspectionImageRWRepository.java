package com.revv.uct.supply.db.inspectionImage.repository;

import com.revv.uct.supply.db.inspectionImage.entity.InspectionImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface InspectionImageRWRepository extends JpaRepository<InspectionImage, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE InspectionImage i SET i.updatedBy = :inspectorID, i.isActive = false where i.inspectionID = :inspectionID")
    void markImagesInactive(@Param("inspectorID") Long inspectorID, @Param("inspectionID") Long inspectionID);
}
