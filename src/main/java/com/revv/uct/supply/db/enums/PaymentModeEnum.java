package com.revv.uct.supply.db.enums;

public enum PaymentModeEnum {
    ONLINE_TRANSFER,
    CARD,
    CHEQUE,
    DD,
    CASH
}