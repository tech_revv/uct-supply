package com.revv.uct.supply.db.enums;

public enum DocumentStage {
    E2_APPROVED,
    OFFER_CREATION,
    TOKEN_PAID,
    REFURB_WORKSHOP
}
