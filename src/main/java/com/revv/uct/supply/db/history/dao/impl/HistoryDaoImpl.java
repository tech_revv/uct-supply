package com.revv.uct.supply.db.history.dao.impl;

import com.revv.uct.supply.db.enums.EditEntity;
import com.revv.uct.supply.db.history.dao.IHistoryDao;
import com.revv.uct.supply.db.history.entity.History;
import com.revv.uct.supply.db.history.repository.HistoryRRepository;
import com.revv.uct.supply.db.history.repository.HistoryRWRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class HistoryDaoImpl implements IHistoryDao {
    private final HistoryRRepository historyRRepository;
    private final HistoryRWRepository historyRWRepository;

    @Autowired
    public HistoryDaoImpl(HistoryRRepository historyRRepository, HistoryRWRepository historyRWRepository) {
        this.historyRRepository = historyRRepository;
        this.historyRWRepository = historyRWRepository;
    }

    @Override
    public History save(History data) {
        return historyRWRepository.save(data);
    }

    @Override
    public List<History> getHistoricalData(Long entityID, EditEntity entity) {
        return historyRRepository.findAllByEditEntityIDAndEditEntity(entityID, entity);
    }
}
