package com.revv.uct.supply.db.refurbishmentJob.repository;

import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RefurbishmentJobRWRepository extends JpaRepository<RefurbishmentJob, Long> {

}


