package com.revv.uct.supply.db.enums;

public enum InsuranceTypeEnum {
    THIRD_PARTY, COMPREHENSIVE
}
