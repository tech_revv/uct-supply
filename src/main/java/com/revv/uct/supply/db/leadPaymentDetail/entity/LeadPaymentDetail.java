package com.revv.uct.supply.db.leadPaymentDetail.entity;

import com.revv.uct.supply.db.enums.PaymentModeEnum;
import com.revv.uct.supply.db.enums.PaymentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@Table(name = "leadPaymentDetails")
@NoArgsConstructor
@AllArgsConstructor
//@Embeddable
public class LeadPaymentDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "leadID")
    private Long leadID;

    @Enumerated(EnumType.STRING)
    @Column(name = "paymentType")
    private PaymentType paymentType;

    @Column(name = "amountPaid")
    private Double amountPaid;

    @Enumerated(EnumType.STRING)
    @Column(name = "paymentMode")
    private PaymentModeEnum paymentMode;

    @Column(name = "referenceNumber")
    private String referenceNumber;

    @Column(name = "paidBy")
    private Long paidBy;

    @Column(name = "noOfDocuments")
    private int noOfDocuments;

    @Column(name = "isActive", nullable = false)
    @Builder.Default
    private Boolean isActive = true;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "updatedBy")
    private Long updatedBy;

    @Column(name = "createdAt")
    @CreatedDate
    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();

    @LastModifiedDate
    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;

    @PreUpdate
    protected void preUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
}
