package com.revv.uct.supply.db.lead.dao.impl;

import com.revv.uct.supply.db.lead.dao.ILeadDao;
import com.revv.uct.supply.db.lead.entity.Lead;
import com.revv.uct.supply.db.lead.repository.LeadRRepository;
import com.revv.uct.supply.db.lead.repository.LeadRWRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class LeadDaoImpl implements ILeadDao
{
    private final LeadRRepository leadRRepository;
    private final LeadRWRepository leadRWRepository;


    @Autowired
    public LeadDaoImpl(LeadRRepository leadRRepository, LeadRWRepository leadRWRepository) {
        this.leadRRepository = leadRRepository;
        this.leadRWRepository = leadRWRepository;
    }


    @Override
    public Lead createLead(Lead lead) {
        return leadRWRepository.save(lead);
    }

    @Override
    public boolean existsByRegistrationNumber(String registrationNumber) {
        if(leadRRepository.existsByRegistrationNumber(registrationNumber)) {
            return true;
        }
        return false;
    }

    @Override
    public Lead findByIdAndIsActive(Long id, boolean b) {
        return leadRRepository.findByIdAndIsActive(id, b);
    }

    @Override
    public Lead updateLead(Lead newData) {
        return leadRWRepository.save(newData);
    }

    @Override
    public boolean existByIdAndIsActive(Long leadID, boolean b) {
        return leadRRepository.existsByIdAndIsActive(leadID, b);
    }

    @Override
    public Long getAllLeadsCount() {
        return leadRRepository.count();
    }

}
