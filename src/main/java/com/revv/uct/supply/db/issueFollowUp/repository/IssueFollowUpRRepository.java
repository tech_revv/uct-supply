package com.revv.uct.supply.db.issueFollowUp.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.issueFollowUp.entity.IssueFollowUp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@ReadOnlyRepository
public interface IssueFollowUpRRepository extends JpaRepository<IssueFollowUp, Long> {
}
