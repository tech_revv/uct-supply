package com.revv.uct.supply.db.history.entity;

import com.revv.uct.supply.db.enums.EditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@Table(name = "history")
@NoArgsConstructor
@AllArgsConstructor
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "editEntityID")
    private Long editEntityID;

    @Column(name = "editEntity")
    @Enumerated(EnumType.STRING)
    private EditEntity editEntity;

    @Column(name = "adminID")
    private Long adminID;

    @Column(name = "oldData", columnDefinition="CLOB NOT NULL")
    @Lob
    private String oldData;

    @Column(name = "newData", columnDefinition="CLOB NOT NULL")
    @Lob
    private String newData;

    @CreationTimestamp
    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;
}