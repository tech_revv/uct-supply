package com.revv.uct.supply.db.jobPriceMap.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "jobpricemap")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class JobPriceMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "jobID")
    private Long jobID;

    @Column(name = "bodyType")
    private String bodyType;

    @Column(name = "model")
    private String model;

    @Column(name = "workshop")
    private String workshop;

    @Column(name = "price")
    private Double price;

    @Column(name = "versionNumber")
    private int versionNumber;

    @Column(name = "isActive")
    private boolean isActive;
}
