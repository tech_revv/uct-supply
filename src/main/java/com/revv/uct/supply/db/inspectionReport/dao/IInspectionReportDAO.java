package com.revv.uct.supply.db.inspectionReport.dao;


import com.revv.uct.supply.db.inspectionReport.entity.InspectionReport;

import java.util.ArrayList;
import java.util.List;

public interface IInspectionReportDAO {

    List<InspectionReport> getInspectionReport(Long inspectionID);

    void markReportsInactive(Long inspectorID, Long inspectionID);

    void saveInspectionReport(List<InspectionReport> inspectionReports);

    List<InspectionReport> getInspectionReportByInspectionIDs(List<Long> inspectionIDs,Boolean isActive);
}
