package com.revv.uct.supply.db.parts.dao;

import com.revv.uct.supply.db.parts.entity.Parts;

public interface IPartsDao {
    Parts uploadPart(Parts part);

    boolean existsByPartNameAndIsActive(String partName, boolean isActive);

    Parts findByPartNameAndVersionNumber(String partName, int partVersion);

    Parts findByPartNameAndIsActive(String partName, boolean isActive);
}
