package com.revv.uct.supply.db.lead.entity;

import com.revv.uct.supply.db.enums.InsuranceTypeEnum;
import com.revv.uct.supply.db.enums.LeadStatus;
import com.revv.uct.supply.db.enums.LeadRejectSubStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@Table(name = "leads")
@NoArgsConstructor
@AllArgsConstructor
//@Embeddable
public class Lead
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "adminID")
    private Long adminID;

    @Column(name = "carModelID")
    private Long carModelID;

    @Column(name = "sellerID")
    private Long sellerID;

    @Column(name = "registrationNumber")
    private String registrationNumber;

    @Column(name = "carColour")
    private String carColour;

    @Column(name = "registrationTimestamp")
    private LocalDateTime registrationTimestamp;

    @Column(name = "manufacturingTimestamp")
    private LocalDateTime manufacturingTimestamp;

    @Column(name = "insuranceValidity")
    private LocalDateTime insuranceValidity;

    @Column(name = "insuranceType")
    private String insuranceType;

    @Column(name = "rto")
    private String rto;

    @Column(name = "registrationType")
    private String registrationType;

    @Column(name = "ownershipSerial")
    private String ownershipSerial;

    @Column(name = "serviceCityID")
    private Long serviceCityID;

    @Column(name = "comments")
    private String comments;

    @Column(name = "expectedOfferPrice")
    private Double expOfferPrice;

    @Column(name = "kmsDriven")
    private Double kmsDriven;

    @Column(name = "leadStatus", columnDefinition = "default 'CREATED'")
    @Enumerated(EnumType.STRING)
    private LeadStatus leadStatus;

    @Column(name = "leadRejectSubStatus")
    @Enumerated(EnumType.STRING)
    private LeadRejectSubStatus leadRejectSubStatus;

    @Column(name = "rejectionReason")
    private String rejectionReason;

    @Column(name = "handoverDate")
    private LocalDateTime handoverDate;

    @Column(name = "pickupBy")
    private Long pickupBy;

    @CreationTimestamp
    @Column(name = "createdAt")
    private LocalDateTime createdAt;

    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;

    @Column(name = "isActive")
    private boolean isActive;
}
