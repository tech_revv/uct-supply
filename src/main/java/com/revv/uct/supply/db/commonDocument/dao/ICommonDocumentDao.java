package com.revv.uct.supply.db.commonDocument.dao;

import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.enums.CommonDocumentEntity;

import java.util.List;

public interface ICommonDocumentDao {
    void save(CommonDocument commonDocument);

    void markDocumentsInactive(CommonDocumentEntity entityName, Long entityID, Long adminID, List<Long> documentIDs);

    void saveAll(List<CommonDocument> commonDocuments);

    List<CommonDocument> findAllByEntityIDAndIsActive(Long existingOfferID, boolean b);

    List<CommonDocument> getDocumentsForEntityIDAndNameAndIsActive(Long entityID, CommonDocumentEntity entityName, Boolean isActive);

    List<CommonDocument> findAllByEntityIDAndDocumentStageMasterIDAndCommonDocumentEntityAndIsActive(Long entityID, Long dsmID, CommonDocumentEntity entity, boolean b);

    List<CommonDocument> getDocumentsForEntityIDAndNameAndStage(Long entityID, CommonDocumentEntity entityName, List<Long> documentStageMasterIDs, Boolean isActive);

    void markDocumentsInactiveAgainstDocumentStageMaster(CommonDocumentEntity entityName, Long entityID, Long adminID, List<Long> documentStageMasterIDs);
}
