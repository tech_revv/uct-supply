package com.revv.uct.supply.db.jobPriceMap.repository;

import com.revv.uct.supply.db.jobPriceMap.entity.JobPriceMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobPriceMapRWRepository extends JpaRepository<JobPriceMap, Long> {
}
