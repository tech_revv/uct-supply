package com.revv.uct.supply.db.subPartIssues.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.subPartIssues.entity.SubPartIssues;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface SubPartIssuesRRepository extends JpaRepository<SubPartIssues, Long> {
    boolean existsBySubPartIDAndIssueName(Long id, String issueName);

    List<SubPartIssues> findBySubPartIDAndIssueNameAndIsActive(Long id, String issueName, boolean b);

    List<SubPartIssues>findAllByIdIn(List<Long> id);
}
