package com.revv.uct.supply.db.offer.repository;

import com.revv.uct.supply.db.offer.entity.Offer;
import org.springframework.data.annotation.Transient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface OfferRWRepository extends JpaRepository<Offer, Long> {
    @Transactional
    @Modifying
    @Query("UPDATE Offer o SET o.updatedBy = :updatedBy, o.isActive = false where o.id = :existingOfferID")
    void markOfferInActive(@Param("existingOfferID") Long existingOfferID, @Param("updatedBy") Long updatedBy);
}

//    @Query("UPDATE InspectionReport i SET i.updatedBy = :inspectorID, i.isActive = false where i.inspectionID = :inspectionID")