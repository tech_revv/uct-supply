package com.revv.uct.supply.db.documentStageMaster.entity;

import com.revv.uct.supply.db.enums.DocumentStage;
import com.revv.uct.supply.db.enums.DocumentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "documentStageMasters")
public class DocumentStageMaster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "documentName")
    private String documentName;

    @Column(name = "documentType")
    @Enumerated(EnumType.STRING)
    private DocumentType documentType;

    @Column(name = "documentStage")
    @Enumerated(EnumType.STRING)
    private DocumentStage documentStage;

    @Column(name = "isMandatory")
    private boolean isMandatory;

    @Column(name = "isActive")
    private boolean isActive;

}
