package com.revv.uct.supply.db.header.dao;

import com.revv.uct.supply.db.header.entity.Header;

public interface IHeaderDao {

    Header uploadHeader(Header header);

    boolean existsByHeaderNameAndIsActive(String headerName, boolean isActive);

    boolean existsByHeaderNameAndVersionNumber(String headerName, int headerVersion);

    Header findByHeaderNameAndIsActive(String headerName, boolean isActive);
}
