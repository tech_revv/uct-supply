package com.revv.uct.supply.db.carModel.entity;

import com.revv.uct.supply.db.enums.Transmission;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "carmodels")
@AllArgsConstructor
@NoArgsConstructor
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "make")
    private String make;

    @Column(name = "model")
    private String model;

    @Column(name = "variant")
    private String variant;

    @Column(name = "fuel")
    private String fuel;

    @Column(name = "transmission")
    @Enumerated(EnumType.STRING)
    private Transmission transmission;

    @Column(name = "bodyType")
    private String bodyType;

    @Column(name = "seats")
    private int seats;

    @Column(name = "startYear")
    private int startYear;

    @Column(name = "endYear")
    private int endYear;

    @Column(name = "price")
    private Double price;

    @Column(name = "versionNumber")
    private int versionNumber;

    @Column(name = "isActive", columnDefinition = "boolean default true")
    private boolean isActive;
}
