package com.revv.uct.supply.db.subPartIssues.dao.impl;

import com.revv.uct.supply.constants.APIConstants;
import com.revv.uct.supply.db.subPartIssues.dao.ISubPartIssuesDao;
import com.revv.uct.supply.db.subPartIssues.entity.SubPartIssues;
import com.revv.uct.supply.db.subPartIssues.repository.SubPartIssuesRRepository;
import com.revv.uct.supply.db.subPartIssues.repository.SubPartIssuesRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SubPartIssuesDaoImpl implements ISubPartIssuesDao {
    private final SubPartIssuesRRepository subPartIssuesRRepository;
    private final SubPartIssuesRWRepository subPartIssuesRWRepository;

    @Autowired
    public SubPartIssuesDaoImpl(SubPartIssuesRRepository subPartIssuesRRepository, SubPartIssuesRWRepository subPartIssuesRWRepository) {
        this.subPartIssuesRRepository = subPartIssuesRRepository;
        this.subPartIssuesRWRepository = subPartIssuesRWRepository;
    }

    @Override
    public SubPartIssues uploadSubPartIssue(SubPartIssues subPartIssue) {
        SubPartIssues savedSubPartIssue = subPartIssuesRWRepository.save(subPartIssue);
        return savedSubPartIssue;
    }

    @Override
    public boolean existsBySubPartIDAndIssueName(Long id, String issueName) {
        if(subPartIssuesRRepository.existsBySubPartIDAndIssueName(id, issueName)) {
            return true;
        }
        return false;
    }

    @Override
    public SubPartIssues findBySubPartIDAndIssueNameAndIsActive(Long id, String issueName, boolean b) {
        List<SubPartIssues> list = subPartIssuesRRepository.findBySubPartIDAndIssueNameAndIsActive(id, issueName, b);
        if(list.size() > 0) {
            SubPartIssues subPartIssue = list.get(0);
            return subPartIssue;
        }
        return null;
    }

    @Override
    public List<SubPartIssues> getDefaultIssues() {
        return subPartIssuesRRepository.findAllByIdIn(APIConstants.DEFAULT_ISSUES_ID);
    }
}
