package com.revv.uct.supply.db.workshop.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.workshop.entity.Workshop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface WorkshopRRepository extends JpaRepository<Workshop, Long> {

    @Query("select w from Workshop w where w.isActive=true")
    List<Workshop> getActiveWorkshops();

    List<Workshop> findAllByIdInAndIsActive(List<Long> workshopIDs, Boolean isActive);
}
