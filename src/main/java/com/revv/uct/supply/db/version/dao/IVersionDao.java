package com.revv.uct.supply.db.version.dao;

import com.revv.uct.supply.db.version.entity.Version;

public interface IVersionDao {
    Version getCurrentVersion(String versionType);

    void disableCurrentVersion(String versionType, int version);

    Version addNewVersion(Version version);
}
