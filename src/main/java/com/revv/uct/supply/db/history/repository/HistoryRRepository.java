package com.revv.uct.supply.db.history.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.enums.EditEntity;
import com.revv.uct.supply.db.history.entity.History;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface HistoryRRepository extends JpaRepository<History, Long> {
    List<History> findAllByEditEntityIDAndEditEntity(Long entityID, EditEntity entity);
}
