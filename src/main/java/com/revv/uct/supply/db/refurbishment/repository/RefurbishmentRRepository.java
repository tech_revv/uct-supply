package com.revv.uct.supply.db.refurbishment.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.refurbishment.entity.Refurbishment;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.InspectionDetail;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.RefurbList;
import com.revv.uct.supply.service.refurbishment.model.RefurbDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface RefurbishmentRRepository extends JpaRepository<Refurbishment, Long> {

    @Query("SELECT new com.revv.uct.supply.service.inspection.model.inspectionDetail.RefurbList" +
            "(r.id, r.status, r.startDate,r.lastDataSyncTime," +
            "w.workshopName, w.address," +
            "c.make, c.model, c.variant, c.fuel, c.transmission, c.startYear, c.endYear)" +
            "from Refurbishment r JOIN Workshop w ON (r.workshopID = w.id) " +
            "JOIN ProcuredCar p ON (r.procuredCarID = p.id) " +
            "JOIN Lead l ON (l.id = p.leadID) " +
            "JOIN Inspection ins ON (l.id = ins.leadID) AND ins.isActive = true AND ins.inspectionType = :inspectionType " +
            "JOIN CarModel c ON (c.id = p.carModelID) " +
            "where r.refurbSpocID =:refurbSpocID AND r.status in (:status) AND r.isActive = true")
    List<RefurbList> findRefurbishmentDetails(@Param("refurbSpocID") Long refurbSpocID, @Param("status") List<InspectionStatus> status, @Param("inspectionType") InspectionType inspectionType);


    @Query("SELECT new com.revv.uct.supply.service.refurbishment.model.RefurbDetail(" +
            "ins.id, ins.lastDataSyncTime, ins.inspectionType," +
            "r.id, r.status, r.procuredCarID," +
            "c.make, c.model, c.variant," +
            "l.registrationNumber)" +
            "from Refurbishment r JOIN ProcuredCar p ON (r.procuredCarID = p.id)" +
            "JOIN Lead l ON (l.id = p.leadID) " +
            "JOIN Inspection ins ON (l.id = ins.leadID) AND ins.isActive = true AND ins.inspectionType = :inspectionType " +
            "JOIN CarModel c ON (c.id = p.carModelID)" +
            "where r.id = :refurbID AND r.isActive = true")
    List<RefurbDetail> getRefurbDetails(@Param("refurbID") Long refurbID, @Param("inspectionType") InspectionType inspectionType);

    Refurbishment findByIdAndIsActive(@Param("id") Long id, boolean isActive);

    List<Refurbishment> findALlByProcuredCarIDInAndStatusInAndIsActive(List<Long> carIDs, List<InspectionStatus> status, Boolean isActive);
}
