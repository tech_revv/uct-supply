package com.revv.uct.supply.db.enums;

public enum AccessoryType {
    UNIVERSAL, MODEL
}
