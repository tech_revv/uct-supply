package com.revv.uct.supply.db.refurbishment.repository;

import com.revv.uct.supply.db.refurbishment.entity.Refurbishment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RefurbishmentRWRepository extends JpaRepository<Refurbishment, Long> {

}


