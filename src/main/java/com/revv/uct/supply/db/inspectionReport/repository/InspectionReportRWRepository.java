package com.revv.uct.supply.db.inspectionReport.repository;

import com.revv.uct.supply.db.inspectionReport.entity.InspectionReport;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface InspectionReportRWRepository extends JpaRepository<InspectionReport, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE InspectionReport i SET i.updatedBy = :inspectorID, i.isActive = false where i.inspectionID = :inspectionID")
    void markReportsInactive(@Param("inspectorID") Long inspectorID, @Param("inspectionID") Long inspectionID);
}
