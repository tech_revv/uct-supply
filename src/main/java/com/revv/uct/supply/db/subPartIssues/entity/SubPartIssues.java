package com.revv.uct.supply.db.subPartIssues.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "subpartissues")
@AllArgsConstructor
@NoArgsConstructor
public class SubPartIssues {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "subPartID")
    private Long subPartID;

    @Column(name = "issueName")
    private String issueName;

    @Column(name = "jobName")
    private String jobName;

    @Column(name = "versionNumber")
    private int versionNumber;

    @Column(name = "isActive")
    private boolean isActive;

    //@OneToOne(mappedBy = "subPartIssue", fetch = FetchType.EAGER)
    //private IssueFollowUp issueFollowUp;
}
