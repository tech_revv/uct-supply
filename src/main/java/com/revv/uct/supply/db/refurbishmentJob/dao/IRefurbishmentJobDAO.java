package com.revv.uct.supply.db.refurbishmentJob.dao;

import com.revv.uct.supply.db.refurbishmentJob.entity.RefurbishmentJob;

import java.util.List;

public interface IRefurbishmentJobDAO {

    List<RefurbishmentJob> getRefurbJobList(Long refurbID);

    void saveAll(List<RefurbishmentJob> refurbishmentJobs);

    List<RefurbishmentJob> getRefurbJobList(List<Long> refurbIDs);

    List<RefurbishmentJob> getCompletedRefurbJobListByCarID(Long carID);

}
