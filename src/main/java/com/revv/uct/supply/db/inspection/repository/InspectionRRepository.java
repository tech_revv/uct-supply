package com.revv.uct.supply.db.inspection.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.InspectionDetail;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.InspectionDetailNormal;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.PostRefurbList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface InspectionRRepository extends JpaRepository<Inspection, Long> {
    @Query("SELECT new com.revv.uct.supply.service.inspection.model.inspectionDetail.InspectionDetailNormal" +
            "(i.id, i.inspectionType, i.inspectionTime, i.status, i.lastDataSyncTime," +
            "s.id, s.sellerType, s.contactNumber,s.name,s.address,s.lat,s.lng, " +
            "c.make, c.model, c.variant, c.fuel, c.transmission, c.startYear, c.endYear, " +
            "l.registrationNumber, l.id, l.rto, l.registrationTimestamp, l.insuranceValidity, l.comments, l.kmsDriven, l.insuranceType, l.leadStatus,l.serviceCityID, l.manufacturingTimestamp, l.registrationType,l.ownershipSerial,l.carColour," +
            "u.userName) " +
            "from Inspection i JOIN Lead l ON (l.id = i.leadID) " +
            "JOIN Seller s ON (s.id = l.sellerID) JOIN CarModel c ON (c.id = l.carModelID) " +
            "JOIN User u ON (u.id = l.adminID)" +
            "where i.inspectorID =:inspectorID AND i.status in (:status) AND i.inspectionType in (:inspectionTypes) AND i.isActive = true")
    List<InspectionDetailNormal> findByInspectorIDAndStatusAndInspectionType(@Param("inspectorID") Long inspectorID, @Param("status") List<InspectionStatus> status, @Param("inspectionTypes") List<InspectionType> inspectionTypes);

    List<Inspection> findByLeadIDAndInspectionType(Long leadID, InspectionType inspectionType);

    Inspection findByIdAndIsActive(Long id, boolean b);

    @Query("SELECT new com.revv.uct.supply.service.inspection.model.inspectionDetail.PostRefurbList" +
            "(i.id, i.inspectionType, i.inspectionTime, i.status, i.lastDataSyncTime," +
            "c.make, c.model, c.variant, c.fuel, c.transmission, c.startYear, c.endYear," +
            "l.id, l.registrationNumber, l.registrationTimestamp, l.kmsDriven," +
            "car.id," +
            "e3User.userName, e3.id)" +
            "FROM Inspection i JOIN Lead l on (l.id = i.leadID )" +
            "JOIN ProcuredCar car on (car.leadID = l.id)" +
            "JOIN Inspection e3 on (e3.leadID = l.id)" +
            "JOIN User u ON (u.id = i.inspectorID)" +
            "JOIN User e3User on (e3User.id = e3.inspectorID)" +
            "JOIN CarModel c on (c.id = car.carModelID)" +
            "WHERE e3.inspectionType = 'E3' AND e3.isActive = true AND i.inspectionType in (:inspectionTypes) AND i.status in (:status)" +
            "AND i.inspectorID =:inspectorID AND i.isActive = true AND car.isActive = true")
    List<PostRefurbList> findByInspectorIDAndStatusAndInspectionTypePostRefurbishment(@Param("inspectorID") Long inspectorID, @Param("status") List<InspectionStatus> statusList, @Param("inspectionTypes") List<InspectionType> inspectionTypes);


    @Query("SELECT i FROM Inspection i where i.inspectionType=:inspection_type and i.leadID in (select pc.leadID from ProcuredCar pc where pc.id=:car_id) and i.isActive=true")
    Inspection findByInspectionTypeAndCarID(@Param("inspection_type")InspectionType inspectionType,@Param("car_id")Long carID);


}
