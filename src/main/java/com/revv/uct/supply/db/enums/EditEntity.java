package com.revv.uct.supply.db.enums;

public enum EditEntity {
    LEAD, SELLER, INSPECTION, OFFER, PROCURED_CAR, REFURBISHMENT, INSPECTION_CHECKLIST
}
