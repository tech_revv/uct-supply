package com.revv.uct.supply.db.enums;

public enum LeadStatus {
    CREATED,
    APPROVED,
    REJECTED,
    E1_SCHEDULED,
    E1_PASS,
    E2_SCHEDULED,
    E2_COMPLETED,
    E2_APPROVED,
    E3_SCHEDULED,
    E3_COMPLETED,
    OFFER_GENERATED,
    OFFER_ACCEPTED,
    OFFER_APPROVED,
    TOKEN_PAID,
    CAR_PROCURED,
    ON_BOARDED
}