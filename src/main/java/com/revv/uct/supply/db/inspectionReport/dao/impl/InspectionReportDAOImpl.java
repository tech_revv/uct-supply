package com.revv.uct.supply.db.inspectionReport.dao.impl;


import com.revv.uct.supply.db.inspectionReport.dao.IInspectionReportDAO;
import com.revv.uct.supply.db.inspectionReport.entity.InspectionReport;
import com.revv.uct.supply.db.inspectionReport.repository.InspectionReportRRepository;
import com.revv.uct.supply.db.inspectionReport.repository.InspectionReportRWRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;


@Component
@AllArgsConstructor
@NoArgsConstructor
public class InspectionReportDAOImpl implements IInspectionReportDAO {

    @Autowired
    InspectionReportRRepository inspectionReportRRepository;
    @Autowired
    InspectionReportRWRepository inspectionReportRWRepository;

    @Override
    public List<InspectionReport> getInspectionReport(Long inspectionID) {
        return inspectionReportRRepository.findAllByInspectionIDAndIsActive(inspectionID, true);
    }

    @Override
    public void markReportsInactive(Long inspectorID, Long inspectionID) {
        inspectionReportRWRepository.markReportsInactive(inspectorID, inspectionID);
    }

    @Override
    @Transactional
    public void saveInspectionReport(List<InspectionReport> inspectionReports) {
        inspectionReportRWRepository.saveAll(inspectionReports);
//        inspectionReports.forEach(inspectionReport -> inspectionReportRWRepository.saveAll(inspectionReport));
    }

    @Override
    public List<InspectionReport> getInspectionReportByInspectionIDs(List<Long> inspectionIDs,Boolean isActive) {
        return inspectionReportRRepository.findAllByInspectionIDInAndIsActive(inspectionIDs,isActive);
    }
}
