package com.revv.uct.supply.db;


import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource({"classpath:application.properties"})
@EnableJpaRepositories(
        basePackages = "com.revv.uct.supply.db",
        includeFilters = @ComponentScan.Filter(ReadOnlyRepository.class),
        entityManagerFactoryRef = "uctSupplySlaveEntityManager",
        transactionManagerRef = "uctSupplySlaveTransactionManager"

)
@EnableTransactionManagement
public class UctSlaveConfig {

    @Autowired
    public UctSlaveConfig() {
        super();
    }

    @Bean("uctSupplySlaveDataSource")
    @ConfigurationProperties(prefix = "spring.uct-slave-datasource")
    public DataSource uctSupplySlaveDataSource(){
        return DataSourceBuilder.create().build();
    }


    @Bean
    public LocalContainerEntityManagerFactoryBean uctSupplySlaveEntityManager(@Qualifier("uctSupplySlaveDataSource") final DataSource uctSupplySlaveDataSource){
        HibernateJpaVendorAdapter vendorAdapter =new HibernateJpaVendorAdapter();
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(uctSupplySlaveDataSource);
        em.setPackagesToScan("com.revv.uct.supply.db");
        em.setJpaVendorAdapter(vendorAdapter);
        Map<String, String> properties = new HashMap<>();
        //properties.put("hibernate.implicit_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy");
        //properties.put("hibernate.physical_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public PlatformTransactionManager uctSupplySlaveTransactionManager(@Qualifier("uctSupplySlaveEntityManager") final LocalContainerEntityManagerFactoryBean uctSupplyMasterEntityManager){
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(uctSupplyMasterEntityManager.getObject());
        return transactionManager;
    }
}
