package com.revv.uct.supply.db.inspection.repository;

import com.revv.uct.supply.db.inspection.entity.Inspection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface InspectionRWRepository extends JpaRepository<Inspection, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE Inspection i SET i.lastDataSyncTime = :lastDataSyncTime where i.id = :inspectionID")
    void updateLastDataSyncTime(Long lastDataSyncTime, Long inspectionID);
}
