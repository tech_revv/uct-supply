package com.revv.uct.supply.db.documentStageMaster.dao;

import com.revv.uct.supply.db.documentStageMaster.entity.DocumentStageMaster;
import com.revv.uct.supply.db.enums.DocumentStage;
import com.revv.uct.supply.db.enums.DocumentType;

import java.util.List;

public interface IDocumentStageMasterDAO {

    List<DocumentStageMaster> findByDocumentTypeAndDocumentStageAndIsActive(DocumentType serviceHistory, DocumentStage offerCreation);

    List<DocumentStageMaster> findByStageName(DocumentStage documentStage);

    List<DocumentStageMaster> findByID(List<Long> documentIDs);
}
