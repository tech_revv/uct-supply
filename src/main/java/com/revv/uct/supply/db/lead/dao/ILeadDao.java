package com.revv.uct.supply.db.lead.dao;

import com.revv.uct.supply.db.lead.entity.Lead;

public interface ILeadDao
{
    Lead createLead(Lead lead);

    boolean existsByRegistrationNumber(String registrationNumber);

    Lead findByIdAndIsActive(Long id, boolean b);

    Lead updateLead(Lead newData);

    boolean existByIdAndIsActive(Long leadID, boolean b);

    Long getAllLeadsCount();
}
