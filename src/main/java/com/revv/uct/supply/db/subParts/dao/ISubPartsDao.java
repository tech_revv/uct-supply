package com.revv.uct.supply.db.subParts.dao;

import com.revv.uct.supply.db.subParts.entity.SubParts;

import java.util.List;

public interface ISubPartsDao {
    SubParts uploadSubPart(SubParts subPart);

    boolean existsBySubPartNameAndIsActive(String subPartName, boolean isActive);

    SubParts findBySubPartNameAndIsActive(String subPartName, boolean b);

    List<SubParts> findAllByIsActive(boolean b);

    List<SubParts> findAllByID(List<Long> subPartIds);
}
