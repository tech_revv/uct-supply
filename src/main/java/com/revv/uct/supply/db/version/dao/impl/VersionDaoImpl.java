package com.revv.uct.supply.db.version.dao.impl;

import com.revv.uct.supply.db.version.dao.IVersionDao;
import com.revv.uct.supply.db.version.entity.Version;
import com.revv.uct.supply.db.version.repository.VersionRRepository;
import com.revv.uct.supply.db.version.repository.VersionRWRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class VersionDaoImpl implements IVersionDao {
    private final VersionRRepository versionRRepository;
    private final VersionRWRepository versionRWRepository;

    @Autowired
    public VersionDaoImpl(VersionRRepository versionRRepository, VersionRWRepository versionRWRepository) {
        this.versionRRepository = versionRRepository;
        this.versionRWRepository = versionRWRepository;
    }

    @Override
    public Version getCurrentVersion(String versionType) {
        Version version = versionRRepository.findByVersionTypeAndEnabled(versionType, true);
        return version;
    }

    @Override
    public void disableCurrentVersion(String versionType, int version) {
        versionRWRepository.disableCurrentVersion(versionType, version);
    }

    @Override
    public Version addNewVersion(Version version) {
        Version addedVersion = versionRWRepository.save(version);
        log.info("Adding version==>>"+addedVersion.toString());
        return addedVersion;
    }
}
