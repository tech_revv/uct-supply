package com.revv.uct.supply.db.refurbishment.dao.impl;

import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.refurbishment.dao.IRefurbishmentDAO;
import com.revv.uct.supply.db.refurbishment.entity.Refurbishment;
import com.revv.uct.supply.db.refurbishment.repository.RefurbishmentRRepository;
import com.revv.uct.supply.db.refurbishment.repository.RefurbishmentRWRepository;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.InspectionDetail;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.RefurbList;
import com.revv.uct.supply.service.refurbishment.model.RefurbDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RefurbishmentDaoImpl implements IRefurbishmentDAO {

    @Autowired
    RefurbishmentRRepository refurbishmentRRepository;

    @Autowired
    RefurbishmentRWRepository refurbishmentRWRepository;

    @Override
    public List<RefurbList> findRefurbishments(Long refurbSpocID, List<InspectionStatus> statusList) {
        return refurbishmentRRepository.findRefurbishmentDetails(refurbSpocID, statusList, InspectionType.E3);
    }

    @Override
    public RefurbDetail getRefurbDetails(Long refurbID) {
        List<RefurbDetail> refurbDetails = refurbishmentRRepository.getRefurbDetails(refurbID, InspectionType.E3);
        if(refurbDetails.size() > 0) {
            return refurbDetails.get(0);
        }
        return null;
    }

    @Override
    public Refurbishment findByIdAndIsActive(Long refurbID, boolean isActive) {
        return refurbishmentRRepository.findByIdAndIsActive(refurbID, isActive);
    }

    @Override
    public Refurbishment updateRefurbishment(Refurbishment data) {
        return refurbishmentRWRepository.save(data);
    }

    @Override
    public List<Refurbishment> findRefurbishmentsByCarIDAndStatus(List<Long> carIDs, List<InspectionStatus> status, Boolean isActive) {
        return refurbishmentRRepository.findALlByProcuredCarIDInAndStatusInAndIsActive(carIDs, status, isActive);
    }
}
