package com.revv.uct.supply.db.issueFollowUp.repository;

import com.revv.uct.supply.db.issueFollowUp.entity.IssueFollowUp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueFollowUpRWRepository extends JpaRepository<IssueFollowUp, Long> {
}
