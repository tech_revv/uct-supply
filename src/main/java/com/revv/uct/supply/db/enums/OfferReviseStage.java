package com.revv.uct.supply.db.enums;

public enum OfferReviseStage {
    POST_E1,
    POST_E2,
    OFFER_GENERATED
}
