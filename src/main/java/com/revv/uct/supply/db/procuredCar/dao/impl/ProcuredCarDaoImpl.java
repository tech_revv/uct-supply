package com.revv.uct.supply.db.procuredCar.dao.impl;

import com.revv.uct.supply.db.enums.ProcuredCarStatus;
import com.revv.uct.supply.db.procuredCar.dao.IProcuredCarDAO;
import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;
import com.revv.uct.supply.db.procuredCar.repository.ProcuredCarRRepository;
import com.revv.uct.supply.db.procuredCar.repository.ProcuredCarRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProcuredCarDaoImpl implements IProcuredCarDAO {
    private final ProcuredCarRRepository procuredCarRRepository;
    private final ProcuredCarRWRepository procuredCarRWRepository;

    @Autowired
    public ProcuredCarDaoImpl(ProcuredCarRRepository procuredCarRRepository, ProcuredCarRWRepository procuredCarRWRepository) {
        this.procuredCarRRepository = procuredCarRRepository;
        this.procuredCarRWRepository = procuredCarRWRepository;
    }

    @Override
    public ProcuredCar saveProcuredCarDetails(ProcuredCar procuredCar) {
        return procuredCarRWRepository.save(procuredCar);
    }

    @Override
    public ProcuredCar findByIdAndIsActive(Long id, boolean b) {
        return procuredCarRRepository.findByIdAndIsActive(id, b);
    }

    @Override
    public ProcuredCar updateProcuredCar(ProcuredCar newData) {
        return procuredCarRWRepository.save(newData);
    }

    @Override
    public ProcuredCar findByLeadIDAndIsActive(Long leadID, boolean isActive) {
        List<ProcuredCar> procuredCarList = procuredCarRRepository.findByLeadIDAndIsActive(leadID, isActive);
        if(procuredCarList.size() > 0) {
            return procuredCarList.get(0);
        }
        return null;
    }
}
