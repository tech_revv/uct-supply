package com.revv.uct.supply.db.issueFollowUp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "issuefollowup")
@AllArgsConstructor
@NoArgsConstructor
public class IssueFollowUp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "subPartIssueID")
    private Long subPartIssueID;

    @Column(name = "jobName")
    private String jobName;

    @Column(name = "jobID")
    private Long jobID;

    @Column(name = "versionNumber")
    private int versionNumber;

    @Column(name = "isActive")
    private boolean isActive;
}
