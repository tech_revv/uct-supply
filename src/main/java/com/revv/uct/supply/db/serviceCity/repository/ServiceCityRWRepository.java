package com.revv.uct.supply.db.serviceCity.repository;


import com.revv.uct.supply.db.annotation.ReadOnlyRepository;

import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceCityRWRepository extends JpaRepository<ServiceCity,Long>
{

}
