package com.revv.uct.supply.db.enums;

public enum IssueStage {
    CAR_ONBOARDED,
    E3_CONDUCTED
}
