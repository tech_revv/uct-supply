package com.revv.uct.supply.db.inspectionJob.repository;

import com.revv.uct.supply.db.inspectionJob.entity.InspectionJob;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface InspectionJobRWRepository extends JpaRepository<InspectionJob, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE InspectionJob i SET i.updatedBy = :inspectorID, i.isActive = false where i.inspectionID = :inspectionID")
    void markJobsInactive(@Param("inspectorID") Long inspectorID, @Param("inspectionID") Long inspectionID);
}
