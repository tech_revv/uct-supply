package com.revv.uct.supply.db.lead.repository;


import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.lead.entity.Lead;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@ReadOnlyRepository
public interface LeadRRepository extends JpaRepository<Lead, Long> {


    boolean existsByRegistrationNumber(String registrationNumber);

    boolean existsByIdAndIsActive(Long leadID, boolean b);

    Lead findByIdAndIsActive(Long id, boolean b);
}
