package com.revv.uct.supply.db.inspectionSubPart.dao;

import com.revv.uct.supply.db.inspectionSubPart.entity.InspectionSubPart;
import com.revv.uct.supply.service.inspection.model.InspectionSubpartDetail;

import java.util.List;

public interface IInspectionSubPartDAO {
    List<InspectionSubpartDetail> getInspectionSubpartDetails(Long inspectionID);

    List<InspectionSubPart> findAllBySubPartIdsAndIsActiveAndInspectionID(List<Long> subPartIDs, Long inspectionID);

    List<InspectionSubPart> updateInspectionSubParts(List<InspectionSubPart> toBeUpdated);

    List<InspectionSubPart> insertInspectionSubParts(List<InspectionSubPart> newData);
}
