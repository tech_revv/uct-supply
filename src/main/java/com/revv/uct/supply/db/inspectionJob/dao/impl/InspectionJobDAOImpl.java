package com.revv.uct.supply.db.inspectionJob.dao.impl;


import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspectionJob.dao.IInspectionJobDAO;
import com.revv.uct.supply.db.inspectionJob.entity.InspectionJob;
import com.revv.uct.supply.db.inspectionJob.repository.InspectionJobRRepository;
import com.revv.uct.supply.db.inspectionJob.repository.InspectionJobRWRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@AllArgsConstructor
@NoArgsConstructor
public class InspectionJobDAOImpl implements IInspectionJobDAO {

    @Autowired
    InspectionJobRRepository inspectionJobRRepository;
    @Autowired
    InspectionJobRWRepository inspectionJobRWRepository;

    @Override
    public List<InspectionJob> getInspectionJobs(Long inspectionID) {
        return inspectionJobRRepository.findAllByInspectionIDAndIsActive(inspectionID, true);
    }

    @Override
    public void markJobsInactive(Long inspectorID, Long inspectionID) {
        inspectionJobRWRepository.markJobsInactive(inspectorID, inspectionID);
    }

    @Override
    public void saveInspectionJob(List<InspectionJob> inspectionJobs) {
        inspectionJobRWRepository.saveAll(inspectionJobs);
    }

    @Override
    public List<InspectionJob> getInspectionJobs(List<Long> inspectionIDs) {
        return inspectionJobRRepository.findAllByInspectionIDInAndIsActive(inspectionIDs, true);
    }

    public List<InspectionJob> getInspectionJobsByCarIDAndType(Long carID, InspectionType inspectionType) {
        return inspectionJobRRepository.getInspectionJobsByCarIDAndInspectionType(carID,inspectionType);
    }
}
