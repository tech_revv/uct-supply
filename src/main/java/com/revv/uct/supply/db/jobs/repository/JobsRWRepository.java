package com.revv.uct.supply.db.jobs.repository;

import com.revv.uct.supply.db.jobs.entity.Jobs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobsRWRepository extends JpaRepository<Jobs, Long> {
}
