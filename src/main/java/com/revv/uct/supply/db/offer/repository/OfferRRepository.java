package com.revv.uct.supply.db.offer.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.offer.entity.Offer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface OfferRRepository extends JpaRepository<Offer, Long> {
    List<Offer> findByLeadIDAndIsActive(Long leadID, boolean b);

    Offer findByIdAndIsActive(Long id, boolean b);
}
