package com.revv.uct.supply.db.offerImage.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.offerImage.entity.OfferImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@ReadOnlyRepository
public interface OfferImageRRepository extends JpaRepository<OfferImage, Long> {
}
