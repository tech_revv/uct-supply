package com.revv.uct.supply.db.inspectionReport.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.inspectionReport.entity.InspectionReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@ReadOnlyRepository
public interface InspectionReportRRepository extends JpaRepository<InspectionReport, Long> {

    List<InspectionReport> findAllByInspectionIDAndIsActive(Long inspectionID, Boolean isActive);

    List<InspectionReport> findAllByInspectionIDInAndIsActive(List<Long> inspectionIDs, Boolean isActive);

}
