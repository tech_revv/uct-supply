package com.revv.uct.supply.db.subParts.dao.impl;

import com.revv.uct.supply.db.subParts.dao.ISubPartsDao;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.db.subParts.repository.SubPartsRRepository;
import com.revv.uct.supply.db.subParts.repository.SubPartsRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SubPartsDaoImpl implements ISubPartsDao {
    private final SubPartsRRepository subPartsRRepository;
    private final SubPartsRWRepository subPartsRWRepository;

    @Autowired
    public SubPartsDaoImpl(SubPartsRRepository subPartsRRepository, SubPartsRWRepository subPartsRWRepository) {
        this.subPartsRRepository = subPartsRRepository;
        this.subPartsRWRepository = subPartsRWRepository;
    }

    @Override
    public SubParts uploadSubPart(SubParts subPart) {
        SubParts savedSubPart = subPartsRWRepository.save(subPart);
        return savedSubPart;
    }

    @Override
    public boolean existsBySubPartNameAndIsActive(String subPartName, boolean isActive) {
        if(subPartsRRepository.existsBySubPartNameAndIsActive(subPartName, isActive)) {
            return true;
        }
        return false;
    }

    @Override
    public SubParts findBySubPartNameAndIsActive(String subPartName, boolean b) {
        SubParts subPart = subPartsRRepository.findBySubPartNameAndIsActive(subPartName, b);
        return subPart;
    }

    @Override
    public List<SubParts> findAllByIsActive(boolean b) {
        return subPartsRRepository.findAllByIsActive(b);
    }

    @Override
    public List<SubParts> findAllByID(List<Long> subPartIds) {
        return subPartsRRepository.findAllById(subPartIds);
    }
}
