package com.revv.uct.supply.db.commonDocument.entity;

import com.revv.uct.supply.db.enums.CommonDocumentEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "commonDocuments")
public class CommonDocument {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "entityID")
    private Long entityID;

    @Column(name = "commonDocumentEntity")
    @Enumerated(EnumType.STRING)
    private CommonDocumentEntity commonDocumentEntity;

    @Column(name = "documentStageMasterID")
    private Long documentStageMasterID;

    @Column(name = "imageURL")
    private String imageURL;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "updatedBy")
    private Long updatedBy;

    @Column(name = "createdAt")
    @CreatedDate
    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();

    @LastModifiedDate
    @Column(name = "updatedAt")
    @Builder.Default
    private LocalDateTime updatedAt = LocalDateTime.now();

    @PreUpdate
    protected void preUpdate() {
        this.updatedAt = LocalDateTime.now();
    }

    @Column(name = "isActive")
    @Builder.Default
    private boolean isActive = true;
}