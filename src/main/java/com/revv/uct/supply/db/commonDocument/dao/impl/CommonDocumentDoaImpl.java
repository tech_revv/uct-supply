package com.revv.uct.supply.db.commonDocument.dao.impl;

import com.revv.uct.supply.db.commonDocument.dao.ICommonDocumentDao;
import com.revv.uct.supply.db.commonDocument.entity.CommonDocument;
import com.revv.uct.supply.db.commonDocument.repository.CommonDocumentRRepository;
import com.revv.uct.supply.db.commonDocument.repository.CommonDocumentRWRepository;
import com.revv.uct.supply.db.enums.CommonDocumentEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class CommonDocumentDoaImpl implements ICommonDocumentDao {
    private final CommonDocumentRRepository commonDocumentRRepository;
    private final CommonDocumentRWRepository commonDocumentRWRepository;

    @Autowired
    public CommonDocumentDoaImpl(CommonDocumentRRepository commonDocumentRRepository, CommonDocumentRWRepository commonDocumentRWRepository) {
        this.commonDocumentRRepository = commonDocumentRRepository;
        this.commonDocumentRWRepository = commonDocumentRWRepository;
    }

    @Override
    public void save(CommonDocument commonDocument) {
        commonDocumentRWRepository.save(commonDocument);
    }

    @Override
    public void markDocumentsInactive(CommonDocumentEntity entityName, Long entityID, Long adminID, List<Long> documentIDs) {
        log.info("documentIDs "+documentIDs);
        log.info("entityName "+entityName);
        log.info("entityID "+entityID);
        log.info("adminID "+adminID);
        commonDocumentRWRepository.markDocumentsInactive(entityName, entityID, adminID, documentIDs);
    }

    @Override
    public void saveAll(List<CommonDocument> commonDocuments) {
        commonDocumentRWRepository.saveAll(commonDocuments);
    }

    @Override
    public List<CommonDocument> findAllByEntityIDAndIsActive(Long existingOfferID, boolean b) {
        return commonDocumentRRepository.findAllByEntityIDAndIsActive(existingOfferID,b);
    }

    @Override
    public List<CommonDocument> getDocumentsForEntityIDAndNameAndIsActive(Long entityID, CommonDocumentEntity entityName, Boolean isActive) {
        return commonDocumentRRepository.findAllByEntityIDAndCommonDocumentEntityAndIsActive(entityID, entityName, isActive);
    }

    @Override
    public List<CommonDocument> getDocumentsForEntityIDAndNameAndStage(Long entityID, CommonDocumentEntity entityName, List<Long> documentStageMasterIDs, Boolean isActive) {
        return commonDocumentRRepository.findAllByEntityIDAndCommonDocumentEntityAndDocumentStageMasterIDInAndIsActive(entityID, entityName, documentStageMasterIDs, isActive);
    }

    @Override
    public void markDocumentsInactiveAgainstDocumentStageMaster(CommonDocumentEntity entityName, Long entityID, Long adminID, List<Long> documentStageMasterIDs) {
        commonDocumentRWRepository.markDocumentsInactiveAgainstDocumentStageMaster(entityName, entityID, adminID, documentStageMasterIDs);
    }

    @Override
    public List<CommonDocument> findAllByEntityIDAndDocumentStageMasterIDAndCommonDocumentEntityAndIsActive(Long entityID, Long dsmID, CommonDocumentEntity entity, boolean b) {
        return commonDocumentRRepository.findAllByEntityIDAndDocumentStageMasterIDAndCommonDocumentEntityAndIsActive(entityID, dsmID, entity, b);
    }
}
