package com.revv.uct.supply.db.seller.dao;

import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.seller.entity.Seller;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ISellerDao
{
    Seller createSeller(@NotNull Seller seller);

    List<Object[]> getSellersForLead(Long serviceCityID, SellerTypeEnum sellerType);

    List<Seller> findAllByNameAndServiceCity(Long serviceCityID, String name);

    Seller findByIdAndIsActive(Long id, boolean b);

    Seller updateSeller(Seller newData);

    List<Seller> findAllByContactNumberAndSellerTypeAndIsActive(String sellerContactNo, SellerTypeEnum b2c, boolean b);
}
