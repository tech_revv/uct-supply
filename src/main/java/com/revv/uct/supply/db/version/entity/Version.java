package com.revv.uct.supply.db.version.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "versions")
@AllArgsConstructor
@NoArgsConstructor
public class Version {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "versionType")
    private String versionType;

    @Column(name = "versionNumber")
    private int versionNumber;

    @Column(name = "enabled", columnDefinition = "boolean default true")
    private boolean enabled;

    public Version(String versionType, int newVersion, boolean enabled) {
        this.versionType = versionType;
        this.versionNumber = newVersion;
        this.enabled = enabled;
    }
}
