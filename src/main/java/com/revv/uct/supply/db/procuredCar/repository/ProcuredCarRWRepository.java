package com.revv.uct.supply.db.procuredCar.repository;

import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcuredCarRWRepository extends JpaRepository<ProcuredCar,Long> {
}
