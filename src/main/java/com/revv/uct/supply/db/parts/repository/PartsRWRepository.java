package com.revv.uct.supply.db.parts.repository;

import com.revv.uct.supply.db.parts.entity.Parts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartsRWRepository extends JpaRepository<Parts, Long> {

}
