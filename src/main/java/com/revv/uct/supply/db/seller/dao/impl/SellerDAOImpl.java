package com.revv.uct.supply.db.seller.dao.impl;

import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.seller.dao.ISellerDao;
import com.revv.uct.supply.db.seller.entity.Seller;
import com.revv.uct.supply.db.seller.repository.SellerRRepository;
import com.revv.uct.supply.db.seller.repository.SellerRWRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@Component
public class SellerDAOImpl implements ISellerDao
{
    private final SellerRRepository sellerRRepository;
    private final SellerRWRepository sellerRWepository;

    @Autowired
    public SellerDAOImpl(SellerRRepository sellerRRepository, SellerRWRepository sellerRWRepository, SellerRWRepository sellerRWepository) {
        this.sellerRRepository = sellerRRepository;
        this.sellerRWepository = sellerRWepository;
    }


    @Override
    public Seller createSeller(@NotNull Seller seller) {
        log.info("create customer request ={}",seller);
        return sellerRWepository.save(seller);
    }

    @Override
    public List<Object[]> getSellersForLead(Long serviceCityID, SellerTypeEnum sellerType) {
        log.info("seller dao");
        return sellerRRepository.getSellersForLead(serviceCityID, sellerType);
    }

    @Override
    public List<Seller> findAllByNameAndServiceCity(Long serviceCityID, String name) {
        return sellerRRepository.findAllByServiceCityIDAndName(serviceCityID, name);
    }

    @Override
    public Seller findByIdAndIsActive(Long id, boolean b) {
        return sellerRRepository.findAllByIdAndIsActive(id, b);
    }

    @Override
    public Seller updateSeller(Seller newData) {
        return sellerRWepository.save(newData);
    }

    @Override
    public List<Seller> findAllByContactNumberAndSellerTypeAndIsActive(String sellerContactNo, SellerTypeEnum b2c, boolean b) {
        return sellerRRepository.findAllByContactNumberAndSellerTypeAndIsActive(sellerContactNo,b2c,b);
    }
}
