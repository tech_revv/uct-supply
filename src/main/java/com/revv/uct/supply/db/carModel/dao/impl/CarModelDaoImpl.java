package com.revv.uct.supply.db.carModel.dao.impl;

import com.revv.uct.supply.db.carModel.dao.ICarModelDao;
import com.revv.uct.supply.db.carModel.entity.CarModel;
import com.revv.uct.supply.db.carModel.repository.CarModelRRepository;
import com.revv.uct.supply.db.carModel.repository.CarModelRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarModelDaoImpl implements ICarModelDao {

    CarModelRRepository carModelRRepository;
    CarModelRWRepository carModelRWRepository;

    @Autowired
    public CarModelDaoImpl(CarModelRRepository carModelRRepository, CarModelRWRepository carModelRWRepository) {
        this.carModelRRepository = carModelRRepository;
        this.carModelRWRepository = carModelRWRepository;
    }

    @Override
    public CarModel uploadCarModel(CarModel carModel) {
        CarModel savedCarModel = carModelRWRepository.save(carModel);
        return savedCarModel;
    }

    @Override
    public List<CarModel> findAllCarModels() {
        List<CarModel> list = carModelRRepository.findAll();
        return list;
    }
}
