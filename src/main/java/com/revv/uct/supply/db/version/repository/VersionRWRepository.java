package com.revv.uct.supply.db.version.repository;

import com.revv.uct.supply.db.version.entity.Version;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VersionRWRepository extends JpaRepository<Version, Long> {
    @Modifying
    @Query("UPDATE Version SET enabled=false where versionType=:currentVersionType and versionNumber=:version")
    void disableCurrentVersion(String currentVersionType, int version);

    //@Modifying
    //@Query("update User u set u.firstname = ?1, u.lastname = ?2 where u.id = ?3")
}
