package com.revv.uct.supply.db.procuredCar.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface ProcuredCarRRepository extends JpaRepository<ProcuredCar,Long> {
    ProcuredCar findByIdAndIsActive(Long id, boolean b);

    List<ProcuredCar> findByLeadIDAndIsActive(Long leadID, boolean isActive);
}
