package com.revv.uct.supply.db.parts.entity;

import com.revv.uct.supply.db.enums.AccessoryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@Table(name = "parts")
@AllArgsConstructor
@NoArgsConstructor
public class Parts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "headerName")
    private String headerName;

    @Column(name = "headerID")
    private Long headerID;

    @Column(name = "partName")
    private String partName;

    @Column(name = "accessoryType")
    @Enumerated(EnumType.STRING)
    private AccessoryType accessoryType;

    @Column(name = "isAccessory")
    private boolean isAccessory;

    @Column(name = "versionNumber")
    private int versionNumber;

    @Column(name = "isActive")
    private boolean isActive;

    //@OneToMany(mappedBy = "part", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    //private List<SubParts> subParts;
}
