package com.revv.uct.supply.db.jobs.dao;

import com.revv.uct.supply.db.jobs.entity.Jobs;

import java.util.List;

public interface IJobsDao {
    Jobs uploadJob(Jobs job);

    boolean existsByJobNameAndIsActive(String jobName, boolean isActive);

    Jobs findByJobNameAndIsActive(String jobName, boolean b);

    List<Jobs> findByJobID(List<Long> v);

    List<Jobs> findAllByIdAndIsActive(List<Long> jobID);
}
