package com.revv.uct.supply.db.documentStageMaster.dao.impl;

import com.revv.uct.supply.db.documentStageMaster.dao.IDocumentStageMasterDAO;
import com.revv.uct.supply.db.documentStageMaster.entity.DocumentStageMaster;
import com.revv.uct.supply.db.documentStageMaster.repository.DocumentStageMasterRRepository;
import com.revv.uct.supply.db.documentStageMaster.repository.DocumentStageMasterRWRepository;
import com.revv.uct.supply.db.enums.DocumentStage;
import com.revv.uct.supply.db.enums.DocumentType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class DocumentStageMasterDAOImpl implements IDocumentStageMasterDAO {
    private final DocumentStageMasterRRepository documentStageMasterRRepository;
    private final DocumentStageMasterRWRepository documentStageMasterRWRepository ;

    @Autowired
    public DocumentStageMasterDAOImpl(DocumentStageMasterRRepository documentStageMasterRRepository, DocumentStageMasterRWRepository documentStageMasterRWRepository) {
        this.documentStageMasterRRepository = documentStageMasterRRepository;
        this.documentStageMasterRWRepository = documentStageMasterRWRepository;
    }

    @Override
    public List<DocumentStageMaster> findByDocumentTypeAndDocumentStageAndIsActive(DocumentType serviceHistory, DocumentStage offerCreation) {
        log.info("docType=={}", serviceHistory);
        log.info("docStage=={}", offerCreation);
        return documentStageMasterRRepository.findAllByDocumentTypeAndDocumentStageAndIsActive(serviceHistory, offerCreation);
    }

    @Override
    public List<DocumentStageMaster> findByStageName(DocumentStage documentStage) {
        return documentStageMasterRRepository.findAllByDocumentStageAndIsActive(documentStage, true);
    }

    @Override
    public List<DocumentStageMaster> findByID(List<Long> documentIDs) {
        return documentStageMasterRRepository.findAllById(documentIDs);
    }
}
