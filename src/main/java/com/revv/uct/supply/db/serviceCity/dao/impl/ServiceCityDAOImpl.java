package com.revv.uct.supply.db.serviceCity.dao.impl;

import com.revv.uct.supply.db.serviceCity.dao.IServiceCityDAO;
import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;
import com.revv.uct.supply.db.serviceCity.repository.ServiceCityRRepository;
import com.revv.uct.supply.db.serviceCity.repository.ServiceCityRWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Component
public class ServiceCityDAOImpl implements IServiceCityDAO {


    private final ServiceCityRRepository serviceCityRRepository;
    private final ServiceCityRWRepository serviceCityRWRepository;

    @Autowired
    public ServiceCityDAOImpl(ServiceCityRRepository serviceCityRRepository, ServiceCityRWRepository serviceCityRWRepository) {
        this.serviceCityRRepository = serviceCityRRepository;
        this.serviceCityRWRepository = serviceCityRWRepository;
    }

    @Override
    public ServiceCity createServiceCity(ServiceCity serviceCity) {
        return serviceCityRWRepository.save(serviceCity);
    }

    @Override
    public List<ServiceCity> findAllServiceCities() {
        return serviceCityRRepository.findAll();
    }

    @Override
    public ServiceCity findServiceCityById(@NotNull Long id) {
        Optional<ServiceCity> serviceCity= serviceCityRRepository.findById(id);
        if (serviceCity.isPresent())
            return serviceCity.get();
        else
            return  null;
    }

}
