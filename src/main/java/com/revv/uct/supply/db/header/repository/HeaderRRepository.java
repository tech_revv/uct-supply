package com.revv.uct.supply.db.header.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.header.entity.Header;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface HeaderRRepository extends JpaRepository<Header, Long> {
    List<Header> findAllByNameAndVersionNumber(String headerName, int newVersion);

    boolean existsByNameAndIsActive(String headerName, boolean isActive);

    boolean existsByNameAndVersionNumber(String headerName, int headerVersion);

    Header findByNameAndIsActive(String headerName, boolean isActive);
}
