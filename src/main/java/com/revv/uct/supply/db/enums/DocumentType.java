package com.revv.uct.supply.db.enums;

public enum DocumentType {
    CAR_PICKUP_DOCUMENT,
    SERVICE_HISTORY,
    LEAD_PAYMENT,
    JOB_CARD
}
