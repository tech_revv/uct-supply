package com.revv.uct.supply.db.parts.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.parts.entity.Parts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@ReadOnlyRepository
public interface PartsRRepository extends JpaRepository<Parts, Long> {
    boolean existsByPartNameAndIsActive(String partName, boolean isActive);

    Parts findByPartNameAndVersionNumber(String partName, int partVersion);

    Parts findByPartNameAndIsActive(String partName, boolean isActive);
}
