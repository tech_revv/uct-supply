package com.revv.uct.supply.db.carModel.repository;

import com.revv.uct.supply.db.carModel.entity.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarModelRWRepository extends JpaRepository<CarModel, Long> {
}
