package com.revv.uct.supply.db.enums;

public enum SellerTypeEnum {
    NCD, UCD, B2C, ONLINE_SELLER
}
