package com.revv.uct.supply.db.documentStageMaster.repository;

import com.revv.uct.supply.db.documentStageMaster.entity.DocumentStageMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentStageMasterRWRepository extends JpaRepository<DocumentStageMaster, Long> {

}
