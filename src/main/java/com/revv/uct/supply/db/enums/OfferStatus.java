package com.revv.uct.supply.db.enums;

public enum OfferStatus {
    CREATED,
    ACCEPTED,
    REJECTED,
    APPROVED,
    TOKEN_PAID,
    FULL_PAYMENT_DONE
}
