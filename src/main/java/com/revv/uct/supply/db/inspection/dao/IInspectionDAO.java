package com.revv.uct.supply.db.inspection.dao;

import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.InspectionDetailNormal;
import com.revv.uct.supply.service.inspection.model.inspectionDetail.PostRefurbList;

import java.util.List;
import java.util.Optional;

public interface IInspectionDAO {
    List<InspectionDetailNormal> findInspectionsByInspectionTypeAndStatus(Long inspectorID, List<InspectionStatus> statusList, List<InspectionType> inspectionTypes);

    Inspection scheduleInspection(Inspection scheduleData);

    Optional<Inspection> getInspectionDetails(Long inspectionID);

    void updateLastDataSyncTime(Long lastDataSyncTime, Long inspectionID);

    Inspection updateInspection(Inspection newData);

    Inspection getInspectionDetailsByLeadIDAndInspectionType(Long leadID, InspectionType inspectionType);

    Inspection findByIdAndIsActive(Long id, boolean b);

    List<PostRefurbList> findInspectionsByInspectionTypeAndStatusPostRefurbishment(Long inspectorID, List<InspectionStatus> statusList, List<InspectionType> inspectionTypes);

    Inspection findByInspectionTypeAndCarID(InspectionType inspectionType,Long carID);

}
