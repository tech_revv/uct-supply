package com.revv.uct.supply.db.inspectionSubPart.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "inspectionSubParts")
public class InspectionSubPart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "inspectionID", nullable = false)
    private Long inspectionID;

    @Column(name = "subPartID", nullable = false)
    private Long subPartID;

    @Column(name = "createdBy")
    private Long createdBy;

    @CreationTimestamp
    @Column(name = "createdAt")
    private LocalDateTime createdAt;

    @Column(name = "updatedBy")
    private Long updatedBy;

    @Column(name = "updatedAt")
    private LocalDateTime updatedAt;

    @Column(name = "isActive", nullable = false)
    @Builder.Default
    private boolean isActive = true;

}
