package com.revv.uct.supply.db.inspectionJob.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspectionJob.entity.InspectionJob;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface InspectionJobRRepository extends JpaRepository<InspectionJob, Long> {

    List<InspectionJob> findAllByInspectionIDAndIsActive(Long inspectionID, Boolean isActive);

    List<InspectionJob> findAllByInspectionIDInAndIsActive(List<Long> inspectionIDs, Boolean isActive);

    @Query("SELECT ij FROM InspectionJob ij where ij.inspectionID in (SELECT i.id FROM Inspection i where i.leadID in (SELECT pc.leadID FROM ProcuredCar pc where pc.id =:car_id) and i.inspectionType=:inspection_type) and ij.isActive=true")
    List<InspectionJob> getInspectionJobsByCarIDAndInspectionType(@Param("car_id")Long carID,@Param("inspection_type") InspectionType inspectionType);

}
