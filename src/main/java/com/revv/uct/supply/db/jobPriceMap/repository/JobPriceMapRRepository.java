package com.revv.uct.supply.db.jobPriceMap.repository;

import com.revv.uct.supply.db.annotation.ReadOnlyRepository;
import com.revv.uct.supply.db.jobPriceMap.entity.JobPriceMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ReadOnlyRepository
public interface JobPriceMapRRepository extends JpaRepository<JobPriceMap, Long> {
    @Query("select j.bodyType,j.model,j.workshop,j.price from JobPriceMap j where j.isActive=:b")
    List<Object[]> getAllJobsPriceByIsActive(boolean b);

    List<JobPriceMap> getAllByJobIDInAndIsActive(List<Long> jobIDs, Boolean isActive);

    List<JobPriceMap> getAllByIsActive(Boolean isActive);

    List<JobPriceMap> findAllByIsActive(boolean b);
}
