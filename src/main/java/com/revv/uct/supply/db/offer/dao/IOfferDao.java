package com.revv.uct.supply.db.offer.dao;

import com.revv.uct.supply.db.offer.entity.Offer;

import java.util.List;

public interface IOfferDao {
    List<Offer> findByLeadIDAndIsActive(Long leadID, boolean b);

    void markOfferInActive(Long existingOfferID, Long updatedBy);

    Offer createOffer(Offer offer);

    Offer findByIdAndIsActive(Long id, boolean b);

    Offer updateOffer(Offer newData);
}
