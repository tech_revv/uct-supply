package com.revv.uct.supply.controller.offer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.OfferStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class UpdateOfferRequest {
    @Valid @JsonProperty("updatedBy") private Long updatedBy;
    @Valid @NotNull @JsonProperty("id") private Long id;
    @Valid @NotNull @JsonProperty("leadID")private Long leadID;
    @Valid @JsonProperty("offerStatus")private OfferStatus offerStatus;
    @Valid @JsonProperty("rejectionReason") private String rejectionReason;
    @Valid @JsonProperty("refubCost") private Double refubCost;
    @Valid @JsonProperty("e2RefurbCost") private Double e2RefurbCost;
    @Valid @JsonProperty("serviceHistoryStatus")private String serviceHistoryStatus;
    @Valid @JsonProperty("serviceDocs")private String[] serviceDocs;
    @Valid @JsonProperty("noOfDocuments")private int noOfDocuments;
}
