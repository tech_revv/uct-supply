package com.revv.uct.supply.controller.procuredCar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.ProcuredCarStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class SaveProcuredCarRequest {
    @Valid @NotNull @JsonProperty("leadID") private Long leadID;
    @Valid @NotNull @JsonProperty("carModelID") private Long carModelID;
    @Valid @NotNull @JsonProperty("offerID") private Long offerID;
    @Valid @NotNull @JsonProperty("onBoardedDate") private LocalDateTime onBoardedDate;
    @Valid @JsonProperty("status") private ProcuredCarStatus status;
    @Valid @JsonProperty("createdAt") private LocalDateTime createdAt;
    @Valid @JsonProperty("createdBy") private Long createdBy;
    @Valid @JsonProperty("isActive") private boolean isActive;
}
