package com.revv.uct.supply.controller.offer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.OfferStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class CreateOfferRequest {
    @Valid @JsonProperty("createdBy") private Long createdBy;
    @Valid @JsonProperty("inspectionID")private Long inspectionID;
    @Valid @NotNull @JsonProperty("leadID")private Long leadID;
    @Valid @NotNull @JsonProperty("challanStatus")private String challanStatus;
    @Valid @NotNull @JsonProperty("serviceHistoryStatus")private String serviceHistoryStatus;
    @Valid @NotNull @JsonProperty("offeredPrice")private Double offeredPrice;
    @Valid @NotNull @JsonProperty("basePrice")private Double basePrice;
    @Valid @NotNull @JsonProperty("refubCost")private Double refubCost;
    @Valid @JsonProperty("noOfDocuments")private int noOfDocuments;
    @Valid @JsonProperty("challanCost")private Double challanCost;
    @Valid @JsonProperty("adjustCost")private Double adjustCost;
    @Valid @JsonProperty("createdAt")private LocalDateTime createdAt;
    @Valid @JsonProperty("offerStatus")private OfferStatus offerStatus;
    @Valid @JsonProperty("serviceDocs")private String[] serviceDocs;
}
