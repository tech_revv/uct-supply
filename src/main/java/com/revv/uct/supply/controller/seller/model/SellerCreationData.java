package com.revv.uct.supply.controller.seller.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.revv.uct.supply.constants.SellerType;
import com.revv.uct.supply.db.carModel.entity.CarModel;
import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;
import lombok.*;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class SellerCreationData {
    private List<ServiceCity> city;
    private List<SellerType> sellerType;
}
