package com.revv.uct.supply.controller.inspection.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.service.inspection.model.InspectionChecklist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class InspectionDifferenceRequest {
    @Valid @NotNull @JsonProperty("leadID")private Long leadID;
    @Valid @NotNull @JsonProperty("inspectionType1")private InspectionType inspectionType1;
    @Valid @NotNull @JsonProperty("inspectionType2")private InspectionType inspectionType2;
}
