package com.revv.uct.supply.controller.upload;

import com.revv.uct.supply.constants.ResponseMessage;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.db.carModel.entity.CarModel;
import com.revv.uct.supply.db.header.entity.Header;
import com.revv.uct.supply.db.jobs.entity.Jobs;
import com.revv.uct.supply.db.parts.entity.Parts;
import com.revv.uct.supply.db.subPartIssues.entity.SubPartIssues;
import com.revv.uct.supply.db.subParts.entity.SubParts;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.carModel.ICarModelService;
import com.revv.uct.supply.service.header.IHeaderService;
import com.revv.uct.supply.service.jobs.IJobsService;
import com.revv.uct.supply.service.parts.IPartsService;
import com.revv.uct.supply.service.subPartIssues.ISubPartIssuesService;
import com.revv.uct.supply.service.subParts.ISubPartsService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/uploadFiles")
@Slf4j
@AllArgsConstructor
public class UploadFileController {

    IHeaderService headerService;
    ICarModelService carModelService;
    IPartsService partsService;
    ISubPartsService subPartsService;
    IJobsService jobsService;
    ISubPartIssuesService subPartIssuesService;


    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to upload csv data.")
    public BaseResponse upload(@PathVariable("id") int id, @RequestBody List<Object> list) throws SupplyException, EntityNotFoundException {
        BaseResponse result = null;

        switch (id) {
            case 1: result = uploadHeaders(list);
                    break;
            case 2: result = uploadCarModels(list);
                    break;
            case 3: result = uploadParts(list);
                    break;
            case 4: result = uploadSubParts(list);
                    break;
            case 5: result = uploadSubPartIssues(list);
                    break;
            case 6: result = uploadJobs(list);
                    break;
        }

        return result;
    }

    public BaseResponse uploadHeaders(@RequestBody List<Object> list) throws SupplyException, EntityNotFoundException {
        List<Header> result = null;
        try {
            result = headerService.uploadNewHeaders(list);
        }catch (SupplyException | EntityNotFoundException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(result)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.upload_successful)
                .data(result)
                .build();
    }

    public BaseResponse uploadCarModels(@RequestBody List<Object> list){
        List<CarModel> result = null;
        result = carModelService.uploadCarModels(list);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.upload_successful)
                .data(result)
                .build();
    }

    public BaseResponse uploadParts(@RequestBody List<Object> list) throws SupplyException, EntityNotFoundException {
        List<Parts> result = null;
        try{
            result = partsService.uploadParts(list);
        } catch (SupplyException | EntityNotFoundException e){
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(result)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.upload_successful)
                .data(result)
                .build();
    }

    public BaseResponse uploadSubParts(@RequestBody List<Object> list) throws EntityNotFoundException, SupplyException {
        List<SubParts> result = null;
        try {
            result = subPartsService.uploadSubParts(list);
        }catch (EntityNotFoundException | SupplyException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(result)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.upload_successful)
                .data(result)
                .build();
    }

    public BaseResponse uploadJobs(@RequestBody List<Object> list) throws EntityNotFoundException, SupplyException {
        List<Jobs> result = null;
        try {
            result = jobsService.uploadJobs(list);
        }catch (EntityNotFoundException | SupplyException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(result)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.upload_successful)
                .data(result)
                .build();
    }

    public BaseResponse uploadSubPartIssues(@RequestBody List<Object> list) throws SupplyException, EntityNotFoundException {

        List<SubPartIssues> result = null;
        try {
            result = subPartIssuesService.uploadSubPartIssues(list);
        }catch (SupplyException | EntityNotFoundException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(result)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.upload_successful)
                .data(result)
                .build();
    }
}
