package com.revv.uct.supply.controller.payment.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.PaymentModeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class ProceedWithoutTokenRequest {
    @Valid @NotNull @JsonProperty("leadID") private Long leadID;
    @Valid @NotNull @JsonProperty("offerID") private Long offerID;
    @Valid @JsonProperty("amountPaid") private Double amountPaid;
    @Valid @JsonProperty("paymentMode") private PaymentModeEnum paymentMode;
    @Valid @JsonProperty("referenceNumber") private String referenceNumber;
    @Valid @JsonProperty("paidBy") private Long paidBy;
    @Valid @JsonProperty("noOfDocuments") private int noOfDocuments;
    @Valid @JsonProperty("createdBy") private Long createdBy;
    @Valid @JsonProperty("paymentDocs") private String[] paymentDocs;
}
