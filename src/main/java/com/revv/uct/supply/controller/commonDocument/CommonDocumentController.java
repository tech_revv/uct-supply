package com.revv.uct.supply.controller.commonDocument;

import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.db.enums.CommonDocumentEntity;
import com.revv.uct.supply.db.enums.DocumentStage;
import com.revv.uct.supply.service.commonDocument.ICommonDocumentService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api/document")
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class CommonDocumentController {

    @Autowired
    ICommonDocumentService commonDocumentService;

    @RequestMapping(value = "/getDocuments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get documents by entityID.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getDocuments(@NotNull @RequestParam Long entityID, @NotNull @RequestParam Long dsmID, @NotNull @RequestParam CommonDocumentEntity entity , HttpServletRequest req) {
        log.info("getDocuments==>>", entityID);

        UserData userData = (UserData) req.getAttribute("user");

        List<String> documents = commonDocumentService.getDocuments(entityID, dsmID, entity);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage("Successful")
                .data(documents)
                .build();
    }

    @RequestMapping(value = "/getDocumentsByDSMStage", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get documents by entityID.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getDocumentsByDSMStage(@NotNull @RequestParam Long entityID, @NotNull @RequestParam DocumentStage documentStage, @NotNull @RequestParam CommonDocumentEntity entity , HttpServletRequest req) {
        log.info("getDocumentsByDSMStage==>>");

        UserData userData = (UserData) req.getAttribute("user");

        List<String> documents = commonDocumentService.getDocumentsByDSMStage(entityID, documentStage, entity);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage("Successful")
                .data(documents)
                .build();
    }
}
