package com.revv.uct.supply.controller.workshop;

import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.constants.ResponseMessage;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.controller.workshop.model.CreateWorkshopRequest;
import com.revv.uct.supply.controller.workshop.model.WorkshopData;
import com.revv.uct.supply.db.workshop.entity.Workshop;
import com.revv.uct.supply.service.workshop.IWorkshopService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api/workshop")
@Slf4j
@AllArgsConstructor
public class WorkshopController {

    @Autowired
    private final IWorkshopService workshopService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get workshops.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getWorkshops(HttpServletRequest req){
        log.info("getAdmins====>>>>");
        UserData userData = (UserData) req.getAttribute("user");

        List<Workshop> workshops = workshopService.getAllActiveWorkshops();

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.success)
                .data(workshops)
                .build();
    }

    @RequestMapping(value = "/dataForWorkshop", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get data for workshops")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getData(){

        List<WorkshopData> data = workshopService.getWorkshopsData();

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.success)
                .data(data)
                .build();
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to create workshop.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse createWorkshop(HttpServletRequest req, @NotNull @Valid @RequestBody CreateWorkshopRequest createWorkshopRequest){
        log.info("req ={}",createWorkshopRequest);
        UserData userData = (UserData) req.getAttribute("user");

        Workshop workshop = workshopService.createWorkshop(createWorkshopRequest,userData);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.success)
                .data(workshop)
                .build();
    }

}
