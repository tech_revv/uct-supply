package com.revv.uct.supply.controller.refurbishment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.controller.inspection.model.ScheduleInspectionRequest;
import com.revv.uct.supply.controller.inspection.model.UpdateInspectionRequest;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.controller.refurbishment.model.ScheduleRefurbishmentRequest;
import com.revv.uct.supply.controller.refurbishment.model.SubmitDocumentRequest;
import com.revv.uct.supply.controller.refurbishment.model.UpdateRefurbRequest;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.db.refurbishment.entity.Refurbishment;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.refurbishment.IRefurbishmentService;
import com.revv.uct.supply.service.refurbishment.model.RefurbJobListForWorkshop;
import com.revv.uct.supply.service.refurbishment.model.RefurbScreen;
import com.revv.uct.supply.service.refurbishment.model.ScheduleRefurbishmentResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.revv.uct.supply.constants.ResponseMessage.*;

@RestController
@RequestMapping("/api/v1/refurb")
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class RefurbishmentController {

    @Autowired
    IRefurbishmentService refurbishmentService;

    @RequestMapping(value="/jobList/{refurbID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse getRefurbJobList(HttpServletRequest request, @NotNull @PathVariable Long refurbID) {
        log.info("getRefurbJobList =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        RefurbScreen refurbScreenDetails = null;
        try {
            refurbScreenDetails = refurbishmentService.getRefurbJobList(refurbID);
        } catch (UCTSupplyException e) {
            return BaseResponse.builder()
                    .statusCode(e.getStatus().value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(refurb_jobs_success)
                .data(Arrays.asList(refurbScreenDetails))
                .build();
    }

    @RequestMapping(value="/{refurbID}/documents/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse saveRefurbishmentDocuments(HttpServletRequest request, @NotNull @PathVariable Long refurbID, @Valid @NotNull @RequestBody SubmitDocumentRequest requestBody) {
        log.info("saveRefurbishmentDocuments =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        try {
            refurbishmentService.saveDocuments(userData.getId(), refurbID, requestBody);

            return BaseResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .statusMessage(refurb_documents_success)
                    .data(null)
                    .build();

        } catch (UCTSupplyException ex) {
            return BaseResponse.builder()
                    .statusCode(ex.getStatus().value())
                    .statusMessage(ex.getMessage())
                    .data(null)
                    .build();
        }
    }

    @RequestMapping(value="/{refurbID}/update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse updateRefurbishment(HttpServletRequest req,@NotNull @PathVariable Long refurbID, @Valid @NotNull @RequestBody UpdateRefurbRequest request) throws EntityNotFoundException {
        log.info("updateRefurbishment =>>>>>");
        UserData userData = getUserData(req);

        Refurbishment refurbishment = null;
        try {
            refurbishment = refurbishmentService.updateRefurbishemnt(refurbID, userData.getId(), request);
        } catch (UCTSupplyException ex) {
            return BaseResponse.builder()
                    .statusCode(ex.getStatus().value())
                    .statusMessage(ex.getMessage())
                    .data(null)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(refurb_update_success)
                .data(Arrays.asList(refurbishment))
                .build();
    }

    @RequestMapping(value="/refurbJobList/{carID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse getRefurbJobListForSendToWorkshop(HttpServletRequest request, @NotNull @PathVariable Long carID) {
        log.info("getRefurbJobList =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        List<RefurbJobListForWorkshop> refurbJobListForWorkshopList = refurbishmentService.getRefurbJobListForSendToWorkshop(carID);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(refurb_jobs_success)
                .data(refurbJobListForWorkshopList)
                .build();
    }

    @RequestMapping(value="/schedule", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse scheduleRefurbishment(HttpServletRequest req, @Valid @NotNull @RequestBody ScheduleRefurbishmentRequest request) throws EntityNotFoundException {
        log.info("scheduleInspection =>>>>>");
        UserData userData = getUserData(req);
        log.info(userData.toString());
        request.setUserID(userData.getId());
        Refurbishment scheduledRefurbishment;
        String refurbMsg;
        try {
            ScheduleRefurbishmentResponse response = refurbishmentService.scheduleRefurbishment(request);
            scheduledRefurbishment = response.getRefurbishment();
            refurbMsg = response.getMessage();
        } catch (EntityNotFoundException | SupplyException | JsonProcessingException | UCTSupplyException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        List<Refurbishment> list = new ArrayList<>();
        list.add(scheduledRefurbishment);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(refurbMsg)
                .data(list)
                .build();
    }



    public UserData getUserData(HttpServletRequest request) {
        return (UserData) request.getAttribute("user");
    }


}
