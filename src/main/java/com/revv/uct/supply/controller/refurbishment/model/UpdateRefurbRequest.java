package com.revv.uct.supply.controller.refurbishment.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.InspectionStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class UpdateRefurbRequest {
    @Valid @JsonProperty("status")private InspectionStatus refurbStatus;
    @Valid @JsonProperty("lastDataSyncTime")private Long lastDataSyncTime;
    @Valid @JsonProperty("refurbSpocID")private Long refurbSpocID;
    @Valid @JsonProperty("workshopID")private Long workshopID;
    @Valid @JsonProperty("refurbStartTime")private LocalDateTime refurbStartTime;
    @Valid @JsonProperty("refurbEndTime")private LocalDateTime refurbEndTime;

}
