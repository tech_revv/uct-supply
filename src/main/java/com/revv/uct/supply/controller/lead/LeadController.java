package com.revv.uct.supply.controller.lead;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.constants.ResponseMessage;
import com.revv.uct.supply.constants.ResponseMessages;
import com.revv.uct.supply.controller.lead.model.CreateLeadRequest;
import com.revv.uct.supply.controller.lead.model.CreateLeadResponseData;
import com.revv.uct.supply.controller.lead.model.LeadCreationData;
import com.revv.uct.supply.controller.lead.model.UpdateLeadRequest;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.lead.entity.Lead;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.lead.ILeadService;
import com.revv.uct.supply.service.seller.ISellerService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
//import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/api/lead")
@Slf4j
public class LeadController
{
    private ILeadService leadService;

    public LeadController(ILeadService leadService) {
        this.leadService = leadService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to create lead")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse createLead(@Valid @NotNull @RequestBody CreateLeadRequest request, HttpServletRequest req) {
        Lead lead = null;
        log.info("req ={}", req);
        UserData userData = (UserData) req.getAttribute("user");
        try {
            log.info("request ={}", request);
            request.setAdminID(userData.getId());
            log.info("request ={}", request);
            lead = leadService.createLead(request);
            log.debug("created lead={}", lead);
        }catch (SupplyException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_MODIFIED.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }
        //response data in form of list
        List<CreateLeadResponseData> data = new ArrayList<>();
        data.add(new CreateLeadResponseData(lead.getId()));
        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessages.leadCreated + lead.getId())
                .data(data)
                .build();
    }

    @RequestMapping(value = "/dataForLeadCreation", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get data for lead creation")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getData(){

        List<LeadCreationData> data = leadService.getLeadCreationData();

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.success)
                .data(data)
                .build();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to update lead")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse updateLead(@Valid @NotNull @RequestBody UpdateLeadRequest request, HttpServletRequest req) throws SupplyException {
        Lead lead = null;
        log.info("req ={}", req);
        UserData userData = (UserData) req.getAttribute("user");
        log.info("request ={}", request);
        request.setAdminID(userData.getId());
        log.info("request ={}", request);
        try {
            lead = leadService.updateLead(request);
            log.debug("updated lead={}", lead);
        } catch (SupplyException | JsonProcessingException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }
        //response data in form of list
        List<CreateLeadResponseData> data = new ArrayList<>();
        data.add(new CreateLeadResponseData(lead.getId()));
        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessages.leadUpdated + lead.getId())
                .data(data)
                .build();
    }

    @RequestMapping(value = "/createOnlineLead", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to create online lead")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse createOnlineLead(@Valid @NotNull @RequestBody CreateLeadRequest request, HttpServletRequest req) throws SupplyException {
        Lead onlineLead = null;
        log.info("req ={}", req);
        UserData userData = (UserData) req.getAttribute("user");

            log.info("request ={}", request);
            request.setAdminID(userData.getId());
            log.info("request ={}", request);
            try {
                onlineLead = leadService.createOnlineLead(request);
                log.debug("created onlineLead={}", onlineLead);
            } catch (SupplyException e) {
                return BaseResponse.builder()
                        .statusCode(HttpStatus.NOT_FOUND.value())
                        .statusMessage(e.getMessage())
                        .data(null)
                        .build();
            }


        //response data in form of list
        List<CreateLeadResponseData> data = new ArrayList<>();
        data.add(new CreateLeadResponseData(onlineLead.getId()));
        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessages.leadCreated + onlineLead.getId())
                .data(data)
                .build();
    }
}
