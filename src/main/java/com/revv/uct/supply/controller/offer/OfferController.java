package com.revv.uct.supply.controller.offer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.constants.ResponseMessage;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.controller.offer.model.CalculateOfferPriceRequest;
import com.revv.uct.supply.controller.offer.model.CreateOfferRequest;
import com.revv.uct.supply.controller.offer.model.ReviseOfferRequest;
import com.revv.uct.supply.controller.offer.model.UpdateOfferRequest;
import com.revv.uct.supply.service.offer.model.CalculateOfferPrice;
import com.revv.uct.supply.service.offer.model.GenerateOfferData;
import com.revv.uct.supply.db.offer.entity.Offer;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.offer.IOfferService;
import com.revv.uct.supply.service.offer.model.GenerateOfferData;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

@RestController
@CrossOrigin
@RequestMapping("/api/offer")
@Slf4j
public class OfferController {
    private IOfferService offerService;

    @Autowired
    public OfferController(IOfferService offerService) {
        this.offerService = offerService;
    }

    @RequestMapping(value = "/getGenerateOfferData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get data to create offer")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getGenerateOfferData(HttpServletRequest req, @RequestParam("inspectionID")Long inspectionID) throws UCTSupplyException {
        log.info("getGenerateOfferData==>>");

        UserData userData = (UserData) req.getAttribute("user");
        GenerateOfferData offerData = null;

        try {
            offerData = offerService.getGenerateOfferData(inspectionID);
        }catch (UCTSupplyException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage("successful")
                .data(Arrays.asList(offerData))
                .build();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to create offer")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse createOffer(@Valid @NotNull @RequestBody CreateOfferRequest request, HttpServletRequest req) throws SupplyException, JsonProcessingException {
        log.info("createOfferRequest==>>", request);

        UserData userData = (UserData) req.getAttribute("user");
        request.setCreatedBy(userData.getId());

        Offer createdOffer = null;
        try {
            createdOffer = offerService.createOffer(request);
        }catch (SupplyException | JsonProcessingException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.offer_created + " with offerID: "+createdOffer.getId())
                .data(Arrays.asList(createdOffer))
                .build();
    }

    @RequestMapping(value = "/calculateOfferPrice", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to calculate offer price.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse calculateOfferPrice(@Valid @NotNull @RequestBody CalculateOfferPriceRequest request, HttpServletRequest req) {
        log.info("calculateOfferPriceRequest==>>", request);

        UserData userData = (UserData) req.getAttribute("user");

        CalculateOfferPrice offerPrice = offerService.calculateOfferPrice(request);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.offer_price)
                .data(Arrays.asList(offerPrice))
                .build();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to update offer")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse updateOffer(@Valid @NotNull @RequestBody UpdateOfferRequest request, HttpServletRequest req) throws SupplyException, JsonProcessingException {
        log.info("updateOfferRequest==>>", request);

        UserData userData = (UserData) req.getAttribute("user");
        request.setUpdatedBy(userData.getId());

        Offer updatedOffer = null;
        try {
            updatedOffer = offerService.updateOffer(request);
        }catch (SupplyException | JsonProcessingException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.offer_updated)
                .data(Arrays.asList(updatedOffer))
                .build();
    }

    @RequestMapping(value = "/revise", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to create offer")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse reviseOffer(@Valid @NotNull @RequestBody ReviseOfferRequest request, HttpServletRequest req) throws SupplyException, JsonProcessingException {
        log.info("createOfferRequest==>>", request);

        UserData userData = (UserData) req.getAttribute("user");
        request.setUpdatedBy(userData.getId());

        Offer createdOffer = null;
        try {
            createdOffer = offerService.reviseOffer(request);
        }catch (SupplyException | JsonProcessingException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.offer_revised)
                .data(Arrays.asList(createdOffer))
                .build();
    }
}
