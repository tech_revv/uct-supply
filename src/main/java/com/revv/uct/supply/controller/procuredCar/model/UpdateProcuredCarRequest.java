package com.revv.uct.supply.controller.procuredCar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.IssueStage;
import com.revv.uct.supply.db.enums.ProcuredCarStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class UpdateProcuredCarRequest {
    @Valid @NotNull @JsonProperty("id") private Long id;
    @Valid @JsonProperty("status") private ProcuredCarStatus status;
    @Valid @JsonProperty("updatedBy") private Long updatedBy;
}
