package com.revv.uct.supply.controller.restartController;

import com.revv.uct.supply.SupplyApplication;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.service.lead.ILeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestartController {

    @Autowired
    ILeadService leadService;

    @PostMapping("/restart")
    public void restart() {
        SupplyApplication.restart();
    }

    @PostMapping("/healthCheck")
    public BaseResponse getHealthCheck() {
        Long count = leadService.getAllLeadsCount();

        return BaseResponse.builder()
                .statusMessage("success")
                .statusCode(200)
                .data(count)
                .build();
    }
}
