package com.revv.uct.supply.controller.serviceCity;

import com.revv.uct.supply.constants.ResponseMessages;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.controller.seller.model.CreateSellerRequest;
import com.revv.uct.supply.controller.serviceCity.model.CreateServiceCityRequest;
import com.revv.uct.supply.db.seller.entity.Seller;
import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.serviceCity.IServiceCityService;
import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/service_city")
@Slf4j
public class ServiceCityController
{
    private IServiceCityService serviceCityService;

    @Autowired
    public ServiceCityController(IServiceCityService serviceCityService) {
        this.serviceCityService = serviceCityService;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to create service city")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse createServiceCity(@Valid @NotNull @RequestBody CreateServiceCityRequest request) throws UCTSupplyException {
        log.info("create service city ={}",request);
        ServiceCity serviceCity = serviceCityService.createServiceCity(request);
        log.debug("created serviceCity={}",serviceCity);
        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessages.serviceCityCreated)
                .data(serviceCity)
                .build();
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get all service cities")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getAllServiceCities(){
        log.info("Get All Service Cities");
        List<ServiceCity> serviceCities = serviceCityService.findAllServiceCities();
        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessages.allCityData)
                .data(serviceCities)
                .build();
    }
}
