package com.revv.uct.supply.controller.offer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.OfferReviseStage;
import com.revv.uct.supply.db.enums.OfferStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class ReviseOfferRequest {
    @Valid @JsonProperty("createdBy") private Long createdBy;
    @Valid @JsonProperty("updatedBy") private Long updatedBy;
    @Valid @NotNull @JsonProperty("id") private Long id;
    //@Valid @JsonProperty("inspectionID")private Long inspectionID;
    @Valid @NotNull @JsonProperty("leadID")private Long leadID;
    //@Valid @NotNull @JsonProperty("challanStatus")private String challanStatus;
    //@Valid @NotNull @JsonProperty("serviceHistoryStatus")private String serviceHistoryStatus;
    @Valid @JsonProperty("offeredPrice")private Double offeredPrice;
    @Valid @JsonProperty("basePrice")private Double basePrice;
    @Valid @JsonProperty("refubCost")private Double refubCost;
    @Valid @JsonProperty("challanCost")private Double challanCost;
    @Valid @JsonProperty("adjustCost")private Double adjustCost;
    //@Valid @JsonProperty("noOfDocuments")private int noOfDocuments;
    //@Valid @JsonProperty("createdAt")private LocalDateTime createdAt;
    //@Valid @NotNull @JsonProperty("offerStatus")private OfferStatus offerStatus;
    //@Valid @JsonProperty("serviceDocs")private String[] serviceDocs;
    @Valid @JsonProperty("refubCostE2")private Double refubCostE2;
    @Valid @NotNull @JsonProperty("offerReviseStage")private OfferReviseStage offerReviseStage;
}
