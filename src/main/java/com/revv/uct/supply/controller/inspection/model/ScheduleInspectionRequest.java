package com.revv.uct.supply.controller.inspection.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PreUpdate;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class ScheduleInspectionRequest {
    @Valid @NotNull @JsonProperty("inspectionType")private InspectionType inspectionType;
    @Valid @NotNull @JsonProperty("leadID")private Long leadID;
    @Valid @NotNull @JsonProperty("inspectorID")private Long inspectorID;
    @Valid @JsonProperty("createdBy")private Long createdBy;
    @Valid @NotNull @JsonProperty("inspectionTime")private LocalDateTime inspectionTime;
    @Valid @JsonProperty("status")private InspectionStatus status;
    @Valid @JsonProperty("createdAt")private LocalDateTime createdAt;
    @Valid @JsonProperty("updatedAt")private LocalDateTime updatedAt;
    @Valid @JsonProperty("isActive")private boolean isActive;
}
