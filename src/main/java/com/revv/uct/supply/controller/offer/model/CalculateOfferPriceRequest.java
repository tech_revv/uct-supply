package com.revv.uct.supply.controller.offer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class CalculateOfferPriceRequest {
    @Valid @NotNull @JsonProperty("basePrice")private Double basePrice;
    @Valid @NotNull @JsonProperty("refubCost")private Double refubCost;
    @Valid @JsonProperty("challanCost")private Double challanCost;
    @Valid @JsonProperty("adjustCost")private Double adjustCost;

}
