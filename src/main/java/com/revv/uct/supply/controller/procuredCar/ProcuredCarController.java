package com.revv.uct.supply.controller.procuredCar;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.constants.ResponseMessage;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.controller.payment.model.SaveLeadPaymentRequest;
import com.revv.uct.supply.controller.procuredCar.model.ReportIssueRequest;
import com.revv.uct.supply.controller.procuredCar.model.SaveProcuredCarRequest;
import com.revv.uct.supply.controller.procuredCar.model.UpdateProcuredCarRequest;
import com.revv.uct.supply.db.leadPaymentDetail.entity.LeadPaymentDetail;
import com.revv.uct.supply.db.procuredCar.entity.ProcuredCar;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.service.procuredCar.IProcuredCarService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

@RestController
@CrossOrigin
@RequestMapping("/api/procuredCar")
@Slf4j
public class ProcuredCarController {
    private final IProcuredCarService procuredCarService;

    @Autowired
    public ProcuredCarController(IProcuredCarService procuredCarService) {
        this.procuredCarService = procuredCarService;
    }

    @RequestMapping(value = "/saveProcuredCarDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to save data in procured car collection.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse saveProcuredCarDetails(@Valid @NotNull @RequestBody SaveProcuredCarRequest request, HttpServletRequest req) throws SupplyException, JsonProcessingException {
        log.info("saveProcuredCarDetails==>>");

        UserData userData = (UserData) req.getAttribute("user");
        request.setCreatedBy(userData.getId());

        ProcuredCar procuredCar = null;
        procuredCar = procuredCarService.saveProcuredCarDetails(request);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.procured_car_details_added + " for Lead ID " + procuredCar.getLeadID())
                .data(Arrays.asList(procuredCar))
                .build();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to save data in procured car collection.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse updateProcuredCarDetails(@Valid @NotNull @RequestBody UpdateProcuredCarRequest request, HttpServletRequest req) throws SupplyException, JsonProcessingException {
        log.info("updateProcuredCarDetails==>>");

        UserData userData = (UserData) req.getAttribute("user");
        request.setUpdatedBy(userData.getId());

        ProcuredCar procuredCar = null;
        procuredCar = procuredCarService.updateProcuredCarDetails(request);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.procured_car_details_updated + " for Car ID " + procuredCar.getId())
                .data(Arrays.asList(procuredCar))
                .build();
    }

    @RequestMapping(value = "/reportIssue", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to save data in procured car collection.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse reportIssue(@Valid @NotNull @RequestBody ReportIssueRequest request, HttpServletRequest req) throws SupplyException, JsonProcessingException {
        log.info("reportIssue==>>");

        UserData userData = (UserData) req.getAttribute("user");
        request.setUpdatedBy(userData.getId());

        ProcuredCar procuredCar = null;
        procuredCar = procuredCarService.reportIssue(request);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.issue_reported + " for Car ID " + procuredCar.getId())
                .data(Arrays.asList(procuredCar))
                .build();
    }
}
