package com.revv.uct.supply.controller.upload.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.Transmission;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class CreateCarModelRequest {
    //@NotNull @JsonProperty("name") private String name;
    @NotNull @JsonProperty("make") private String make;
    @NotNull @JsonProperty("model") private String model;
    @NotNull @JsonProperty("variant") private String variant;
    @NotNull @JsonProperty("fuel") private String fuel;
    @NotNull @JsonProperty("transmission") private Transmission transmission;
    @NotNull @JsonProperty("bodyType") private String bodyType;
    @NotNull @JsonProperty("seats") private int seats;
    @NotNull @JsonProperty("startYear") private int startYear;
    @NotNull @JsonProperty("endYear") private int endYear;
    @NotNull @JsonProperty("price") private Long price;
}
