package com.revv.uct.supply.controller.refurbishment.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.service.refurbishment.model.RefurbJobListForWorkshop;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class ScheduleRefurbishmentRequest {

    @Valid @NotNull @JsonProperty("workshopID")private Long workshopID;
    @Valid @NotNull @JsonProperty("refurbSpocID")private Long refurbSpocID;
    @Valid @NotNull @JsonProperty("carID")private Long carID;
    @Valid @NotNull @JsonProperty("refurbJobs")private List<RefurbJobListForWorkshop> refurbJobs;
    @Valid @NotNull @JsonProperty("refurbishmentStartTime")private LocalDateTime refurbishmentStartTime;
    @Valid @NotNull @JsonProperty("refurbishmentEndTime")private LocalDateTime refurbishmentEndTime;
    @Valid @JsonProperty("createdAt")private LocalDateTime createdAt;
    @Valid @JsonProperty("updatedAt")private LocalDateTime updatedAt;
    @Valid @JsonProperty("userID")private Long userID;

}
