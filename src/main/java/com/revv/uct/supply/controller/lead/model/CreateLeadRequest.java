package com.revv.uct.supply.controller.lead.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.InsuranceTypeEnum;
import com.revv.uct.supply.db.enums.LeadStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class CreateLeadRequest
{
    @Valid @JsonProperty("adminID") private Long adminID;
    @Valid @NotNull @JsonProperty("carModelID")private Long carModelID;
    @Valid @JsonProperty("sellerID")private Long sellerID;
    @Valid @JsonProperty("registrationNumber")private String registrationNumber;
    @Valid @JsonProperty("carColour")private String carColour;
    @Valid @JsonProperty("registrationTimestamp")private LocalDateTime registrationTimestamp;
    @Valid @JsonProperty("manufacturingTimestamp")private LocalDateTime manufacturingTimestamp;
    @Valid @JsonProperty("insuranceValidity")private LocalDateTime insuranceValidity;
    @Valid @JsonProperty("insuranceType")private String insuranceType;
    @Valid @NotNull @JsonProperty("rto")private String rto;
    @Valid @NotNull @JsonProperty("registrationType")private String registrationType;
    @Valid @NotNull @JsonProperty("ownershipSerial")private String ownershipSerial;
    @Valid @JsonProperty("serviceCityID")private Long serviceCityID;
    @Valid @JsonProperty("comments")private String comments;
    @Valid @JsonProperty("expOfferPrice") private Double expOfferPrice;
    @Valid @JsonProperty("kmsDriven")private Double kmsDriven;
    @Valid @JsonProperty("leadStatus")private LeadStatus leadStatus;
    @Valid @JsonProperty("rejectionReason")private String rejectionReason;
    @Valid @JsonProperty("updatedAt") private LocalDateTime updatedAt;
    @Valid @JsonProperty("sellerContactNo")private String sellerContactNo;

    @Valid @JsonProperty("challanStatus")private String challanStatus;
    @Valid @JsonProperty("serviceHistoryStatus")private String serviceHistoryStatus;
    @Valid @JsonProperty("offeredPrice")private Double offeredPrice;
    @Valid @JsonProperty("basePrice")private Double basePrice;
    @Valid @JsonProperty("refubCost")private Double refubCost;
}
