package com.revv.uct.supply.controller.user;

import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.constants.ResponseMessage;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.db.enums.AdminType;
import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.user.entity.User;
import com.revv.uct.supply.service.user.IUserService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/usersupply")
@Slf4j
@AllArgsConstructor
public class UserController {
    IUserService userService;

    @RequestMapping(value = "/getAdmins", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get admins.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getAdmins(HttpServletRequest req, @RequestParam("adminType")String adminType){
        log.info("getAdmins====>>>>");
        UserData userData = (UserData) req.getAttribute("user");

        List<User> users = userService.getAdmins(AdminType.valueOf(adminType));

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.success)
                .data(users)
                .build();
    }

    @RequestMapping(value = "/getAdminsByCity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get admins for inspection.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getAdminsByCity(HttpServletRequest req, @RequestParam("adminType")String adminType, @RequestParam("cityID")Long cityID){
        log.info("getAdmins====>>>>");
        UserData userData = (UserData) req.getAttribute("user");

        List<User> users = userService.getAdminsByCity(AdminType.valueOf(adminType), cityID);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.success)
                .data(users)
                .build();
    }
}
