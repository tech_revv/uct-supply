package com.revv.uct.supply.controller.inspection.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.InspectionStatus;
import com.revv.uct.supply.db.enums.InspectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class UpdateInspectionRequest {
    @Valid @NotNull @JsonProperty("id")private Long id;
    @Valid @NotNull @JsonProperty("leadID")private Long leadID;
    @Valid @JsonProperty("inspectorID")private Long inspectorID;
    @Valid @JsonProperty("updatedBy")private Long updatedBy;
    @Valid @JsonProperty("inspectionTime")private LocalDateTime inspectionTime;
    @Valid @JsonProperty("updatedAt")private LocalDateTime updatedAt;
    @Valid @JsonProperty("refurbJobCost")private Double refurbJobCost;
    @Valid @JsonProperty("lastDataSyncTime")private Long lastDataSyncTime;
    @Valid @JsonProperty("pickupDocumentsCount")private int pickupDocumentsCount;
    @Valid @JsonProperty("refurbID")private Long refurbID;

}
