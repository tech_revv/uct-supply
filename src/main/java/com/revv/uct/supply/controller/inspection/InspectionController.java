package com.revv.uct.supply.controller.inspection;

import static com.revv.uct.supply.constants.ResponseMessage.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.controller.inspection.model.*;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.db.enums.InspectionType;
import com.revv.uct.supply.db.inspection.entity.Inspection;
import com.revv.uct.supply.exceptions.EntityNotFoundException;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.inspection.IInspectionService;
import com.revv.uct.supply.service.inspection.model.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/inspection")
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class InspectionController {

    @Autowired
    IInspectionService inspectionService;


    @RequestMapping(value="/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse getInspectionList(HttpServletRequest request) {
        log.info("getInspectionList =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        List<InspectionListHeader> inspections = inspectionService.getInspectionList(userData.getId());

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(inspection_list_success)
                .data(inspections)
                .build();
    }

    @RequestMapping(value="/checklist/{inspectionID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse getInspectionChecklist(HttpServletRequest request, @NotNull @PathVariable Long inspectionID) {
        log.info("getInspectionChecklist =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        try {
            List<InspectionChecklist> inspectionChecklist = inspectionService.getInspectionChecklist(inspectionID);
            return BaseResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .statusMessage(inspection_checklist_success)
                    .data(inspectionChecklist)
                    .build();
        } catch (UCTSupplyException e) {
            return BaseResponse.builder()
                    .statusCode(e.getStatus().value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }
    }

    @RequestMapping(value="/checklistFlat/{inspectionID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse getInspectionChecklistFlat(HttpServletRequest request, @NotNull @PathVariable Long inspectionID) {
        log.info("getInspectionChecklistFlat =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        try {
            List<InspectionChecklistFlat> inspectionChecklist = inspectionService.getInspectionChecklistFlattened(inspectionID);
            return BaseResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .statusMessage(inspection_checklist_success)
                    .data(inspectionChecklist)
                    .build();
        } catch (UCTSupplyException e) {
            return BaseResponse.builder()
                    .statusCode(e.getStatus().value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }
    }

    @RequestMapping(value="/pickupChecklist/{inspectionID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse getInspectionPickupChecklist(HttpServletRequest request, @NotNull @PathVariable Long inspectionID) {
        log.info("getInspectionPickupChecklist =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        try {
            List<InspectionPickupChecklist> inspectionPickupChecklist = inspectionService.getInspectionPickupChecklist(inspectionID);
            return BaseResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .statusMessage(inspection_pickup_checklist_success)
                    .data(inspectionPickupChecklist)
                    .build();
        } catch (UCTSupplyException e) {
            return BaseResponse.builder()
                    .statusCode(e.getStatus().value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }
    }

    @RequestMapping(value="/schedule", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse scheduleInspection(HttpServletRequest req, @Valid @NotNull @RequestBody ScheduleInspectionRequest request) throws EntityNotFoundException {
        log.info("scheduleInspection =>>>>>");
        UserData userData = getUserData(req);
        log.info(userData.toString());
        request.setCreatedBy(userData.getId());
        Inspection scheduledInspection = null;
        try {
            scheduledInspection = inspectionService.scheduleInspection(request);
        } catch (EntityNotFoundException | SupplyException | JsonProcessingException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        List<Inspection> list = new ArrayList<>();
        list.add(scheduledInspection);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(scheduledInspection.getInspectionType()+" "+inspection_scheduled+" for leadID: "+scheduledInspection.getLeadID())
                .data(list)
                .build();
    }

    @RequestMapping(value="/checklist/{inspectionID}/saveData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse saveInspectionChecklist(HttpServletRequest request, @NotNull @PathVariable Long inspectionID, @Valid @NotNull @RequestBody InspectionChecklistRequest requestBody) {
        log.info("saveInspectionChecklist =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        try {
            inspectionService.saveInspectionChecklist(userData.getId(), inspectionID, requestBody);

            return BaseResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .statusMessage(inspection_checklist_save_success)
                    .data(null)
                    .build();

        } catch (UCTSupplyException ex) {
            return BaseResponse.builder()
                    .statusCode(ex.getStatus().value())
                    .statusMessage(ex.getMessage())
                    .data(null)
                    .build();
        }
    }

    @RequestMapping(value="/pickupChecklist/{inspectionID}/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse saveInspectionPickupChecklist(HttpServletRequest request, @NotNull @PathVariable Long inspectionID, @Valid @NotNull @RequestBody List<InspectionPickupChecklist> requestBody) {
        log.info("saveInspectionPickupChecklist =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        try {
            inspectionService.saveInspectionPickupChecklist(userData.getId(), inspectionID, requestBody);

            return BaseResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .statusMessage(inspection_pickup_checklist_save_success)
                    .data(null)
                    .build();

        } catch (UCTSupplyException ex) {
            return BaseResponse.builder()
                    .statusCode(ex.getStatus().value())
                    .statusMessage(ex.getMessage())
                    .data(null)
                    .build();
        }
    }

    @RequestMapping(value="/checklist/{inspectionID}/saveImage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse saveInspectionImages(HttpServletRequest request, @NotNull @PathVariable Long inspectionID, @Valid @NotNull @RequestBody InspectionChecklistFileRequest requestBody) {
        log.info("saveInspectionImages =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        inspectionService.saveInspectionImages(userData.getId(), inspectionID, requestBody);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(inspection_checklist_image_save_success)
                .data(null)
                .build();
    }

    @RequestMapping(value="/verdict/{inspectionID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse getInspectionVerdict(HttpServletRequest request, @NotNull @PathVariable Long inspectionID) {
        log.info("getInspectionVerdict =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        try {
            InspectionVerdictDetail inspectionVerdictData = inspectionService.getInspectionVerdictData(inspectionID);
            return BaseResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .statusMessage(inspection_verdict_success)
                    .data(Arrays.asList(inspectionVerdictData))
                    .build();

        } catch (UCTSupplyException ex) {
            return BaseResponse.builder()
                    .statusCode(ex.getStatus().value())
                    .statusMessage(ex.getMessage())
                    .data(null)
                    .build();
        }
    }

    @RequestMapping(value="/verdict/{inspectionID}/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse saveInspectionVerdict(HttpServletRequest request, @NotNull @PathVariable Long inspectionID, @Valid @NotNull @RequestBody InspectionVerdictRequest requestBody) {
        log.info("saveInspectionVerdict =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        try {
            inspectionService.saveInspectionVerdict(userData.getId(), inspectionID, requestBody);
            return BaseResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .statusMessage(inspection_verdict_save_success)
                    .data(null)
                    .build();

        } catch (UCTSupplyException | JsonProcessingException | SupplyException ex) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.BAD_REQUEST.value())
                    .statusMessage(ex.getMessage())
                    .data(null)
                    .build();
        }
    }

    @RequestMapping(value="/difference", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse getInspectionVerdict(HttpServletRequest request, @Valid @NotNull @RequestParam Long leadID, @Valid @NotNull @RequestParam InspectionType inspectionType1, @Valid @NotNull @RequestParam InspectionType inspectionType2) {
        log.info("getInspectionVerdict =>>>>>");
        UserData userData = getUserData(request);
        log.info(userData.toString());

        InspectionDifferenceRequest requestParams = InspectionDifferenceRequest.builder()
                .leadID(leadID)
                .inspectionType1(inspectionType1)
                .inspectionType2(inspectionType2)
                .build();

        try {
            List<InspectionChecklistFlat> differences = inspectionService.getInspectionDifferences(requestParams);
            return BaseResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .statusMessage(inspection_difference_success)
                    .data(differences)
                    .build();

        } catch (UCTSupplyException ex) {
            return BaseResponse.builder()
                    .statusCode(ex.getStatus().value())
                    .statusMessage(ex.getMessage())
                    .data(null)
                    .build();
        }
    }

    @RequestMapping(value="/update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse updateInspection(HttpServletRequest req, @Valid @NotNull @RequestBody UpdateInspectionRequest request) throws EntityNotFoundException {
        log.info("updateInspection =>>>>>");
        UserData userData = getUserData(req);
        log.info(userData.toString());
        request.setUpdatedBy(userData.getId());

        Inspection updatedInspection = null;
        try {
            updatedInspection = inspectionService.updateInspection(request);
        } catch (EntityNotFoundException | SupplyException | JsonProcessingException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(updatedInspection.getInspectionType()+" "+inspection_updated+" for leadID: "+updatedInspection.getLeadID())
                .data(Arrays.asList(updatedInspection))
                .build();
    }

    public UserData getUserData(HttpServletRequest request) {
        return (UserData) request.getAttribute("user");
    }

}
