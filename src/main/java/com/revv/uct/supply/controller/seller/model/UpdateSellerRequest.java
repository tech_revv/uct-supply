package com.revv.uct.supply.controller.seller.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revv.uct.supply.db.enums.SellerTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class UpdateSellerRequest
{
    @Valid @NotNull @JsonProperty("id") private Long id;
    @Valid @JsonProperty("adminID") private Long adminID;
    //@Valid @NotNull @JsonProperty("name") private String name;
    //@Valid @NotNull @JsonProperty("sellerType") private SellerTypeEnum sellerType;
    //@Valid @NotNull @JsonProperty("contactNumber") private String contactNumber;
    @Valid @JsonProperty("email") private String email;
    @Valid @JsonProperty("spocName") private String spocName;
    //@Valid @NotNull @JsonProperty("serviceCityID") private Long serviceCityID;
    //@Valid @NotNull @JsonProperty("lat") private Double lat;
    //@Valid @NotNull @JsonProperty("lng") private Double lng;
    //@Valid @NotNull @JsonProperty("address") private String address;
    @Valid @JsonProperty("bankAcDetails") private String bankAcDetails;
    @Valid @JsonProperty("gstNumber") private String gstNumber;
    @Valid @JsonProperty("panNumber") private String panNumber;
}
