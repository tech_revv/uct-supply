package com.revv.uct.supply.controller.workshop.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.revv.uct.supply.db.workshop.entity.Workshop;
import com.revv.uct.supply.service.workshop.model.RefurbSpoc;
import lombok.*;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class WorkshopData {

    List<Workshop> workshops;
    List<RefurbSpoc> refurbSpocs;

}
