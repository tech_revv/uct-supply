package com.revv.uct.supply.controller.inspection.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class InspectionImageRequest {
    @Valid @NotNull @JsonProperty("subPartID")private Long subPartID;
    @Valid @NotNull @JsonProperty("fileRequired")private Boolean fileRequired;
    @Valid @NotNull @JsonProperty("images")private List<String> images;
}
