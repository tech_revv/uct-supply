package com.revv.uct.supply.controller.payment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.constants.ResponseMessage;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.controller.payment.model.GetLeadPaymentData;
import com.revv.uct.supply.controller.payment.model.ProceedWithoutTokenRequest;
import com.revv.uct.supply.controller.payment.model.SaveLeadPaymentRequest;
import com.revv.uct.supply.db.leadPaymentDetail.entity.LeadPaymentDetail;
import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.leadPaymentDetail.ILeadPaymentDetailService;
import com.revv.uct.supply.service.offer.model.GenerateOfferData;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

@RestController
@CrossOrigin
@RequestMapping("/api/payment")
@Slf4j
public class PaymentController {
    private final ILeadPaymentDetailService leadPaymentDetailService;

    @Autowired
    public PaymentController(ILeadPaymentDetailService leadPaymentDetailService) {
        this.leadPaymentDetailService = leadPaymentDetailService;
    }

    @RequestMapping(value = "/leadPaymentDetail/getLeadPaymentData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get data to save lead paymnet details.")
    public BaseResponse getLeadPaymentData(HttpServletRequest req) {
        log.info("getLeadPaymentData==>>");

        UserData userData = (UserData) req.getAttribute("user");
        GetLeadPaymentData data = leadPaymentDetailService.getLeadPaymentData();

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage("successful")
                .data(Arrays.asList(data))
                .build();
    }

    @RequestMapping(value = "/leadPaymentDetail/saveLeadPaymentDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to save lead payment details.")
    public BaseResponse saveLeadPaymentDetails(@Valid @NotNull @RequestBody SaveLeadPaymentRequest request, HttpServletRequest req) throws SupplyException, JsonProcessingException {
        log.info("saveLeadPaymentDetails==>>");

        UserData userData = (UserData) req.getAttribute("user");
        request.setCreatedBy(userData.getId());

        LeadPaymentDetail data = null;
        try {
            data = leadPaymentDetailService.saveLeadPaymentDetails(request);
        } catch (SupplyException | JsonProcessingException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.token_paid_details_added + " for Lead ID " + data.getLeadID())
                .data(Arrays.asList(data))
                .build();
    }

    @RequestMapping(value = "/leadPaymentDetail/proceedWithoutToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get data to save lead payment details.")
    public BaseResponse proceedWithoutToken(@Valid @NotNull @RequestBody ProceedWithoutTokenRequest request, HttpServletRequest req) throws SupplyException, JsonProcessingException {
        log.info("proceedWithoutToken==>>");

        UserData userData = (UserData) req.getAttribute("user");
        request.setCreatedBy(userData.getId());

        LeadPaymentDetail data = null;
        try {
            data = leadPaymentDetailService.proceedWithoutToken(request);
        } catch (SupplyException | JsonProcessingException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.token_paid_details_added + " for Lead ID " + data.getLeadID())
                .data(Arrays.asList(data))
                .build();
    }
}
