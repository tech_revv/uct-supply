package com.revv.uct.supply.controller.lead.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.revv.uct.supply.constants.InsuranceType;
import com.revv.uct.supply.constants.RegistrationType;
import com.revv.uct.supply.constants.SellerType;
import com.revv.uct.supply.db.carModel.entity.CarModel;
import com.revv.uct.supply.db.serviceCity.entity.ServiceCity;
import lombok.*;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class LeadCreationData {
    private List<ServiceCity> city;
    private List<CarModel> carModel;
    private List<RegistrationType> registrationType;
    private List<SellerType> sellerType;
    private List<InsuranceType> insuranceType;
}
