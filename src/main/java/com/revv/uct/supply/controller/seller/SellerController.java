package com.revv.uct.supply.controller.seller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revv.uct.supply.client.model.UserData;
import com.revv.uct.supply.constants.ResponseMessage;
import com.revv.uct.supply.constants.ResponseMessages;
import com.revv.uct.supply.controller.seller.model.SellerCreationData;
import com.revv.uct.supply.controller.seller.model.UpdateSellerRequest;
import com.revv.uct.supply.db.enums.SellerTypeEnum;
import com.revv.uct.supply.db.seller.entity.Seller;
import com.revv.uct.supply.controller.model.BaseResponse;
import com.revv.uct.supply.controller.seller.model.CreateSellerRequest;

import com.revv.uct.supply.exceptions.SupplyException;
import com.revv.uct.supply.exceptions.UCTSupplyException;
import com.revv.uct.supply.service.seller.ISellerService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.revv.uct.supply.constants.ResponseMessage.*;
import static com.revv.uct.supply.constants.ResponseMessages.sellCreated;

@RestController
@CrossOrigin
@RequestMapping("/api/seller")
@Slf4j
public class SellerController
{
    private ISellerService sellerService;
    @Autowired
    public SellerController(ISellerService sellerService) {
        this.sellerService = sellerService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to create the seller")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse createSeller(@Valid @NotNull @RequestBody CreateSellerRequest request, HttpServletRequest req) throws UCTSupplyException {
        log.info("create customer request ={}",request);
        UserData userData = (UserData) req.getAttribute("user");
        request.setAdminID(userData.getId());
        List<Seller> list = new ArrayList<>();
        Seller seller = sellerService.createSeller(request);
        list.add(seller);
        log.debug("created customer={}",seller);
        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(sellCreated + seller.getId())
                .data(list)
                .build();
    }

    @RequestMapping(value="/check", method= RequestMethod.GET)
    public BaseResponse check(HttpServletRequest request){
        UserData userData = (UserData) request.getAttribute("user");
        log.info("request "+userData);
        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage("checked")
                .data(null)
                .build();
    }

    @RequestMapping(value = "/sellersForLeadCreation", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get seller data for lead creation")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getSellers(@RequestParam("serviceCityID") Long serviceCityID, @RequestParam("sellerType") SellerTypeEnum sellerType){

        List<Object> sellers = (List<Object>) sellerService.getSellersForLead(serviceCityID, sellerType);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.success)
                .data(sellers)
                .build();
    }

    @RequestMapping(value = "/dataForSellerCreation", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get data for seller creation")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getData(){

        List<SellerCreationData> data = sellerService.getSellerCreationData();

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.success)
                .data(data)
                .build();
    }

    @RequestMapping(value = "/getSellerByNameAndCity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to get seller data by name and service city id")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse getSellersByNameAndServiceCity(@RequestParam("serviceCityID") Long serviceCityID, @RequestParam("name") String name){

        List<Seller> data = sellerService.getSellersByNameAndServiceCity(serviceCityID, name);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessage.success)
                .data(data)
                .build();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "This api will be used to update seller data.")
//    @Timed(value="create_customer", histogram = true)
    public BaseResponse updateSeller(@Valid @NotNull @RequestBody UpdateSellerRequest request, HttpServletRequest req) throws SupplyException {
        UserData userData = (UserData) req.getAttribute("user");
        request.setAdminID(userData.getId());

        Seller updatedSeller = null;
        try {
            updatedSeller = sellerService.updateSeller(request);
        } catch (SupplyException | JsonProcessingException e) {
            return BaseResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .statusMessage(e.getMessage())
                    .data(null)
                    .build();
        }

        List<Seller> list = new ArrayList<>();
        list.add(updatedSeller);

        return BaseResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .statusMessage(ResponseMessages.sellerUpdated + updatedSeller.getId())
                .data(list)
                .build();
    }
}
